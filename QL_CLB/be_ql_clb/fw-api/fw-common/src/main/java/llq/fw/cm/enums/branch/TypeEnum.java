package llq.fw.cm.enums.branch;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TypeEnum {
	DIEMGIAODICH(1), ATM(2), BRANCH(3);
//	1: Điểm giao dịch|2:ATM|3: Chi nhánh
	private Integer value;

	TypeEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static TypeEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		TypeEnum statusEnum= Arrays.stream(TypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

package llq.fw.cm.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import llq.fw.cm.config.AuditTrailListener;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
// @EntityListeners(AuditingEntityListener.class)
@EntityListeners(AuditTrailListener.class)
@Getter
@Setter
public class Auditable {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CREATED_BY", updatable = false)
    // @CreatedBy
    protected User createdBy;

    @Column(name = "CREATED_AT", updatable = false)
    @CreatedDate
    protected Date createdAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UPDATED_BY")
    // @LastModifiedBy
    protected User updatedBy;

    @Column(name = "UPDATED_AT")
    // @LastModifiedDate
    protected Date updatedAt;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CREATED_BY_CUST", updatable = false)
    // @CreatedBy
    protected Member createdByCust;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UPDATED_BY_CUST")
    // @LastModifiedBy
    @JsonIgnore
    protected Member updatedByCust;

}
package llq.fw.cm.enums.packagefee;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class IntFeeMethodEnumConverter implements AttributeConverter<IntFeeMethodEnum, String>{

    @Override
    public String convertToDatabaseColumn(IntFeeMethodEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public IntFeeMethodEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	IntFeeMethodEnum statusEnum= Arrays.stream(IntFeeMethodEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

package llq.fw.cm.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import llq.fw.cm.models.Member;
import llq.fw.cm.security.repository.MemberRepository;

@Service
public class CustDetailsServiceImpl implements UserDetailsService {
  @Autowired
  MemberRepository custRepository;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Member user = custRepository.findByCode(username)
        .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

    return CustDetailsImpl.build(user);
  }
}

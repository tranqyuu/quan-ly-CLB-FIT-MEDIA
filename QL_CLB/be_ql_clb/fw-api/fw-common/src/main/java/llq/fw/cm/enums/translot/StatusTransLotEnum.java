package llq.fw.cm.enums.translot;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum StatusTransLotEnum {
	CHODUYET(0), TUCHOI(1),DADUYET(2),HUY(3),HOANTHANH(4);
/*
 * "0: Chờ duyệt, 1: Từ chối, 2: Đã duyệt,3: Hủy, 4: Hoàn thành
"
 * 
 */
	private Integer value;

	StatusTransLotEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static StatusTransLotEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		StatusTransLotEnum statusEnum= Arrays.stream(StatusTransLotEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

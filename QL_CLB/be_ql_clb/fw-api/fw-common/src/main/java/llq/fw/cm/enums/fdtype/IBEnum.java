package llq.fw.cm.enums.fdtype;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum IBEnum {
	
	THUOCIB(1), KHONGTHUOCIB(0);
/*
 * 1: Thuoc IB doanh nghiep| 0: Khong thuoc IB doanh nghiep
 * 
 */
	private Integer value;

	IBEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static IBEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		IBEnum statusEnum= Arrays.stream(IBEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

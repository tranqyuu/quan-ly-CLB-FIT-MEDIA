package llq.fw.cm.enums.bankreceiving;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum NapasEnum {
	THAMGIA(1),KHONGTHAMGIA(0) ;
//	1:Tham gia| 0:Không tham gia
	private Integer value;

	NapasEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static NapasEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		NapasEnum statusEnum= Arrays.stream(NapasEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

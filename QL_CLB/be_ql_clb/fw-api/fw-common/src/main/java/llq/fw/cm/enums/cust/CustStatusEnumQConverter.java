package llq.fw.cm.enums.cust;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class CustStatusEnumQConverter implements Converter<String, CustStatusEnum> {
	@Override
	public CustStatusEnum convert(String source) {
		CustStatusEnum statusEnum= Arrays.stream(CustStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
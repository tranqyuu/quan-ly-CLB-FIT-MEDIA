package llq.fw.cm.enums.company;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class AuthorizedEnumConverter implements AttributeConverter<AuthorizedEnum, String>{

    @Override
    public String convertToDatabaseColumn(AuthorizedEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public AuthorizedEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	AuthorizedEnum statusEnum= Arrays.stream(AuthorizedEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

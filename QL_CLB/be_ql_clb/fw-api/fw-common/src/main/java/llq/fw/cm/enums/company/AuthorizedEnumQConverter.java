package llq.fw.cm.enums.company;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class AuthorizedEnumQConverter implements Converter<String, AuthorizedEnum> {
	@Override
	public AuthorizedEnum convert(String source) {
		AuthorizedEnum statusEnum= Arrays.stream(AuthorizedEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
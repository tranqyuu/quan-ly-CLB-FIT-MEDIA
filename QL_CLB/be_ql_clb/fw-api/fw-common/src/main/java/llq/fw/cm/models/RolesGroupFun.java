package llq.fw.cm.models;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * The persistent class for the ROLES_GROUP_FUN database table.
 * 
 */
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Table(name="ROLES_GROUP_FUN")
public class RolesGroupFun implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ROLES_GROUP_FUN_ID_GENERATOR", sequenceName="ROLES_GROUP_FUN_SEQ",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ROLES_GROUP_FUN_ID_GENERATOR")
	private Long id;

	//bi-directional many-to-one association to Function
    @ManyToOne
	private Function function;

	//bi-directional many-to-one association to RolesGroup
    @ManyToOne
	@JoinColumn(name="ROLES_GROUP_ID")
    @JsonIgnore
	private RolesGroup rolesGroup;

	//bi-directional many-to-one association to RolesGroupSub
	@OneToMany(mappedBy="rolesGroupFun",fetch = FetchType.EAGER, cascade = CascadeType.ALL,orphanRemoval = true)
	private Set<RolesGroupSub> rolesGroupSubs;

    @Transient
    @JsonProperty("deleted")
	private boolean deleted;

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RolesGroupFun other = (RolesGroupFun) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "RolesGroupFun [id=" + id + ", function=" + function + ", rolesGroupSubs=" + rolesGroupSubs
				+ ", deleted=" + deleted + "]";
	}
    

}
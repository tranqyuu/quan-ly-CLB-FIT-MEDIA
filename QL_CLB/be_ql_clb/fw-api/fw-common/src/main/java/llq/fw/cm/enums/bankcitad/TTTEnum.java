package llq.fw.cm.enums.bankcitad;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TTTEnum {
	Y(1), N(0);
//	1: Thanh toan tap trung| 0 : Khong thanh toan tap trung
	private Integer value;

	TTTEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static TTTEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		TTTEnum statusEnum= Arrays.stream(TTTEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

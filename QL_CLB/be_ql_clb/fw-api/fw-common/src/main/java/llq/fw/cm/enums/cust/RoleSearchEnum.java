package llq.fw.cm.enums.cust;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum RoleSearchEnum {
	Y(1), N(0);
//	Phương thức xác thực 1: SMS| 2: Smart OTP
	private Integer value;

	RoleSearchEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static RoleSearchEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		RoleSearchEnum statusEnum= Arrays.stream(RoleSearchEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

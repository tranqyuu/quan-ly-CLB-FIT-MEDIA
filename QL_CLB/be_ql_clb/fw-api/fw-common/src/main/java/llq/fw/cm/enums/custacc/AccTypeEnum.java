package llq.fw.cm.enums.custacc;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum AccTypeEnum {
	THANHTOAN(1), TIENGUI(2),TIENVAY(3);
//	1: Thanh toán| 2: Tiền gửi | 3: Tiền vay
	private Integer value;

	AccTypeEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static AccTypeEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		AccTypeEnum statusEnum= Arrays.stream(AccTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

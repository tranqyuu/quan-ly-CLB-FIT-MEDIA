package llq.fw.cm.enums.smshis;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SmsHisStatusEnum {
	THANHCONG(1), LOI(2);
/*
 *1: Thành công: 2: Lỗi
 * 
 */
	private Integer value;

	SmsHisStatusEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static SmsHisStatusEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		SmsHisStatusEnum statusEnum= Arrays.stream(SmsHisStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

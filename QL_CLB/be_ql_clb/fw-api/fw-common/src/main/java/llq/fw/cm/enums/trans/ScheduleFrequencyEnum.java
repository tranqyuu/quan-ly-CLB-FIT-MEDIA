package llq.fw.cm.enums.trans;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ScheduleFrequencyEnum {
    NGAY(1), TUAN(2), THANG(3), QUY(4), NAM(5);

    /*
     * 0: Tháng|1: Năm
     * 
     */

    private Integer value;

    ScheduleFrequencyEnum(Integer i) {
        this.value = i;
    }

    @JsonValue
    public Integer getValue() {
        return value;
    }

    @JsonCreator
    public static ScheduleFrequencyEnum forValue(String v) {
        if (v == null) {
            return null;
        }
        ScheduleFrequencyEnum statusEnum = Arrays.stream(ScheduleFrequencyEnum.values()).filter(
                p -> {
                    return p.getValue().toString().equals(v);
                }).findFirst().orElseThrow(IllegalArgumentException::new);
        return statusEnum;
    }
}

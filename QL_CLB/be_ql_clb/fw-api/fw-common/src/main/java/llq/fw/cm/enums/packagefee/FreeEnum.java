package llq.fw.cm.enums.packagefee;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum FreeEnum {
	MIENPHI(1), KHONGMIENPHI(0);
/*
 * 1: Miễn phí|0: Không miễn phí
 * 
 */
	private Integer value;

	FreeEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static FreeEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		FreeEnum statusEnum= Arrays.stream(FreeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

package llq.fw.cm.security.services;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import llq.fw.cm.models.Function;
import llq.fw.cm.models.RolesGroup;
import llq.fw.cm.models.RolesGroupFun;
import llq.fw.cm.models.RolesGroupSub;
import llq.fw.cm.models.Subfunction;
import llq.fw.cm.models.User;

public class UserDetailsImpl implements UserDetails {
	private static final long serialVersionUID = 1L;

	private Long id;

	private String username;

	private String email;

	@JsonIgnore
	private String password;
	
	private Collection<? extends GrantedAuthority> authorities;


	public UserDetailsImpl(Long id, String username, String email, String password,
			Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.authorities = authorities;
	}

	public static UserDetailsImpl build(User user) {
//		List<GrantedAuthority> authorities = user.getRolesGroup().getRolesGroupFuns().stream()
//				.map(role -> new SimpleGrantedAuthority(role.getFunction().getName())).collect(Collectors.toList());
		List<GrantedAuthority> authorities = user.getRolesGroup().getRolesGroupFuns().stream()
				.flatMap( rolesGroupFun1-> rolesGroupFun1.getRolesGroupSubs().stream().map((b) ->  new SimpleGrantedAuthority(rolesGroupFun1.getFunction().getCode() + b.getSubfunction().getCode())))
				.collect(Collectors.toList());

		return new UserDetailsImpl(user.getId(), user.getUsername(), user.getEmail(), user.getPassword(), authorities);
	}

	public static void main(String a[]) throws Exception {
		User user=new User();
		RolesGroup rolesGroup=new RolesGroup();
		user.setRolesGroup(rolesGroup);
		RolesGroupFun rolesGroupFun=new RolesGroupFun();
		Function function=new Function();
		function.setCode("fun01");
		rolesGroupFun.setFunction(function);
		Subfunction subfunction=new Subfunction();
		subfunction.setCode("sub01");
		RolesGroupSub rolesGroupSub=new RolesGroupSub();
		rolesGroupSub.setSubfunction(subfunction);
		rolesGroupSub.setRolesGroupFun(rolesGroupFun);
		Set<RolesGroupSub> rolesGroupSubs=new HashSet<>();
		rolesGroupSubs.add(rolesGroupSub);
		rolesGroupFun.setRolesGroupSubs(rolesGroupSubs);
		Set<RolesGroupFun> rolesGroupFuns=new HashSet<>();
		rolesGroupFuns.add(rolesGroupFun);
		rolesGroup.setRolesGroupFuns(rolesGroupFuns);
		
		List<SimpleGrantedAuthority> xx=user.getRolesGroup().getRolesGroupFuns().stream()
				.flatMap( rolesGroupFun1-> rolesGroupFun1.getRolesGroupSubs().stream().map((b) ->  new SimpleGrantedAuthority(rolesGroupFun1.getFunction().getCode() + b.getSubfunction().getCode())))
				.collect(Collectors.toList());
		System.out.println(xx);

	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public Long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserDetailsImpl user = (UserDetailsImpl) o;
		return Objects.equals(id, user.id);
	}
}

package llq.fw.cm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import llq.fw.cm.models.TaiChinh;

public interface FinanceRepository
        extends JpaRepository<TaiChinh, java.lang.Long>, JpaSpecificationExecutor<TaiChinh> {
}

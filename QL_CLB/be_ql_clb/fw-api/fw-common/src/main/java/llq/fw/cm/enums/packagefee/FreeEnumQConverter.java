package llq.fw.cm.enums.packagefee;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class FreeEnumQConverter implements Converter<String, FreeEnum> {
	@Override
	public FreeEnum convert(String source) {
		FreeEnum statusEnum= Arrays.stream(FreeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
package llq.fw.cm.enums.smartotphis;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum CustPositionEnum {
	QUANLY(1), LAPLENH(2),DUYETLENH(3);
/*
 *Vai trò 1: Quản lý|2: Lập lệnh|3: Duyệt lệnh
 * 
 */
	private Integer value;

	CustPositionEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static CustPositionEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		CustPositionEnum statusEnum= Arrays.stream(CustPositionEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

package llq.fw.cm.enums.company;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class CompanyHiActionEnumQConverter implements Converter<String, CompanyHiActionEnum> {
	@Override
	public CompanyHiActionEnum convert(String source) {
		CompanyHiActionEnum statusEnum= Arrays.stream(CompanyHiActionEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
}

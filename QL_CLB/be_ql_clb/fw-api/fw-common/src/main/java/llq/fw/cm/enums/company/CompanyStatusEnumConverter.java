package llq.fw.cm.enums.company;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class CompanyStatusEnumConverter implements AttributeConverter<CompanyStatusEnum, String>{

    @Override
    public String convertToDatabaseColumn(CompanyStatusEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public CompanyStatusEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	CompanyStatusEnum statusEnum= Arrays.stream(CompanyStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

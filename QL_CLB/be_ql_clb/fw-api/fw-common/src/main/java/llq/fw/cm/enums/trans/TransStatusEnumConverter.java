package llq.fw.cm.enums.trans;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TransStatusEnumConverter implements AttributeConverter<TransStatusEnum, String>{

    @Override
    public String convertToDatabaseColumn(TransStatusEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public TransStatusEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	TransStatusEnum statusEnum= Arrays.stream(TransStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}
package llq.fw.cm.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import llq.fw.cm.models.RolesGroup;
import llq.fw.cm.models.RolesGroupSub;

public interface RolesGroupSubRepository extends JpaRepository<RolesGroupSub, Long>, JpaSpecificationExecutor<RolesGroupSub> {

}

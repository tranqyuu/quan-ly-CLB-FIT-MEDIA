package llq.fw.cm.enums.smshis;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class SmsHisStatusEnumQConverter implements Converter<String, SmsHisStatusEnum> {
	@Override
	public SmsHisStatusEnum convert(String source) {
		SmsHisStatusEnum statusEnum= Arrays.stream(SmsHisStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
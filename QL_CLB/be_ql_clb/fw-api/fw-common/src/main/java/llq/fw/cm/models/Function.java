package llq.fw.cm.models;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

import llq.fw.cm.enums.IBStatusEnum;

import javax.persistence.*;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Set;

/**
 * The persistent class for the "FUNCTION" database table.
 * 
 */
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)

@Table(name = "FUNCTION")
public class Function extends Auditable implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	private String code;

	private String name;

	@Column(name = "PARENT_CODE")
	private String parentCode;

	private IBStatusEnum status;

	// bi-directional many-to-one association to RolesGroupFun
	@OneToMany(mappedBy = "function")
	private Set<RolesGroupFun> rolesGroupFuns;

	//bi-directional many-to-one association to Subfunction
	@OneToMany(mappedBy="function", fetch = FetchType.EAGER)
	private Set<Subfunction> subfunctions;

}
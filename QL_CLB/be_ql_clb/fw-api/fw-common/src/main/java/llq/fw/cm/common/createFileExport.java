package llq.fw.cm.common;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import net.sf.jxls.transformer.XLSTransformer;

public class createFileExport {
	public static void createFileToOutputstream(OutputStream outputStream, InputStream isFile, Map<String, Object> map)
			throws Exception {
		try {
			XLSTransformer transformer = new XLSTransformer();	
			Workbook wb = transformer.transformXLS(isFile, map);
			wb.write(outputStream);
			isFile.close();
			wb.close();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}

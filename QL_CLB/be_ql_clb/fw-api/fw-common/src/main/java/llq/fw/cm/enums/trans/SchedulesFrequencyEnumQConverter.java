package llq.fw.cm.enums.trans;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class SchedulesFrequencyEnumQConverter implements Converter<String, ScheduleFrequencyEnum> {
	@Override
	public ScheduleFrequencyEnum convert(String source) {
		ScheduleFrequencyEnum statusEnum= Arrays.stream(ScheduleFrequencyEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
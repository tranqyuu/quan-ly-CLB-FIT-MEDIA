package llq.fw.cm.enums.trans;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TransPaymentStatusEnum {

	CHOXULY(0), THANHCONG(1),LOI(2), HUY(3);
/*
 * 0: Chưa thực hiện,1: Thành công, 2: Lỗi
 * 
 */
	private Integer value;

	TransPaymentStatusEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static TransPaymentStatusEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		TransPaymentStatusEnum statusEnum= Arrays.stream(TransPaymentStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

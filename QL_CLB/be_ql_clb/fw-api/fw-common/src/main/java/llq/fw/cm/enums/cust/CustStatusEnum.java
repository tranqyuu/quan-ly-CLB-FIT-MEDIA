package llq.fw.cm.enums.cust;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum CustStatusEnum {
	CHOKICHHOAT(0), HOATDONG(1), KHOA(2), HUY(3);
/*
 * 0: Chờ kích hoạt|1: Hoạt động|2: Khóa|3: Hủy
 * 
 */
	private Integer value;

	CustStatusEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static CustStatusEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		CustStatusEnum statusEnum= Arrays.stream(CustStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

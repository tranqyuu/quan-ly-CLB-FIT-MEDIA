package llq.fw.cm.enums.trans;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TransStatusEnum {

	CHODUYET(0),TUCHOI(1),DADUYET(2),HUY(3),HOANTHANH(4),KHONGTHUCHIENDUOC(5);

	/*
	 * "0: Chờ duyệt, 1: Từ chối, 2: Đã duyệt,3: Hủy, 4: Hoàn thành, 5: Không thực hiện được
	 * 
	 */
	private Integer value;

	TransStatusEnum(Integer i) {
		this.value = i;
	}

	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static TransStatusEnum forValue(String v) {
		if (v == null) {
			return null;
		}
		TransStatusEnum statusEnum = Arrays.stream(TransStatusEnum.values()).filter(p -> {
			return p.getValue().toString().equals(v);
		}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

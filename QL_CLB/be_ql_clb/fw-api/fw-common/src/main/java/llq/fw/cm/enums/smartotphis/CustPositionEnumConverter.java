package llq.fw.cm.enums.smartotphis;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class CustPositionEnumConverter implements AttributeConverter<CustPositionEnum, String>{

    @Override
    public String convertToDatabaseColumn(CustPositionEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public CustPositionEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	CustPositionEnum statusEnum= Arrays.stream(CustPositionEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

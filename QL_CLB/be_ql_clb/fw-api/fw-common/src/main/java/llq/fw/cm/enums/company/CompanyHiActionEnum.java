package llq.fw.cm.enums.company;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum CompanyHiActionEnum {
	/*
	 * 0. Mở mới
	 * 1. Chỉnh sửa thông tin
	 * 2. Khóa
	 * 3. Mở khóa
	 * 4. Hủy
	 */
	MOMOI(0), CHINHSUATHONGTIN(1),KHOA(2), MOKHOA(3), HUY(4);
	
	private Integer value;

	CompanyHiActionEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static CompanyHiActionEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		CompanyHiActionEnum statusEnum= Arrays.stream(CompanyHiActionEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

	
	
	
}

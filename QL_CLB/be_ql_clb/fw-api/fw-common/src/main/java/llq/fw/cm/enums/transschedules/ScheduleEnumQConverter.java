package llq.fw.cm.enums.transschedules;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class ScheduleEnumQConverter implements Converter<String, ScheduleEnum> {
	@Override
	public ScheduleEnum convert(String source) {
		ScheduleEnum statusEnum= Arrays.stream(ScheduleEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
package llq.fw.cm.models;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The persistent class for the REFRESHTOKEN_IB database table.
 * 
 */
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Table(name = "REFRESHTOKEN_IB")
public class RefreshtokenIb extends Auditable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "REFRESHTOKEN_IB_ID_GENERATOR", sequenceName = "REFRESHTOKEN_IB_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REFRESHTOKEN_IB_ID_GENERATOR")
	private Long id;

	@Column(name = "EXPIRY_DATE")
	private Instant expiryDate;

	private String token;

	// bi-directional many-to-one association to Cust
	@ManyToOne
	@JsonIgnore
	private Member cust;

}
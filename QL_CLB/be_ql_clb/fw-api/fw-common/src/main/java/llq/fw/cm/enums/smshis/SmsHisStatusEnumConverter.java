package llq.fw.cm.enums.smshis;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class SmsHisStatusEnumConverter implements AttributeConverter<SmsHisStatusEnum, String>{

    @Override
    public String convertToDatabaseColumn(SmsHisStatusEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public SmsHisStatusEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	SmsHisStatusEnum statusEnum= Arrays.stream(SmsHisStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

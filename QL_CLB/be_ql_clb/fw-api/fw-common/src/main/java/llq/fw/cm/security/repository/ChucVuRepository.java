package llq.fw.cm.security.repository;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import llq.fw.cm.models.Member;
import llq.fw.cm.models.ChucVu;

public interface ChucVuRepository
		extends JpaRepository<ChucVu, java.lang.Long>, JpaSpecificationExecutor<ChucVu> {
}

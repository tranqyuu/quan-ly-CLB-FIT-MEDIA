package llq.fw.cm.enums.sysparameters;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class SysParametersTypeEnumConverter implements AttributeConverter<SysParametersTypeEnum, String>{

    @Override
    public String convertToDatabaseColumn(SysParametersTypeEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public SysParametersTypeEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	SysParametersTypeEnum statusEnum= Arrays.stream(SysParametersTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

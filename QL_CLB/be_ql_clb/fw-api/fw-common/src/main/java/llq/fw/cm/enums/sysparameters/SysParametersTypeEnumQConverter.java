package llq.fw.cm.enums.sysparameters;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class SysParametersTypeEnumQConverter implements Converter<String, SysParametersTypeEnum> {
	@Override
	public SysParametersTypeEnum convert(String source) {
		SysParametersTypeEnum statusEnum= Arrays.stream(SysParametersTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
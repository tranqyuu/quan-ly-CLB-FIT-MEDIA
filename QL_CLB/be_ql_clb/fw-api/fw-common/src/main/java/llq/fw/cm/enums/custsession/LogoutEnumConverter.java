package llq.fw.cm.enums.custsession;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class LogoutEnumConverter implements AttributeConverter<LogoutEnum, String>{

    @Override
    public String convertToDatabaseColumn(LogoutEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public LogoutEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	LogoutEnum statusEnum= Arrays.stream(LogoutEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

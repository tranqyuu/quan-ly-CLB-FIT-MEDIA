package llq.fw.cm.enums.cust;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum RoleTransEnum {
	Y(1), N(0);
//	Phương thức xác thực 1: SMS| 2: Smart OTP
	private Integer value;

	RoleTransEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static RoleTransEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		RoleTransEnum statusEnum= Arrays.stream(RoleTransEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

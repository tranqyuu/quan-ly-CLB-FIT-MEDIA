package llq.fw.cm.enums.custsession;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class LogoutEnumQConverter implements Converter<String, LogoutEnum> {
	@Override
	public LogoutEnum convert(String source) {
		LogoutEnum statusEnum= Arrays.stream(LogoutEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
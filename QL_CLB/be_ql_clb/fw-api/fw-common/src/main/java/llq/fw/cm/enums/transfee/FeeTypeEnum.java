package llq.fw.cm.enums.transfee;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum FeeTypeEnum {
	GIAODICH(2), THANG(0),NAM(1);
/*
 * 1:USER|2:Chức danh
 * 
 */
	private Integer value;

	FeeTypeEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static FeeTypeEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		FeeTypeEnum statusEnum= Arrays.stream(FeeTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

package llq.fw.cm.enums.cust;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class RoleTransQConverter implements Converter<String, RoleTransEnum> {
	@Override
	public RoleTransEnum convert(String source) {
		RoleTransEnum statusEnum= Arrays.stream(RoleTransEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
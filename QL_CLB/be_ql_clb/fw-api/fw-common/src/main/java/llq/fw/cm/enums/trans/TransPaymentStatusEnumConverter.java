package llq.fw.cm.enums.trans;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TransPaymentStatusEnumConverter implements AttributeConverter<TransPaymentStatusEnum, String>{

    @Override
    public String convertToDatabaseColumn(TransPaymentStatusEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public TransPaymentStatusEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	TransPaymentStatusEnum statusEnum= Arrays.stream(TransPaymentStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}
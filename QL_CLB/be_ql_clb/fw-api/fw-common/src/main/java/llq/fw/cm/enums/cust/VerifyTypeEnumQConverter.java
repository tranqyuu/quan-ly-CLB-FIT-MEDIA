package llq.fw.cm.enums.cust;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class VerifyTypeEnumQConverter implements Converter<String, VerifyTypeEnum> {
	@Override
	public VerifyTypeEnum convert(String source) {
		VerifyTypeEnum statusEnum= Arrays.stream(VerifyTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
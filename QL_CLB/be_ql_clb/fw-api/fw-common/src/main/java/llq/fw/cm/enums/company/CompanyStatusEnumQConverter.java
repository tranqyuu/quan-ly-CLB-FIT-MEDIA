package llq.fw.cm.enums.company;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class CompanyStatusEnumQConverter implements Converter<String, ServicePackageEnum> {
	@Override
	public ServicePackageEnum convert(String source) {
		ServicePackageEnum statusEnum= Arrays.stream(ServicePackageEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
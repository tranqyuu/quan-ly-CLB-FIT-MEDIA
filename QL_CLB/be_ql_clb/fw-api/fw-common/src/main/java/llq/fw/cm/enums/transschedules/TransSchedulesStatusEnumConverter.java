package llq.fw.cm.enums.transschedules;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TransSchedulesStatusEnumConverter implements AttributeConverter<TransSchedulesStatusEnum, String>{

    @Override
    public String convertToDatabaseColumn(TransSchedulesStatusEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public TransSchedulesStatusEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	TransSchedulesStatusEnum statusEnum= Arrays.stream(TransSchedulesStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}
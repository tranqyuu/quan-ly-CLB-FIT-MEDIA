package llq.fw.cm.common;

import java.util.List;
import java.util.TimeZone;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

import llq.fw.cm.enums.IBStatusEnumQConverter;
import llq.fw.cm.enums.ProductEnumQConverter;
import llq.fw.cm.enums.bankcitad.TTTEnumQConverter;
import llq.fw.cm.enums.bankreceiving.CitadEnumQConverter;
import llq.fw.cm.enums.bankreceiving.NapasEnumQConverter;
import llq.fw.cm.enums.company.AuthorizedEnumQConverter;
import llq.fw.cm.enums.company.CompanyHiActionEnumQConverter;
import llq.fw.cm.enums.company.CompanyHiStatusEnumQConverter;
import llq.fw.cm.enums.company.CompanyStatusEnumQConverter;
import llq.fw.cm.enums.company.ServicePackageEnumQConverter;
import llq.fw.cm.enums.cust.CustStatusEnumQConverter;
import llq.fw.cm.enums.cust.NotificationQConverter;
import llq.fw.cm.enums.cust.PositionEnumQConverter;
import llq.fw.cm.enums.cust.RoleSearchQConverter;
import llq.fw.cm.enums.cust.RoleTransQConverter;
import llq.fw.cm.enums.cust.SmsEnumQConverter;
import llq.fw.cm.enums.cust.VerifyTypeEnumQConverter;
import llq.fw.cm.enums.custacc.AccTypeEnumQConverter;
import llq.fw.cm.enums.custacc.CustAccTypeEnumQConverter;
import llq.fw.cm.enums.custsession.LogoutEnumQConverter;
import llq.fw.cm.enums.fdtype.IBEnumQConverter;
import llq.fw.cm.enums.package1.PublicEnumQConverter;
import llq.fw.cm.enums.packagefee.FreeEnumQConverter;
import llq.fw.cm.enums.packagefee.IntFeeMethodEnumQConverter;

import llq.fw.cm.enums.rolesgroup.TypeEnumQConverter;

import llq.fw.cm.enums.smartotphis.ActionEnumQConverter;
import llq.fw.cm.enums.smartotphis.CustPositionEnumQConverter;
import llq.fw.cm.enums.smartotphis.SmartOtpHisEnumQConverter;
import llq.fw.cm.enums.smshis.SmsHisStatusEnumQConverter;
import llq.fw.cm.enums.sysparameters.SysParametersTypeEnumQConverter;
import llq.fw.cm.enums.trans.SchedulesEnumQConverter;
import llq.fw.cm.enums.trans.SchedulesFrequencyEnumQConverter;
import llq.fw.cm.enums.trans.TransFeeTpyeEnumQConverter;
import llq.fw.cm.enums.trans.TransPaymentStatusEnumQConverter;
import llq.fw.cm.enums.trans.TransStatusEnumQConverter;
import llq.fw.cm.enums.transfee.FeeTypeEnumQConverter;
import llq.fw.cm.enums.translot.StatusTransLotEnumQConverter;
import llq.fw.cm.enums.translot.TransLotStatusEnumQConverter;
import llq.fw.cm.enums.translotdetail.PaymentStatusEnumQConverter;
import llq.fw.cm.enums.transschedules.ScheduleEnumQConverter;
import llq.fw.cm.enums.transschedules.TransSchedulesStatusEnumQConverter;

@Configuration
public class WebConfig implements WebMvcConfigurer {
	@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		for (HttpMessageConverter<?> converter : converters) {
            if (converter instanceof org.springframework.http.converter.json.MappingJackson2HttpMessageConverter) {
                ObjectMapper mapper = ((MappingJackson2HttpMessageConverter) converter).getObjectMapper();
                mapper.registerModule(new Hibernate5Module());
                mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
                mapper.setTimeZone(TimeZone.getDefault());
                // replace Hibernate4Module() with the proper class for your hibernate version.
            }
        }
	}
	@Override
	public void addFormatters (FormatterRegistry registry) {
	    registry.addConverter(new IBStatusEnumQConverter());
	    registry.addConverter(new FeeTypeEnumQConverter());
	    registry.addConverter(new TypeEnumQConverter());
	    registry.addConverter(new FreeEnumQConverter());
	    registry.addConverter(new IntFeeMethodEnumQConverter());
	    registry.addConverter(new PublicEnumQConverter());
	    registry.addConverter(new IBEnumQConverter());
	    registry.addConverter(new CustStatusEnumQConverter());
	    registry.addConverter(new PositionEnumQConverter());
	    registry.addConverter(new VerifyTypeEnumQConverter());
	    registry.addConverter(new AuthorizedEnumQConverter());
	    registry.addConverter(new CompanyStatusEnumQConverter());
	    registry.addConverter(new CompanyHiActionEnumQConverter());
	    registry.addConverter(new CompanyHiStatusEnumQConverter());
	    registry.addConverter(new ServicePackageEnumQConverter());
	    registry.addConverter(new llq.fw.cm.enums.branch.TypeEnumQConverter());
	    registry.addConverter(new TTTEnumQConverter());
	    
	    registry.addConverter(new RoleSearchQConverter());
	    registry.addConverter(new RoleTransQConverter());
	    registry.addConverter(new SmsEnumQConverter());
	    registry.addConverter(new NotificationQConverter());

      
	    // New
	    registry.addConverter(new CitadEnumQConverter());
	    registry.addConverter(new NapasEnumQConverter());
	    registry.addConverter(new AccTypeEnumQConverter());
	    registry.addConverter(new CustAccTypeEnumQConverter());
	    registry.addConverter(new NotificationQConverter());
	    registry.addConverter(new LogoutEnumQConverter());
	    registry.addConverter(new ActionEnumQConverter());
	    registry.addConverter(new CustPositionEnumQConverter());
	    registry.addConverter(new SmartOtpHisEnumQConverter());
	    registry.addConverter(new SmsHisStatusEnumQConverter());
	    registry.addConverter(new NotificationQConverter());
	    registry.addConverter(new SysParametersTypeEnumQConverter());
	    registry.addConverter(new FeeTypeEnumQConverter());
	    registry.addConverter(new TransLotStatusEnumQConverter());
	    registry.addConverter(new NotificationQConverter());
	    registry.addConverter(new PaymentStatusEnumQConverter());
	    registry.addConverter(new ScheduleEnumQConverter());
	    registry.addConverter(new TransSchedulesStatusEnumQConverter());
	    registry.addConverter(new TransFeeTpyeEnumQConverter());
	    registry.addConverter(new TransPaymentStatusEnumQConverter());
	    registry.addConverter(new TransStatusEnumQConverter());
	    registry.addConverter(new SchedulesFrequencyEnumQConverter());
	    registry.addConverter(new StatusTransLotEnumQConverter());
	    
	    
	    
	    
	    
//	    product: custproduct,transfee, translimitcust
	    registry.addConverter(new ProductEnumQConverter());

	    registry.addConverter(new SchedulesEnumQConverter());
	    
	}
}
package llq.fw.cm.enums.trans;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class TransStatusEnumQConverter implements Converter<String, TransStatusEnum> {
	@Override
	public TransStatusEnum convert(String source) {
		TransStatusEnum statusEnum= Arrays.stream(TransStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
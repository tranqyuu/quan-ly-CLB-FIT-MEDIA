package llq.fw.cm.security.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import llq.fw.cm.models.Activities;
import llq.fw.cm.models.ActivitiesMembers;
import llq.fw.cm.models.Member;

public interface ActivitiesRepository
                extends JpaRepository<Activities, java.lang.Long>, JpaSpecificationExecutor<Activities> {

        @Query(value = "select * from ACTIVITIES_MEMBERS inner join ACTIVITIES on ACTIVITIES_MEMBERS.ACTIVITES_ID = ACTIVITIES.ACTIVITIES_ID inner join MEMBER on ACTIVITIES_MEMBERS.MEMBERS_ID = MEMBER.MEMBER_ID where ACTIVITIES.ACTIVITIES_ID = :activitiesId", nativeQuery = true)
        ArrayList<ActivitiesMembers> getAllActivitiesMembers(@Param("activitiesId") Long activitiesId,
                        Pageable pageable);
}

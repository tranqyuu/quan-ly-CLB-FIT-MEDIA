package llq.fw.cm.enums.smartotphis;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ActionEnum {
	DANGKY(1), HUY(2),GIAODICH(3);
/*
 *1: Đăng ký| 2: Hủy|3: Giao dịch
 * 
 */
	private Integer value;

	ActionEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static ActionEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		ActionEnum statusEnum= Arrays.stream(ActionEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

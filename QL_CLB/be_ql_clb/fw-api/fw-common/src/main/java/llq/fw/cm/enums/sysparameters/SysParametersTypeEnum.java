package llq.fw.cm.enums.sysparameters;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SysParametersTypeEnum {
	THAMSO(0), THOIGIANCHAYBATCH(1);
/*
 *0: Tham số | 1: Thời gian chạy batch
 * 
 */
	private Integer value;

	SysParametersTypeEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static SysParametersTypeEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		SysParametersTypeEnum statusEnum= Arrays.stream(SysParametersTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

package llq.fw.cm.enums.trans;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TransFeeTpyeEnum {

	NGUOICHUYENTRA(1),NGUOINHANTRA(2);

	/*
	 *"1: Người chuyển trả| 2: Người nhận trả
	 * 
	 */
	private Integer value;

	TransFeeTpyeEnum(Integer i) {
		this.value = i;
	}

	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static TransFeeTpyeEnum forValue(String v) {
		if (v == null) {
			return null;
		}
		TransFeeTpyeEnum statusEnum = Arrays.stream(TransFeeTpyeEnum.values()).filter(p -> {
			return p.getValue().toString().equals(v);
		}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

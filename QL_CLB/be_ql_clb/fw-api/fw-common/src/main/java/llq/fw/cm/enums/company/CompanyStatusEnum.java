package llq.fw.cm.enums.company;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author BONTV
 *
 */
public enum CompanyStatusEnum {
	CHODUYETMOMOI(0), TUCHOI(1),DANGHOATDONG(2), CHODUYETKHOA(3), KHOA(4), CHODUYETMOKHOA(5), CHODUYETCHINHSUA(6), CHOKICHHOAT(7), CHODUYETHUY(8),HUY(9);
	/*
	 * 0: Chờ duyệt mở mới|1: Từ chối|2:Đang hoạt động|
	 * 3: Chờ duyệt khóa|4: Khóa|5: Chờ duyệt mở khóa| 6: Chờ duyệt chỉnh sửa
	 * 7: Chờ kích hoạt | 8: Chờ duyệt hủy | 9. Hủy
	 * */
	
	private Integer value;

	CompanyStatusEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static CompanyStatusEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		CompanyStatusEnum statusEnum= Arrays.stream(CompanyStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

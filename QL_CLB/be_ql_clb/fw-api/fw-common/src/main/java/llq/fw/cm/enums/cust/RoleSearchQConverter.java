package llq.fw.cm.enums.cust;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class RoleSearchQConverter implements Converter<String, RoleSearchEnum> {
	@Override
	public RoleSearchEnum convert(String source) {
		RoleSearchEnum statusEnum= Arrays.stream(RoleSearchEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
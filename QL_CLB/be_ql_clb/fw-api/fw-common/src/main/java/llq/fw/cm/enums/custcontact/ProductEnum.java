package llq.fw.cm.enums.custcontact;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ProductEnum {

	NAPAS(1), CITAD(2), CORE(3);

	/*
	 * 1: Chuyển khoản 24/7 đến số tài khoản|2: Chuyển khoản liên ngân hàng|3:
	 * Chuyển khoản nội bộ CB
	 * 
	 */
	private Integer value;

	ProductEnum(Integer i) {
		this.value = i;
	}

	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static ProductEnum forValue(String v) {
		if (v == null) {
			return null;
		}
		ProductEnum statusEnum = Arrays.stream(ProductEnum.values()).filter(p -> {
			return p.getValue().toString().equals(v);
		}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

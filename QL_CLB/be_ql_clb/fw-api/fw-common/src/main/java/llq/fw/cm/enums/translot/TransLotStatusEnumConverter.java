package llq.fw.cm.enums.translot;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TransLotStatusEnumConverter implements AttributeConverter<TransLotStatusEnum, String>{

    @Override
    public String convertToDatabaseColumn(TransLotStatusEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public TransLotStatusEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	TransLotStatusEnum statusEnum= Arrays.stream(TransLotStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}
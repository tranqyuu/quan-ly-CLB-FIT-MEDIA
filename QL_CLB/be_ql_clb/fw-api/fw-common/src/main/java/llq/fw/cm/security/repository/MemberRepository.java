package llq.fw.cm.security.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import llq.fw.cm.models.Member;

public interface MemberRepository extends JpaRepository<Member, java.lang.Long>, JpaSpecificationExecutor<Member> {
	Optional<Member> findByCode(String username);

	boolean existsByCode(String username);

	@Query(value = "select t.code from (select code from cust where code like %:code% order by CREATED_AT desc) t where ROWNUM <= 1", nativeQuery = true)
	String getCode(@Param("code") String code);

	@Query(value = "select code from cust where COMPANY_ID = :company", nativeQuery = true)
	Set<String> getAllCode(@Param("company") String company);

	@Query(value = "select tel from cust where COMPANY_ID = :company", nativeQuery = true)
	Set<String> getAllTel(@Param("company") String company);

	@Query(value = "select email from cust where COMPANY_ID = :company", nativeQuery = true)
	Set<String> getAllEmail(@Param("company") String company);

	@Query(value = "select * from cust where COMPANY_ID = :company and POSITION = :position", nativeQuery = true)
	List<Member> getCustByPosition(@Param("company") String company, @Param("position") Long position);

	@Query(value = "select count(*) from cust where TEL = :value", nativeQuery = true)
	Long countCustDupTel(@Param("value") String value);

	@Query(value = "select count(*) from cust where TEL = :value and id <> :id", nativeQuery = true)
	Long countCustDupTel(@Param("value") String value, @Param("id") Long id);

	@Query(value = "select count(*) from cust where EMAIL = :value", nativeQuery = true)
	Long countCustDupEmail(@Param("value") String value);

	@Query(value = "select count(*) from cust where EMAIL = :value and id <> :id", nativeQuery = true)
	Long countCustDupEmail(@Param("value") String value, @Param("id") Long id);

	@Query(value = "select count(*) from cust where IDNO = :value", nativeQuery = true)
	Long countCustDupIdno(@Param("value") String value);

	@Query(value = "select count(*) from cust where IDNO = :value and id <> :id", nativeQuery = true)
	Long countCustDupIdno(@Param("value") String value, @Param("id") Long id);

}

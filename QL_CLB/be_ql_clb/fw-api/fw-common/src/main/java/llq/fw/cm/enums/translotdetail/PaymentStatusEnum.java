package llq.fw.cm.enums.translotdetail;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum PaymentStatusEnum {

	CHODUYET(0),TUCHOI(1),DADUYET(2),HUY(3),HOANTHANH(4);

	/*
	 * "0: Chờ duyệt, 1: Từ chối, 2: Đã duyệt,3: Hủy, 4: Hoàn thành
	 * 
	 */
	private Integer value;

	PaymentStatusEnum(Integer i) {
		this.value = i;
	}

	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static PaymentStatusEnum forValue(String v) {
		if (v == null) {
			return null;
		}
		PaymentStatusEnum statusEnum = Arrays.stream(PaymentStatusEnum.values()).filter(p -> {
			return p.getValue().toString().equals(v);
		}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

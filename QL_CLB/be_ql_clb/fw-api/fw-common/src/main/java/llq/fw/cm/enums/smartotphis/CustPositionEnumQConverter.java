package llq.fw.cm.enums.smartotphis;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class CustPositionEnumQConverter implements Converter<String, CustPositionEnum> {
	@Override
	public CustPositionEnum convert(String source) {
		CustPositionEnum statusEnum= Arrays.stream(CustPositionEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
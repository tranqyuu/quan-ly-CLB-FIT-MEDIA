package llq.fw.cm.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import llq.fw.cm.enums.IBStatusEnum;
import llq.fw.cm.enums.cust.CustStatusEnum;
import llq.fw.cm.enums.cust.IndentifypapersEnum;
import llq.fw.cm.enums.cust.MemberSexEnum;
import llq.fw.cm.enums.cust.NotificationEnum;
import llq.fw.cm.enums.cust.PositionEnum;
import llq.fw.cm.enums.cust.RoleSearchEnum;
import llq.fw.cm.enums.cust.RoleTransEnum;
import llq.fw.cm.enums.cust.SmsEnum;
import llq.fw.cm.enums.cust.VerifyTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * The persistent class for the CUST database table.
 * 
 */
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Member extends Auditable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "CUST_ID_GENERATOR", sequenceName = "CUST_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUST_ID_GENERATOR")
	@Column(name = "MEMBER_ID")
	private Long id;

	private Date birthday;

	private String cif;

	private String code;

	private String email;

	private String fullname;

	private String idno;

	private IndentifypapersEnum indentitypapers;

	private Date lastlogin;

	@Column(name = "LOGINFALSE_NUMBER")
	private Long loginfalseNumber;

	@Column(name = "NMEMONIC_NAME")
	private String nmemonicName;

	private NotificationEnum notification;

	private String password;

	@Column(name = "POSITION")
	private PositionEnum position;

	private String ban;

	private RoleSearchEnum rolesearch;

	private RoleTransEnum roletrans;

	@Column(name = "S_ALL_DD")
	private String sAllDd;

	@Column(name = "S_ALL_FD")
	private String sAllFd;

	@Column(name = "S_ALL_LN")
	private String sAllLn;

	private SmsEnum sms;

	private CustStatusEnum status;

	private MemberSexEnum sex;

	@Column(name = "T_ALL_DD")
	private String tAllDd;

	private String tel;

	private String classes;

	@Column(name = "SCHOOL_YEAR")
	private String schoolYear;

	private String location;

	private String point;

	@Column(name = "VERIFY_TYPE")
	private VerifyTypeEnum verifyType;

	@Column(name = "APPROVE_AT")
	private Date approveAt;

	@Column(name = "APPROVE_BY")
	private Long approveBy;

	private String note;

	@JsonIgnore
	@OneToMany(mappedBy = "members")
	private Set<ActivitiesMembers> activitiesMembers;

	// bi-directional many-to-one association to Company

	@ManyToOne
	private ChucVu chucVu;

	@OneToMany(mappedBy = "cust")
	private Set<RefreshtokenIb> refreshtokenIbs;

	@Override
	public String toString() {
		return "Cust [id=" + id + ", birthday=" + birthday + ", cif=" + cif + ", code=" + code + ", email=" + email
				+ ", fullname=" + fullname + ", idno=" + idno + ", indentitypapers=" + indentitypapers + ", lastlogin="
				+ lastlogin + ", loginfalseNumber=" + loginfalseNumber + ", nmemonicName=" + nmemonicName
				+ ", notification=" + notification + ", password=" + password + ", position=" + position
				+ ", rolesearch=" + rolesearch + ", roletrans=" + roletrans + ", sAllDd=" + sAllDd + ", sAllFd="
				+ sAllFd + ", sAllLn=" + sAllLn + ", sms=" + sms + ", status=" + status + ", tAllDd=" + tAllDd
				+ ", tel=" + tel + ", verifyType=" + verifyType + ", approveAt=" + approveAt + ", approveBy="
				+ approveBy + ", note=" + note + "]";
	}
}
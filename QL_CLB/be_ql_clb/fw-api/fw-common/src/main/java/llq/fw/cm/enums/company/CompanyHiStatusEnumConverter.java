package llq.fw.cm.enums.company;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class CompanyHiStatusEnumConverter implements AttributeConverter<CompanyHiStatusEnum, String>{

    @Override
    public String convertToDatabaseColumn(CompanyHiStatusEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public CompanyHiStatusEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	CompanyHiStatusEnum statusEnum= Arrays.stream(CompanyHiStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

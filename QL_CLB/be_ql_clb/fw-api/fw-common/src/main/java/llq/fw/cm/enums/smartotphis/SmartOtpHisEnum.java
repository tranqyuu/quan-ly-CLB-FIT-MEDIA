package llq.fw.cm.enums.smartotphis;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SmartOtpHisEnum {
	THANHCONG(1), THATBAI(2);
/*
 *1: Thành công| 2: Thất bại
 * 
 */
	private Integer value;

	SmartOtpHisEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static SmartOtpHisEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		SmartOtpHisEnum statusEnum= Arrays.stream(SmartOtpHisEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

package llq.fw.cm.enums.custacc;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class CustAccTypeEnumConverter implements AttributeConverter<CustAccTypeEnum, String>{

    @Override
    public String convertToDatabaseColumn(CustAccTypeEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public CustAccTypeEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	CustAccTypeEnum statusEnum= Arrays.stream(CustAccTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

package llq.fw.cm.enums.trans;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class SchedulesFrequencyEnumConverter implements AttributeConverter<ScheduleFrequencyEnum, String>{

    @Override
    public String convertToDatabaseColumn(ScheduleFrequencyEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public ScheduleFrequencyEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	ScheduleFrequencyEnum statusEnum= Arrays.stream(ScheduleFrequencyEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

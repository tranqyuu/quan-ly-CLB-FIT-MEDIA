package llq.fw.cm.enums.transfee;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class FeeTypeEnumConverter implements AttributeConverter<FeeTypeEnum, String>{

    @Override
    public String convertToDatabaseColumn(FeeTypeEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public FeeTypeEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	FeeTypeEnum statusEnum= Arrays.stream(FeeTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

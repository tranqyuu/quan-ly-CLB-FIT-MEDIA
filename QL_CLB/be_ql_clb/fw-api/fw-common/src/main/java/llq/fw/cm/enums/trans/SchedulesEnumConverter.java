package llq.fw.cm.enums.trans;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class SchedulesEnumConverter implements AttributeConverter<SchedulesEnum, String>{

    @Override
    public String convertToDatabaseColumn(SchedulesEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public SchedulesEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	SchedulesEnum statusEnum= Arrays.stream(SchedulesEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

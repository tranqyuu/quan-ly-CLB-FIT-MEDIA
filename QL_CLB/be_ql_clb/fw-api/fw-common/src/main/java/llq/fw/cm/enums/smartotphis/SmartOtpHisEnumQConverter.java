package llq.fw.cm.enums.smartotphis;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class SmartOtpHisEnumQConverter implements Converter<String, SmartOtpHisEnum> {
	@Override
	public SmartOtpHisEnum convert(String source) {
		SmartOtpHisEnum statusEnum= Arrays.stream(SmartOtpHisEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
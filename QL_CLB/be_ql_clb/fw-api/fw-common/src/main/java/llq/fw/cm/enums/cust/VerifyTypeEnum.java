package llq.fw.cm.enums.cust;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum VerifyTypeEnum {
	SMS(1), SMARTOTP(2);
//	Phương thức xác thực 1: SMS| 2: Smart OTP
	private Integer value;

	VerifyTypeEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static VerifyTypeEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		VerifyTypeEnum statusEnum= Arrays.stream(VerifyTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

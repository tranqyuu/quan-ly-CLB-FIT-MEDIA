package llq.fw.cm.enums.translot;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class StatusTransLotEnumQConverter implements Converter<String, StatusTransLotEnum> {
	@Override
	public StatusTransLotEnum convert(String source) {
		StatusTransLotEnum statusEnum= Arrays.stream(StatusTransLotEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
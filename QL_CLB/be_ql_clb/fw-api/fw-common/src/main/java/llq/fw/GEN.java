// package llq.fw;

// import java.io.IOException;
// import java.lang.reflect.Field;
// import java.lang.reflect.Method;
// import java.lang.reflect.ParameterizedType;
// import java.nio.file.Files;
// import java.nio.file.Path;
// import java.nio.file.Paths;
// import java.nio.file.StandardOpenOption;
// import java.sql.Blob;
// import java.text.Normalizer;
// import java.text.SimpleDateFormat;
// import java.time.Instant;
// import java.util.Arrays;
// import java.util.Date;
// import java.util.List;
// import java.util.Set;
// import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

// import javax.persistence.Column;
// import javax.persistence.GeneratedValue;
// import javax.persistence.Id;
// import javax.persistence.ManyToMany;
// import javax.persistence.ManyToOne;
// import javax.persistence.OneToMany;

// import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
// import org.springframework.beans.factory.config.BeanDefinition;
// import
// org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
// import org.springframework.core.type.AnnotationMetadata;
// import org.springframework.core.type.filter.RegexPatternTypeFilter;
// import org.springframework.http.ResponseEntity;
// import org.springframework.util.CollectionUtils;
// import org.springframework.util.ObjectUtils;
// import org.springframework.util.StringUtils;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.PathVariable;

// import llq.fw.cm.common.Constants.FwError;
// import llq.fw.cm.common.Gen;
// import llq.fw.cm.enums.IBStatusEnum;
// import llq.fw.cm.models.Auditable;
// import llq.fw.cm.models.Branch;
// import llq.fw.cm.models.CustContact;
// import llq.fw.cm.payload.response.BaseResponse;

// public class GEN {
// static String pathRepository =
// "D:\\Projects\\cb\\1.SMA_IB\\1.Sourcecode\\API\\ib_sme_api\\fw-api\\fw-common\\src\\main\\java\\llq\\fw\\cm\\repository\\gen\\";
// static String pathService =
// "D:\\Projects\\cb\\1.SMA_IB\\1.Sourcecode\\API\\ib_sme_api\\fw-api\\fw-cms-api\\src\\main\\java\\llq\\fw\\services\\gen\\";
// static String pathController =
// "D:\\Projects\\cb\\1.SMA_IB\\1.Sourcecode\\API\\ib_sme_api\\fw-api\\fw-cms-api\\src\\main\\java\\llq\\fw\\controllers\\gen\\";
// static String pathSearchRequest =
// "D:\\Projects\\cb\\1.SMA_IB\\1.Sourcecode\\API\\ib_sme_api\\fw-api\\fw-cms-api\\src\\main\\java\\llq\\fw\\payload\\request\\gen\\";

// static String pathComponent =
// "D:\\Projects\\cb\\1.SMA_IB\\1.Sourcecode\\cms\\cms_sme_web\\src\\app\\page\\gen\\";

// public static void main(String axx[]) throws Exception {

// // final ClassPathScanningCandidateComponentProvider provider = new
// // ClassPathScanningCandidateComponentProvider(
// // false);
// // // add include filters which matches all the classes (or use your own)
// // provider.addIncludeFilter(new
// RegexPatternTypeFilter(Pattern.compile(".*")));
// //
// // // get matching classes defined in the package
// // final Set<BeanDefinition> classes =
// // provider.findCandidateComponents("llq.fw.cm.models");
// //
// // // this is how you can load the class type from BeanDefinition instance
// // for (BeanDefinition bean : classes) {
// // Class<?> clazz = Class.forName(bean.getBeanClassName());
// // // System.out.println(clazz.getName());
// //
// // if (clazz.isAnnotationPresent(Gen.class) &&
// !clazz.getName().contains("$")) {
// //// genClass(clazz);
// //// genRepository(clazz);
// // Gen gen = clazz.getAnnotation(Gen.class);
// // if(!ObjectUtils.isEmpty(gen.name())) {
// // genComponent(clazz, gen.name());
// //
// // }
// // }
// // // genClass(clazz);
// // }

// genAPI();

// }

// public static void genService(Class<?> clazz, String componentName) throws
// Exception {
// String pathName = deAccent(componentName);
// Files.createDirectories(Paths.get(pathComponent + pathName));

// }

// public static void genComponent(Class<?> clazz, String componentName) throws
// Exception {
// String pathName = deAccent(componentName);
// Files.createDirectories(Paths.get(pathComponent + pathName));

// }

// public static String deAccent(String str) {
// String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
// Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
// return
// pattern.matcher(nfdNormalizedString).replaceAll("").toLowerCase().replaceAll("
// ", "-").replace('đ', 'd');
// }

// public static void genAPI() throws Exception {

// final ClassPathScanningCandidateComponentProvider provider = new
// ClassPathScanningCandidateComponentProvider(
// false);
// // add include filters which matches all the classes (or use your own)
// provider.addIncludeFilter(new RegexPatternTypeFilter(Pattern.compile(".*")));

// // get matching classes defined in the package
// final Set<BeanDefinition> classes =
// provider.findCandidateComponents("llq.fw.cm.models");

// // this is how you can load the class type from BeanDefinition instance
// for (BeanDefinition bean : classes) {
// Class<?> clazz = Class.forName(bean.getBeanClassName());
// // System.out.println(clazz.getName());
// if (!clazz.equals(GEN.class) && !clazz.equals(Auditable.class) &&
// !clazz.getName().contains("$")) {
// // genClass(clazz);
// System.out.println(clazz.getName());

// Class<?> clazzRepository = repositoryExist(clazz);
// // if (clazzRepository == null) {
// // genRepository(clazz);
// // }

// // if (clazzRepository != null) {
// // genSearchRequest(clazz);
// //
// // }
// // if (!requestSerarchExist(clazz)) {
// // genSearchRequest(clazz);
// // }
// if (!serviceExist(clazz)) {
// genService(clazz, clazzRepository);
// }
// if (!controllerExist(clazz)) {
// genController(clazz);
// }

// }
// // genClass(clazz);
// }
// }

// public static void genSearchRequest(Class<?> clazz) throws IOException {
// String clazzName = getName(clazz);
// String filePath = pathSearchRequest + clazzName + "SearchRequest.java";
// Field fieldId = getIdField(clazz);
// if (fieldId == null) {
// return;
// }

// appendToFile("package llq.fw.payload.request.gen;", filePath);
// appendToFile("import lombok.AllArgsConstructor;", filePath);
// appendToFile("import lombok.Builder;", filePath);
// appendToFile("import lombok.Getter;", filePath);
// appendToFile("import lombok.NoArgsConstructor;", filePath);
// appendToFile("import lombok.Setter;", filePath);
// appendToFile("", filePath);
// appendToFile("@Getter", filePath);
// appendToFile("@Setter", filePath);
// appendToFile("@Builder", filePath);
// appendToFile("@AllArgsConstructor", filePath);
// appendToFile("@NoArgsConstructor", filePath);
// appendToFile("public class " + clazzName + "SearchRequest {", filePath);

// Field[] allFields = clazz.getDeclaredFields();
// if (!ObjectUtils.isEmpty(allFields)) {
// Arrays.stream(allFields).forEach(field -> {
// try {
// boolean isManyToOne = field.isAnnotationPresent(ManyToOne.class);
// String fieldName = field.getName();
// Method methodGetter = getGetterMethod(fieldName, clazz);
// if (methodGetter != null && !field.getType().equals(byte[].class)) {
// if (!isManyToOne) {
// appendToFile(" private " + field.getType().getName() + " " + fieldName + ";",
// filePath);
// } else {
// appendToFile(" private String " + fieldName + ";", filePath);
// }
// }
// } catch (IOException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// });
// }
// appendToFile("}", filePath);

// }

// public static void genController(Class<?> clazz) throws IOException {
// String clazzName = getName(clazz);
// String filePath = pathController + getName(clazz) + "Controller.java";
// String objName = getNameObj(clazz);
// Field fieldId = getIdField(clazz);
// if (fieldId == null) {
// return;
// }
// appendToFile("package llq.fw.controllers.gen;", filePath);
// appendToFile("", filePath);
// appendToFile("import
// org.springframework.beans.factory.annotation.Autowired;", filePath);
// appendToFile("import org.springframework.data.domain.Pageable;", filePath);
// appendToFile("import org.springframework.http.HttpStatus;", filePath);
// appendToFile("import org.springframework.http.ResponseEntity;", filePath);
// appendToFile("import org.springframework.web.bind.annotation.CrossOrigin;",
// filePath);
// appendToFile("import org.springframework.web.bind.annotation.DeleteMapping;",
// filePath);
// appendToFile("import org.springframework.web.bind.annotation.GetMapping;",
// filePath);
// appendToFile("import org.springframework.web.bind.annotation.PathVariable;",
// filePath);
// appendToFile("import org.springframework.web.bind.annotation.PostMapping;",
// filePath);
// appendToFile("import org.springframework.web.bind.annotation.PutMapping;",
// filePath);
// appendToFile("import org.springframework.web.bind.annotation.RequestBody;",
// filePath);
// appendToFile("import
// org.springframework.web.bind.annotation.RequestMapping;", filePath);
// appendToFile("import
// org.springframework.web.bind.annotation.ResponseStatus;", filePath);
// appendToFile("import
// org.springframework.web.bind.annotation.RestController;", filePath);
// appendToFile("", filePath);
// appendToFile("import llq.fw.cm.models." + clazzName + ";", filePath);
// appendToFile("import llq.fw.cm.payload.response.BaseResponse;", filePath);
// appendToFile("import llq.fw.services.gen." + clazzName + "ServiceImpl;",
// filePath);
// appendToFile("import llq.fw.payload.request.gen." + clazzName +
// "SearchRequest;", filePath);

// appendToFile("@RestController", filePath);
// appendToFile("@RequestMapping(\"api/" + clazzName.toLowerCase() + "\")",
// filePath);
// appendToFile("public class " + clazzName + "Controller {", filePath);
// appendToFile(" @Autowired", filePath);
// appendToFile(" " + clazzName + "ServiceImpl " + objName + "ServiceImpl;",
// filePath);
// appendToFile(" @GetMapping // Tim kiem theo nhieu dieu kien", filePath);
// appendToFile(" public ResponseEntity<BaseResponse> searchAll(" + clazzName +
// "SearchRequest " + objName
// + "Request,Pageable pageable) {", filePath);
// appendToFile(
// " BaseResponse baseResponse=" + objName + "ServiceImpl.search(" + objName +
// "Request, pageable);",
// filePath);
// appendToFile(" return ResponseEntity.ok(baseResponse);", filePath);
// appendToFile(" }", filePath);
// appendToFile(" @PostMapping // Tao moi ", filePath);
// appendToFile(" @ResponseStatus(HttpStatus.CREATED)", filePath);
// appendToFile(" public ResponseEntity<BaseResponse> create(@RequestBody " +
// clazzName + " " + objName
// + "Request) {", filePath);
// appendToFile(" BaseResponse baseResponse=" + objName + "ServiceImpl.create("
// + objName + "Request);",
// filePath);
// appendToFile(" return ResponseEntity.ok(baseResponse);", filePath);
// appendToFile(" }", filePath);
// appendToFile(" @PutMapping // Cap nhat ", filePath);
// appendToFile(" public ResponseEntity<BaseResponse> update(@RequestBody " +
// clazzName + " " + objName
// + "Request) {", filePath);
// appendToFile(" BaseResponse baseResponse=" + objName + "ServiceImpl.update("
// + objName + "Request);",
// filePath);
// appendToFile(" return ResponseEntity.ok(baseResponse);", filePath);
// appendToFile(" }", filePath);
// appendToFile(" @DeleteMapping(\"{id}\") // Xoa", filePath);
// appendToFile(
// " public ResponseEntity<BaseResponse> delete(@PathVariable " +
// fieldId.getType().getName() + " id) {",
// filePath);
// appendToFile(" BaseResponse baseResponse=" + objName +
// "ServiceImpl.delete(id);", filePath);
// appendToFile(" return ResponseEntity.ok(baseResponse);", filePath);
// appendToFile(" }", filePath);
// appendToFile(" ", filePath);
// appendToFile(" @GetMapping(\"{id}\") // Xoa", filePath);
// appendToFile(" public ResponseEntity<BaseResponse>
// getDetailById(@PathVariable " + fieldId.getType().getName()
// + " id) {", filePath);
// appendToFile(" BaseResponse baseResponse=" + objName +
// "ServiceImpl.getDetail(id);", filePath);
// appendToFile(" return ResponseEntity.ok(baseResponse);", filePath);
// appendToFile(" }", filePath);

// appendToFile(" @GetMapping(\"keyexist/{id}\") // check exist", filePath);
// appendToFile(" public ResponseEntity<BaseResponse> checkExist(@PathVariable "
// + fieldId.getType().getName()
// + " id) {", filePath);
// appendToFile(" BaseResponse baseResponse=" + objName +
// "ServiceImpl.checkExist(id);", filePath);
// appendToFile(" return ResponseEntity.ok(baseResponse);", filePath);
// appendToFile(" }", filePath);

// appendToFile("}", filePath);
// appendToFile("", filePath);

// }

// public static void genService(Class<?> clazz, Class<?> clazzRepository)
// throws Exception {
// // Field field=getIdField(clazz);

// String filePath = pathService + getName(clazz) + "ServiceImpl.java";
// String objName = getNameObj(clazz);
// Field fieldId = getIdField(clazz);
// List<Field> noGens = getNoGen(clazz);
// if (fieldId == null) {
// return;
// }
// Method idMethodGetter = getGetterMethod(fieldId.getName(), clazz);
// appendToFile("package llq.fw.services.gen;", filePath);

// appendToFile("import java.util.ArrayList;", filePath);
// appendToFile("import java.util.List;", filePath);
// appendToFile("import java.util.Date;", filePath);
// appendToFile("import java.text.SimpleDateFormat;", filePath);
// appendToFile("import javax.persistence.criteria.CriteriaBuilder;", filePath);
// appendToFile("import javax.persistence.criteria.CriteriaQuery;", filePath);
// appendToFile("import javax.persistence.criteria.Predicate;", filePath);
// appendToFile("import javax.persistence.criteria.Root;", filePath);
// appendToFile("import javax.persistence.criteria.CriteriaBuilder.In;",
// filePath);

// appendToFile("import org.slf4j.Logger;", filePath);
// appendToFile("import org.slf4j.LoggerFactory;", filePath);
// appendToFile("import
// org.springframework.beans.factory.annotation.Autowired;", filePath);
// appendToFile("import org.springframework.data.domain.Page;", filePath);
// appendToFile("import org.springframework.data.domain.Pageable;", filePath);
// appendToFile("import org.springframework.data.jpa.domain.Specification;",
// filePath);
// appendToFile("import org.springframework.stereotype.Component;", filePath);
// appendToFile("import
// org.springframework.transaction.annotation.Propagation;", filePath);
// appendToFile("import
// org.springframework.transaction.annotation.Transactional;", filePath);
// appendToFile("import org.springframework.util.ObjectUtils;", filePath);

// appendToFile("import llq.fw.cm.common.Constants.FwError;", filePath);
// appendToFile("import " + clazz.getName() + ";", filePath);
// appendToFile("import llq.fw.cm.payload.response.BaseResponse;", filePath);
// appendToFile("import " + clazzRepository.getName() + ";", filePath);
// appendToFile("import llq.fw.payload.request.gen." + getName(clazz) +
// "SearchRequest;", filePath);
// appendToFile("import llq.fw.cm.services.BaseService;", filePath);

// appendToFile("@Component", filePath);
// appendToFile("public class " + getName(clazz) + "ServiceImpl implements
// BaseService<BaseResponse, "
// + getName(clazz) + "," + getName(clazz) + "SearchRequest, " +
// fieldId.getType().getName() + "> {",
// filePath);
// appendToFile(" private static final Logger logger = LoggerFactory.getLogger("
// + getName(clazz)
// + "ServiceImpl.class);", filePath);
// appendToFile(" private final SimpleDateFormat formatDate = new
// SimpleDateFormat(\"dd-MM-yyyy\");", filePath);

// appendToFile(" @Autowired", filePath);
// appendToFile(" protected " + getName(clazz) + "Repository " + objName +
// "Repository;", filePath);

// appendToFile(" @Override", filePath);
// appendToFile(" public BaseResponse create(" + getName(clazz) + " " + objName
// + ") {", filePath);

// appendToFile(" BaseResponse baseResponse = new BaseResponse();", filePath);
// appendToFile(" baseResponse.setFwError(FwError.THANHCONG);", filePath);
// appendToFile(" try {", filePath);

// if (!fieldId.isAnnotationPresent(GeneratedValue.class)) {
// appendToFile(" var " + objName + "Cur = " + objName + "Repository.findById("
// + objName + "."
// + idMethodGetter.getName() + "()).orElse(null);", filePath);
// appendToFile(" if (" + objName + "Cur != null) {", filePath);
// appendToFile(" baseResponse.setFwError(FwError.DLDATONTAI);", filePath);
// appendToFile(" return baseResponse;", filePath);
// appendToFile(" }", filePath);
// appendToFile(" ", filePath);
// }

// appendToFile(" var " + objName + "New = " + getName(clazz) + ".builder()",
// filePath);

// Field[] allFields = clazz.getDeclaredFields();
// if (!ObjectUtils.isEmpty(allFields)) {
// Arrays.stream(allFields).forEach(field -> {
// if (!field.getName().equals(fieldId.getName()) ||
// !field.isAnnotationPresent(GeneratedValue.class)) {
// String fieldName = field.getName();
// Method methodGetter = getGetterMethod(fieldName, clazz);
// if (methodGetter != null) {
// try {
// if (field.getName().equals(fieldId.getName()) &&
// field.getType().equals(String.class)) {
// appendToFile(" ." + fieldName + "(" + objName + "."
// + methodGetter.getName() + "().toUpperCase())", filePath);
// } else {
// appendToFile(" ." + fieldName + "(" + objName + "."
// + methodGetter.getName() + "())", filePath);
// }
// } catch (IOException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// }
// }
// });
// }

// appendToFile(" .build();", filePath);
// appendToFile(" jpaExecute(" + objName + "New);", filePath);
// appendToFile(" baseResponse.setData(" + objName + "New);", filePath);
// appendToFile(" } catch (Exception e) {", filePath);
// appendToFile(" baseResponse.setFwError(FwError.KHONGTHANHCONG);", filePath);
// appendToFile(" logger.error(\"error\",e);", filePath);
// appendToFile(" }", filePath);
// appendToFile(" return baseResponse;", filePath);
// appendToFile(" }", filePath);

// appendToFile(" @Override", filePath);
// appendToFile(" public BaseResponse update(" + getName(clazz) + " " + objName
// + ") {", filePath);
// appendToFile(" BaseResponse baseResponse = new BaseResponse();", filePath);
// appendToFile(" baseResponse.setFwError(FwError.THANHCONG);", filePath);
// appendToFile(" try {", filePath);

// appendToFile(" var " + objName + "Cur = " + objName + "Repository.findById("
// + objName + "."
// + idMethodGetter.getName() + "()).orElse(null);", filePath);
// appendToFile(" if (" + objName + "Cur == null) {", filePath);
// appendToFile(" baseResponse.setFwError(FwError.DLKHONGTONTAI);", filePath);
// appendToFile(" return baseResponse;", filePath);
// appendToFile(" }", filePath);

// Arrays.stream(allFields).forEach(field -> {
// String fieldName = field.getName();
// if (!field.getName().equals(fieldId.getName())) {
// Method methodGetter = getGetterMethod(fieldName, clazz);
// Method methodSetter = getSetterMethod(fieldName, clazz);
// if (!ObjectUtils.isEmpty(methodGetter) && !ObjectUtils.isEmpty(methodSetter))
// {
// try {
// appendToFile(" " + objName + "Cur." + methodSetter.getName() + "(" + objName
// + "."
// + methodGetter.getName() + "());", filePath);
// } catch (IOException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// }
// }
// });

// appendToFile(" jpaExecute(" + objName + "Cur);", filePath);
// appendToFile(" /*", filePath);
// appendToFile(" * Luu obj lich su neu can", filePath);
// appendToFile(" */", filePath);

// appendToFile(" baseResponse.setData(" + objName + "Cur);", filePath);
// appendToFile(" } catch (Exception e) {", filePath);
// appendToFile(" baseResponse.setFwError(FwError.KHONGTHANHCONG);", filePath);
// appendToFile(" logger.error(\"error\",e);", filePath);
// appendToFile(" }", filePath);
// appendToFile(" return baseResponse;", filePath);
// appendToFile(" }", filePath);

// appendToFile(" @Override", filePath);
// appendToFile(
// " public BaseResponse search(" + getName(clazz) + "SearchRequest " + objName
// + ", Pageable pageable) {",
// filePath);
// appendToFile(" BaseResponse baseResponse = new BaseResponse();", filePath);
// appendToFile(" baseResponse.setFwError(FwError.THANHCONG); // TODO : Lay tham
// so xu ly song ngu",
// filePath);
// appendToFile(" try {", filePath);

// appendToFile(" Specification<" + getName(clazz) + "> specification = new
// Specification<"
// + getName(clazz) + ">() {", filePath);
// appendToFile(" /**", filePath);
// appendToFile(" * ", filePath);
// appendToFile(" */", filePath);
// appendToFile(" private static final long serialVersionUID = 1L;", filePath);

// appendToFile(" @Override", filePath);
// appendToFile(" public Predicate toPredicate(Root<" + getName(clazz)
// + "> root, CriteriaQuery<?> query,", filePath);
// appendToFile(" CriteriaBuilder criteriaBuilder) {", filePath);
// appendToFile(" List<Predicate> predicates = new ArrayList<>();", filePath);

// Arrays.stream(allFields).forEach(field -> {
// System.out.println(field.getName());
// String fieldName = field.getName();
// // if(!field.getName().equals(fieldId.getName())) {
// Method methodGetter = getGetterMethod(fieldName, clazz);

// if (!ObjectUtils.isEmpty(methodGetter) && !noGens.contains(field)) {
// try {
// boolean isManyToOne = field.isAnnotationPresent(ManyToOne.class);

// if (!isManyToOne) {
// appendToFile(" if (!ObjectUtils.isEmpty(" + objName + "."
// + methodGetter.getName() + "())) {", filePath);

// if (field.getType().equals(Date.class)) {
// appendToFile(
// "
// predicates.add(criteriaBuilder.and(criteriaBuilder.equal(criteriaBuilder.function(\"TRUNC\",
// Date.class, root.get(\""
// + fieldName + "\")),",
// filePath);
// appendToFile(
// " criteriaBuilder.function(\"TO_DATE\", Date.class,
// criteriaBuilder.literal(formatDate.format("
// + objName + "." + methodGetter.getName()
// + "())), criteriaBuilder.literal(\"DD-MM-YYYY\")))));",
// filePath);

// } else if (!field.isEnumConstant() && field.getType().equals(String.class)) {
// appendToFile(
// "
// predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.upper(root.get(\""
// + fieldName + "\")),",
// filePath);
// appendToFile(" \"%\" + " + objName + "."
// + methodGetter.getName() + "().toUpperCase() + \"%\")));", filePath);
// } else {
// appendToFile(
// " predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get(\""
// + fieldName + "\"),",
// filePath);
// appendToFile(" " + objName + "." + methodGetter.getName()
// + "())));", filePath);
// }
// appendToFile(" }", filePath);

// } else {

// // GEN MANYTOONE
// Class<?> subClazz = field.getType();

// Field subClassId = getIdField(subClazz);
// // Method methodGetterSubClazz = getGetterMethod(subClassId.getName(),
// // subClazz);

// appendToFile(" if (!ObjectUtils.isEmpty(" + objName + "."
// + methodGetter.getName() + "())) {", filePath);
// appendToFile(" String[] ids=" + objName + "." + methodGetter.getName()
// + "().split(\",\");", filePath);
// appendToFile(" if(ids.length >1) {", filePath);
// appendToFile(" In<String> inClause = criteriaBuilder.in(root.get(\""
// + fieldName + "\").get(\"" + subClassId.getName() + "\"));", filePath);
// appendToFile(" for (String id : ids) {", filePath);
// appendToFile(" inClause.value(id.toUpperCase());", filePath);
// appendToFile(" }", filePath);
// appendToFile(" predicates.add(criteriaBuilder.and(inClause));",
// filePath);
// appendToFile(" }else {", filePath);
// appendToFile(
// " predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get(\""
// + fieldName + "\").get(\"" + subClassId.getName() + "\"),",
// filePath);
// appendToFile(" " + objName + "." + methodGetter.getName()
// + "().toUpperCase())));", filePath);
// appendToFile(" }", filePath);
// appendToFile(" }", filePath);

// //
// //
// // ///
// //
// // appendToFile(" if (!ObjectUtils.isEmpty(" + objName + "."
// // + methodGetter.getName() + "()." + methodGetterSubClazz.getName() + "()))
// {",
// // filePath);
// // appendToFile(
// // " predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get(\""
// // + fieldName + "\").get(\"" + subClassId.getName() + "\"),",
// // filePath);
// // appendToFile(" " + objName + "." + methodGetter.getName() + "()."
// // + methodGetterSubClazz.getName() + "())));", filePath);
// // appendToFile(" }", filePath);
// }

// } catch (IOException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// }
// // }
// });

// appendToFile(
// " return criteriaBuilder.and(predicates.toArray(new
// Predicate[predicates.size()]));",
// filePath);
// appendToFile(" }", filePath);
// appendToFile(" };", filePath);
// appendToFile(" Page<" + getName(clazz) + "> page = " + objName
// + "Repository.findAll(specification, pageable);", filePath);
// appendToFile(" baseResponse.setData(page);", filePath);
// appendToFile(" } catch (Exception e) {", filePath);
// appendToFile(" baseResponse.setFwError(FwError.KHONGTHANHCONG);", filePath);
// appendToFile(" logger.error(\"error\",e);", filePath);
// appendToFile(" }", filePath);
// appendToFile(" return baseResponse;", filePath);
// appendToFile(" }", filePath);

// appendToFile(" @Override", filePath);
// appendToFile(" public BaseResponse delete(" + fieldId.getType().getName() + "
// id) {", filePath);
// appendToFile(" BaseResponse baseResponse = new BaseResponse();", filePath);
// appendToFile(" baseResponse.setFwError(FwError.THANHCONG);", filePath);
// appendToFile(" try {", filePath);
// appendToFile(" var " + objName + "Cur = " + objName +
// "Repository.findById(id).orElse(null);",
// filePath);
// appendToFile(" if (" + objName + "Cur == null) {", filePath);
// appendToFile(" baseResponse.setFwError(FwError.DLKHONGTONTAI);", filePath);
// appendToFile(" return baseResponse;", filePath);
// appendToFile(" }", filePath);
// appendToFile(" jpaDeleteExecute(id);", filePath);
// appendToFile(" } catch (Exception e) {", filePath);
// appendToFile(" baseResponse.setFwError(FwError.KHONGTHANHCONG);", filePath);
// appendToFile(" logger.error(\"error\",e);", filePath);
// appendToFile(" }", filePath);
// appendToFile(" return baseResponse;", filePath);
// appendToFile(" }", filePath);

// appendToFile(" @Transactional(propagation = Propagation.REQUIRES_NEW)",
// filePath);
// appendToFile(" public void jpaExecute(" + getName(clazz) + " " + objName + ")
// {", filePath);
// appendToFile(" " + objName + "Repository.save(" + objName + ");", filePath);
// appendToFile(" }", filePath);

// appendToFile(" @Transactional(propagation = Propagation.REQUIRES_NEW)",
// filePath);
// appendToFile(" public void jpaDeleteExecute(" + fieldId.getType().getName() +
// " id) {", filePath);
// appendToFile(" " + objName + "Repository.deleteById(id);", filePath);
// appendToFile(" }", filePath);

// appendToFile(" @Override", filePath);
// appendToFile(" public BaseResponse getDetail(" + fieldId.getType().getName()
// + " id) {", filePath);
// appendToFile(" BaseResponse baseResponse = new BaseResponse();", filePath);
// appendToFile(" baseResponse.setFwError(FwError.THANHCONG);", filePath);

// appendToFile(" var " + objName + "Cur = " + objName +
// "Repository.findById(id).orElse(null);", filePath);
// appendToFile(" if (" + objName + "Cur == null) {", filePath);
// appendToFile(" baseResponse.setFwError(FwError.DLKHONGTONTAI);", filePath);
// appendToFile(" return baseResponse;", filePath);
// appendToFile(" }", filePath);

// appendToFile(" baseResponse.setData(" + objName + "Cur);", filePath);
// appendToFile(" return baseResponse;", filePath);
// appendToFile(" }", filePath);

// appendToFile(" @Override", filePath);
// appendToFile(" public BaseResponse checkExist(" + fieldId.getType().getName()
// + " id) {", filePath);
// appendToFile(" BaseResponse baseResponse = new BaseResponse();", filePath);
// appendToFile(" baseResponse.setFwError(FwError.THANHCONG);", filePath);
// if (fieldId.getType().equals(String.class)) {
// appendToFile(" baseResponse.setData(" + objName +
// "Repository.existsById(id.toUpperCase()));",
// filePath);
// } else {
// appendToFile(" baseResponse.setData(" + objName +
// "Repository.existsById(id));", filePath);
// }
// appendToFile(" return baseResponse;", filePath);
// appendToFile(" }", filePath);

// appendToFile("}", filePath);

// }

// public static Method getGetterMethod(String variableName, Class<?> aClass) {

// Method[] declaredMethods = aClass.getMethods();
// for (Method method : declaredMethods) {
// if (variableName.length() + 3 == method.getName().length() &&
// isGetter(method)
// && method.getName().toUpperCase().contains(variableName.toUpperCase())) {
// return method;
// }
// }
// return null;
// }

// public static Method getSetterMethod(String variableName, Class<?> aClass) {

// Method[] declaredMethods = aClass.getDeclaredMethods();
// for (Method method : declaredMethods) {
// if (variableName.length() + 3 == method.getName().length() &&
// isSetter(method)
// && method.getName().toUpperCase().contains(variableName.toUpperCase())) {
// return method;
// }
// }
// return null;
// }

// private static boolean isGetter(Method method) {
// // check for getter methods
// if ((method.getName().startsWith("get") || method.getName().startsWith("is"))
// && method.getParameterCount() == 0
// && !method.getReturnType().equals(void.class)) {
// return true;
// }
// return false;
// }

// private static boolean isSetter(Method method) {
// // check for setter methods
// if (method.getName().startsWith("set") && method.getParameterCount() == 1
// && method.getReturnType().equals(void.class)) {
// return true;
// }
// return false;
// }

// public static void genRepository(Class<?> clazz) throws Exception {
// String filePath = pathRepository + getName(clazz) + "Repository.java";
// Field field = getIdField(clazz);
// if (field == null) {
// return;
// }
// appendToFile("package llq.fw.cm.repository.gen;", filePath);
// appendToFile("import org.springframework.data.jpa.repository.JpaRepository;",
// filePath);
// appendToFile("import
// org.springframework.data.jpa.repository.JpaSpecificationExecutor;",
// filePath);
// appendToFile("import " + clazz.getName() + ";", filePath);

// appendToFile(
// "public interface " + getName(clazz) + "Repository extends JpaRepository<" +
// getName(clazz) + ", "
// + field.getType().getName() + ">, JpaSpecificationExecutor<" + getName(clazz)
// + "> {",
// filePath);
// // appendToFile(" Optional<SysErrorDefine> findByCode(String id);",
// filePath);
// appendToFile("}", filePath);

// }

// public static Field getIdField(Class<?> clazz) {
// Field[] allFields = clazz.getDeclaredFields();
// return Arrays.stream(allFields).filter(field ->
// field.isAnnotationPresent(Id.class)).findFirst().orElseThrow();
// }

// // public static List<Field> getManyToOneFields(Class<?> clazz) {
// // Field[] allFields = clazz.getDeclaredFields();
// // List<Field> fields = Arrays.stream(allFields).filter(field ->
// // field.isAnnotationPresent(ManyToOne.class))
// // .toList();
// // return fields;
// // }

// // public static List<Field> getNoGen(Class<?> clazz) {
// // Field[] allFields = clazz.getDeclaredFields();
// // List<Field> fields = Arrays.stream(allFields).filter(field ->
// // field.isAnnotationPresent(OneToMany.class)
// // // ||field.isAnnotationPresent(ManyToOne.class)
// // || field.getType().equals(byte[].class)
// // || field.isAnnotationPresent(ManyToMany.class)).toList();
// // return fields;
// // }

// public static void appendToFile(String content, String filePath) throws
// IOException {
// Files.writeString(Path.of(filePath), content + System.lineSeparator(),
// StandardOpenOption.CREATE,
// StandardOpenOption.APPEND);
// }

// public static void genClass(Class<?> c) throws Exception {
// Class<?> supper = c.getSuperclass();
// genFields(c);
// // genFields(supper);
// System.out.println("\n}");
// }

// public static void genFields(Class<?> c) throws Exception {
// Field[] allFields = c.getDeclaredFields();
// Arrays.stream(allFields).forEach(field -> {
// if (field.getType().equals(String.class)) {
// System.out.println(field.getName() + ": string | undefined");
// } else if (field.getType().equals(Long.class)) {
// System.out.println(field.getName() + "?:Number");
// } else if (field.getType().equals(Date.class)) {
// System.out.println(field.getName() + ": Date | undefined");
// } else if (field.getType().equals(Date.class) ||
// Instant.class.equals(field.getType())) {
// System.out.println(field.getName() + ": Date | undefined");
// } else if (field.getType().equals(byte[].class) ||
// Instant.class.equals(field.getType())) {
// System.out.println(field.getName() + ": Blob | undefined");
// } else if (Set.class.equals(field.getType())) {
// ParameterizedType integerListType = (ParameterizedType)
// field.getGenericType();
// Class<?> integerListClass = (Class<?>)
// integerListType.getActualTypeArguments()[0];
// System.out.println(field.getName() + ": " + getName(integerListClass) + "[] |
// undefined");
// } else if (!long.class.equals(field.getType())) {
// System.out.println(field.getName() + ": " + getName(field.getType()) + " |
// undefined");
// }

// System.out.print(" ");
// });
// }

// public static Class<?> repositoryExist(Class<?> clazz1) throws
// ClassNotFoundException {
// // final ClassPathScanningCandidateComponentProvider provider = new
// // ClassPathScanningCandidateComponentProvider(
// // false);

// ClassPathScanningCandidateComponentProvider provider = new
// ClassPathScanningCandidateComponentProvider(false) {
// // Override isCandidateComponent to only scan for interface
// @Override
// protected boolean isCandidateComponent(AnnotatedBeanDefinition
// beanDefinition) {
// AnnotationMetadata metadata = beanDefinition.getMetadata();
// return metadata.isIndependent() && metadata.isInterface();
// }
// };
// // add include filters which matches all the classes (or use your own)
// provider.addIncludeFilter(new RegexPatternTypeFilter(Pattern.compile(".*")));

// // get matching classes defined in the package
// final Set<BeanDefinition> classes =
// provider.findCandidateComponents("llq.fw.cm.repository");

// // this is how you can load the class type from BeanDefinition instance
// for (BeanDefinition bean : classes) {
// Class<?> clazz = Class.forName(bean.getBeanClassName());
// // System.out.println(clazz.getName());

// if (getName(clazz).equals(getName(clazz1) + "Repository")) {
// // genClass(clazz);
// // genRepository(clazz);
// return clazz;
// }
// // genClass(clazz);
// }
// final Set<BeanDefinition> classes1 =
// provider.findCandidateComponents("llq.fw.cm.security.repository");

// // this is how you can load the class type from BeanDefinition instance
// for (BeanDefinition bean : classes1) {
// Class<?> clazz = Class.forName(bean.getBeanClassName());
// // System.out.println(clazz.getName());

// if (getName(clazz).equals(getName(clazz1) + "Repository")) {
// // genClass(clazz);
// // genRepository(clazz);
// return clazz;
// }
// // genClass(clazz);
// }
// return null;
// }

// public static boolean serviceExist(Class<?> clazz1) throws Exception {
// Set<String> set = listFilesUsingFilesList(
// "D:\\Projects\\cb\\1.SMA_IB\\1.Sourcecode\\API\\ib_sme_api\\fw-api\\fw-cms-api\\src\\main\\java\\llq\\fw\\services");
// if (set.contains(getName(clazz1) + "ServiceImpl.java")) {
// return true;
// }
// set = listFilesUsingFilesList(
// "D:\\Projects\\cb\\1.SMA_IB\\1.Sourcecode\\API\\ib_sme_api\\fw-api\\fw-cms-api\\src\\main\\java\\llq\\fw\\security\\security\\services");
// if (set.contains(getName(clazz1) + "ServiceImpl.java")) {
// return true;
// }
// return false;
// }

// public static boolean controllerExist(Class<?> clazz1) throws Exception {

// Set<String> set = listFilesUsingFilesList(
// "D:\\Projects\\cb\\1.SMA_IB\\1.Sourcecode\\API\\ib_sme_api\\fw-api\\fw-cms-api\\src\\main\\java\\llq\\fw\\controllers");
// if (set.contains(getName(clazz1) + "Controller.java")) {
// return true;
// }
// set = listFilesUsingFilesList(
// "D:\\Projects\\cb\\1.SMA_IB\\1.Sourcecode\\API\\ib_sme_api\\fw-api\\fw-cms-api\\src\\main\\java\\llq\\fw\\security\\controllers");
// if (set.contains(getName(clazz1) + "Controller.java")) {
// return true;
// }
// return false;
// }

// public static boolean requestSerarchExist(Class<?> clazz1) throws Exception {

// Set<String> set = listFilesUsingFilesList(
// "D:\\Projects\\cb\\1.SMA_IB\\1.Sourcecode\\API\\ib_sme_api\\fw-api\\fw-cms-api\\src\\main\\java\\llq\\fw\\payload\\request");
// return set.contains(getName(clazz1) + "SearchRequest.java");
// }

// public static String getName(Class<?> c) {
// return c.getName().replace(c.getPackageName() + ".", "");
// }

// public static String getNameObj(Class<?> c) {
// String INPUT = c.getName().replace(c.getPackageName() + ".", "");
// String output = INPUT.substring(0, 1).toLowerCase() + INPUT.substring(1);
// if ("package".equals(output))
// output += 1;
// return output;
// }

// public static Set<String> listFilesUsingFilesList(String dir) throws
// IOException {
// try (Stream<Path> stream = Files.list(Paths.get(dir))) {
// return stream
// .filter(file -> !Files.isDirectory(file))
// .map(Path::getFileName)
// .map(Path::toString)
// .collect(Collectors.toSet());
// }
// }
// }

package llq.fw.cm.enums.bankreceiving;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class CitadEnumQConverter implements Converter<String, CitadEnum> {
	@Override
	public CitadEnum convert(String source) {
		CitadEnum statusEnum= Arrays.stream(CitadEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
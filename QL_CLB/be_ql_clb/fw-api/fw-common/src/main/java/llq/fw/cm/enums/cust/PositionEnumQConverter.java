package llq.fw.cm.enums.cust;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class PositionEnumQConverter implements Converter<String, PositionEnum> {
	@Override
	public PositionEnum convert(String source) {
		PositionEnum statusEnum= Arrays.stream(PositionEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
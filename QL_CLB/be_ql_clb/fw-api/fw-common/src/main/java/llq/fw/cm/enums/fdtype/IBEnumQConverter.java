package llq.fw.cm.enums.fdtype;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class IBEnumQConverter implements Converter<String, IBEnum> {
	@Override
	public IBEnum convert(String source) {
		IBEnum statusEnum= Arrays.stream(IBEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
package llq.fw.cm.config;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import llq.fw.cm.models.Auditable;
import llq.fw.cm.models.Member;
import llq.fw.cm.models.User;
import llq.fw.cm.security.services.CustDetailsImpl;
import llq.fw.cm.security.services.UserDetailsImpl;

public class AuditTrailListener {
	private static Log log = LogFactory.getLog(AuditTrailListener.class);

	@PrePersist
	private void beforeAnyInsert(Object curUser) {
		if (curUser instanceof Auditable) {
			Auditable auditable = (Auditable) curUser;
			auditable.setCreatedAt(new Date());
			Authentication e = SecurityContextHolder.getContext().getAuthentication();
			if (!(e instanceof AnonymousAuthenticationToken)) {
				if (e.getPrincipal() instanceof CustDetailsImpl) {
					CustDetailsImpl userDetails = (CustDetailsImpl) e.getPrincipal();
					auditable.setCreatedByCust(Member.builder().id(userDetails.getId()).build());
					// return Cust.builder().id(userDetails.getId()).build();
				}
				if (e.getPrincipal() instanceof UserDetailsImpl) {
					UserDetailsImpl userDetails = (UserDetailsImpl) e.getPrincipal();
					auditable.setCreatedBy(User.builder().id(userDetails.getId()).build());
					// return Cust.builder().id(userDetails.getId()).build();
				}
			}
		}
	}

	@PreUpdate
	@PreRemove
	private void beforeAnyUpdate(Object curUser) {
		if (curUser instanceof Auditable) {
			Auditable auditable = (Auditable) curUser;
			auditable.setUpdatedAt(new Date());
			Authentication e = SecurityContextHolder.getContext().getAuthentication();
			if (!(e instanceof AnonymousAuthenticationToken)) {
				if (e.getPrincipal() instanceof CustDetailsImpl) {
					CustDetailsImpl userDetails = (CustDetailsImpl) e.getPrincipal();
					auditable.setUpdatedByCust(Member.builder().id(userDetails.getId()).build());
					// return Cust.builder().id(userDetails.getId()).build();
				}
				if (e.getPrincipal() instanceof UserDetailsImpl) {
					UserDetailsImpl userDetails = (UserDetailsImpl) e.getPrincipal();
					auditable.setUpdatedBy(User.builder().id(userDetails.getId()).build());
					// return Cust.builder().id(userDetails.getId()).build();
				}
			}
		}
	}

}

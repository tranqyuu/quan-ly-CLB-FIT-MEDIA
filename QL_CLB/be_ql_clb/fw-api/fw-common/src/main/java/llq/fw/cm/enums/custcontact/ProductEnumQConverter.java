package llq.fw.cm.enums.custcontact;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class ProductEnumQConverter implements Converter<String, ProductEnum> {
	@Override
	public ProductEnum convert(String source) {
		ProductEnum statusEnum= Arrays.stream(ProductEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
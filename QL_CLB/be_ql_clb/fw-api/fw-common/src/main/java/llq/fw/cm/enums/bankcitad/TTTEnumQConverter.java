package llq.fw.cm.enums.bankcitad;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class TTTEnumQConverter implements Converter<String, TTTEnum> {
	@Override
	public TTTEnum convert(String source) {
		TTTEnum statusEnum= Arrays.stream(TTTEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
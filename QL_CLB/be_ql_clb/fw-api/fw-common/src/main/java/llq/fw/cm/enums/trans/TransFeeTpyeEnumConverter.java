package llq.fw.cm.enums.trans;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TransFeeTpyeEnumConverter implements AttributeConverter<TransFeeTpyeEnum, String>{

    @Override
    public String convertToDatabaseColumn(TransFeeTpyeEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public TransFeeTpyeEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	TransFeeTpyeEnum statusEnum= Arrays.stream(TransFeeTpyeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}
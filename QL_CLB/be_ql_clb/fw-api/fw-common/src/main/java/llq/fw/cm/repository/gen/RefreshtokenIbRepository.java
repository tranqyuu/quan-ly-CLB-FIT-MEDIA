package llq.fw.cm.repository.gen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import llq.fw.cm.models.RefreshtokenIb;
public interface RefreshtokenIbRepository  extends JpaRepository<RefreshtokenIb, java.lang.Long>, JpaSpecificationExecutor<RefreshtokenIb> {
}

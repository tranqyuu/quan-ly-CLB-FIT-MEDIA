package llq.fw.cm.enums.company;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum AuthorizedEnum {
	Y(1), N(0);
//	1: Ủy quyền|0: Không ủy quyền
	private Integer value;

	AuthorizedEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static AuthorizedEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		AuthorizedEnum statusEnum= Arrays.stream(AuthorizedEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

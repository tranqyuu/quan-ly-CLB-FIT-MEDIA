package llq.fw.cm.enums.company;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ServicePackageEnumConverter implements AttributeConverter<ServicePackageEnum, String>{

    @Override
    public String convertToDatabaseColumn(ServicePackageEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public ServicePackageEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	ServicePackageEnum statusEnum= Arrays.stream(ServicePackageEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

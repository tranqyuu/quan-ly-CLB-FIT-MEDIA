package llq.fw.cm.enums.transschedules;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ScheduleEnumConverter implements AttributeConverter<ScheduleEnum, String>{

    @Override
    public String convertToDatabaseColumn(ScheduleEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public ScheduleEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	ScheduleEnum statusEnum= Arrays.stream(ScheduleEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}
package llq.fw.cm.enums.translot;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class TransLotStatusEnumQConverter implements Converter<String, TransLotStatusEnum> {
	@Override
	public TransLotStatusEnum convert(String source) {
		TransLotStatusEnum statusEnum= Arrays.stream(TransLotStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
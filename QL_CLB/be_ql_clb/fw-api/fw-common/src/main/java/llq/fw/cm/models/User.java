package llq.fw.cm.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import llq.fw.cm.enums.IBStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The persistent class for the USERS database table.
 * 
 */
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)

@Table(name = "USERS")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class User extends Auditable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "USERS_ID_GENERATOR", sequenceName = "USERS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USERS_ID_GENERATOR")
	private Long id;

	@Column(name = "BRANCH_CODE")
	private String branchCode;

	@Column(name = "DEPARTMENT_CODE")
	private String departmentCode;

	@Column(name = "DEPARTMENT_ID")
	private String departmentId;

	@Column(name = "DEPARTMENT_NAME")
	private String departmentName;

	private String email;

	private String fullname;
	@JsonIgnore
	private String password;

	@Column(name = "POSITION_CODE")
	private String positionCode;

	@Column(name = "POSITION_ID")
	private String positionId;

	@Column(name = "POSITION_NAME")
	private String positionName;

	private IBStatusEnum status;

	private String username;

	// bi-directional many-to-one association to Refreshtoken
	@OneToMany(mappedBy = "user")
	private Set<Refreshtoken> refreshtokens;

	// bi-directional many-to-one association to RolesGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ROLES_GROUP_ID")
	private RolesGroup rolesGroup;

}
package llq.fw.cm.enums.company;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;


/**
 * @author BONTV
 *
 */
public enum ServicePackageEnum {
	TRONGOI(1), VANTIN(2);
//	Gói dịch vụ 1: Trọn gói | 2: Vấn tin
	private Integer value;

	ServicePackageEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static ServicePackageEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		ServicePackageEnum statusEnum= Arrays.stream(ServicePackageEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

package llq.fw.cm.enums.company;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class ServicePackageEnumQConverter implements Converter<String, CompanyStatusEnum> {
	@Override
	public CompanyStatusEnum convert(String source) {
		CompanyStatusEnum statusEnum= Arrays.stream(CompanyStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
package llq.fw.cm.enums.translot;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TransLotStatusEnum {

	DANGXULY(0),BANGKELOI(1),BANGKEHOPLE(2);

	/*
	 * "0: Đang xử lý,1: Bảng kê lỗi, 2: Bảng kê hợp lệ
	 * 
	 */
	private Integer value;

	TransLotStatusEnum(Integer i) {
		this.value = i;
	}

	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static TransLotStatusEnum forValue(String v) {
		if (v == null) {
			return null;
		}
		TransLotStatusEnum statusEnum = Arrays.stream(TransLotStatusEnum.values()).filter(p -> {
			return p.getValue().toString().equals(v);
		}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

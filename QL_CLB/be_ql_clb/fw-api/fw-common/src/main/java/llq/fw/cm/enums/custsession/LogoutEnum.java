package llq.fw.cm.enums.custsession;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum LogoutEnum {
	DALOGOUT(1), CHUALOGOUT(0);
//	1: Đã Logout| 0 : Chưa Logout
	private Integer value;

	LogoutEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static LogoutEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		LogoutEnum statusEnum= Arrays.stream(LogoutEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

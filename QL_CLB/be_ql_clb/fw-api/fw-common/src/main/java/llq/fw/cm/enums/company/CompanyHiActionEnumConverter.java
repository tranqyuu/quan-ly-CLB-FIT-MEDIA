package llq.fw.cm.enums.company;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


@Converter(autoApply = true)
public class CompanyHiActionEnumConverter implements AttributeConverter<CompanyHiActionEnum, String> {
	@Override
    public String convertToDatabaseColumn(CompanyHiActionEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public CompanyHiActionEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	CompanyHiActionEnum statusEnum= Arrays.stream(CompanyHiActionEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

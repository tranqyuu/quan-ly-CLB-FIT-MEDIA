package llq.fw.cm.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import llq.fw.cm.enums.IBStatusEnum;
import llq.fw.cm.enums.cust.CustStatusEnum;
import llq.fw.cm.enums.cust.IndentifypapersEnum;
import llq.fw.cm.enums.cust.MemberSexEnum;
import llq.fw.cm.enums.cust.NotificationEnum;
import llq.fw.cm.enums.cust.PositionEnum;
import llq.fw.cm.enums.cust.RoleSearchEnum;
import llq.fw.cm.enums.cust.RoleTransEnum;
import llq.fw.cm.enums.cust.SmsEnum;
import llq.fw.cm.enums.cust.VerifyTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * The persistent class for the CUST database table.
 * 
 */
@Entity(name = "CHUC_VU")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ChucVu extends Auditable implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "CHUC_VU_GENERATOR", sequenceName = "CHUC_VU_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CHUC_VU_GENERATOR")
    private Long id;

    private String ma;

    private String mota;

    private String status;

    @JsonIgnore
    @OneToMany(mappedBy = "chucVu", fetch = FetchType.LAZY)
    private Set<Member> members;

}

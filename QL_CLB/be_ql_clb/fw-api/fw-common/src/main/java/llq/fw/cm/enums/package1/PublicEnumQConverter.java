package llq.fw.cm.enums.package1;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class PublicEnumQConverter implements Converter<String, PublicEnum> {
	@Override
	public PublicEnum convert(String source) {
		PublicEnum statusEnum= Arrays.stream(PublicEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
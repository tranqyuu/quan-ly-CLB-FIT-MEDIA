package llq.fw.cm.enums.custacc;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class AccTypeEnumQConverter implements Converter<String, AccTypeEnum> {
	@Override
	public AccTypeEnum convert(String source) {
		AccTypeEnum statusEnum= Arrays.stream(AccTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
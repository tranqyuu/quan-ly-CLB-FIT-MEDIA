package llq.fw.cm.enums.cust;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum PositionEnum {
	QUANLY(1), LAPLENH(2), DUYETLENH(3);
//	Vai trò 1: Quản lý|2: Lập lệnh|3: Duyệt lệnh
	private Integer value;

	PositionEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static PositionEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		PositionEnum statusEnum= Arrays.stream(PositionEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

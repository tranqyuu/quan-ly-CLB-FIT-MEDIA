package llq.fw.cm.enums.packagefee;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum IntFeeMethodEnum {
	THANG(0), QUY(1), NAM(2);
/*
 * Định kỳ thu phí: 0: Tháng|1:Quý|2:Năm
 * 
 */
	private Integer value;

	IntFeeMethodEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static IntFeeMethodEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		IntFeeMethodEnum statusEnum= Arrays.stream(IntFeeMethodEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
}

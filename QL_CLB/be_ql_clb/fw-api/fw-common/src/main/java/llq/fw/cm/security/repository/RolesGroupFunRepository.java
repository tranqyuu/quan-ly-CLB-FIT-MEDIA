package llq.fw.cm.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import llq.fw.cm.models.RolesGroupFun;

public interface RolesGroupFunRepository extends JpaRepository<RolesGroupFun, Long>, JpaSpecificationExecutor<RolesGroupFun> {

}

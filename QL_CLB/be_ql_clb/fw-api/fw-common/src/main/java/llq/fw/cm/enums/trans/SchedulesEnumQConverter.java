package llq.fw.cm.enums.trans;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class SchedulesEnumQConverter implements Converter<String, SchedulesEnum> {
	@Override
	public SchedulesEnum convert(String source) {
		SchedulesEnum statusEnum= Arrays.stream(SchedulesEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
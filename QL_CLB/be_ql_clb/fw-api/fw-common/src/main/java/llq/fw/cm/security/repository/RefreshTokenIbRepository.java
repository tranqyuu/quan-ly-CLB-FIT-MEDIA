package llq.fw.cm.security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import llq.fw.cm.models.Member;
import llq.fw.cm.models.RefreshtokenIb;

@Repository
public interface RefreshTokenIbRepository extends JpaRepository<RefreshtokenIb, Long> {
  Optional<RefreshtokenIb> findByToken(String token);

  @Modifying
  int deleteByCust(Member cust);
}

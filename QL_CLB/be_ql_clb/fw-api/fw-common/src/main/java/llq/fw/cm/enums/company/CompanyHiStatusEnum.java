package llq.fw.cm.enums.company;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum CompanyHiStatusEnum {
	/*
	 * O. Từ chối
	 * 1. Phê duyệt
	 */
	TUCHOI(0), PHEDUYET(1);
	
	private Integer value;

	CompanyHiStatusEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static CompanyHiStatusEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		CompanyHiStatusEnum statusEnum= Arrays.stream(CompanyHiStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

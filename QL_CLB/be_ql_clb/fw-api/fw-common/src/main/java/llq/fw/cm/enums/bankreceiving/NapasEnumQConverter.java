package llq.fw.cm.enums.bankreceiving;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class NapasEnumQConverter implements Converter<String, NapasEnum> {
	@Override
	public NapasEnum convert(String source) {
		NapasEnum statusEnum= Arrays.stream(NapasEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
package llq.fw.cm.enums.trans;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SchedulesEnum {
	TUONGLAI(0), DINHKY(1);

	/*
	 * 0: Tương lại|1: Định kỳ
	 * 
	 */
	private Integer value;

	SchedulesEnum(Integer i) {
		this.value = i;
	}

	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static SchedulesEnum forValue(String v) {
		if (v == null) {
			return null;
		}
		SchedulesEnum statusEnum = Arrays.stream(SchedulesEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

package llq.fw.cm.enums.transfee;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class FeeTypeEnumQConverter implements Converter<String, FeeTypeEnum> {
	@Override
	public FeeTypeEnum convert(String source) {
		FeeTypeEnum statusEnum= Arrays.stream(FeeTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
package llq.fw.cm.enums.cust;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class CustStatusEnumConverter implements AttributeConverter<CustStatusEnum, String>{

    @Override
    public String convertToDatabaseColumn(CustStatusEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public CustStatusEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	CustStatusEnum statusEnum= Arrays.stream(CustStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

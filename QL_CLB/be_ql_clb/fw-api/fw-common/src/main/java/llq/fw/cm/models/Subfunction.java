package llq.fw.cm.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import llq.fw.cm.enums.IBStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The persistent class for the SUBFUNCTION database table.
 * 
 */
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)

public class Subfunction extends Auditable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String code;

	private String name;

	private IBStatusEnum status;

	// bi-directional many-to-one association to RolesGroupSub
	@OneToMany(mappedBy = "subfunction")
	private Set<RolesGroupSub> rolesGroupSubs;

	//bi-directional many-to-one association to Function
    @ManyToOne
    @JsonIgnore
	private Function function;

}
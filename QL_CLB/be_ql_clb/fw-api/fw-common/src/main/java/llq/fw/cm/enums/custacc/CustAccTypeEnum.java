package llq.fw.cm.enums.custacc;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum CustAccTypeEnum {
	GIAODICH(1), TRUYVAN(2);
//	1: Giao dịch| 2 : Truy vấn
	private Integer value;

	CustAccTypeEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static CustAccTypeEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		CustAccTypeEnum statusEnum= Arrays.stream(CustAccTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

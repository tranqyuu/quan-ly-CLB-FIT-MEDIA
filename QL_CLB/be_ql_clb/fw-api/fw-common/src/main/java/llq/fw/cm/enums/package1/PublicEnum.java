package llq.fw.cm.enums.package1;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum PublicEnum {
	PUBLIC(1), PRIVATE(2);
/*
 * 1: Public | 2: Private
 * 
 */
	private Integer value;

	PublicEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static PublicEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		PublicEnum statusEnum= Arrays.stream(PublicEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

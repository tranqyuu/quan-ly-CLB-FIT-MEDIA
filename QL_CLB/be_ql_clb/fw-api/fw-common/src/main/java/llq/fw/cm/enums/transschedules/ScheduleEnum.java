package llq.fw.cm.enums.transschedules;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ScheduleEnum {

	TUONGLAI(0),DINHKY(1);

	/*
	 *"1: định kỳ, 0: tương lai
	 * 
	 */
	private Integer value;

	ScheduleEnum(Integer i) {
		this.value = i;
	}

	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static ScheduleEnum forValue(String v) {
		if (v == null) {
			return null;
		}
		ScheduleEnum statusEnum = Arrays.stream(ScheduleEnum.values()).filter(p -> {
			return p.getValue().toString().equals(v);
		}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

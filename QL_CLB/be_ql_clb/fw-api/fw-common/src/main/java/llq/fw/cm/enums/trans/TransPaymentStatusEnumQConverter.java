package llq.fw.cm.enums.trans;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class TransPaymentStatusEnumQConverter implements Converter<String, TransPaymentStatusEnum> {
	@Override
	public TransPaymentStatusEnum convert(String source) {
		TransPaymentStatusEnum statusEnum= Arrays.stream(TransPaymentStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
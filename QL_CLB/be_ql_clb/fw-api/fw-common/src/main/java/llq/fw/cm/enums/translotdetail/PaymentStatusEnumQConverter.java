package llq.fw.cm.enums.translotdetail;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class PaymentStatusEnumQConverter implements Converter<String, PaymentStatusEnum> {
	@Override
	public PaymentStatusEnum convert(String source) {
		PaymentStatusEnum statusEnum= Arrays.stream(PaymentStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
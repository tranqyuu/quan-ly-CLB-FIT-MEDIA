package llq.fw.cm.enums.translot;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StatusTransLotEnumConverter implements AttributeConverter<StatusTransLotEnum, String>{

    @Override
    public String convertToDatabaseColumn(StatusTransLotEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public StatusTransLotEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	StatusTransLotEnum statusEnum= Arrays.stream(StatusTransLotEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

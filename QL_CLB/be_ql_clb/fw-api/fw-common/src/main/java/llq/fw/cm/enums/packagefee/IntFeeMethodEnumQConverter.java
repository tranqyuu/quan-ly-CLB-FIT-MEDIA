package llq.fw.cm.enums.packagefee;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class IntFeeMethodEnumQConverter implements Converter<String, IntFeeMethodEnum> {
	@Override
	public IntFeeMethodEnum convert(String source) {
		IntFeeMethodEnum statusEnum= Arrays.stream(IntFeeMethodEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
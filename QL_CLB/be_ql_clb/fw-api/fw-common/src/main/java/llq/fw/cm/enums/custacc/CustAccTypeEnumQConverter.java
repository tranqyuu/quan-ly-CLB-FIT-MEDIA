package llq.fw.cm.enums.custacc;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class CustAccTypeEnumQConverter implements Converter<String, CustAccTypeEnum> {
	@Override
	public CustAccTypeEnum convert(String source) {
		CustAccTypeEnum statusEnum= Arrays.stream(CustAccTypeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
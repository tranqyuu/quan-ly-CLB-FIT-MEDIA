package llq.fw.cm.enums.cust;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SmsEnum {
	Y(1), N(0);
//	Nhận SMS 1: Nhận SMS| 2: Không nhận
	private Integer value;

	SmsEnum(Integer i) {
		this.value = i;
	}
	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static SmsEnum forValue(String v) {
		if(v==null) {
			return null;
		}
		SmsEnum statusEnum= Arrays.stream(SmsEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(v);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

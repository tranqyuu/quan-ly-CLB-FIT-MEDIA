package llq.fw.cm.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

import llq.fw.cm.common.Gen;
import llq.fw.cm.enums.IBStatusEnum;
import llq.fw.cm.enums.rolesgroup.TypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the ROLES_GROUP database table.
 * 
 */
@Entity
@Builder(toBuilder=true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Gen(name = "")
@Table(name="ROLES_GROUP")
public class RolesGroup extends Auditable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ROLES_GROUP_ID_GENERATOR", sequenceName="ROLES_GROUP_SEQ",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ROLES_GROUP_ID_GENERATOR")
	private Long id;

	@Column(name="DEPARTMENT_CODE")
	private String departmentCode;

	@Column(name="DEPARTMENT_ID")
	private String departmentId;

	@Column(name="DEPARTMENT_NAME")
	private String departmentName;

	private String description;

	private String name;

	@Column(name="POSITION_CODE")
	private String positionCode;

	@Column(name="POSITION_ID")
	private String positionId;

	@Column(name="POSITION_NAME")
	private String positionName;

	private IBStatusEnum status;

	@Column(name="TYPE")
	private TypeEnum type;


	//bi-directional many-to-one association to RolesGroupFun
	@OneToMany(mappedBy="rolesGroup", cascade = CascadeType.ALL,orphanRemoval = true)
	private Set<RolesGroupFun> rolesGroupFuns;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="rolesGroup")
	private Set<User> users;

	@Override
	public String toString() {
		return "RolesGroup [id=" + id + ", rolesGroupFuns=" + rolesGroupFuns + "]";
	}

 	
}
package llq.fw.cm.enums.trans;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class TransFeeTpyeEnumQConverter implements Converter<String, TransFeeTpyeEnum> {
	@Override
	public TransFeeTpyeEnum convert(String source) {
		TransFeeTpyeEnum statusEnum= Arrays.stream(TransFeeTpyeEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
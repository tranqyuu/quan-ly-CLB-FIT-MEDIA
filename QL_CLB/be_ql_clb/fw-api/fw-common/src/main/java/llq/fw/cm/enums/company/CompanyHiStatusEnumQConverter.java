package llq.fw.cm.enums.company;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class CompanyHiStatusEnumQConverter implements Converter<String, CompanyHiStatusEnum> {
	@Override
	public CompanyHiStatusEnum convert(String source) {
		CompanyHiStatusEnum statusEnum= Arrays.stream(CompanyHiStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
package llq.fw.cm.models;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * The persistent class for the ROLES_GROUP_SUB database table.
 * 
 */
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Table(name="ROLES_GROUP_SUB")
public class RolesGroupSub implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ROLES_GROUP_SUB_ID_GENERATOR", sequenceName="ROLES_GROUP_SUB_SEQ",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ROLES_GROUP_SUB_ID_GENERATOR")
	private Long id;

	//bi-directional many-to-one association to RolesGroupFun
    @ManyToOne
	@JoinColumn(name="ROLES_GROUP_FUN_ID")
    @JsonIgnore
	private RolesGroupFun rolesGroupFun;

	//bi-directional many-to-one association to Subfunction
    @ManyToOne
	private Subfunction subfunction;
    
    @Transient
    @JsonProperty("deleted")
	private boolean deleted;

	@Override
	public String toString() {
		return "RolesGroupSub [id=" + id + ", subfunction=" + subfunction + ", deleted=" + deleted + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RolesGroupSub other = (RolesGroupSub) obj;
		return Objects.equals(id, other.id);
	}

}
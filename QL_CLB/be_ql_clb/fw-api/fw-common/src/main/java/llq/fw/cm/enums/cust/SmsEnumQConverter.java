package llq.fw.cm.enums.cust;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class SmsEnumQConverter implements Converter<String, SmsEnum> {
	@Override
	public SmsEnum convert(String source) {
		SmsEnum statusEnum= Arrays.stream(SmsEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
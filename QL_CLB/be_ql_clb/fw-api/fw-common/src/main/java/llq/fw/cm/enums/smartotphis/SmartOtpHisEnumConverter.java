package llq.fw.cm.enums.smartotphis;

import java.util.Arrays;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class SmartOtpHisEnumConverter implements AttributeConverter<SmartOtpHisEnum, String>{

    @Override
    public String convertToDatabaseColumn(SmartOtpHisEnum attribute) {
    	if (attribute == null) {
            return null;
        }
        return attribute.getValue().toString();
    }
    
    @Override
    public SmartOtpHisEnum convertToEntityAttribute(String dbData) {
    	if(dbData==null) {
    		return null;
    	}
    	SmartOtpHisEnum statusEnum= Arrays.stream(SmartOtpHisEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(dbData);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
    }
}

package llq.fw.cm.enums.smartotphis;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class ActionEnumQConverter implements Converter<String, ActionEnum> {
	@Override
	public ActionEnum convert(String source) {
		ActionEnum statusEnum= Arrays.stream(ActionEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
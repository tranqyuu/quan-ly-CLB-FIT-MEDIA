package llq.fw.cm.enums.transschedules;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class TransSchedulesStatusEnumQConverter implements Converter<String, TransSchedulesStatusEnum> {
	@Override
	public TransSchedulesStatusEnum convert(String source) {
		TransSchedulesStatusEnum statusEnum= Arrays.stream(TransSchedulesStatusEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
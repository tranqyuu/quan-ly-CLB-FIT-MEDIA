package llq.fw.cm.enums.cust;

import java.util.Arrays;

import org.springframework.core.convert.converter.Converter;

public class NotificationQConverter implements Converter<String, NotificationEnum> {
	@Override
	public NotificationEnum convert(String source) {
		NotificationEnum statusEnum= Arrays.stream(NotificationEnum.values()).filter(
				p -> {
					return p.getValue().toString().equals(source);
				}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}
	
	
}
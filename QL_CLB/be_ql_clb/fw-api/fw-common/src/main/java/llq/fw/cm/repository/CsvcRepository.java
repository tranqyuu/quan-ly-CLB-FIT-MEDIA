package llq.fw.cm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import llq.fw.cm.models.Csvc;

public interface CsvcRepository extends JpaRepository<Csvc, java.lang.Long>, JpaSpecificationExecutor<Csvc> {

}

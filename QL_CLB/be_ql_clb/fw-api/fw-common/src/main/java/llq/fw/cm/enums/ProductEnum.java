package llq.fw.cm.enums;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ProductEnum {

	CK247(1), CKLIENNH(2), CKNOIBO(3),CKTHEODANHSACH(4),TTHOADONDIEN(5),
	TTHOADONNUOC(6),TTDIENTHOAICODINH(7),TTDIDONGTRASAU(8),TTINTERNETADSL(9),GUITKONLINE(10);

	/*
	 * 1: Chuyển khoản 24/7 đến số tài khoản|2: Chuyển khoản liên ngân hàng|3: Chuyển khoản nội bộ CB|
	 * 4: Chuyển khoản theo danh sách|5: Thanh toán hóa đơn điện|6: Thanh toán hóa đơn nước|
	 * 7: Thanh toán cước điện thoại cố định|8: Thanh toán cước di động trả sau|
	 * 9: Thanh toán cước Internet ADSL|10: Gửi tiết kiệm online
	 * 
	 */
	private Integer value;

	ProductEnum(Integer i) {
		this.value = i;
	}

	@JsonValue
	public Integer getValue() {
		return value;
	}

	@JsonCreator
	public static ProductEnum forValue(String v) {
		if (v == null) {
			return null;
		}
		ProductEnum statusEnum = Arrays.stream(ProductEnum.values()).filter(p -> {
			return p.getValue().toString().equals(v);
		}).findFirst().orElseThrow(IllegalArgumentException::new);
		return statusEnum;
	}

}

package llq.fw.cm.enums.cust;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.Arrays;

public enum MemberSexEnum {
    NAM(0), NU(1);

    /*
     * 0: Chờ kích hoạt|1: Hoạt động|2: Khóa|3: Hủy
     * 
     */
    private Integer value;

    MemberSexEnum(Integer i) {
        this.value = i;
    }

    @JsonValue
    public Integer getValue() {
        return value;
    }

    @JsonCreator
    public static CustStatusEnum forValue(String v) {
        if (v == null) {
            return null;
        }
        CustStatusEnum statusEnum = Arrays.stream(CustStatusEnum.values()).filter(
                p -> {
                    return p.getValue().toString().equals(v);
                }).findFirst().orElseThrow(IllegalArgumentException::new);
        return statusEnum;
    }

}

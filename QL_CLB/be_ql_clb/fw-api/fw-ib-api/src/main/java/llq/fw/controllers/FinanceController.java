package llq.fw.controllers;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import llq.fw.cm.common.createFileExport;
import llq.fw.cm.enums.cust.PositionEnum;
import llq.fw.cm.models.Member;
import llq.fw.cm.models.TaiChinh;
import llq.fw.cm.payload.response.BaseResponse;
import llq.fw.payload.request.TaiChinhRequest;
import llq.fw.payload.request.TranSearchRequest;
import llq.fw.payload.request.TransRequest;
import llq.fw.services.FinanceServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/taichinh")
public class FinanceController {
	@Autowired
	FinanceServiceImpl transScheduleServiceImpl;

	@GetMapping // Tim kiem theo nhieu dieu kien
	public ResponseEntity<BaseResponse> searchAll(TaiChinhRequest tranRequest, Pageable pageable) {
		BaseResponse baseResponse = transScheduleServiceImpl.search(tranRequest, pageable);
		return ResponseEntity.ok(baseResponse);
	}

	@PostMapping // Tao moi
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<BaseResponse> create(@RequestBody TaiChinh transScheduleRequest) {
		BaseResponse baseResponse = transScheduleServiceImpl.create(transScheduleRequest);
		return ResponseEntity.ok(baseResponse);
	}

	@PutMapping // Cap nhat
	public ResponseEntity<BaseResponse> update(@RequestBody TaiChinh transScheduleRequest) {
		BaseResponse baseResponse = transScheduleServiceImpl.update(transScheduleRequest);
		return ResponseEntity.ok(baseResponse);
	}

	@DeleteMapping("{id}") // Xoa
	public ResponseEntity<BaseResponse> delete(@PathVariable java.lang.String id) {
		BaseResponse baseResponse = transScheduleServiceImpl.delete(id);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("{id}") // Xoa
	public ResponseEntity<BaseResponse> getDetailById(@PathVariable java.lang.String id) {
		BaseResponse baseResponse = transScheduleServiceImpl.getDetail(id);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("keyexist/{id}") // check exist
	public ResponseEntity<BaseResponse> checkExist(@PathVariable java.lang.String id) {
		BaseResponse baseResponse = transScheduleServiceImpl.checkExist(id);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("gettaichinh") // check exist
	public ResponseEntity<BaseResponse> getTaiChinh() {
		BaseResponse baseResponse = transScheduleServiceImpl.countTaiChinh();
		return ResponseEntity.ok(baseResponse);
	}

	@PostMapping(value = "/export")
    public void exportBangKe(@RequestBody TaiChinhRequest transLotSearch, HttpServletResponse response,Pageable pageable) 
    		throws Exception {
				DecimalFormat df=new DecimalFormat("#,###");
        Map<String, Object> map;
            try (InputStream isFileSample = new ClassPathResource("template/danhsachtaichinh.xlsx")
                    .getInputStream();) {
            	
            	//List<TransLot> lst = transLotServiceImpl.findAllTransLot();
            	BaseResponse baseRessponse = transScheduleServiceImpl.search(transLotSearch, pageable);
            	Page<TaiChinh> page =  (Page<TaiChinh>) baseRessponse.getData();
            	List<TaiChinh> lst1 = page.getContent();
                // Doc du lieu mau
					lst1.stream().forEach(item -> {
						if(item.getType().equals("1")){
							item.setType("Khoản chi");
						}else{
							item.setType("Khoản thu");
						}

						item.setQuantity(df.format(Long.valueOf(item.getQuantity())));
					});
                // Gan du lieu cho File
                map = new HashMap<String, Object>();
                map.put("RESULT",  lst1);
                map.put("NGAY_GIO_IN", new Date());

                // set response
                response.setHeader("listbangke",".xlsx");
                response.setHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "listbangke");
                createFileExport.createFileToOutputstream(response.getOutputStream(), isFileSample, map);
            } catch (Exception  e) {
                throw new Exception( e.getMessage());
            } 
        }
}

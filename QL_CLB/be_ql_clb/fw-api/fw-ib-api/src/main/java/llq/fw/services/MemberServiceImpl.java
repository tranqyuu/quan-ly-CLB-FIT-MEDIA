package llq.fw.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import llq.fw.cm.common.Constants;
import llq.fw.cm.common.Constants.FwError;
import llq.fw.cm.enums.cust.CustStatusEnum;
import llq.fw.cm.enums.cust.MemberSexEnum;
import llq.fw.cm.enums.cust.NotificationEnum;
import llq.fw.cm.enums.cust.PositionEnum;
import llq.fw.cm.enums.cust.SmsEnum;
import llq.fw.cm.models.Member;

import llq.fw.cm.payload.response.BaseResponse;

import llq.fw.cm.security.repository.MemberRepository;

import llq.fw.cm.security.services.CustDetailsImpl;
import llq.fw.cm.services.BaseService;
import llq.fw.payload.request.gen.MemberRequest;

@Component
public class MemberServiceImpl implements BaseService<BaseResponse, Member, MemberRequest, java.lang.Long> {
	private static final Logger logger = LoggerFactory.getLogger(MemberServiceImpl.class);
	private final SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
	@Autowired
	MemberRepository custRepository;

	@Override
	public BaseResponse create(Member cust) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			if (cust.getId() != null) {
				var custCur = custRepository.findById(cust.getId()).orElse(null);
				if (custCur != null) {
					baseResponse.setFwError(FwError.DLDATONTAI);
					return baseResponse;
				}
			}

			var custNew = Member.builder()
					.birthday(cust.getBirthday())
					.email(cust.getEmail())
					.code(cust.getEmail())
					.fullname(cust.getFullname())
					.ban(cust.getBan())
					.status(cust.getStatus())
					.tel(cust.getTel())
					.sex(cust.getSex())
					.point("0")
					.location(cust.getLocation())
					.classes(cust.getClasses())
					.schoolYear(cust.getSchoolYear())
					.password(cust.getPassword())
					.status(cust.getStatus())
					.chucVu(cust.getChucVu())
					.password("$2a$10$XFzfPLFpwLbERYOZRwox7Op8mZAvkiAY/5cA1IDgGaxLioo8g.zs2")
					.build();
			jpaExecuteTransLimitCust(custNew);
			baseResponse.setData(custNew);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse update(Member cust) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			var custCur = custRepository.findById(cust.getId()).orElse(null);
			if (custCur == null) {
				baseResponse.setFwError(FwError.DLKHONGTONTAI);
				return baseResponse;
			}
			// NamDD: set default value
			custCur.setBirthday(cust.getBirthday());
			custCur.setCif(cust.getCif());
			custCur.setCode(cust.getCode());
			custCur.setEmail(cust.getEmail());
			custCur.setFullname(cust.getFullname());
			custCur.setIdno(cust.getIdno());
			custCur.setIndentitypapers(cust.getIndentitypapers());
			custCur.setNmemonicName(cust.getNmemonicName());
			custCur.setNotification(cust.getNotification());
			custCur.setPosition(cust.getPosition());
			custCur.setSms(cust.getSms());
			custCur.setStatus(cust.getStatus());
			custCur.setTel(cust.getTel());
			custCur.setPassword(cust.getPassword());
			custCur.setVerifyType(cust.getVerifyType());
			custCur.setRolesearch(cust.getRolesearch());
			custCur.setRoletrans(cust.getRoletrans());
			custCur.setSAllDd(cust.getSAllDd());
			custCur.setSAllFd(cust.getSAllFd());
			custCur.setSAllLn(cust.getSAllLn());
			custCur.setTAllDd(cust.getTAllDd());
			custCur.setBirthday(cust.getBirthday());
			custCur.setSex(cust.getSex());
			custCur.setLocation(cust.getLocation());
			custCur.setSchoolYear(cust.getSchoolYear());
			custCur.setClasses(cust.getClasses());
			custCur.setBan(cust.getBan());
			custCur.setChucVu(cust.getChucVu());
			jpaExecute(custCur);
			/*
			 * Luu obj lich su neu can
			 */
			baseResponse.setData(custCur);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse search(MemberRequest cust, Pageable pageable) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG); // TODO : Lay tham so xu ly song ngu
		try {
			Specification<Member> specification = new Specification<Member>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<Member> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					if (!ObjectUtils.isEmpty(cust.getBirthday())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(
								criteriaBuilder.function("TRUNC", Date.class, root.get("birthday")),
								criteriaBuilder.function("TO_DATE", Date.class,
										criteriaBuilder.literal(formatDate.format(cust.getBirthday())),
										criteriaBuilder.literal("DD-MM-YYYY")))));
					}

					if (!ObjectUtils.isEmpty(cust.getSchoolYear())) {
						predicates.add(
								criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.upper(root.get("schoolYear")),
										"%" + cust.getSchoolYear().toUpperCase() + "%")));
					}
					if (!ObjectUtils.isEmpty(cust.getCode())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.upper(root.get("code")),
								"%" + cust.getCode().toUpperCase() + "%")));
					}

					if (!ObjectUtils.isEmpty(cust.getFullname())) {
						predicates.add(
								criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.upper(root.get("fullname")),
										"%" + cust.getFullname().toUpperCase() + "%")));
					}

					if (!ObjectUtils.isEmpty(cust.getPosition())) {
						predicates.add(
								criteriaBuilder.and(criteriaBuilder.equal(criteriaBuilder.upper(root.get("position")),
										cust.getPosition())));
					}

					if (!ObjectUtils.isEmpty(cust.getLocation())) {
						predicates.add(
								criteriaBuilder.and(criteriaBuilder.equal(criteriaBuilder.upper(root.get("location")),
										cust.getLocation())));
					}

					if (!ObjectUtils.isEmpty(cust.getStatus())) {
						predicates.add(
								criteriaBuilder.and(criteriaBuilder.equal(root.get("status"),
										cust.getStatus())));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Page<Member> page = custRepository.findAll(specification, pageable);
			baseResponse.setData(page);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	public BaseResponse searchCust(MemberRequest cust, Pageable pageable) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG); // TODO : Lay tham so xu ly song ngu
		CustDetailsImpl userDetail = (CustDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Member currentCust = custRepository.findById(userDetail.getId()).orElse(null);
		try {
			Specification<Member> specification = new Specification<Member>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<Member> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					predicates.add(
							criteriaBuilder.and(criteriaBuilder.notEqual(criteriaBuilder.upper(root.get("position")),
									PositionEnum.QUANLY)));

					if (!ObjectUtils.isEmpty(cust.getBirthday())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(
								criteriaBuilder.function("TRUNC", Date.class, root.get("birthday")),
								criteriaBuilder.function("TO_DATE", Date.class,
										criteriaBuilder.literal(formatDate.format(cust.getBirthday())),
										criteriaBuilder.literal("DD-MM-YYYY")))));
					}
					if (!ObjectUtils.isEmpty(cust.getCif())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.upper(root.get("cif")),
								"%" + cust.getCif().toUpperCase() + "%")));
					}
					if (!ObjectUtils.isEmpty(cust.getCode())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.upper(root.get("code")),
								"%" + cust.getCode().toUpperCase() + "%")));
					}
					if (!ObjectUtils.isEmpty(cust.getEmail())) {
						predicates
								.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.upper(root.get("email")),
										"%" + cust.getEmail().toUpperCase() + "%")));
					}
					if (!ObjectUtils.isEmpty(cust.getFullname())) {
						predicates.add(
								criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.upper(root.get("fullname")),
										"%" + cust.getFullname().toUpperCase() + "%")));
					}
					if (!ObjectUtils.isEmpty(cust.getIdno())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.upper(root.get("idno")),
								"%" + cust.getIdno().toUpperCase() + "%")));
					}
					if (!ObjectUtils.isEmpty(cust.getIndentitypapers())) {
						predicates.add(criteriaBuilder
								.and(criteriaBuilder.equal(criteriaBuilder.upper(root.get("indentitypapers")),
										cust.getIndentitypapers())));
					}
					if (!ObjectUtils.isEmpty(cust.getNmemonicName())) {
						predicates.add(criteriaBuilder
								.and(criteriaBuilder.like(criteriaBuilder.upper(root.get("nmemonicName")),
										"%" + cust.getNmemonicName().toUpperCase() + "%")));
					}
					if (!ObjectUtils.isEmpty(cust.getNotification())) {
						predicates.add(criteriaBuilder
								.and(criteriaBuilder.equal(criteriaBuilder.upper(root.get("notification")),
										cust.getNotification())));
					}

					if (!ObjectUtils.isEmpty(cust.getSms())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(criteriaBuilder.upper(root.get("sms")),
								cust.getSms())));
					}
					if (!ObjectUtils.isEmpty(cust.getStatus())) {
						predicates.add(
								criteriaBuilder.and(criteriaBuilder.equal(criteriaBuilder.upper(root.get("status")),
										cust.getStatus())));
					}
					if (!ObjectUtils.isEmpty(cust.getTel())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.upper(root.get("tel")),
								"%" + cust.getTel().toUpperCase() + "%")));
					}
					if (!ObjectUtils.isEmpty(cust.getVerifyType())) {
						predicates.add(
								criteriaBuilder.and(criteriaBuilder.equal(criteriaBuilder.upper(root.get("verifyType")),
										cust.getVerifyType())));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Page<Member> page = custRepository.findAll(specification, pageable);
			baseResponse.setData(page);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	public BaseResponse getAllMember() {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG); // TODO : Lay tham so xu ly song ngu
		CustDetailsImpl userDetail = (CustDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		try {
			Specification<Member> specification = new Specification<Member>() {

				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<Member> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					predicates.add(
							criteriaBuilder.and(criteriaBuilder.equal(root.get("status"),
									CustStatusEnum.HOATDONG)));
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			List<Member> page = custRepository.findAll(specification, Sort.by(Sort.Direction.DESC, "point"));
			baseResponse.setData(page);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	public BaseResponse getAllMemberRank() {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG); // TODO : Lay tham so xu ly song ngu
		CustDetailsImpl userDetail = (CustDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		Pageable pageable = PageRequest.of(0, 5, Sort.by(Sort.Direction.DESC, "point"));
		try {
			Specification<Member> specification = new Specification<Member>() {

				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<Member> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					predicates.add(
							criteriaBuilder.and(criteriaBuilder.equal(root.get("status"),
									CustStatusEnum.HOATDONG)));
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Page<Member> pageMember = custRepository.findAll(specification, pageable);
			baseResponse.setData(pageMember.getContent());
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse delete(java.lang.Long id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			var custCur = custRepository.findById(id).orElse(null);
			if (custCur == null) {
				baseResponse.setFwError(FwError.DLKHONGTONTAI);
				return baseResponse;
			}
			jpaDeleteExecute(id);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void jpaExecute(Member cust) {
		custRepository.save(cust);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Member jpaExecuteTransLimitCust(Member cust) {
		custRepository.save(cust);

		return cust;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void jpaDeleteExecute(java.lang.Long id) {
		custRepository.deleteById(id);
	}

	@Override
	public BaseResponse getDetail(java.lang.Long id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		var custCur = custRepository.findById(id).orElse(null);
		if (custCur == null) {
			baseResponse.setFwError(FwError.DLKHONGTONTAI);
			return baseResponse;
		}
		baseResponse.setData(custCur);
		return baseResponse;
	}

	@Override
	public BaseResponse checkExist(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public BaseResponse checkTel(String value, Long id) {
		// TODO Auto-generated method stub
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		Long count;
		if (id != null) {
			count = custRepository.countCustDupTel(value, id);
		} else {
			count = custRepository.countCustDupTel(value);
		}
		if (count > 0)
			baseResponse.setData(true);
		else
			baseResponse.setData(false);
		return baseResponse;
	}

	public BaseResponse checkEmail(String value, Long id) {
		// TODO Auto-generated method stub
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		Long count;
		if (id != null) {
			count = custRepository.countCustDupEmail(value, id);
		} else {
			count = custRepository.countCustDupEmail(value);
		}
		if (count > 0)
			baseResponse.setData(true);
		else
			baseResponse.setData(false);
		return baseResponse;
	}

	public BaseResponse checkIdno(String value, Long id) {
		// TODO Auto-generated method stub
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		Long count;
		if (id != null) {
			count = custRepository.countCustDupIdno(value, id);
		} else {
			count = custRepository.countCustDupIdno(value);
		}
		if (count > 0)
			baseResponse.setData(true);
		else
			baseResponse.setData(false);
		return baseResponse;
	}

	// private static Random generator = new Random();
	// private static final String alpha = "abcdefghijklmnopqrstuvwxyz"; // a-z
	// private static final String alphaUpperCase = alpha.toUpperCase(); // A-Z
	// private static final String digits = "0123456789"; // 0-9
	// private static final String specials = "~=+%^*/()[]{}/!@#$?|";//ky tu dac
	// biet
	// private static final String ALL = alpha + alphaUpperCase + digits + specials;
	//
	// public String randomPassword(int numberOfCharactor) {
	// List<String> result = new ArrayList<>();
	// Consumer<String> appendChar = s -> {
	// int number = randomNumber(0, s.length() - 1);
	// result.add("" + s.charAt(number));
	// };
	// appendChar.accept(digits);
	// appendChar.accept(specials);
	// while (result.size() < numberOfCharactor) {
	// appendChar.accept(ALL);
	// }
	// Collections.shuffle(result, generator);
	// return String.join("", result);
	// }
	// public static int randomNumber(int min, int max) {
	// return generator.nextInt((max - min) + 1) + min;
	// }
	// public static void main(String[] args) {
	// CustServiceImpl rand = new CustServiceImpl();
	// String a = rand.randomPassword(8);
	// System.out.println(a);
	// }
}

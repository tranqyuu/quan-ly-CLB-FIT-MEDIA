package llq.fw.payload.request;

import java.util.Date;

import lombok.Data;

@Data
public class TaiChinhRequest {
    private Date tuNgay;
    private Date denNgay;
}

package llq.fw.payload.request.gen;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActivitiesSearchRequest {
	private Long id;
	private java.lang.String ma;
	private java.lang.String type;
	private java.lang.String status;
	private java.lang.String fax;
	private java.lang.String hotline;
	private java.lang.String tel;
	private Long membersId;
	private Date tuNgay;
	private Date denNgay;
}

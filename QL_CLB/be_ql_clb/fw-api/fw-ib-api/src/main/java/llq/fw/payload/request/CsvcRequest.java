package llq.fw.payload.request;

import lombok.Data;

@Data
public class CsvcRequest {
    private Long id;

    private String ma;

    private String mota;

    private Long quantity;

    private String status;
}

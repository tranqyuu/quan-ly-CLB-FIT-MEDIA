package llq.fw.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import llq.fw.cm.models.Member;
import llq.fw.cm.payload.response.BaseResponse;

import llq.fw.payload.request.gen.MemberRequest;

import llq.fw.services.MemberServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/member")
public class MemberController {
	@Autowired
	MemberServiceImpl custServiceImpl;

	@GetMapping // Tim kiem theo nhieu dieu kien
	public ResponseEntity<BaseResponse> searchAll(MemberRequest custRequest, Pageable pageable) {
		BaseResponse baseResponse = custServiceImpl.search(custRequest, pageable);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("/search") // Tim kiem theo nhieu dieu kien
	public ResponseEntity<BaseResponse> searchCust(MemberRequest custRequest, Pageable pageable) {
		BaseResponse baseResponse = custServiceImpl.searchCust(custRequest, pageable);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("getallmembers") // Tim kiem theo nhieu dieu kien
	public ResponseEntity<BaseResponse> getAllMembers() {
		BaseResponse baseResponse = custServiceImpl.getAllMember();
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("getallmembersrank") // Tim kiem theo nhieu dieu kien
	public ResponseEntity<BaseResponse> getAllMembersRank() {
		BaseResponse baseResponse = custServiceImpl.getAllMemberRank();
		return ResponseEntity.ok(baseResponse);
	}

	@PostMapping // Tao moi
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<BaseResponse> create(@RequestBody Member custRequest) {
		BaseResponse baseResponse = custServiceImpl.create(custRequest);
		return ResponseEntity.ok(baseResponse);
	}

	@PutMapping // Cap nhat
	public ResponseEntity<BaseResponse> update(@RequestBody Member custRequest) {
		BaseResponse baseResponse = custServiceImpl.update(custRequest);
		return ResponseEntity.ok(baseResponse);
	}

	@DeleteMapping("{id}") // Xoa
	public ResponseEntity<BaseResponse> delete(@PathVariable java.lang.Long id) {
		BaseResponse baseResponse = custServiceImpl.delete(id);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("{id}") // Xoa
	public ResponseEntity<BaseResponse> getDetailById(@PathVariable java.lang.Long id) {
		BaseResponse baseResponse = custServiceImpl.getDetail(id);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("/checkTel")
	public ResponseEntity<BaseResponse> checkTel(String value, String custId) {
		BaseResponse baseResponse = custServiceImpl.checkTel(value, Long.parseLong(custId));
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("/checkEmail")
	public ResponseEntity<BaseResponse> checkEmail(String value, String custId) {
		BaseResponse baseResponse = custServiceImpl.checkEmail(value, Long.parseLong(custId));
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("/checkIdno")
	public ResponseEntity<BaseResponse> checkIdno(String value, String custId) {
		BaseResponse baseResponse = custServiceImpl.checkIdno(value, Long.parseLong(custId));
		return ResponseEntity.ok(baseResponse);
	}
}

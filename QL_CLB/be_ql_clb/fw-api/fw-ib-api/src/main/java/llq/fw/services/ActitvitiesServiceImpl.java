package llq.fw.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.CriteriaBuilder.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import llq.fw.cm.common.Constants.FwError;
import llq.fw.cm.models.Activities;
import llq.fw.cm.models.ActivitiesMembers;

import llq.fw.cm.models.Member;
import llq.fw.cm.payload.response.BaseResponse;

import llq.fw.cm.security.repository.ActivitiesMembersRepository;
import llq.fw.cm.security.repository.ActivitiesRepository;
import llq.fw.cm.security.repository.MemberRepository;
import llq.fw.payload.request.gen.ActivitiesSearchRequest;
import llq.fw.cm.services.BaseService;

@Component
public class ActitvitiesServiceImpl
		implements BaseService<BaseResponse, Activities, ActivitiesSearchRequest, java.lang.String> {
	private static final Logger logger = LoggerFactory.getLogger(ActitvitiesServiceImpl.class);
	private final SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");

	@Autowired
	protected ActivitiesRepository activitiesRepository;

	@Autowired
	protected ActivitiesMembersRepository activitiesMembersRepository;

	@Autowired
	MemberRepository custRepository;

	@Override
	public BaseResponse create(Activities activities) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {

			var activitiesNew = Activities.builder()
					.ma(activities.getMa())
					.type(activities.getType())
					.status("0")
					.startDate(activities.getStartDate())
					.memberCurrent(0L)
					.memberRequire(activities.getMemberRequire())
					.mota(activities.getMota())
					.fileName(activities.getFileName())
					.missionCurrent(0L)
					.progress(0L)
					.build();
			jpaExecute(activitiesNew);
			baseResponse.setData(activitiesNew);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	public BaseResponse createActivitiesMembers(ActivitiesMembers activitiesMembers) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			if (activitiesMembers.getActivities().getMemberRequire() <= activitiesMembers.getActivities()
					.getMemberCurrent() && activitiesMembers.getType().equals("2")) {
				baseResponse.setFwError(FwError.KHONGTHANHCONG);
				return baseResponse;
			}

			var activitiesNew = ActivitiesMembers.builder()
					.award(activitiesMembers.getAward())
					.members(activitiesMembers.getMembers())
					.activities(activitiesMembers.getActivities())
					.mission(activitiesMembers.getMission())
					.type(activitiesMembers.getType())
					.mota(activitiesMembers.getMota())
					.missionStatus("0")
					.target(activitiesMembers.getTarget())
					.build();

			if (activitiesMembers.getActivities() != null && activitiesMembers.getType().equals("2")) {
				activitiesMembers.getActivities()
						.setMemberCurrent(activitiesMembers.getActivities().getMemberCurrent() + 1L);
			} else {
				activitiesMembers.getActivities()
						.setMissionCurrent((activitiesMembers.getActivities().getMissionCurrent() + 1L));
				if (activitiesMembers.getMissionStatus().equals("1") && activitiesMembers.getActivities()
						.getMissionCurrent() > activitiesMembers.getActivities().getProgress())
					activitiesMembers.getActivities().setProgress(activitiesMembers.getActivities().getProgress() + 1L);
			}

			jpaExecute(activitiesMembers.getActivities());
			jpaExecute2(activitiesNew);
			baseResponse.setData(activitiesNew);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse update(Activities activities) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			var contactInfoCur = activitiesRepository.findById(Long.valueOf(activities.getId())).orElse(null);
			if (contactInfoCur == null) {
				baseResponse.setFwError(FwError.DLKHONGTONTAI);
				return baseResponse;
			}
			contactInfoCur.setMa(activities.getMa());
			contactInfoCur.setType(activities.getType());
			contactInfoCur.setStatus(activities.getStatus());
			contactInfoCur.setStartDate(activities.getStartDate());
			contactInfoCur.setMemberCurrent(activities.getMemberCurrent());
			contactInfoCur.setMemberRequire(activities.getMemberRequire());
			contactInfoCur.setMota(activities.getMota());
			contactInfoCur.setFileName(activities.getFileName());

			jpaExecute(contactInfoCur);

			baseResponse.setData(contactInfoCur);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	public BaseResponse updateActivitiesMembers(ActivitiesMembers activitiesMembers) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			var contactInfoCur = activitiesMembersRepository.findById(Long.valueOf(activitiesMembers.getId()))
					.orElse(null);
			if (contactInfoCur == null) {
				baseResponse.setFwError(FwError.DLKHONGTONTAI);
				return baseResponse;
			}
			if (activitiesMembers.getAward() != null) {
				contactInfoCur.setAward(activitiesMembers.getAward());
				if (activitiesMembers.getAward().equals("0")) {
					contactInfoCur.getMembers()
							.setPoint(String.valueOf(Long.valueOf(contactInfoCur.getMembers().getPoint()) + 1L));
				} else {
					contactInfoCur.getMembers()
							.setPoint(String.valueOf(Long.valueOf(contactInfoCur.getMembers().getPoint()) - 1L));
				}

				custRepository.save(contactInfoCur.getMembers());
			}

			contactInfoCur.setMembers(activitiesMembers.getMembers());
			contactInfoCur.setMission(activitiesMembers.getMission());
			contactInfoCur.setType(activitiesMembers.getType());
			contactInfoCur.setMota(activitiesMembers.getMota());
			contactInfoCur.setMissionStatus(activitiesMembers.getMissionStatus());
			contactInfoCur.setTarget(activitiesMembers.getTarget());

			if (activitiesMembers.getActivities() != null && activitiesMembers.getType().equals("1")
					) {
				if (activitiesMembers.getMissionStatus().equals("1")){
					activitiesMembers.getActivities().setProgress(activitiesMembers.getActivities().getProgress() + 1L);
				}else{
					activitiesMembers.getActivities().setProgress(activitiesMembers.getActivities().getProgress() - 1L);
				}

				if(activitiesMembers.getActivities().getProgress() == activitiesMembers.getActivities().getMissionCurrent()){
					activitiesMembers.getActivities().setStatus("2");
				}else{
					activitiesMembers.getActivities().setStatus("0");
				}
						
					
			}

			jpaExecute(activitiesMembers.getActivities());
			jpaExecute2(contactInfoCur);

			baseResponse.setData(contactInfoCur);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	public BaseResponse getActivitiesMembers(ActivitiesSearchRequest activitiesSearchRequest, Pageable pageable) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);

		try {
			Specification<ActivitiesMembers> specification = new Specification<ActivitiesMembers>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ActivitiesMembers> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					if (!ObjectUtils.isEmpty(activitiesSearchRequest.getId())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("activities").get("id"),
								activitiesSearchRequest.getId())));
					}

					if (!ObjectUtils.isEmpty(activitiesSearchRequest.getMembersId())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("members").get("id"),
								activitiesSearchRequest.getMembersId())));
					}

					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("type"),
							"1")));

					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Page<ActivitiesMembers> page = activitiesMembersRepository.findAll(specification, pageable);
			baseResponse.setData(page);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	public BaseResponse getActivitiesMembersAward(ActivitiesSearchRequest activitiesSearchRequest, Pageable pageable) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);

		try {
			Specification<ActivitiesMembers> specification = new Specification<ActivitiesMembers>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ActivitiesMembers> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					if (!ObjectUtils.isEmpty(activitiesSearchRequest.getId())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("activities").get("id"),
								activitiesSearchRequest.getId())));
					}

					if (!ObjectUtils.isEmpty(activitiesSearchRequest.getMembersId())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("members").get("id"),
								activitiesSearchRequest.getMembersId())));
					}

					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("type"),
							"2")));

					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Page<ActivitiesMembers> page = activitiesMembersRepository.findAll(specification, pageable);
			baseResponse.setData(page);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse search(ActivitiesSearchRequest contactInfo, Pageable pageable) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG); // TODO : Lay tham so xu ly song ngu
		try {
			Specification<Activities> specification = new Specification<Activities>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<Activities> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					if (!ObjectUtils.isEmpty(contactInfo.getMa())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.upper(root.get("ma")),
								"%" + contactInfo.getMa().toUpperCase() + "%")));
					}
					if (!ObjectUtils.isEmpty(contactInfo.getType())) {
						predicates.add(
								criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.upper(root.get("type")),
										"%" + contactInfo.getType().toUpperCase() + "%")));
					}
					if (!ObjectUtils.isEmpty(contactInfo.getStatus())) {
						predicates
								.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("status"),
										contactInfo.getStatus())));
					}

	
					if (!ObjectUtils.isEmpty(contactInfo.getTuNgay())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(
								criteriaBuilder.function("TRUNC", Date.class, root.get("startDate")),
								criteriaBuilder.function("TO_DATE", Date.class,
										criteriaBuilder.literal(formatDate.format(contactInfo.getTuNgay())),
										criteriaBuilder.literal("DD-MM-YYYY")))));
					}

					if (!ObjectUtils.isEmpty(contactInfo.getDenNgay())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(
								criteriaBuilder.function("TRUNC", Date.class, root.get("startDate")),
								criteriaBuilder.function("TO_DATE", Date.class,
										criteriaBuilder.literal(formatDate.format(contactInfo.getDenNgay())),
										criteriaBuilder.literal("DD-MM-YYYY")))));
					}

					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Page<Activities> page = activitiesRepository.findAll(specification, pageable);
			baseResponse.setData(page);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	public BaseResponse getAllActivities() {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG); // TODO : Lay tham so xu ly song ngu
		try {
			Specification<Activities> specification = new Specification<Activities>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<Activities> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			List<Activities> page = activitiesRepository.findAll(specification);
			baseResponse.setData(page);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	public BaseResponse getAll(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG); // TODO : Lay tham so xu ly song ngu
		try {
			Specification<ActivitiesMembers> specification = new Specification<ActivitiesMembers>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ActivitiesMembers> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("activities").get("id"),
							id)));

					predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("target"),
							"1")));

					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			List<ActivitiesMembers> page = activitiesMembersRepository.findAll(specification);
			List<Member> members = new ArrayList<Member>();
			for (ActivitiesMembers activitiesMembers : page) {
				members.add(activitiesMembers.getMembers());
			}
			baseResponse.setData(members);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void jpaExecute(Activities activities) {
		activitiesRepository.save(activities);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void jpaExecute2(ActivitiesMembers activitiesMembers) {
		activitiesMembersRepository.save(activitiesMembers);
	}

	@Override
	public BaseResponse getDetail(java.lang.String id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		var contactInfoCur = activitiesRepository.findById(Long.valueOf(id)).orElse(null);
		if (contactInfoCur == null) {
			baseResponse.setFwError(FwError.DLKHONGTONTAI);
			return baseResponse;
		}
		baseResponse.setData(contactInfoCur);
		return baseResponse;
	}

	public BaseResponse getActivitiesMembersDetail(java.lang.String id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		var contactInfoCur = activitiesMembersRepository.findById(Long.valueOf(id)).orElse(null);
		if (contactInfoCur == null) {
			baseResponse.setFwError(FwError.DLKHONGTONTAI);
			return baseResponse;
		}
		baseResponse.setData(contactInfoCur);
		return baseResponse;
	}

	@Override
	public BaseResponse checkExist(java.lang.String id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		// baseResponse.setData(contactInfoRepository.existsById(id.toUpperCase()));
		return baseResponse;
	}

	@Override
	public BaseResponse delete(String r) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Unimplemented method 'delete'");
	}

}

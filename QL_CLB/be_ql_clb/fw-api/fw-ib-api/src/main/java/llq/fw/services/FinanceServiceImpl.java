package llq.fw.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Date;
import java.util.HashMap;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import llq.fw.cm.common.AddDate;
import llq.fw.cm.common.createFileExport;
import llq.fw.cm.common.Constants.FwError;
import llq.fw.cm.enums.cust.PositionEnum;
import llq.fw.cm.enums.trans.ScheduleFrequencyEnum;
import llq.fw.cm.enums.trans.SchedulesEnum;

import llq.fw.cm.models.Member;
import llq.fw.cm.models.TaiChinh;

import llq.fw.cm.payload.response.BaseResponse;
import llq.fw.cm.repository.FinanceRepository;

import llq.fw.cm.security.services.CustDetailsImpl;
import llq.fw.payload.request.TaiChinhRequest;
import llq.fw.payload.request.TranSearchRequest;
import llq.fw.payload.request.TransRequest;

import llq.fw.cm.services.BaseService;
import llq.fw.common.Constants.LoaiGiaoDich;
import llq.fw.common.Constants.TranStatus;

@Component
public class FinanceServiceImpl
		implements BaseService<BaseResponse, TaiChinh, TaiChinhRequest, java.lang.String> {
	private static final Logger logger = LoggerFactory.getLogger(FinanceServiceImpl.class);
	private final SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
	@Autowired
	protected FinanceRepository transScheduleRepository;

	@Override
	public BaseResponse create(TaiChinh transSchedule) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			// var transScheduleCur =
			// transScheduleRepository.findById(transSchedule.getId()).orElse(null);
			// if (transScheduleCur != null) {
			// baseResponse.setFwError(FwError.DLDATONTAI);
			// return baseResponse;
			// }

			var transScheduleNew = TaiChinh.builder()
					.ma(transSchedule.getMa())
					.mota(transSchedule.getMota())
					.type(transSchedule.getType())
					.quantity(transSchedule.getQuantity().replaceAll(",", ""))
					.build();
			jpaExecute(transScheduleNew);
			baseResponse.setData(transScheduleNew);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse update(TaiChinh transSchedule) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {

			// check OTP

			//

			var transScheduleCur = transScheduleRepository.findById(transSchedule.getId()).orElse(null);
			if (transScheduleCur == null) {
				baseResponse.setFwError(FwError.DLKHONGTONTAI);
				return baseResponse;
			}
			transScheduleCur.setQuantity(transSchedule.getQuantity());
			transScheduleCur.setMa(transSchedule.getMa());
			transScheduleCur.setType(transSchedule.getType());
			transScheduleCur.setMota(transSchedule.getMota());
			transScheduleCur.setQuantity(transSchedule.getQuantity().replaceAll(",", ""));
			jpaExecute(transScheduleCur);
			/*
			 * Luu obj lich su neu can
			 */
			baseResponse.setData(transScheduleCur);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse search(TaiChinhRequest tranSearch, Pageable pageable) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG); // TODO : Lay tham so xu ly song ngu

		CustDetailsImpl userDetail = (CustDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		try {
			Specification<TaiChinh> specification = new Specification<TaiChinh>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<TaiChinh> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					if (!ObjectUtils.isEmpty(tranSearch.getTuNgay())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(
								criteriaBuilder.function("TRUNC", Date.class, root.get("createdAt")),
								criteriaBuilder.function("TO_DATE", Date.class,
										criteriaBuilder.literal(formatDate.format(tranSearch.getTuNgay())),
										criteriaBuilder.literal("DD-MM-YYYY")))));
					}

					if (!ObjectUtils.isEmpty(tranSearch.getDenNgay())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(
								criteriaBuilder.function("TRUNC", Date.class, root.get("createdAt")),
								criteriaBuilder.function("TO_DATE", Date.class,
										criteriaBuilder.literal(formatDate.format(tranSearch.getDenNgay())),
										criteriaBuilder.literal("DD-MM-YYYY")))));
					}

					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Page<TaiChinh> page = transScheduleRepository.findAll(specification, pageable);
			baseResponse.setData(page);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse delete(java.lang.String id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			// var transScheduleCur = transScheduleRepository.findById(id).orElse(null);
			// if (transScheduleCur == null) {
			// baseResponse.setFwError(FwError.DLKHONGTONTAI);
			// return baseResponse;
			// }
			// jpaDeleteExecute(id);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void jpaExecute(TaiChinh transSchedule) {
		transScheduleRepository.save(transSchedule);
	}

	@Override
	public BaseResponse getDetail(java.lang.String id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		var transScheduleCur = transScheduleRepository.findById(Long.valueOf(id)).orElse(null);
		if (transScheduleCur == null) {
			baseResponse.setFwError(FwError.DLKHONGTONTAI);
			return baseResponse;
		}
		baseResponse.setData(transScheduleCur);
		return baseResponse;
	}

	@Override
	public BaseResponse checkExist(java.lang.String id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		// baseResponse.setData(transScheduleRepository.existsById(id.toUpperCase()));
		return baseResponse;
	}

	public BaseResponse countTaiChinh() {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		Long countTaiChinh = 0L;
		List<TaiChinh> listTaiChinh = transScheduleRepository.findAll();
		for (TaiChinh taiChinh : listTaiChinh) {
			if (taiChinh.getType().equals("0")) {
				countTaiChinh = countTaiChinh + Long.valueOf(taiChinh.getQuantity().replaceAll(",", ""));
			} else {
				countTaiChinh = countTaiChinh - Long.valueOf(taiChinh.getQuantity().replaceAll(",", ""));
			}
		}
		baseResponse.setData(countTaiChinh);

		return baseResponse;
	}

}

package llq.fw.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Date;
import java.util.HashMap;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import llq.fw.cm.common.createFileExport;
import llq.fw.cm.common.Constants.FwError;

import llq.fw.cm.models.Csvc;
import llq.fw.cm.enums.cust.PositionEnum;
import llq.fw.cm.models.Member;

import llq.fw.cm.payload.response.BaseResponse;
import llq.fw.cm.repository.CsvcRepository;
import llq.fw.cm.security.repository.MemberRepository;
import llq.fw.cm.security.services.CustDetailsImpl;
import llq.fw.payload.request.CsvcRequest;
import llq.fw.payload.request.TranSearchRequest;
import llq.fw.payload.request.TransRequest;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import llq.fw.cm.services.BaseService;
import llq.fw.common.Constants.LoaiGiaoDich;
import llq.fw.common.Constants.TranStatus;

@Component
public class CsvcServiceImpl implements BaseService<BaseResponse, Csvc, CsvcRequest, java.lang.String> {
	private static final Logger logger = LoggerFactory.getLogger(CsvcServiceImpl.class);
	private final SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
	@Autowired
	protected CsvcRepository tranRepository;
	@Autowired
	protected MemberRepository custRepository;

	@Override
	public BaseResponse create(Csvc tran) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			var tranNew = Csvc.builder()
					.ma(tran.getMa())
					.mota(tran.getMota())
					.status(tran.getStatus())
					.quantity(tran.getQuantity())
					.build();

			jpaExecute(tranNew);
			baseResponse.setData(tranNew);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse update(Csvc tran) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {

			// check OTP

			//

			var tranCur = tranRepository.findById(tran.getId()).orElse(null);
			if (tranCur == null) {
				baseResponse.setFwError(FwError.DLKHONGTONTAI);
				return baseResponse;
			}

			tranCur.setStatus(tran.getStatus());
			tranCur.setMa(tran.getMa());
			tranCur.setQuantity(tran.getQuantity());
			tranCur.setMota(tran.getMota());
			jpaExecute(tranCur);
			/*
			 * Luu obj lich su neu can
			 */
			baseResponse.setData(tranCur);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse search(CsvcRequest tran, Pageable pageable) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG); // TODO : Lay tham so xu ly song ngu
		CustDetailsImpl userDetail = (CustDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		try {
			Specification<Csvc> specification = new Specification<Csvc>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<Csvc> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					if (!ObjectUtils.isEmpty(tran.getMa())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("ma"),
								tran.getMa())));
					}

					if (!ObjectUtils.isEmpty(tran.getStatus())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("status"),
								tran.getStatus())));
					}

					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Page<Csvc> page = tranRepository.findAll(specification, pageable);
			baseResponse.setData(page);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse delete(java.lang.String id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			var tranCur = tranRepository.findById(Long.valueOf(id)).orElse(null);
			if (tranCur == null) {
				baseResponse.setFwError(FwError.DLKHONGTONTAI);
				return baseResponse;
			}
			jpaDeleteExecute(id);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void jpaExecute(Csvc tran) {
		tranRepository.save(tran);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void jpaDeleteExecute(java.lang.String id) {
		tranRepository.deleteById(Long.valueOf(id));
	}

	@Override
	public BaseResponse getDetail(java.lang.String id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		var tranCur = tranRepository.findById(Long.valueOf(id)).orElse(null);
		if (tranCur == null) {
			baseResponse.setFwError(FwError.DLKHONGTONTAI);
			return baseResponse;
		}
		baseResponse.setData(tranCur);
		return baseResponse;
	}

	@Override
	public BaseResponse checkExist(java.lang.String id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		// baseResponse.setData(tranRepository.existsById(id.toUpperCase()));
		return baseResponse;
	}

}

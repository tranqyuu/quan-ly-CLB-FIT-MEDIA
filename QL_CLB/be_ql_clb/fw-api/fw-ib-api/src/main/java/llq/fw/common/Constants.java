package llq.fw.common;

public class Constants {
	public final static String OK = "OK";
	public final static String ERROR = "ERROR";
	
	public enum PaymentStatus{
		NULL("", ""),
		CHOXULY("0","Chờ xử lý"),
		THANHCONG("1","Thành công"),
		LOI("2","Lỗi"),
		HUY("3","Đã hủy");
		
	    private final String code;
	    private final String message;
	    PaymentStatus(String code,String message) {
	    	this.code=code;
	    	this.message=message;
	    }
		public String getCode() {
			return code;
		}
		public String getMessage() {
			return message;
		}
		
		public static PaymentStatus getByCode(String code) {
			if (CHOXULY.getCode().equals(code)) {
				return PaymentStatus.CHOXULY;
			} else if (THANHCONG.getCode().equals(code)) {
				return PaymentStatus.THANHCONG;
			} else if (LOI.getCode().equals(code)) {
				return PaymentStatus.LOI;
			} else if (HUY.getCode().equals(code)) {
				return PaymentStatus.HUY;
			}
			return PaymentStatus.NULL;
		}
	}
	
	public enum TranStatus{
		NULL("", ""),
		CHODUYET("0","Chờ duyệt"),
		TUCHOI("1","Từ chối"),
		DADUYET("2","Đã duyệt"),
		HUY("3","Đã hủy"),
		HOANTHANH("4","Hoàn thành"),
		LOI("5","Lỗi");
		
	    private final String code;
	    private final String message;
	    TranStatus(String code,String message) {
	    	this.code=code;
	    	this.message=message;
	    }
		public String getCode() {
			return code;
		}
		public String getMessage() {
			return message;
		}
		
		public static TranStatus getByCode(String code) {
			if (CHODUYET.getCode().equals(code)) {
				return TranStatus.CHODUYET;
			} else if (TUCHOI.getCode().equals(code)) {
				return TranStatus.TUCHOI;
			} else if (DADUYET.getCode().equals(code)) {
				return TranStatus.DADUYET;
			} else if (HUY.getCode().equals(code)) {
				return TranStatus.HUY;
			} else if (HOANTHANH.getCode().equals(code)) {
				return TranStatus.HOANTHANH;
			} else if (LOI.getCode().equals(code)) {
				return TranStatus.LOI;
			}
			
			return TranStatus.NULL;
		}
	}
	
	public enum LoaiGiaoDich{
		NULL("", "", ""),
		CHUYENTIENNOIBO("", "CORE", "Chuyển tiền nội bộ"),
		CHUYENTIENDINHKYNOIBO("1", "CORE", "Chuyển tiền định kỳ nội bộ"),
		CHUYENTIENTUONGLAINOIBO("0", "CORE", "Chuyển tiền tương lai nội bộ"),
		CHUYENTIENLIENNGANHANG("", "CITAD", "Chuyển tiền liên ngân hàng"),
		CHUYENTIENDINHKYLIENNGANHANG("1", "CITAD", "Chuyển tiền định kỳ liên ngân hàng"),
		CHUYENTIENTUONGLAILIENNGANHANG("0", "CITAD", "Chuyển tiền tương lai liên ngân hàng"),
		CHUYENTIENNHANHNAPAS("", "NAPAS","Chuyển tiền nhanh NAPAS 247"),
		CHUYENTIENDINHKYNAPAS("1", "NAPAS","Chuyển tiền định kỳ NAPAS 247"),
		CHUYENTIENTUONGLAINAPAS("0", "NAPAS","Chuyển tiền tương lai NAPAS 247");
		
	    private final String schedule;
	    private final String type;
	    private final String message;
	    LoaiGiaoDich(String schedule, String type,String message) {
	    	this.schedule=schedule;
	    	this.type=type;
	    	this.message=message;
	    }
		public String getSchedule() {
			return schedule;
		}
		public String getType() {
			return type;
		}
		public String getMessage() {
			return message;
		}
		
		public static LoaiGiaoDich getByScheduleType(String schedule, String type) {
			if (CHUYENTIENNOIBO.getSchedule().equals(schedule) && CHUYENTIENNOIBO.getType().equals(type)) {
				return LoaiGiaoDich.CHUYENTIENNOIBO;
			} else if (CHUYENTIENDINHKYNOIBO.getSchedule().equals(schedule) && CHUYENTIENDINHKYNOIBO.getType().equals(type)) {
				return LoaiGiaoDich.CHUYENTIENDINHKYNOIBO;
			} else if (CHUYENTIENTUONGLAINOIBO.getSchedule().equals(schedule) && CHUYENTIENTUONGLAINOIBO.getType().equals(type)) {
				return LoaiGiaoDich.CHUYENTIENTUONGLAINOIBO;
			} else if (CHUYENTIENLIENNGANHANG.getSchedule().equals(schedule) && CHUYENTIENLIENNGANHANG.getType().equals(type)) {
				return LoaiGiaoDich.CHUYENTIENLIENNGANHANG;
			} else if (CHUYENTIENDINHKYLIENNGANHANG.getSchedule().equals(schedule) && CHUYENTIENDINHKYLIENNGANHANG.getType().equals(type)) {
				return LoaiGiaoDich.CHUYENTIENDINHKYLIENNGANHANG;
			} else if (CHUYENTIENTUONGLAILIENNGANHANG.getSchedule().equals(schedule) && CHUYENTIENTUONGLAILIENNGANHANG.getType().equals(type)) {
				return LoaiGiaoDich.CHUYENTIENTUONGLAILIENNGANHANG;
			} else if (CHUYENTIENNHANHNAPAS.getSchedule().equals(schedule) && CHUYENTIENNHANHNAPAS.getType().equals(type)) {
				return LoaiGiaoDich.CHUYENTIENNHANHNAPAS;
			} 
//			else if (CHUYENTIENDINHKYNAPAS.getSchedule().equals(schedule) && CHUYENTIENDINHKYNAPAS.getType().equals(type)) {
//				return LoaiGiaoDich.CHUYENTIENDINHKYNAPAS;
//			} else if (CHUYENTIENTUONGLAINAPAS.getSchedule().equals(schedule) && CHUYENTIENTUONGLAINAPAS.getType().equals(type)) {
//				return LoaiGiaoDich.CHUYENTIENTUONGLAINAPAS;
//			}
			return LoaiGiaoDich.NULL;
		}
	}

}

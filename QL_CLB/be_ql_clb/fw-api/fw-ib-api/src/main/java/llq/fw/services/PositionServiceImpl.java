package llq.fw.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Date;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.CriteriaBuilder.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import llq.fw.cm.common.Constants.FwError;
import llq.fw.cm.models.Member;
import llq.fw.cm.models.ChucVu;

import llq.fw.cm.payload.response.BaseResponse;
import llq.fw.cm.security.repository.ChucVuRepository;
import llq.fw.cm.security.repository.MemberRepository;
import llq.fw.cm.security.services.CustDetailsImpl;
import llq.fw.payload.request.ChucVuRequest;
import llq.fw.cm.services.BaseService;

@Component
public class PositionServiceImpl
		implements BaseService<BaseResponse, ChucVu, ChucVuRequest, java.lang.Long> {
	private static final Logger logger = LoggerFactory.getLogger(PositionServiceImpl.class);
	private final SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
	@Autowired
	protected ChucVuRepository custProductRepository;

	@Autowired
	protected MemberRepository custRepository;

	@Override
	public BaseResponse create(ChucVu custProduct) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			var custProductNew = ChucVu.builder()
					.ma(custProduct.getMa())
					.status(custProduct.getStatus())
					.mota(custProduct.getMota())
					.build();
			jpaExecute(custProductNew);
			baseResponse.setData(custProductNew);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse update(ChucVu custProduct) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			var custProductCur = custProductRepository.findById(custProduct.getId()).orElse(null);
			if (custProductCur == null) {
				baseResponse.setFwError(FwError.DLKHONGTONTAI);
				return baseResponse;
			}
			custProductCur.setMa(custProduct.getMa());
			custProductCur.setMota(custProduct.getMota());
			custProductCur.setStatus(custProduct.getStatus());
			jpaExecute(custProductCur);
			/*
			 * Luu obj lich su neu can
			 */
			baseResponse.setData(custProductCur);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse search(ChucVuRequest custProduct, Pageable pageable) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG); // TODO : Lay tham so xu ly song ngu
		CustDetailsImpl userDetail = (CustDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Member currentCust = custRepository.findById(userDetail.getId()).orElse(null);
		try {
			Specification<ChucVu> specification = new Specification<ChucVu>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ChucVu> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					if (!ObjectUtils.isEmpty(custProduct.getMa())) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("ma"),
								custProduct.getMa())));
					}
					if (!ObjectUtils.isEmpty(custProduct.getStatus())) {
						predicates.add(
								criteriaBuilder.and(criteriaBuilder.equal(root.get("status"),
										custProduct.getStatus())));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			Page<ChucVu> page = custProductRepository.findAll(specification, pageable);
			baseResponse.setData(page);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	public BaseResponse getAll() {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG); // TODO : Lay tham so xu ly song ngu
		CustDetailsImpl userDetail = (CustDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		Member currentCust = custRepository.findById(userDetail.getId()).orElse(null);
		try {
			Specification<ChucVu> specification = new Specification<ChucVu>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ChucVu> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			List<ChucVu> page = custProductRepository.findAll(specification);
			baseResponse.setData(page);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Override
	public BaseResponse delete(java.lang.Long id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		try {
			var custProductCur = custProductRepository.findById(id).orElse(null);
			if (custProductCur == null) {
				baseResponse.setFwError(FwError.DLKHONGTONTAI);
				return baseResponse;
			}
			jpaDeleteExecute(id);
		} catch (Exception e) {
			baseResponse.setFwError(FwError.KHONGTHANHCONG);
			logger.error("error", e);
		}
		return baseResponse;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void jpaExecute(ChucVu custProduct) {
		custProductRepository.save(custProduct);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void jpaDeleteExecute(java.lang.Long id) {
		custProductRepository.deleteById(id);
	}

	@Override
	public BaseResponse getDetail(java.lang.Long id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		var custProductCur = custProductRepository.findById(id).orElse(null);
		if (custProductCur == null) {
			baseResponse.setFwError(FwError.DLKHONGTONTAI);
			return baseResponse;
		}
		baseResponse.setData(custProductCur);
		return baseResponse;
	}

	@Override
	public BaseResponse checkExist(java.lang.Long id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setFwError(FwError.THANHCONG);
		baseResponse.setData(custProductRepository.existsById(id));
		return baseResponse;
	}

}

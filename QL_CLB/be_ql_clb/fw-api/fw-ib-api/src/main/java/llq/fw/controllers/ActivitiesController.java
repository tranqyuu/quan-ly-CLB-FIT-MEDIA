package llq.fw.controllers;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import llq.fw.cm.common.createFileExport;
import llq.fw.cm.models.Activities;
import llq.fw.cm.models.ActivitiesMembers;
import llq.fw.cm.payload.response.BaseResponse;
import llq.fw.payload.request.gen.ActivitiesSearchRequest;
import llq.fw.services.ActitvitiesServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/activities")
public class ActivitiesController {
	@Autowired
	ActitvitiesServiceImpl contactInfoServiceImpl;

	@GetMapping // Tim kiem theo nhieu dieu kien
	public ResponseEntity<BaseResponse> searchAll(ActivitiesSearchRequest contactInfoRequest, Pageable pageable) {
		BaseResponse baseResponse = contactInfoServiceImpl.search(contactInfoRequest, pageable);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("getallactivities") // Tim kiem theo nhieu dieu kien
	public ResponseEntity<BaseResponse> getAllActivities() {
		BaseResponse baseResponse = contactInfoServiceImpl.getAllActivities();
		return ResponseEntity.ok(baseResponse);
	}


	@GetMapping("activitiesmembers") // Tim kiem theo nhieu dieu kien
	public ResponseEntity<BaseResponse> searchAllActivitiesMembers(ActivitiesSearchRequest contactInfoRequest,
			Pageable pageable) {
		BaseResponse baseResponse = contactInfoServiceImpl.getActivitiesMembers(contactInfoRequest, pageable);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("activitiesmembersaward") // Tim kiem theo nhieu dieu kien
	public ResponseEntity<BaseResponse> searchAllActivitiesMembersAward(ActivitiesSearchRequest contactInfoRequest,
			Pageable pageable) {
		BaseResponse baseResponse = contactInfoServiceImpl.getActivitiesMembersAward(contactInfoRequest, pageable);
		return ResponseEntity.ok(baseResponse);
	}

	@PostMapping // Tao moi
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<BaseResponse> create(@RequestBody Activities activities) {
		BaseResponse baseResponse = contactInfoServiceImpl.create(activities);
		return ResponseEntity.ok(baseResponse);
	}

	@PostMapping("createactivitiesmembers") // Tao moi
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<BaseResponse> createActivitiesMembers(@RequestBody ActivitiesMembers activitiesMembers) {
		BaseResponse baseResponse = contactInfoServiceImpl.createActivitiesMembers(activitiesMembers);
		return ResponseEntity.ok(baseResponse);
	}

	@PutMapping // Cap nhat
	public ResponseEntity<BaseResponse> update(@RequestBody Activities contactInfoRequest) {
		BaseResponse baseResponse = contactInfoServiceImpl.update(contactInfoRequest);
		return ResponseEntity.ok(baseResponse);
	}

	@DeleteMapping("{id}") // Xoa
	public ResponseEntity<BaseResponse> delete(@PathVariable java.lang.String id) {
		BaseResponse baseResponse = contactInfoServiceImpl.delete(id);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("{id}") // Xoa
	public ResponseEntity<BaseResponse> getDetailById(@PathVariable java.lang.String id) {
		BaseResponse baseResponse = contactInfoServiceImpl.getDetail(id);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("/activitiesmembersdetail/{id}") // Xoa
	public ResponseEntity<BaseResponse> getDetailActivitiesMembersById(@PathVariable java.lang.String id) {
		BaseResponse baseResponse = contactInfoServiceImpl.getActivitiesMembersDetail(id);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("keyexist/{id}") // check exist
	public ResponseEntity<BaseResponse> checkExist(@PathVariable java.lang.String id) {
		BaseResponse baseResponse = contactInfoServiceImpl.checkExist(id);
		return ResponseEntity.ok(baseResponse);
	}

	@GetMapping("getall/{id}") // check exist
	public ResponseEntity<BaseResponse> getAll(@PathVariable java.lang.Long id) {
		BaseResponse baseResponse = contactInfoServiceImpl.getAll(id);
		return ResponseEntity.ok(baseResponse);
	}

	@PutMapping("update") // Cap nhat
	public ResponseEntity<BaseResponse> update(@RequestBody ActivitiesMembers activitiesMembers) {
		BaseResponse baseResponse = contactInfoServiceImpl.updateActivitiesMembers(activitiesMembers);
		return ResponseEntity.ok(baseResponse);
	}

	@PostMapping(value = "/export")
    public void exportBangKe(@RequestBody ActivitiesSearchRequest transLotSearch, HttpServletResponse response,Pageable pageable) 
    		throws Exception {
        Map<String, Object> map;
            try (InputStream isFileSample = new ClassPathResource("template/danhsachbangke.xlsx")
                    .getInputStream();) {
            	
            	//List<TransLot> lst = transLotServiceImpl.findAllTransLot();
            	BaseResponse baseRessponse = contactInfoServiceImpl.search(transLotSearch, pageable);
            	Page<Activities> page =  (Page<Activities>) baseRessponse.getData();
            	List<Activities> lst1 = page.getContent();
                // Doc du lieu mau
					lst1.stream().forEach(item -> {
						if(item.getType().equals("0")){
							item.setType("Hoạt động của Khoa");
						}else if(item.getType().equals("1")){
							item.setType("Hoạt động của Trường");
						}else{
							item.setType("Hoạt động của CLB");
						}

						if(item.getStatus().equals("0")){
							item.setStatus("Đang thực hiện");
						}else{
							item.setStatus("Đã hoàn thành");
						}
					});
                // Gan du lieu cho File
                map = new HashMap<String, Object>();
                map.put("RESULT",  lst1);
                map.put("NGAY_GIO_IN", new Date());

                // set response
                response.setHeader("listbangke",".xlsx");
                response.setHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "listbangke");
                createFileExport.createFileToOutputstream(response.getOutputStream(), isFileSample, map);
            } catch (Exception  e) {
                throw new Exception( e.getMessage());
            } 
        }
	
}

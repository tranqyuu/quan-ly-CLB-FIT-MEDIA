// toggle text
$.fn.extend({
    toggleText: function(a, b) {
        return this.text(this.text() == b ? a : b);
    }
});
// end toggle text
// app height
function appHeight() {
    const doc = document.documentElement
    doc.style.setProperty('--vh', (window.innerHeight * .01) + 'px');
}
window.addEventListener('resize', appHeight);
appHeight();
// end app height

// fix modal
$.fn.modal.Constructor.prototype._enforceFocus = function() {};
// end fix modal


$(document).ready(function() {
    // fix tooltip mobile
    $('.input-label').on('touchend', function(e) {
        console.log('a')
        e.stopPropagation()
    })
    //input file upload
    $('.input-file').each(function() {
        var uploadTxt = $(this).parent('.input-inner-wrap').find('.input-fake-inner')
        $(this).on('change', function(e) {
            uploadTxt.addClass('has-file')
            uploadTxt.text(e.target.files[0].name)
        })
    })
    //end input file upload
    // lock sidebar left
    if ($(window).width() < 769) {
        $('#sidebar-left-main-trigger').on('change', function() {
            if ($(this).is(':checked')) {
                BNS.on()
            } else {
                BNS.off()
            }
        })
    }
    // end lock sidebar left
    // checkbox close modal
    $('input.close-modal').on('change', function() {
        $('.modal').modal('hide')
    })
    // end checkbox close modal
    if (window.innerWidth < 1200) {
        var sidebarId = $('label.sidebar-mini__item.active').attr('for');
        console.log(sidebarId)
        if (sidebarId) {
            $('#' + sidebarId).prop("checked", true);
        } else {
            $('#sidebarReport').prop("checked", true);
        }
    } else {
        $('label.sidebar-mini__item').on('click', function(e) {
            var sidebarId = '#' + $(this).attr('for');
            if ($(sidebarId).is(':checked')) {
                e.preventDefault();
                console.log($('#sidebar-left-close'))
                $('#sidebar-left-close').prop("checked", true);
            }
        });
    }
    $('.sidebar-input-trigger').on('change', function() {
        $('[data-tooltip]').tooltip('hide')
    });
    $('#sidebar-trigger').on('change', function() {
        $('[data-tooltip]').tooltip('hide')
    });

    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        let target = $(e.target).data('target');
        $(target)
            .addClass('active')
            .siblings('.tab-pane.active')
            .removeClass('active show')
    }).on('shown.bs.tab', function(e) {
        let target = $(e.target).data('target');
        $(target)
            .addClass('show')
    });
    // show hide content
    $('.show-hide-content').each(function() {
        var $this = $(this);
        if ($this.data('hide')) {
            var item = $this.data('hide');
            var itemShowNumber = $this.data('item');
            itemHide = item + ':nth-last-child(-n+' + (itemShowNumber) + ')';
            $this.find(itemHide).addClass('hidden');

            $this.find('.btn-show-hide').on('click', function() {
                $(this).find('.ubtn-text').toggleText('Xem tất cả', 'Rút gọn')
                $this.find(itemHide).toggleClass('hidden')
            })
        }

    })
    // end show hide content

    $('#selectBank').on('change', function() {
        $('.bank-level-2').removeClass('hidden')
    })
})
if ($('body').has('.fav-section').length > 0) {
    var hieghtThreshold;

    function stickyHeightCal() {
        if ($(".fav-section").length) {
            hieghtThreshold = $(".fav-section").offset().top + $(".fav-section").height() - 80;
        }
    };
    $(window).resize(function() {
        stickyHeightCal()
    });
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= hieghtThreshold) {
            $('.sidebar-right').addClass('sticky-fav');
        } else {
            $('.sidebar-right').removeClass('sticky-fav');
        }
    });

}

// table
var maxWidth = 260;
var tableCellsTd = document.querySelectorAll('td');
var tableCellsTh = document.querySelectorAll('th');

function tableCell(el) {
    for (x = 0; x < el.length; x++) {
        if (el[x].clientWidth > maxWidth) {
            el[x].classList.add("cell-long");
            el[x].style.minWidth = maxWidth + 'px'
        }
    }
}

tableCell(tableCellsTd)
tableCell(tableCellsTh)
// end table
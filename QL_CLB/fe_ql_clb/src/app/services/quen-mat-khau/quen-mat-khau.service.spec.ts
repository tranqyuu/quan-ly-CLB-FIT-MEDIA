import { TestBed } from '@angular/core/testing';

import { QuenMatKhauService } from './quen-mat-khau.service';

describe('QuenMatKhauService', () => {
  let service: QuenMatKhauService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuenMatKhauService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse, Cust, ForgotPasswordRequest } from 'src/app/model/model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QuenMatKhauService {

  private baseUrl = environment.urlServer + '/api/auth';
  constructor(private http: HttpClient,) { }

  forgotPassword(data: ForgotPasswordRequest): Observable<BaseResponse<Cust>> {
    return this.http.post<BaseResponse<Cust>>(this.baseUrl + '/forgotpassword', data);
} 
}

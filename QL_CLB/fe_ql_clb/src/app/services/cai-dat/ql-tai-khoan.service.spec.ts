import { TestBed } from '@angular/core/testing';

import { QlTaiKhoanService } from './ql-tai-khoan.service';

describe('QlTaiKhoanService', () => {
  let service: QlTaiKhoanService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QlTaiKhoanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

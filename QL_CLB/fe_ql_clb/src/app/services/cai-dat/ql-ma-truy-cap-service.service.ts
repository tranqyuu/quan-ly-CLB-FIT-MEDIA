import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { catchError, firstValueFrom, map, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FwError } from '../../common/constants';
import { BasePagingResponse, BaseResponse, Cust, CustAcc, PagesRequest } from '../../model/model';
import { BaseService } from './../BaseService';

@Injectable({
  providedIn: 'root'
})
export class QlMaTruyCapServiceService implements BaseService<Cust>{

  private baseUrl = environment.urlServer + '/api/cust';
  constructor(private http: HttpClient,private toastrs: ToastrService) {
  }
  searchSlect2(turn: string|null): Observable<Cust[]> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    params = params.set('page', 0);
    params = params.set('size', 10);
    if (turn) {
      params = params.set('code', turn);
      params.set('name', turn)
    }
    return this.http.get<Cust[]>(this.baseUrl, {
      params
    }).pipe(
      map((res:any) => {
        if(FwError.THANHCONG == res.errorCode){
          return res.data.content.map((item:any)=> {
            item.label=item.code+ (item.name ? ' - ' + item.name:'')
            item.track=item.code
            return item
          });
        }else{
          this.toastrs.error(res.errorMessage);
        }
      }),
      catchError(() => of([]))
    );
  }
  searchPaging(
    page: PagesRequest,
    cust: Cust
  ): Observable<BasePagingResponse<Cust>> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (cust.code) params = params.set('code', cust.code);
    if (cust.tel) params =params.set('tel', cust.tel);
    if (cust.email) params =params.set('email', cust.email);
    if (cust.status) params =params.set('status', cust.status);
    if (cust.position) params =params.set('position', cust.position);
    return this.http.get<BasePagingResponse<Cust>>(this.baseUrl, {
      params
    });
  }

  searchCustPaging(
    page: PagesRequest,
    cust: Cust
  ): Observable<BasePagingResponse<Cust>> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);
    return this.http.get<BasePagingResponse<Cust>>(this.baseUrl + "/search", {
      params
    });
  }

  getDetail(
    id: any,
  ): Observable<BaseResponse<Cust>> {
    return this.http.get<BaseResponse<Cust>>(this.baseUrl+'/'+ id);
  }
  update(cust: Cust): Observable<BaseResponse<Cust>> {
    return this.http.put<BaseResponse<Cust>>(this.baseUrl, cust);
  }
  create(cust: Cust): Observable<BaseResponse<Cust>> {
    return this.http.post<BaseResponse<Cust>>(this.baseUrl, cust);
  }

  updateSAcc(cust: Cust): Observable<BaseResponse<Cust>> {
    return this.http.put<BaseResponse<Cust>>(this.baseUrl+'/updatetruyvan', cust);
  }

  updateGiaoDich(cust: Cust): Observable<BaseResponse<Cust>> {
    return this.http.put<BaseResponse<Cust>>(this.baseUrl+'/updategiaodich', cust);
  }

  getAllCode(): Observable<BaseResponse<any>> {
    return this.http.get<BaseResponse<any>>(this.baseUrl+'/getcode');
  }

  getAllTel(): Observable<BaseResponse<any>> {
    return this.http.get<BaseResponse<any>>(this.baseUrl+'/gettel');
  }

  getAllEmail(): Observable<BaseResponse<any>> {
    return this.http.get<BaseResponse<any>>(this.baseUrl+'/getemail');
  }

  async checkTel(value: any, custId: any){
    let params = new HttpParams();
    if (custId) params = params.set('custId', custId);
    if (value) params = params.set('value', value);
    
    return firstValueFrom(this.http.get(this.baseUrl+'/checkTel', {params}));
  }

  async checkEmail(value: any, custId: any){
    let params = new HttpParams();
    if (custId) params = params.set('custId', custId);
    if (value) params = params.set('value', value);
    
    return firstValueFrom(this.http.get(this.baseUrl+'/checkEmail', {params}));
  }

  async checkIdno(value: any, custId: any){
    let params = new HttpParams();
    if (custId) params = params.set('custId', custId);
    if (value) params = params.set('value', value);
    
    return firstValueFrom(this.http.get(this.baseUrl+'/checkIdno', {params}));
  }

}

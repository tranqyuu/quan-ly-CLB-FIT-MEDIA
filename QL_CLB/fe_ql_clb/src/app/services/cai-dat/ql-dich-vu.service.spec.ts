import { TestBed } from '@angular/core/testing';

import { QlDichVuService } from './ql-dich-vu.service';

describe('QlDichVuService', () => {
  let service: QlDichVuService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QlDichVuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

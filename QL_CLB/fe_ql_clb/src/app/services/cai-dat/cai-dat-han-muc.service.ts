import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { BaseResponse, Cust, TransLimitCust } from 'src/app/model/model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CaiDatHanMucService {
  
  private baseUrl = environment.urlServer + '/api/translimitcust';

  constructor(
    private http: HttpClient,
    private toastrs: ToastrService) {
  }
  
  getCust(position: number): Observable<BaseResponse<Cust[]>> {
    let params = new HttpParams();
    params = params.set('position',position);
    return this.http.get<BaseResponse<Cust[]>>(this.baseUrl+'/getcust',{params});
  }

  getDetailTransLimitCust(data:Cust): Observable<BaseResponse<TransLimitCust[]>> {
    return this.http.post<BaseResponse<TransLimitCust[]>>(this.baseUrl + '/getbycust', data);
  }

  getCustByCode(code: string): Observable<Cust> {
    let params = new HttpParams();
    params = params.set('code',code);
    return this.http.get<Cust>(this.baseUrl + '/getcustbycode', {params});
  }

  update(data: TransLimitCust[]): Observable<BaseResponse<TransLimitCust>> {
    debugger
    return this.http.put<BaseResponse<TransLimitCust>>(this.baseUrl, data);
  }
}


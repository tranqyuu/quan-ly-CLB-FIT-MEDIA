import { TestBed } from '@angular/core/testing';

import { CaiDatHanMucService } from './cai-dat-han-muc.service';

describe('CaiDatHanMucService', () => {
  let service: CaiDatHanMucService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaiDatHanMucService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { QlMaTruyCapServiceService } from './ql-ma-truy-cap-service.service';

describe('QlMaTruyCapServiceService', () => {
  let service: QlMaTruyCapServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QlMaTruyCapServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

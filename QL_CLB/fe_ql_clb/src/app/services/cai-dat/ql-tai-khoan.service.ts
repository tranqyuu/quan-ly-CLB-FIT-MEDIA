import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { catchError, map, Observable, of } from 'rxjs';
import { FwError } from 'src/app/common/constants';
import { Account, BasePagingResponse, BaseResponse, Cust, CustAcc, PagesRequest } from 'src/app/model/model';
import { environment } from 'src/environments/environment';
import { BaseService } from '../BaseService';

@Injectable({
  providedIn: 'root'
})
export class QlTaiKhoanService implements BaseService<CustAcc>{

  private baseUrl = environment.urlServer + '/api/custacc';
  constructor(private http: HttpClient,private toastrs: ToastrService) {
  }
  searchSlect2(turn: string|null): Observable<CustAcc[]> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    params = params.set('page', 0);
    params = params.set('size', 10);
    if (turn) {
      params = params.set('code', turn);
      params.set('name', turn)
    }
    return this.http.get<CustAcc[]>(this.baseUrl, {
      params
    }).pipe(
      map((res:any) => {
        if(FwError.THANHCONG == res.errorCode){
          return res.data.content.map((item:any)=> {
            item.label=item.code+ (item.name ? ' - ' + item.name:'')
            item.track=item.code
            return item
          });
        }else{
          this.toastrs.error(res.errorMessage);
        }
      }),
      catchError(() => of([]))
    );
  }
  searchPaging(
    page: PagesRequest,
    custAcc: CustAcc
  ): Observable<BasePagingResponse<CustAcc>> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (custAcc.type) params =params.set('type', custAcc.type);
    return this.http.get<BasePagingResponse<CustAcc>>(this.baseUrl, {
      params
    });
  }

  searchCustSAccDd(id: string): Observable<BaseResponse<Account>> {//check acc đã sử dụng
    let params = new HttpParams();
    if (id) params =params.set('id', id);
    return this.http.get<BaseResponse<Account>>(this.baseUrl + "/sdd", {params});
  }

  searchCustTAccDd(id: string): Observable<BaseResponse<Account>> {//check acc đã sử dụng
    let params = new HttpParams();
    if (id) params =params.set('id', id);
    return this.http.get<BaseResponse<Account>>(this.baseUrl + "/tdd", {params});
  }

  searchCustAccFd(id: string): Observable<BaseResponse<Account>> {//check acc đã sử dụng
    let params = new HttpParams();
    if (id) params =params.set('id', id);
    return this.http.get<BaseResponse<Account>>(this.baseUrl + "/fd", {params});
  }

  searchCustAccLn(id: string): Observable<BaseResponse<Account>> {//check acc đã sử dụng
    let params = new HttpParams();
    if (id) params =params.set('id', id);
    return this.http.get<BaseResponse<Account>>(this.baseUrl + "/ln", {params});
  }

  getAccDd(): Observable<BaseResponse<Account>> {//get Acc
    let params = new HttpParams();
    return this.http.get<BaseResponse<Account>>(this.baseUrl + "/accdd");
  }

  getAccFd(): Observable<BaseResponse<Account>> {//get Acc
    let params = new HttpParams();
    return this.http.get<BaseResponse<Account>>(this.baseUrl + "/accfd");
  }

  getAccLn(): Observable<BaseResponse<Account>> {//get Acc
    let params = new HttpParams();
    return this.http.get<BaseResponse<Account>>(this.baseUrl + "/accln");
  }

  getDetail(
    id: any,
  ): Observable<BaseResponse<CustAcc>> {
    return this.http.get<BaseResponse<CustAcc>>(this.baseUrl+'/'+ id);
  }
  update(custAcc: CustAcc): Observable<BaseResponse<CustAcc>> {
    return this.http.put<BaseResponse<CustAcc>>(this.baseUrl, custAcc);
  }
  create(custAcc: CustAcc): Observable<BaseResponse<CustAcc>> {
    return this.http.post<BaseResponse<CustAcc>>(this.baseUrl, custAcc);
  }

}

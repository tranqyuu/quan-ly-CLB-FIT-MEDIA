import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse, Company } from 'src/app/model/model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private baseUrl = environment.urlServer + '/api/company';
  constructor(private http: HttpClient,) { }

  getDetailByCust(
    id: any,
  ): Observable<BaseResponse<Company>> {
    return this.http.get<BaseResponse<Company>>(this.baseUrl + '/getcompanybycustid/' + id);
  }

}

import { Directive, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Directive()
export class InInitComponent implements OnInit {
  public showModalEdit: boolean = false;
  public showModalView: boolean = false;

  public formError: { [key: string]: string } = {};
  public formEdit: FormGroup = new FormGroup({});
  public validationMessages: any = {};

  ngOnInit(): void { }

  public cleanFormError() {
    for (const field in this.formError) {
      this.formError[field] = '';
    }
  }

  public closeModalEdit(val: any) {
    this.showModalEdit = val;
    this.formEdit.reset();
  }

  // Validate Message
  public getValidationMessages() {
    return this.validationMessages;
  }

  // Validate
  public validateForm(form: FormGroup) {
    let checkOk = true;
    if (!form) {
      return false;
    }
    const errorMsg = this.getValidationMessages();
    for (const field in this.formError) {
      this.formError[field] = '';
      const control = form.get(field);
      if (control && control.invalid) {
        // @ts-ignore
        const messages = errorMsg[field];
        for (const key in control.errors) {
          if (messages.hasOwnProperty(key)) {
            // @ts-ignore
            this.formError[field] = errorMsg[field][key];
            checkOk = false;
          }
        }
      }
    }
    console.log(checkOk);
    return checkOk;
  }


}

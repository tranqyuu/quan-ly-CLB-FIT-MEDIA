import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BankReceiving, BaseResponse } from 'src/app/model/model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NganHangThuHuongService {

  private baseUrl = environment.urlServer + '/api/bankreceiving';
  constructor(private http: HttpClient,) { }

  getBankReceive(): Observable<BaseResponse<BankReceiving[]>> {
    return this.http
      .get<BaseResponse<BankReceiving[]>>
      (this.baseUrl + "/getallbankreceive");
  }
}

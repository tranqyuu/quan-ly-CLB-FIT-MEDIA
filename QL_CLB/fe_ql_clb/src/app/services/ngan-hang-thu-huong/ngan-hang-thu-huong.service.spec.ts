import { TestBed } from '@angular/core/testing';

import { NganHangThuHuongService } from './ngan-hang-thu-huong.service';

describe('NganHangThuHuongService', () => {
  let service: NganHangThuHuongService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NganHangThuHuongService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

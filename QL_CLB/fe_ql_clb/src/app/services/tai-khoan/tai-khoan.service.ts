import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse, Cust, CustAcc } from 'src/app/model/model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TaiKhoanService {


  private baseCusAccUrl = environment.urlServer + '/api/custacc';
  constructor(private http: HttpClient) { }


  getCusAccByName(findAcc?: string): Observable<CustAcc[]> {
    let params = new HttpParams();
    console.log('service');
    if (findAcc)
      params = params.set('findAcc', findAcc);

    return this.http
      .get<CustAcc[]>
      (this.baseCusAccUrl + "/getcusaccbyname", { params });
  }

  getCusAccById(cus: Cust): Observable<BaseResponse<CustAcc[]>> {
    return this.http
      .post<BaseResponse<CustAcc[]>>
      (this.baseCusAccUrl + "/getcusbyid", cus);
  }

  getCusAccByAcc(cusAcc: CustAcc): Observable<BaseResponse<Cust>> {
    return this.http
      .post<BaseResponse<Cust>>
      (this.baseCusAccUrl + "/getcusbyacc", cusAcc);
  }
}

import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { catchError, map, Observable, of } from 'rxjs';
import { commons } from 'src/app/common/commons';
import { FwError } from 'src/app/common/constants';
import { Activities, ActivitiesMembers, ActivitiesRequest, BasePagingResponse, BaseResponse, ChucVu, Csvc, Member, MemberRequest, PagesRequest, TaiChinh, TaiChinhRequest, Tran } from 'src/app/model/model';
import { CsvcSearch, TranSearch } from 'src/app/model/modelSearch';
import { environment } from 'src/environments/environment';
import { BaseService } from '../BaseService';


@Injectable({
  providedIn: 'root'
})
export class MemberService {
  private baseUrl = environment.urlServer + '/api/member';
  private baseTaiChinhUrl = environment.urlServer + '/api/taichinh';
  private baseCsvcUrl = environment.urlServer + '/api/csvc';
  private baseChucVuUrl = environment.urlServer + '/api/chucvu';
  private baseActivitiesUrl = environment.urlServer + '/api/activities';
  constructor(private http: HttpClient, private toastrs: ToastrService, private commons: commons) {
  }
  searchSlect2(turn: string | null): Observable<Member[]> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    params = params.set('page', 0);
    params = params.set('size', 10);
    if (turn) {
      params = params.set('code', turn);
      params.set('name', turn)
    }
    return this.http.get<Tran[]>(this.baseUrl, {
      params
    }).pipe(
      map((res: any) => {
        if (FwError.THANHCONG == res.errorCode) {
          return res.data.content.map((item: any) => {
            item.label = item.code + (item.name ? ' - ' + item.name : '')
            item.track = item.code
            return item
          });
        } else {
          this.toastrs.error(res.errorMessage);
        }
      }),
      catchError(() => of([]))
    );
  }

  getAllMembers(): Observable<BaseResponse<Member[]>> {
    return this.http.get<BaseResponse<Member[]>>(this.baseUrl + '/getallmembers');
  }

  getAllMembersRank(): Observable<BaseResponse<Member[]>> {
    return this.http.get<BaseResponse<Member[]>>(this.baseUrl + '/getallmembersrank');
  }

  searchPaging(
    page: PagesRequest,
    memberRequest: MemberRequest
  ): Observable<BasePagingResponse<Member>> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (memberRequest.fullname) params = params.set('fullname', memberRequest.fullname);
    if (memberRequest.schoolYear) params = params.set('schoolYear', memberRequest.schoolYear);
    if (memberRequest.location) params = params.set('location', memberRequest.location);
    if (memberRequest.status) params = params.set('status', memberRequest.status);


    return this.http.get<BasePagingResponse<Member>>(this.baseUrl, {
      params
    });
  }

  searchCsvc(
    page: PagesRequest,
    memberRequest: CsvcSearch
  ): Observable<BasePagingResponse<Csvc>> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (memberRequest.ma) params = params.set('ma', memberRequest.ma);
    if (memberRequest.status) params = params.set('status', memberRequest.status);
    // if (memberRequest.location) params = params.set('location', memberRequest.location);
    // if (memberRequest.status) params = params.set('status', memberRequest.status);


    return this.http.get<BasePagingResponse<Csvc>>(this.baseCsvcUrl, {
      params
    });
  }

  searchChucVu(
    page: PagesRequest,
    memberRequest: CsvcSearch
  ): Observable<BasePagingResponse<ChucVu>> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (memberRequest.ma) params = params.set('ma', memberRequest.ma);
    if (memberRequest.status) params = params.set('status', memberRequest.status);
    // if (memberRequest.location) params = params.set('location', memberRequest.location);
    // if (memberRequest.status) params = params.set('status', memberRequest.status);


    return this.http.get<BasePagingResponse<ChucVu>>(this.baseChucVuUrl, {
      params
    });
  }

  searchTaiChinh(
    page: PagesRequest,
    memberRequest: TaiChinhRequest
  ): Observable<BasePagingResponse<TaiChinh>> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    // if (memberRequest.ma) params = params.set('ma', memberRequest.ma);
    // if (memberRequest.status) params = params.set('status', memberRequest.status);
    // if (memberRequest.location) params = params.set('location', memberRequest.location);
    // if (memberRequest.status) params = params.set('status', memberRequest.status);


    return this.http.get<BasePagingResponse<TaiChinh>>(this.baseCsvcUrl, {
      params
    });
  }

  getAllChucVu(): Observable<BaseResponse<ChucVu[]>> {
    return this.http.get<BaseResponse<ChucVu[]>>(this.baseChucVuUrl + '/getall');
  }

  getTaiChinh(): Observable<BaseResponse<number>> {
    return this.http.get<BaseResponse<number>>(this.baseTaiChinhUrl + '/gettaichinh');
  }


  searchActivitiesPaging(
    page: PagesRequest,
    activitiesRequest: ActivitiesRequest
  ): Observable<BasePagingResponse<Activities>> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (activitiesRequest.ma) params = params.set('ma', activitiesRequest.ma);
    if (activitiesRequest.type) params = params.set('type', activitiesRequest.type);
    if (activitiesRequest.status) params = params.set('status', activitiesRequest.status);

    if (activitiesRequest.tuNgay) {
      let date = this.commons.toDateUri(activitiesRequest.tuNgay)
      if (date) params = params.set('tuNgay', date);
    }
    if (activitiesRequest.denNgay) {
      let date = this.commons.toDateUri(activitiesRequest.denNgay)
      if (date) params = params.set('denNgay', date);
    }


    return this.http.get<BasePagingResponse<Activities>>(this.baseActivitiesUrl, {
      params
    });
  }

  searchActivities(
  ): Observable<BaseResponse<Activities[]>> {
  
    return this.http.get<BaseResponse<Activities[]>>(this.baseActivitiesUrl + '/getallactivities');
  }

  searchActivitiesMembersPaging(
    page: PagesRequest,
    activitiesRequest: ActivitiesRequest
  ): Observable<BasePagingResponse<ActivitiesMembers>> {
    let params = new HttpParams();
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (activitiesRequest.id) params = params.set('id', activitiesRequest.id);
    if (activitiesRequest.membersId) params = params.set('membersId', activitiesRequest.membersId);
    // if (activitiesRequest.type) params = params.set('type', activitiesRequest.type);
    // if (activitiesRequest.status) params = params.set('status', activitiesRequest.status);

    console.log('paran');
    console.log(params);
    return this.http.get<BasePagingResponse<ActivitiesMembers>>(this.baseActivitiesUrl + '/activitiesmembers', {
      params
    });
  }


  searchActivitiesMembersAwardPaging(
    page: PagesRequest,
    activitiesRequest: ActivitiesRequest
  ): Observable<BasePagingResponse<ActivitiesMembers>> {
    let params = new HttpParams();
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (activitiesRequest.id) params = params.set('id', activitiesRequest.id);
    if (activitiesRequest.membersId) params = params.set('membersId', activitiesRequest.membersId);
    // if (activitiesRequest.type) params = params.set('type', activitiesRequest.type);
    // if (activitiesRequest.status) params = params.set('status', activitiesRequest.status);


    return this.http.get<BasePagingResponse<ActivitiesMembers>>(this.baseActivitiesUrl + '/activitiesmembersaward', {
      params
    });
  }

  searchTaiChinhPaging(
    page: PagesRequest,
    activitiesRequest: TaiChinhRequest
  ): Observable<BasePagingResponse<TaiChinh>> {
    let params = new HttpParams();
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (activitiesRequest.tuNgay) {
      let date = this.commons.toDateUri(activitiesRequest.tuNgay)
      if (date) params = params.set('tuNgay', date);
    }
    if (activitiesRequest.denNgay) {
      let date = this.commons.toDateUri(activitiesRequest.denNgay)
      if (date) params = params.set('denNgay', date);
    }
    // if (activitiesRequest.type) params = params.set('type', activitiesRequest.type);
    // if (activitiesRequest.status) params = params.set('status', activitiesRequest.status);


    return this.http.get<BasePagingResponse<TaiChinh>>(this.baseTaiChinhUrl, {
      params
    });
  }


  getDetail(
    id: any,
  ): Observable<BaseResponse<Member>> {
    return this.http.get<BaseResponse<Member>>(this.baseUrl + '/' + id);
  }

  deleteCsvc(
    id: any,
  ): Observable<BaseResponse<Csvc>> {
    return this.http.delete<BaseResponse<Csvc>>(this.baseCsvcUrl + '/' + id);
  }

  getDetailCsvc(
    id: any,
  ): Observable<BaseResponse<Csvc>> {
    return this.http.get<BaseResponse<Csvc>>(this.baseCsvcUrl + '/' + id);
  }

  getDetailChucVu(
    id: any,
  ): Observable<BaseResponse<ChucVu>> {
    return this.http.get<BaseResponse<ChucVu>>(this.baseChucVuUrl + '/' + id);
  }

  getDetailTaiChinh(
    id: any,
  ): Observable<BaseResponse<TaiChinh>> {
    return this.http.get<BaseResponse<TaiChinh>>(this.baseTaiChinhUrl + '/' + id);
  }

  getDetailActivities(
    id: any,
  ): Observable<BaseResponse<Activities>> {
    return this.http.get<BaseResponse<Activities>>(this.baseActivitiesUrl + '/' + id);
  }

  getDetailActivitiesMembers(
    id: any,
  ): Observable<BaseResponse<ActivitiesMembers>> {
    return this.http.get<BaseResponse<ActivitiesMembers>>(this.baseActivitiesUrl + '/activitiesmembersdetail/' + id);
  }

  updateActivitiesMembers(activitiesMembers: ActivitiesMembers): Observable<BaseResponse<ActivitiesMembers>> {
    return this.http.put<BaseResponse<ActivitiesMembers>>(this.baseActivitiesUrl + '/update', activitiesMembers);
  }

  updateCssvc(csvc: Csvc): Observable<BaseResponse<Csvc>> {
    return this.http.put<BaseResponse<Csvc>>(this.baseCsvcUrl, csvc);
  }

  updateChucVu(csvc: ChucVu): Observable<BaseResponse<ChucVu>> {
    return this.http.put<BaseResponse<ChucVu>>(this.baseChucVuUrl, csvc);
  }

  updateTaiChinh(csvc: TaiChinh): Observable<BaseResponse<TaiChinh>> {
    return this.http.put<BaseResponse<TaiChinh>>(this.baseTaiChinhUrl, csvc);
  }



  updateActivities(activities: Activities): Observable<BaseResponse<Activities>> {
    return this.http.put<BaseResponse<Activities>>(this.baseActivitiesUrl, activities);
  }

  updateMember(member: Member): Observable<BaseResponse<Member>> {
    return this.http.put<BaseResponse<Member>>(this.baseUrl, member);
  }


  update(tran: Tran): Observable<BaseResponse<Tran>> {
    return this.http.put<BaseResponse<Tran>>(this.baseUrl, tran);
  }

  create(member: Member): Observable<BaseResponse<Member>> {
    return this.http.post<BaseResponse<Member>>(this.baseUrl, member);
  }

  createCsvc(csvc: Csvc): Observable<BaseResponse<Csvc>> {
    return this.http.post<BaseResponse<Csvc>>(this.baseCsvcUrl, csvc);
  }

  createChucVu(csvc: ChucVu): Observable<BaseResponse<ChucVu>> {
    return this.http.post<BaseResponse<ChucVu>>(this.baseChucVuUrl, csvc);
  }

  createTaiChinh(csvc: TaiChinh): Observable<BaseResponse<TaiChinh>> {
    return this.http.post<BaseResponse<TaiChinh>>(this.baseTaiChinhUrl, csvc);
  }

  createActivities(activities: Activities): Observable<BaseResponse<Activities>> {
    return this.http.post<BaseResponse<Activities>>(this.baseActivitiesUrl, activities);
  }

  createActivitiesMembers(activities: ActivitiesMembers): Observable<BaseResponse<ActivitiesMembers>> {
    return this.http.post<BaseResponse<ActivitiesMembers>>(this.baseActivitiesUrl + '/createactivitiesmembers', activities);
  }

  downloadPDF(tran: Tran) {
    this.http.post(this.baseUrl + '/chung-tu', tran,
      { responseType: 'blob', observe: 'response' }).subscribe((blob: any) => {

        var fileName: string = "chungTu";
        fileName = fileName + ".pdf";
        FileSaver.saveAs(blob.body, fileName);
        console.log(blob);

      });
  }

  exportExcel(tranSearch: TranSearch, page: PagesRequest) {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    this.http.post(this.baseUrl + '/export', tranSearch,
      { responseType: 'blob', observe: 'response' }).subscribe((blob: any) => {

        var fileName: string = "Danh sách lệnh chuyển tiền thông thường";
        fileName = fileName + blob.headers.get('export');
        FileSaver.saveAs(blob.body, fileName);
      });
  }

  downloadGdExcel(tranSearch: TranSearch, page: PagesRequest) {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');

    this.http.post(this.baseUrl + '/downloadgiaodich', tranSearch,
      { responseType: 'blob', observe: 'response', params: params }).subscribe((blob: any) => {

        var fileName: string = "Danh sách giao dịch chờ duyệt";
        fileName = fileName + blob.headers.get('export');
        FileSaver.saveAs(blob.body, fileName);
        console.log(blob);

      });
  }

  getAllActivitiesMember(
    id: any,
  ): Observable<BaseResponse<Member[]>> {
    return this.http.get<BaseResponse<Member[]>>(this.baseActivitiesUrl + '/getall/' + id);
  }


  exportActivities(tranSearch: ActivitiesRequest, page: PagesRequest) {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');

    this.http.post(this.baseActivitiesUrl + '/export', tranSearch,
      { responseType: 'blob', observe: 'response', params: params }).subscribe((blob: any) => {

        var fileName: string = "Danh sách hoạt động";
        fileName = fileName + blob.headers.get('listbangke');
        FileSaver.saveAs(blob.body, fileName);
        console.log(blob);

      });
  }

  exportTaiChinh(tranSearch: TaiChinhRequest, page: PagesRequest) {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');

    this.http.post(this.baseTaiChinhUrl + '/export', tranSearch,
      { responseType: 'blob', observe: 'response', params: params }).subscribe((blob: any) => {

        var fileName: string = "Danh sách tài chính";
        fileName = fileName + blob.headers.get('listbangke');
        FileSaver.saveAs(blob.body, fileName);
        console.log(blob);

      });
  }
}

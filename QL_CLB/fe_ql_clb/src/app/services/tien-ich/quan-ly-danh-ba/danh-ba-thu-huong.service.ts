import { Injectable } from '@angular/core';
import { BaseService } from '../../BaseService';
import { BankReceiving, BasePagingResponse, BaseResponse, CustContact, PagesRequest } from 'src/app/model/model';
import { Observable, catchError, map, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { FwError } from 'src/app/common/constants';

@Injectable({
  providedIn: 'root'
})
export class DanhBaThuHuongService implements BaseService<CustContact>{

  private baseUrl = environment.urlServer + '/api/custcontact';
  constructor(
    private http: HttpClient,
    private toastrs: ToastrService) { }

  searchSlect2(turn: string | null): Observable<CustContact[]> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    params = params.set('page', 0);
    params = params.set('size', 10);
    if (turn) {
      params = params.set('code', turn);
      params.set('name', turn)
    }
    return this.http.get<CustContact[]>(this.baseUrl, {
      params
    }).pipe(
      map((res: any) => {
        if (FwError.THANHCONG == res.errorCode) {
          return res.data.content.map((item: any) => {
            item.label = item.code + (item.name ? ' - ' + item.name : '')
            item.track = item.code
            return item
          });
        } else {
          this.toastrs.error(res.errorMessage);
        }
      }),
      catchError(() => of([]))
    );
  }

  searchPaging(page: PagesRequest, custContact: CustContact): Observable<BasePagingResponse<CustContact>> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (custContact.keyWord) params = params.set('keyWord', custContact.keyWord);
    return this.http.get<BasePagingResponse<CustContact>>(this.baseUrl, { params });
  }

  getDetail(
    id: any,
  ): Observable<BaseResponse<CustContact>> {
    return this.http.get<BaseResponse<CustContact>>(this.baseUrl + '/' + id);
  }
  update(custContact: CustContact): Observable<BaseResponse<CustContact>> {
    return this.http.put<BaseResponse<CustContact>>(this.baseUrl, custContact);
  }
  create(custContact: CustContact): Observable<BaseResponse<CustContact>> {
    return this.http.post<BaseResponse<CustContact>>(this.baseUrl, custContact);
  }
  getDsNH(): Observable<BankReceiving[]> {
    return this.http.get<BankReceiving[]>(this.baseUrl + '/getDsBank');
  }
  deleteItem(id: any): Observable<BaseResponse<CustContact>> {
    return this.http.delete<BaseResponse<CustContact>>(this.baseUrl + '/' + id);
  }

  //quandev
  getCustContact(product: string, custId: number): Observable<BaseResponse<CustContact[]>> {

    let params = new HttpParams();
    params = params.set('productValue', product);
    params = params.set('custId', custId);

    return this.http.get<BaseResponse<CustContact[]>>(this.baseUrl + "/getcustcontact", {
      params
    });
  }
  //end quandev
}

import { TestBed } from '@angular/core/testing';

import { DanhBaThuHuongService } from './danh-ba-thu-huong.service';

describe('DanhBaThuHuongService', () => {
  let service: DanhBaThuHuongService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DanhBaThuHuongService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

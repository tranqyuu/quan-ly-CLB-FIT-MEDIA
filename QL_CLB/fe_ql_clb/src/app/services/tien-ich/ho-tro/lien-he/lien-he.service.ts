import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse, ContactInfo } from 'src/app/model/model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LienHeService {
  private baseUrl = environment.urlServer + '/api/auth';
  constructor(    private http: HttpClient) { }

  getDetail(): Observable<BaseResponse<ContactInfo>> {
    return this.http.get<BaseResponse<ContactInfo>>(this.baseUrl+'/getContactInfo');
  }
}

import { TestBed } from '@angular/core/testing';

import { DieuKhoanDichVuService } from './dieu-khoan-dich-vu.service';

describe('DieuKhoanDichVuService', () => {
  let service: DieuKhoanDichVuService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DieuKhoanDichVuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

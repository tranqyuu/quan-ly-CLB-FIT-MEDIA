import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse, TermsOfUse } from 'src/app/model/model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DieuKhoanDichVuService {
  private baseUrl = environment.urlServer + '/api/auth';
  constructor(private http: HttpClient) { }

  getAllDk():Observable<BaseResponse<TermsOfUse[]>>{
    return this.http.get<BaseResponse<TermsOfUse[]>>(this.baseUrl+'/getdk');
  }

  getDetail(
    id: any,
  ): Observable<BaseResponse<TermsOfUse>> {
    return this.http.get<BaseResponse<TermsOfUse>>(this.baseUrl+'/getdetail'+'/' + id);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { BaseResponse, Cust, UpdatePasswordRequest } from 'src/app/model/model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DoiMatKhauService {
  private baseUrl = environment.urlServer + '/api/cust';
  constructor(
    private http: HttpClient,
    private toastrs: ToastrService) { }

  updatePassword(data: UpdatePasswordRequest): Observable<BaseResponse<Cust>> {
      return this.http.post<BaseResponse<Cust>>(this.baseUrl + '/updatepassword', data);
  }  
}

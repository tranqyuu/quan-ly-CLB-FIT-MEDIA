import { TestBed } from '@angular/core/testing';

import { DoiMatKhauService } from './doi-mat-khau.service';

describe('DoiMatKhauService', () => {
  let service: DoiMatKhauService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DoiMatKhauService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

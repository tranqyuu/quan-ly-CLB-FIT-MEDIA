import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { catchError, map, Observable, of } from 'rxjs';
import { commons } from 'src/app/common/commons';
import { FwError } from 'src/app/common/constants';
import { BasePagingResponse, BaseResponse, PagesRequest, Tran } from 'src/app/model/model';
import { TranSearch } from 'src/app/model/modelSearch';
import { environment } from 'src/environments/environment';
import { BaseService } from '../BaseService';

@Injectable({
  providedIn: 'root'
})
export class TranService implements BaseService<Tran>{

  private baseUrl = environment.urlServer + '/api/tran';
  constructor(private http: HttpClient, private toastrs: ToastrService, private commons: commons) {
  }
  searchSlect2(turn: string | null): Observable<Tran[]> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    params = params.set('page', 0);
    params = params.set('size', 10);
    if (turn) {
      params = params.set('code', turn);
      params.set('name', turn)
    }
    return this.http.get<Tran[]>(this.baseUrl, {
      params
    }).pipe(
      map((res: any) => {
        if (FwError.THANHCONG == res.errorCode) {
          return res.data.content.map((item: any) => {
            item.label = item.code + (item.name ? ' - ' + item.name : '')
            item.track = item.code
            return item
          });
        } else {
          this.toastrs.error(res.errorMessage);
        }
      }),
      catchError(() => of([]))
    );
  }

  searchPaging(
    page: PagesRequest,
    tranSearch: TranSearch
  ): Observable<BasePagingResponse<Tran>> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (tranSearch.type) params = params.set('type', tranSearch.type);
    if (tranSearch.schedule) params = params.set('schedule', tranSearch.schedule);
    if (tranSearch.taiKhoanNguon) params = params.set('taiKhoanNguon', tranSearch.taiKhoanNguon);
    if (tranSearch.taiKhoanThuHuong) params = params.set('taiKhoanThuHuong', tranSearch.taiKhoanThuHuong);
    if (tranSearch.tenNguoiThuHuong) params = params.set('tenNguoiThuHuong', tranSearch.tenNguoiThuHuong);
    if (tranSearch.nganHangThuHuong) params = params.set('nganHangThuHuong', tranSearch.nganHangThuHuong);
    if (tranSearch.thoiGianLapLenhTu) {
      let date = this.commons.toDateUri(tranSearch.thoiGianLapLenhTu)
      if (date) params = params.set('thoiGianLapLenhTu', date);
    }
    if (tranSearch.thoiGianLapLenhDen) {
      let date = this.commons.toDateUri(tranSearch.thoiGianLapLenhDen)
      if (date) params = params.set('thoiGianLapLenhDen', date);
    }
    if (tranSearch.thoiGianDuyetLenhTu) {
      let date = this.commons.toDateUri(tranSearch.thoiGianDuyetLenhTu)
      if (date) params = params.set('thoiGianDuyetLenhTu', date);
    }
    if (tranSearch.thoiGianDuyetLenhDen) {
      let date = this.commons.toDateUri(tranSearch.thoiGianDuyetLenhDen)
      if (date) params = params.set('thoiGianDuyetLenhDen', date);
    }
    if (tranSearch.userLapLenh) params = params.set('userLapLenh', tranSearch.userLapLenh);
    if (tranSearch.userDuyetLenh) params = params.set('userDuyetLenh', tranSearch.userDuyetLenh);
    if (tranSearch.status) params = params.set('status', tranSearch.status);
    if (tranSearch.maGiaoDich) params = params.set('maGiaoDich', tranSearch.maGiaoDich);
    if (tranSearch.khoangTienTu) params = params.set('khoangTienTu', tranSearch.khoangTienTu);
    if (tranSearch.khoangTienDen) params = params.set('khoangTienDen', tranSearch.khoangTienDen);

    return this.http.get<BasePagingResponse<Tran>>(this.baseUrl, {
      params
    });
  }


  getDetail(
    id: any,
  ): Observable<BaseResponse<Tran>> {
    return this.http.get<BaseResponse<Tran>>(this.baseUrl + '/' + id);
  }
  update(tran: Tran): Observable<BaseResponse<Tran>> {
    return this.http.put<BaseResponse<Tran>>(this.baseUrl, tran);
  }
  create(tran: Tran): Observable<BaseResponse<Tran>> {
    return this.http.post<BaseResponse<Tran>>(this.baseUrl, tran);
  }

  downloadPDF(tran: Tran) {
    this.http.post(this.baseUrl + '/chung-tu', tran,
      { responseType: 'blob', observe: 'response' }).subscribe((blob: any) => {

        var fileName: string = "chungTu";
        fileName = fileName + ".pdf";
        FileSaver.saveAs(blob.body, fileName);
        console.log(blob);

      });
  }

  exportExcel(tranSearch: TranSearch, page: PagesRequest) {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    this.http.post(this.baseUrl + '/export', tranSearch,
      { responseType: 'blob', observe: 'response' }).subscribe((blob: any) => {

        var fileName: string = "Danh sách lệnh chuyển tiền thông thường";
        fileName = fileName + blob.headers.get('export');
        FileSaver.saveAs(blob.body, fileName);
      });
  }

  downloadGdExcel(tranSearch: TranSearch, page: PagesRequest) {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');

    this.http.post(this.baseUrl + '/export', tranSearch,
      { responseType: 'blob', observe: 'response', params: params }).subscribe((blob: any) => {

        var fileName: string = "Danh sách giao dịch chờ duyệt";
        fileName = fileName + blob.headers.get('export');
        FileSaver.saveAs(blob.body, fileName);
        console.log(blob);

      });
  }
}

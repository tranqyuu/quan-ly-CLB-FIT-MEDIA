import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { catchError, map, Observable, of } from 'rxjs';
import { commons } from 'src/app/common/commons';
import { FwError } from 'src/app/common/constants';
import { BasePagingResponse, BaseResponse, PagesRequest, TransSchedule } from 'src/app/model/model';
import { TranSearch } from 'src/app/model/modelSearch';
import { environment } from 'src/environments/environment';
import { BaseService } from '../BaseService';

@Injectable({
  providedIn: 'root'
})
export class TransScheduleService implements BaseService<TransSchedule>{

  private baseUrl = environment.urlServer + '/api/transschedule';
  constructor(private http: HttpClient, private toastrs: ToastrService, private commons: commons) {
  }
  searchSlect2(turn: string | null): Observable<TransSchedule[]> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    params = params.set('page', 0);
    params = params.set('size', 10);
    if (turn) {
      params = params.set('code', turn);
      params.set('name', turn)
    }
    return this.http.get<TransSchedule[]>(this.baseUrl, {
      params
    }).pipe(
      map((res: any) => {
        if (FwError.THANHCONG == res.errorCode) {
          return res.data.content.map((item: any) => {
            item.label = item.code + (item.name ? ' - ' + item.name : '')
            item.track = item.code
            return item
          });
        } else {
          this.toastrs.error(res.errorMessage);
        }
      }),
      catchError(() => of([]))
    );
  }

  searchPaging(
    page: PagesRequest,
    tranSearch: TranSearch
  ): Observable<BasePagingResponse<TransSchedule>> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (tranSearch.type) params = params.set('type', tranSearch.type);
    if (tranSearch.schedule) params = params.set('schedule', tranSearch.schedule);
    if (tranSearch.taiKhoanNguon) params = params.set('taiKhoanNguon', tranSearch.taiKhoanNguon);
    if (tranSearch.taiKhoanThuHuong) params = params.set('taiKhoanThuHuong', tranSearch.taiKhoanThuHuong);
    if (tranSearch.tenNguoiThuHuong) params = params.set('tenNguoiThuHuong', tranSearch.tenNguoiThuHuong);
    if (tranSearch.thoiGianLapLenhTu) {
      let date = this.commons.toDateUri(tranSearch.thoiGianLapLenhTu)
      if (date) params = params.set('thoiGianLapLenhTu', date);
    }
    if (tranSearch.thoiGianLapLenhDen) {
      let date = this.commons.toDateUri(tranSearch.thoiGianLapLenhDen)
      if (date) params = params.set('thoiGianLapLenhDen', date);
    }
    if (tranSearch.thoiGianDuyetLenhTu) {
      let date = this.commons.toDateUri(tranSearch.thoiGianDuyetLenhTu)
      if (date) params = params.set('thoiGianDuyetLenhTu', date);
    }
    if (tranSearch.thoiGianDuyetLenhDen) {
      let date = this.commons.toDateUri(tranSearch.thoiGianDuyetLenhDen)
      if (date) params = params.set('thoiGianDuyetLenhDen', date);
    }
    if (tranSearch.userLapLenh) params = params.set('userLapLenh', tranSearch.userLapLenh);
    if (tranSearch.userDuyetLenh) params = params.set('userDuyetLenh', tranSearch.userDuyetLenh);
    if (tranSearch.status) params = params.set('status', tranSearch.status);
    if (tranSearch.maGiaoDich) params = params.set('maGiaoDich', tranSearch.maGiaoDich);
    if (tranSearch.khoangTienTu) params = params.set('khoangTienTu', tranSearch.khoangTienTu);
    if (tranSearch.khoangTienDen) params = params.set('khoangTienDen', tranSearch.khoangTienDen);
    return this.http.get<BasePagingResponse<TransSchedule>>(this.baseUrl, {
      params
    });
  }

  //quandev
  searchPagings(
    page: PagesRequest,
    tranSearch: TranSearch
  ): Observable<BasePagingResponse<TransSchedule>> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    if (tranSearch.type) params = params.set('type', tranSearch.type);
    if (tranSearch.schedule) params = params.set('schedule', tranSearch.schedule);
    if (tranSearch.taiKhoanNguon) params = params.set('taiKhoanNguon', tranSearch.taiKhoanNguon);
    if (tranSearch.taiKhoanThuHuong) params = params.set('taiKhoanThuHuong', tranSearch.taiKhoanThuHuong);
    if (tranSearch.tenNguoiThuHuong) params = params.set('tenNguoiThuHuong', tranSearch.tenNguoiThuHuong);
    if (tranSearch.thoiGianLapLenhTu) {
      let date = this.commons.toDateUri(tranSearch.thoiGianLapLenhTu)
      if (date) params = params.set('thoiGianLapLenhTu', date);
    }
    if (tranSearch.thoiGianLapLenhDen) {
      let date = this.commons.toDateUri(tranSearch.thoiGianLapLenhDen)
      if (date) params = params.set('thoiGianLapLenhDen', date);
    }
    if (tranSearch.thoiGianDuyetLenhTu) {
      let date = this.commons.toDateUri(tranSearch.thoiGianDuyetLenhTu)
      if (date) params = params.set('thoiGianDuyetLenhTu', date);
    }
    if (tranSearch.thoiGianDuyetLenhDen) {
      let date = this.commons.toDateUri(tranSearch.thoiGianDuyetLenhDen)
      if (date) params = params.set('thoiGianDuyetLenhDen', date);
    }
    if (tranSearch.userLapLenh) params = params.set('userLapLenh', tranSearch.userLapLenh);
    if (tranSearch.userDuyetLenh) params = params.set('userDuyetLenh', tranSearch.userDuyetLenh);
    if (tranSearch.status) params = params.set('status', tranSearch.status);
    if (tranSearch.maGiaoDich) params = params.set('maGiaoDich', tranSearch.maGiaoDich);
    if (tranSearch.khoangTienTu) params = params.set('khoangTienTu', tranSearch.khoangTienTu);
    if (tranSearch.khoangTienDen) params = params.set('khoangTienDen', tranSearch.khoangTienDen);
    return this.http.get<BasePagingResponse<TransSchedule>>(this.baseUrl, {
      params
    });
  }

  //end quandev

  getDetail(
    id: any,
  ): Observable<BaseResponse<TransSchedule>> {
    return this.http.get<BaseResponse<TransSchedule>>(this.baseUrl + '/' + id);
  }
  update(transSchedule: TransSchedule): Observable<BaseResponse<TransSchedule>> {
    return this.http.put<BaseResponse<TransSchedule>>(this.baseUrl, transSchedule);
  }
  create(transSchedule: TransSchedule): Observable<BaseResponse<TransSchedule>> {
    return this.http.post<BaseResponse<TransSchedule>>(this.baseUrl, transSchedule);
  }

  exportExcel(tranSearch: TranSearch, page: PagesRequest) {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    // if (page.curentPage) params = params.set('page', page.curentPage - 1);
    // if (page.size) params = params.set('size', page.size);

    this.http.post(this.baseUrl + '/export', tranSearch,
      { responseType: 'blob', observe: 'response' }).subscribe((blob: any) => {

        var fileName: string = "Danh sách lệnh chuyển tiền tương lai/ định kỳ";
        fileName = fileName + blob.headers.get('export');
        FileSaver.saveAs(blob.body, fileName);
      });
  }

  downloadGdExcel(tranSearch: TranSearch, page: PagesRequest) {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    if (page.curentPage) params = params.set('page', page.curentPage - 1);
    if (page.size) params = params.set('size', page.size);

    this.http.post(this.baseUrl + '/downloadgiaodich', tranSearch,
      { responseType: 'blob', observe: 'response', params: params }).subscribe((blob: any) => {

        var fileName: string = "Danh sách giao dịch chờ duyệt";
        fileName = fileName + blob.headers.get('export');
        FileSaver.saveAs(blob.body, fileName);
        console.log(blob);

      });
  }
}


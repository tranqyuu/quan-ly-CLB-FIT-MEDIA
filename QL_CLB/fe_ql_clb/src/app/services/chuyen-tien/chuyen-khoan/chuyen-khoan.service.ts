import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse, Cust, CustAcc, Tran, Trans, TransLimitCust, TransRequest, TransSchedule } from 'src/app/model/model';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ChuyenKhoanService {

    private baseUrl = environment.urlServer + '/api/tran';
    private baseUrlSchedule = environment.urlServer + '/api/transschedule';
    private baseCusAccUrl = environment.urlServer + '/api/custacc';
    private baseTransLimitCustUrl = environment.urlServer + '/api/translimitcust';
    constructor(private http: HttpClient,) { }

    saveTrans(itemTrans: TransRequest): Observable<BaseResponse<Tran>> {
        return this.http
            .post<BaseResponse<Tran>>
            (this.baseUrl + "/createtrans", itemTrans);
    }


    saveTranSchedule(itemTrans: TransRequest): Observable<BaseResponse<Trans>> {
        return this.http
            .post<BaseResponse<Trans>>
            (this.baseUrlSchedule + "/createtranschedule", itemTrans);
    }


    getCusAccById(cus: Cust): Observable<BaseResponse<CustAcc[]>> {
        return this.http
            .post<BaseResponse<CustAcc[]>>
            (this.baseCusAccUrl + "/getcusbyid", cus);
    }

    getCusAccByAcc(cusAcc: CustAcc): Observable<BaseResponse<Cust>> {
        return this.http
            .post<BaseResponse<Cust>>
            (this.baseCusAccUrl + "/getcusbyacc", cusAcc);
    }


    updateTrans(itemTrans: Tran): Observable<BaseResponse<Tran>> {
        return this.http
            .put<BaseResponse<Tran>>
            (this.baseUrl, itemTrans);
    }


    updateTranSchedule(itemTrans: TransSchedule): Observable<BaseResponse<TransSchedule>> {
        return this.http
            .put<BaseResponse<TransSchedule>>
            (this.baseUrlSchedule, itemTrans);
    }

    getCustTrans(custId: number, type: string): Observable<BaseResponse<number>> {

        let params = new HttpParams();
        params = params.set('id', custId);
        params = params.set('type', type);

        return this.http
            .get<BaseResponse<number>>
            (this.baseUrl + "/custtrans/", { params: params });
    }

    getTransLimitByCust(custId: number, product: string): Observable<BaseResponse<TransLimitCust>> {

        let params = new HttpParams();
        params = params.set('custId', custId);
        params = params.set('product', product);

        return this.http
            .get<BaseResponse<TransLimitCust>>
            (this.baseTransLimitCustUrl + "/tranlimitbycust", { params: params });
    }

}

import { TestBed } from '@angular/core/testing';

import { TransScheduleDetailService } from './trans-schedule-detail.service';

describe('TransScheduleDetailService', () => {
  let service: TransScheduleDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransScheduleDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

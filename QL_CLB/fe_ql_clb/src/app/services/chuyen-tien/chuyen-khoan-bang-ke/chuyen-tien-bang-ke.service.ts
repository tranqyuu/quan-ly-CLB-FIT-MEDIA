import { Injectable } from '@angular/core';
import { BaseService } from '../../BaseService';
import { BasePagingResponse, BaseResponse, PagesRequest, TransLot } from 'src/app/model/model';
import { environment } from 'src/environments/environment';
import { Observable, catchError, map, of } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { FwError } from 'src/app/common/constants';
import * as FileSaver from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ChuyenTienBangKeService implements BaseService<TransLot>{

  private baseUrl = environment.urlServer + '/api/translot';
  
  constructor(
    private http: HttpClient,
    private toastrs: ToastrService) { }

  searchSlect2(turn: string | null): Observable<TransLot[]> {
    let params = new HttpParams();
    params = params.set('sort', 'createdAt,desc');
    params = params.set('page', 0);
    params = params.set('size', 10);
    if(turn){
      params = params.set('code', turn);
      params.set('name', turn)
    }
    return this.http.get<TransLot[]>(this.baseUrl, {
      params
    }).pipe(
      map((res:any) => {
        if(FwError.THANHCONG == res.errorCode){
          return res.data.content.map((item:any) => {
            item.label = item.code + (item.name ? ' - ' + item.name:'')
            item.track = item.code
            return item
          });
        }else{
          this.toastrs.error(res.errorMessage);
        }
      }),
      catchError(() => of([]))
  );
}

searchPaging(
  page: PagesRequest,
  translot: TransLot
):Observable<BasePagingResponse<TransLot>>{
  let params = new HttpParams();
  params = params.set('sort', 'createdAt,desc');
  if (page.curentPage) params = params.set('page', page.curentPage - 1);
  if (page.size) params = params.set('size', page.size);

  if(translot.status) params = params.set('status',translot.status);
  if(translot.startAmount) params = params.set('startAmount', translot.startAmount);
  if(translot.endAmount) params = params.set('endAmount', translot.endAmount); 
  if(translot.startDate){
    let date = new Date(translot.startDate);
    let startDate = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
     params.set('startDate',startDate);
  }
  if(translot.endDate){
    let date = new Date(translot.endDate);
    let endDate = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
    params.set('endDate', endDate);
  }
  return this.http.get<BasePagingResponse<TransLot>>(this.baseUrl,{params});
}

getDetail(
  id: any,
): Observable<BaseResponse<TransLot>> {
  return this.http.get<BaseResponse<TransLot>>(this.baseUrl+'/'+ id);
}

update(transLot: TransLot): Observable<BaseResponse<TransLot>> {
  return this.http.put<BaseResponse<TransLot>>(this.baseUrl, transLot);
}
create(transLot: TransLot): Observable<BaseResponse<TransLot>> {
  return this.http.post<BaseResponse<TransLot>>(this.baseUrl, transLot);
}

download() {
  this.http.get(this.baseUrl + '/download', { responseType: 'blob', observe: 'response'}).subscribe(
      (response : any) => {
          console.log(response);
          var mediaType = 'application/octet-stream';
          var blob = new Blob([response], { type: mediaType });
          FileSaver.saveAs(blob, "TemplateBangKe.xlsx");
      },
      (error: HttpErrorResponse) => {
          console.log(error);
      });
}

import(formData: FormData): Observable<BaseResponse<TransLot>> {
  return this.http.post<BaseResponse<TransLot>>(this.baseUrl +'/import', formData);
}

exportExcel(transLot: TransLot,page: PagesRequest){
  this.http.post(this.baseUrl +'/export', transLot,
   { responseType: 'blob', observe: 'response'}).subscribe((blob:any) => {

    var fileName: string = "bang-ke";
    fileName = fileName + blob.headers.get('listbangke');
    FileSaver.saveAs(blob.body, fileName);
    console.log(blob);
    
  });
}

downloadBangKe(){
  this.http.post(this.baseUrl +'/downloadbangke',
   { responseType: 'blob', observe: 'response'}).subscribe((blob:any) => {
    console.log(blob);
    var fileName: string = "Template";
    fileName = fileName + blob.headers.get('TemplateBangKe');
    FileSaver.saveAs(blob.body, fileName);
    console.log(blob);
    
  });
}

}
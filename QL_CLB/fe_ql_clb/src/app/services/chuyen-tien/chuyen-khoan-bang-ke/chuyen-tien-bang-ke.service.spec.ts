import { TestBed } from '@angular/core/testing';

import { ChuyenTienBangKeService } from './chuyen-tien-bang-ke.service';

describe('ChuyenTienBangKeService', () => {
  let service: ChuyenTienBangKeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChuyenTienBangKeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { TransScheduleService } from './trans-schedule.service';

describe('TransScheduleService', () => {
  let service: TransScheduleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransScheduleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

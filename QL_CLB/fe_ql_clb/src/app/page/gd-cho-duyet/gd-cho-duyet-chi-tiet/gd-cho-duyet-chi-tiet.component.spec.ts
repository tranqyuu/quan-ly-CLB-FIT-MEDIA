import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GdChoDuyetChiTietComponent } from './gd-cho-duyet-chi-tiet.component';

describe('GdChoDuyetChiTietComponent', () => {
  let component: GdChoDuyetChiTietComponent;
  let fixture: ComponentFixture<GdChoDuyetChiTietComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GdChoDuyetChiTietComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GdChoDuyetChiTietComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { FwError } from 'src/app/common/constants';
import { Activities, ActivitiesRequest, Member, PagesRequest, PhotosModel, Tran } from 'src/app/model/model';
import { MemberService } from 'src/app/services/member/member.service';
import { BaseComponent } from '../base/BaseComponent';
import {
  ApexAxisChartSeries,
  ApexChart,
  ChartComponent,
  ApexDataLabels,
  ApexXAxis,
  ApexPlotOptions,
  ApexStroke,
  ApexTitleSubtitle,
  ApexYAxis,
  ApexTooltip,
  ApexFill,
  ApexLegend
} from "ng-apexcharts";
import { map } from 'rxjs';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
  tooltip: ApexTooltip;
  fill: ApexFill;
  legend: ApexLegend;
};

@Component({
  selector: 'app-trang-chu',
  templateUrl: './trang-chu.component.html',
  styleUrls: [
    './trang-chu.component.css',
  ],
  encapsulation: ViewEncapsulation.None,
})
export class TrangChuComponent extends BaseComponent<Tran>  {
  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  slidesData: PhotosModel[] = [
    { id: '1', url: 'assets/banner_1.png' },
    { id: '2', url: 'assets/banner_1.png' },
    { id: '3', url: 'assets/banner_1.png' },
  ];
  customOptions: OwlOptions = {
    loop: true,
    autoplay: true,
    center: true,
    dotsEach: true,
    autoHeight: true,
    autoWidth: true,
    autoplayHoverPause: true,
    autoplayMouseleaveTimeout: 500,
    autoplaySpeed: 1000,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 1,
      },
    },
  };

  labelsTran: any = {
    previousLabel: 'Trước',
    nextLabel: 'Sau',
  };

  labelsTranSchedule: any = {
    previousLabel: 'Trước',
    nextLabel: 'Sau',
  };
  constructor(private memberServices: MemberService, private injector: Injector,) {
    super(injector);
    this.pageTrans.curentPage = 1;
    this.pageTrans.size = 10;
    this.pageTransSchedule.curentPage = 1;
    this.pageTransSchedule.size = 10;
  }

  taiChinh: number;
  haNoiMembers: number;
  haNamMembers: number;
  asyncDataTran: Activities[];
  totalTran: number = 0;
  maxSizeTran: number = 10;
  ngOnInit(
  ) {
    this.getTaiChinh();
    this.getMembers();
    this.getMembersRank();
    this.searchActivities(1);
    this.getAllActivities();
  }
  pageTrans: PagesRequest = new PagesRequest();
  pageTransSchedule: PagesRequest = new PagesRequest();

  getTaiChinh() {
    this.memberServices
      .getTaiChinh()
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.taiChinh = res.data;
          }
        }
      });
  }

  getCurrentTranPageNumber(index: number) {
    if (this.pageTrans.size) {
      return (this.pageTrans.curentPage - 1) * this.pageTrans.size + index;
    }
    return 0;
  }

  activitiesRequest: ActivitiesRequest = new ActivitiesRequest();

  members: Member[] = [];

  getMembers() {
    this.haNamMembers = 0;
    this.haNoiMembers = 0;
    this.memberServices
      .getAllMembers()
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            res.data.forEach(item => {
              if (item.location == "0") {
                this.haNoiMembers = this.haNoiMembers + 1;
              } else {
                this.haNamMembers = this.haNamMembers + 1;
              }
            });
          }
        }
      });
  }

  getMembersRank() {

    this.memberServices
      .getAllMembersRank()
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.members = res.data;
          }
        }
      });
  }

  totalDone: number =0;
  totalUndone: number=0;

  searchActivities(page: any) {
  
    this.pageTrans.curentPage = page;
    this.memberServices
      .searchActivitiesPaging(this.pageTrans, this.activitiesRequest)
     
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {

          if (res.data?.totalElements) this.totalTran = res.data.totalElements;
          if (res.data?.content) this.asyncDataTran = res.data.content;
        }
      });
  }

  getAllActivities() {
    this.memberServices
      .searchActivities()
      .pipe(
        map((res) => {
          if (res.data)
            if (res.data) {
              res.data.forEach(data => {
                if(data.progress == data.missionCurrent && data.missionCurrent != 0 && data.progress != 0 && data.status == '2'){
                  this.totalDone = this.totalDone + 1;
                }else{
                  this.totalUndone = this.totalUndone + 1;
                }
              });
            }

          return res;
        })
      )
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {

          // if (res.data?.totalElements) this.totalTran = res.data.totalElements;
          // if (res.data?.content) this.asyncDataTran = res.data.content;
        }
      });
  }

}

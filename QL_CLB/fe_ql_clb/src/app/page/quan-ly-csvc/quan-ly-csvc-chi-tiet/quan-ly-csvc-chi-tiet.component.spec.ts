import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanLyCsvcChiTietComponent } from './quan-ly-csvc-chi-tiet.component';

describe('QuanLyCsvcChiTietComponent', () => {
  let component: QuanLyCsvcChiTietComponent;
  let fixture: ComponentFixture<QuanLyCsvcChiTietComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanLyCsvcChiTietComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuanLyCsvcChiTietComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CsvcSearch, TranSearch } from 'src/app/model/modelSearch';
import { BaseComponent } from '../base/BaseComponent';
import { Activities, BankReceiving, Csvc, Cust, CustAcc, PagesRequest, Tran, TransSchedule } from 'src/app/model/model';
import { TranService } from 'src/app/services/chuyen-tien/tran.service';
import { TransScheduleService } from 'src/app/services/chuyen-tien/trans-schedule.service';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';
import { QlTaiKhoanService } from 'src/app/services/cai-dat/ql-tai-khoan.service';
import { ACTION, CSVCSTATUS, FwError, LoaiGiaoDich, ProductEnum, ProductType, STATUSTRAN, StatusTran, TYPE, TYPETRAN } from 'src/app/common/constants';
import { TaiKhoanService } from 'src/app/services/tai-khoan/tai-khoan.service';
import { Observable, Subject, catchError, concat, distinctUntilChanged, of, switchMap, tap } from 'rxjs';
import { NganHangThuHuongService } from 'src/app/services/ngan-hang-thu-huong/ngan-hang-thu-huong.service';
import { MemberService } from 'src/app/services/member/member.service';


@Component({
  selector: 'app-quan-ly-csvc',
  templateUrl: './quan-ly-csvc.component.html',
  styleUrls: ['./quan-ly-csvc.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class QuanLyCsvcComponent extends BaseComponent<Tran>{
  filterForm: FormGroup;

  productType = ProductType;

  CSVCSTATUS = CSVCSTATUS;

  csvc: Csvc[];

  loaiGDList: any[] = [
    { value: '0', label: 'Tất cả' },
    { value: '1', label: 'Loại GD 1' },
    { value: '2', label: 'Loại GD 1' },
  ];
  taiKhoanNguonList: any[] = [
    { value: '1', label: 'Tài khoản nguồn 1' },
    { value: '2', label: 'Tài khoản nguồn 2' },
    { value: '3', label: 'Tài khoản nguồn 3' },
  ];
  taiKhoanThuHuongList: any[] = [
    { value: '1', label: 'Tài khoản thụ hưởng 1' },
    { value: '2', label: 'Tài khoản thụ hưởng 2' },
  ];
  nguoiLapList: any[] = [
    { value: '1', label: 'Người lập 1' },
    { value: '2', label: 'Người lập 2' },
  ];
  loaiTienList: any[] = [
    { value: '1', label: 'Loại tiền 1' },
    { value: '2', label: 'Loại tiền 2' },
  ];
  displayMonths = 2;
  placement: any = ['bottom-start', 'bottom-end', 'top-start', 'top-end'];

  tranSearch: TranSearch = new TranSearch();

  csvcSearch: CsvcSearch = new CsvcSearch();

  LoaiGiaoDichs = LoaiGiaoDich;
  statusTrans = StatusTran;
  ACTION = ACTION;
  TYPE = TYPE;

  STATUS = STATUSTRAN;
  pageTrans: PagesRequest = new PagesRequest();
  pageTransSchedule: PagesRequest = new PagesRequest();

  labelsTran: any = {
    previousLabel: 'Trước',
    nextLabel: 'Sau',
  };

  labelsTranSchedule: any = {
    previousLabel: 'Trước',
    nextLabel: 'Sau',
  };


  constructor(
    private tranService: TranService,
    private service: MemberService,
    private transScheduleService: TransScheduleService,
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private qlTaiKhoanService: QlTaiKhoanService,
    private injector: Injector,
    private modalService: NgbModal,
    private taiKhoanService: TaiKhoanService,
    private bankReceiveService: NganHangThuHuongService,

  ) {
    super(injector);
    this.pageTrans.curentPage = 1;
    this.pageTrans.size = 10;
    this.pageTransSchedule.curentPage = 1;
    this.pageTransSchedule.size = 10;
    this.tranSearch.status = STATUSTRAN.CHODUYET;

  }


  asyncDataTran?: Csvc[];
  totalTran: number = 0;
  maxSizeTran: number = 10;

  asyncDataTranSchedule?: TransSchedule[];
  totalTranSchedule: number = 0;
  maxSizeTranSchedule: number = 10;

  loadCustAccItem: CustAcc;
  loadCustAcc: Observable<CustAcc[]>;
  loadCustAccInput = new Subject<string>();
  loadCustAccLoading = false;

  bankReceive: BankReceiving[] = [];


  ngOnInit(): void {
    this.onInit();
  }

  getBankReceive() {
    this.bankReceiveService
      .getBankReceive()
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.bankReceive = res.data;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  override search(page: any) {
    this.searchCsvc(page);
  }

  deleteOpen : boolean

  deleteId : number;

  closeDelete(val: any) {
    this.deleteOpen = val;
  }

  openDelete(id ?: number) {

    if(id)
    this.deleteId = id;
    this.deleteOpen = true;
  }

  private getCustAcc() {
    this.loadCustAcc = concat(
      this.taiKhoanService.getCusAccByName(), // default items
      this.loadCustAccInput.pipe(
        distinctUntilChanged(), tap(),
        switchMap(accInput => this.taiKhoanService.getCusAccByName(accInput).pipe(
          catchError(() => of([])), tap()
        ))
      )
    );
  }


  searchCsvc(page: any) {
    this.pageTrans.curentPage = page;
    this.service
      .searchCsvc(this.pageTrans, this.csvcSearch)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          console.log(res);

          if (res.data?.totalElements) this.totalTran = res.data.totalElements;
          if (res.data?.content) this.asyncDataTran = res.data.content;
          if (res.data?.content) this.csvc = res.data?.content;

          this.closeFilterGDC();
        } else {
          this.toastrs.error(res.errorMessage);
        }

      });
  }

  searchTranSchedule(page: any) {
    this.pageTransSchedule.curentPage = page;
    this.transScheduleService
      .searchPagings(this.pageTransSchedule, this.tranSearch)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data?.totalElements) this.totalTranSchedule = res.data.totalElements;
          if (res.data?.content) this.asyncDataTranSchedule = res.data.content;
          this.closeFilterGDC();
        } else {
          this.toastrs.error(res.errorMessage);
        }

      });
  }

  public open(filterGDCDModal: any): void {
    this.modalService.open(filterGDCDModal, {
      scrollable: true,
      centered: true,
    });
  }

  getCurrentTranPageNumber(index: number) {
    if (this.pageTrans.size) {
      return (this.pageTrans.curentPage - 1) * this.pageTrans.size + index;
    }
    return 0;
  }

  getCurrentTranSchedulePageNumber(index: number) {
    if (this.pageTransSchedule.size) {
      return (this.pageTransSchedule.curentPage - 1) * this.pageTransSchedule.size + index;
    }
    return 0;
  }

  public closeFilterGDC(filterGDCDModal?: any): void {
    this.modalService.dismissAll(filterGDCDModal);
  }


  trackByFnCustAcc(item: CustAcc) {
    return item.acc;
  }

  CommaFormatted(event: { which: number; }, type: string) {
    // skip for arrow keys
    if (event.which >= 37 && event.which <= 40) return;
    // format number
    if (type == 'from') {
      if (this.tranSearch.khoangTienTu) {
        this.tranSearch.khoangTienTu = this.tranSearch.khoangTienTu.replace(/\D/g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
    } else {
      if (this.tranSearch.khoangTienDen) {
        this.tranSearch.khoangTienDen = this.tranSearch.khoangTienDen.replace(/\D/g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
    }

  }

  numberCheck(args: { key: string; }) {
    if (args.key === 'e' || args.key === '+' || args.key === '-') {
      return false;
    } else {
      return true;
    }
  }

  checkLoaiGiaoDich(type: any, schedule: any) {
    let se = this.LoaiGiaoDichs.find(element => element.value === type && element.schedule === schedule);
    return se?.name;
  }


  checkCsvcStatus(status: any) {
    let se = this.CSVCSTATUS.find(element => element.value === status);
    return se?.name;
  }

  genStatus(status: any) {
    let se = this.statusTrans.find(element => element.value === status);
    return se?.name;
  }

  downloadExcel(type: number) {
    if (type == 0) {
      //0 la giao dich thuong
      this.tranService
        .downloadGdExcel(this.tranSearch, this.pageTrans);
    } else {
      //giao dich tuong lai dinh ky
      this.transScheduleService
        .downloadGdExcel(this.tranSearch, this.pageTransSchedule);
    }
  }

  deleteCsvc(id: any){
    this.service
    .deleteCsvc(id)
    .subscribe((res) => {
      if (FwError.THANHCONG == res.errorCode) {
        this.toastrs.success(res.errorMessage);
        this.searchCsvc(1);
      } else {
        this.toastrs.error(res.errorMessage);
      }

    });
  }
}

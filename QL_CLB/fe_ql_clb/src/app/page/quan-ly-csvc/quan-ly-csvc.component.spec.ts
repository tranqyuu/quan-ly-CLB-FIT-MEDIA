import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanLyCsvcComponent } from './quan-ly-csvc.component';

describe('QuanLyCsvcComponent', () => {
  let component: QuanLyCsvcComponent;
  let fixture: ComponentFixture<QuanLyCsvcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanLyCsvcComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuanLyCsvcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

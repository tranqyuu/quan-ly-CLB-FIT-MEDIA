import { Location } from '@angular/common';
import { Component, ElementRef, Injector } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FwAction } from 'src/app/common/constants';
import { PagesRequest } from 'src/app/model/model';

export class BaseComponent<T> {
  public el: ElementRef;
  public toastrs: ToastrService;
  public activatedRoute: ActivatedRoute;
  public _location: Location;
  public formBuilder: FormBuilder;
  public action?: string;
  isSubmit: boolean = false;
  constructor(private injectorObj: Injector) {
    this.el = this.injectorObj.get(ElementRef);
    this.toastrs = this.injectorObj.get(ToastrService);
    this.activatedRoute = this.injectorObj.get(ActivatedRoute);
    this._location = this.injectorObj.get(Location);
    this.formBuilder = this.injectorObj.get(FormBuilder);
    this.page.size = 10;
  }
  maxSize: number = 10;
  autoHide: boolean = false;
  responsive: boolean = true;
  labels: any = {
    previousLabel: 'Trước',
    nextLabel: 'Sau',
  };
  asyncData?: T[];
  page: PagesRequest = new PagesRequest();
  total: number = 0;

  onInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.action = this.activatedRoute.snapshot.data['action'];
    switch (this.action) {
      case FwAction.DETAIL: {
        this.getDetail(id);
        break;
      }
      case FwAction.CREATE: {
        break;
      }
      case FwAction.UPDATE: {
        this.getDetail(id);
        break;
      }
      case FwAction.SEARCH: {
        this.search(1);
        break;
      }
      default: {
        // this.search(1)
        break;
      }
    }
  }
  isSearchView(): boolean {
    return FwAction.SEARCH == this.action;
  }

  isReadOnly() {
    return FwAction.DETAIL == this.action;
  }
  isCreate() {
    return FwAction.CREATE == this.action;
  }
  isUpdate() {
    return FwAction.UPDATE == this.action;
  }
  isDetail() {
    return FwAction.DETAIL == this.action;
  }
  getCurrentPageNumber(index: number) {
    if (this.page.size) {
      return (this.page.curentPage - 1) * this.page.size + index;
    }
    return 0;
  }

  focusInValidForm() {
    let invalidElements = this.el.nativeElement.querySelectorAll(
      'textarea.form-control.ng-invalid, input.form-control.ng-invalid, datepicker.form-control.ng-invalid input'
    );
    if (invalidElements.length > 0) {
      invalidElements[0].focus();
    }
  }
  create() { }
  update() { }
  submitDetail() {
    if (this.isCreate()) {
      this.create();
    } else if (this.isUpdate()) {
      this.update();
    }
  }
  getDetail(id: any) { }
  search(page: any) { }
}

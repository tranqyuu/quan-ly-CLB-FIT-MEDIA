import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LichSuChinhSuaDanhBaThuHuongComponent } from './lich-su-chinh-sua-danh-ba-thu-huong.component';

describe('LichSuChinhSuaDanhBaThuHuongComponent', () => {
  let component: LichSuChinhSuaDanhBaThuHuongComponent;
  let fixture: ComponentFixture<LichSuChinhSuaDanhBaThuHuongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LichSuChinhSuaDanhBaThuHuongComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LichSuChinhSuaDanhBaThuHuongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

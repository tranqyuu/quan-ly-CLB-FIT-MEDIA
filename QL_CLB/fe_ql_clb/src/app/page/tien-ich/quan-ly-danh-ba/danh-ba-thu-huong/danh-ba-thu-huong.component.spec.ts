import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DanhBaThuHuongComponent } from './danh-ba-thu-huong.component';

describe('DanhBaThuHuongComponent', () => {
  let component: DanhBaThuHuongComponent;
  let fixture: ComponentFixture<DanhBaThuHuongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DanhBaThuHuongComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DanhBaThuHuongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

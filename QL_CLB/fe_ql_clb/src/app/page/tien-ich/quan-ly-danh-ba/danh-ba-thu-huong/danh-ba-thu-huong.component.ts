import { Component, Injector } from '@angular/core';
import { FwError, ProductEnum } from 'src/app/common/constants';
import { CustContact } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { DanhBaThuHuongService } from 'src/app/services/tien-ich/quan-ly-danh-ba/danh-ba-thu-huong.service';

@Component({
  selector: 'app-danh-ba-thu-huong',
  templateUrl: './danh-ba-thu-huong.component.html',
  styleUrls: ['./danh-ba-thu-huong.component.scss']
})
export class DanhBaThuHuongComponent extends BaseComponent<CustContact>{
  item: CustContact = new CustContact();
  items: CustContact[]= [];
  custContactSearch: CustContact = new CustContact();
  asyncDataCustContact:CustContact[];
  sumbg: number = 0;
  products = ProductEnum;
  parentSelector: boolean = false;
  pageSizes:number[] = [];
  size: number=5;
  showModalDelete: boolean = false;
  public nameModal: string = 'Thông báo';

  constructor(
    private injector: Injector,
    private danhBaThuHuongService: DanhBaThuHuongService
    ){
    super(injector);
  }

  ngOnInit() : void{
    this.onInit();
    this.search(1);
    this.pageSizes = [1,5,10,20,50,100];
  }


  onChangeCheckBox($event:any){
    this.sumbg = 0;
    const id = $event.target.value;
    const isChecked = $event.target.checked;
      this.asyncDataCustContact =this.asyncDataCustContact.map((d)=>{
        if(d.id== id){
          d.checked=isChecked;
          if(isChecked){
            this.sumbg = this.sumbg +1;
            this.parentSelector=false;
          }else{
            this.sumbg = 0;
          }
          
          return d;
        }
        if(id == -1){
          d.checked=this.parentSelector;
          if(d.checked){
            this.sumbg = this.sumbg +1;
          }else{
            this.sumbg = 0;
          }
          
          return d;
        }

        return d;
      });
    console.log(this.asyncDataCustContact);
  }
  
  override search(page: any) {
    this.parentSelector = false;
    this.page.curentPage = page;
    this.page.size = this.size;
    this.danhBaThuHuongService
      .searchPaging(this.page, this.custContactSearch)
      .subscribe((res) => {
        if(FwError.THANHCONG== res.errorCode){
          if (res.data?.totalElements) this.total = res.data.totalElements;
          if (res.data?.content) this.asyncDataCustContact = res.data.content;
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  gennerateProduct(product: any) {
    for (var i = 0; i < this.products.length; i++) {
      if (product == this.products[i].value)
        return this.products[i].name;
    }
    return "";
  }

  openModalDelete(){
    this.showModalDelete = true;
  }

  closeModalDelete(){
    this.showModalDelete = false;
  }

  deleteItems(){
    debugger
    let lstItemDelete: CustContact[]= [];
    console.log(this.asyncDataCustContact);
    this.asyncDataCustContact.forEach((element) =>{
      if(element.checked){
        lstItemDelete.push(element);
      }
    });
    if(lstItemDelete.length > 0){
      lstItemDelete.forEach((element) =>{
        this.danhBaThuHuongService.deleteItem(element.id).subscribe((res) =>{
          if(FwError.THANHCONG== res.errorCode){
            this.search(1);
            this.closeModalDelete();
            this.toastrs.success("Thành công");
            this.sumbg = 0;
          }else{
            this.toastrs.error("Không thành công");
          }
        })
      })
    }
  }
}

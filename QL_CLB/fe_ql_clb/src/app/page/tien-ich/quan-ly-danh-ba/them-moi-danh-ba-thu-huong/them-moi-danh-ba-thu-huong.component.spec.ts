import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemMoiDanhBaThuHuongComponent } from './them-moi-danh-ba-thu-huong.component';

describe('ThemMoiDanhBaThuHuongComponent', () => {
  let component: ThemMoiDanhBaThuHuongComponent;
  let fixture: ComponentFixture<ThemMoiDanhBaThuHuongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThemMoiDanhBaThuHuongComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ThemMoiDanhBaThuHuongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

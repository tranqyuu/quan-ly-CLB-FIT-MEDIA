import { Component, Injector } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FwError, ProductEnum } from 'src/app/common/constants';
import { BankReceiving, CustContact } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { DanhBaThuHuongService } from 'src/app/services/tien-ich/quan-ly-danh-ba/danh-ba-thu-huong.service';

@Component({
  selector: 'app-them-moi-danh-ba-thu-huong',
  templateUrl: './them-moi-danh-ba-thu-huong.component.html',
  styleUrls: ['./them-moi-danh-ba-thu-huong.component.scss']
})
export class ThemMoiDanhBaThuHuongComponent extends BaseComponent<CustContact>{

  item: CustContact = new CustContact();
  items: CustContact[]= [];
  isFormSubmit = false;
  bankReceivings: BankReceiving[] = [];
  products = ProductEnum;

  constructor(
    private injector: Injector,
    private danhBaThuHuongService: DanhBaThuHuongService,
    private router: Router
    ){
    super(injector);
  }

  ngOnInit() : void{
    this.onInit();
    this.getBankReceiving();
  }

  addForm = this.formBuilder.group({
    product: new FormControl('', Validators.required),
    bankReceiving: new FormControl( Validators.required),
    receiveName: new FormControl('', Validators.required),
    receiveAccount: new FormControl('', Validators.required),
    sortname: new  FormControl(''),
  });

  getBankReceiving(){
    this.danhBaThuHuongService.getDsNH().subscribe((result) =>{
      if(result){
        this.bankReceivings = result;
      }
    })
  }

  back(){
   this.router.navigate(['/tien-ich/danh-ba-thu-huong']);
  }
  submit(){
    let item = Object.assign({}, this.item);
    console.log(item);
    this.isFormSubmit = true;
    if(this.addForm.valid){
      this.danhBaThuHuongService.create(item).subscribe((result) =>{
        if(result){
          if(FwError.THANHCONG === result.errorCode){
            this.toastrs.success("Thành công");
            this.back();
          }
          if(FwError.KHONGTHANHCONG === result.errorCode){
            this.toastrs.error("Không thành công");
          }
        }
      })
    }
  }

  fillSortName(){
    this.item.sortname = this.item.receiveName;
  }
}

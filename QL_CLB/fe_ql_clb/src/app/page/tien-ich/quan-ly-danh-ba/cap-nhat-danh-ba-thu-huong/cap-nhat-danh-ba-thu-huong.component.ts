import { Component, Injector } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FwError, ProductEnum } from 'src/app/common/constants';
import { BankReceiving, CustContact } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { DanhBaThuHuongService } from 'src/app/services/tien-ich/quan-ly-danh-ba/danh-ba-thu-huong.service';

@Component({
  selector: 'app-cap-nhat-danh-ba-thu-huong',
  templateUrl: './cap-nhat-danh-ba-thu-huong.component.html',
  styleUrls: ['./cap-nhat-danh-ba-thu-huong.component.scss']
})
export class CapNhatDanhBaThuHuongComponent extends BaseComponent<CustContact>{
  item: CustContact = new CustContact();
  items: CustContact[]= [];
  isFormSubmit = false;
  bankReceivings: BankReceiving[] = [];
  products = ProductEnum;
  selectedCar: number = 3; 

  constructor(
    private danhBaThuHuongService: DanhBaThuHuongService,
    private injector: Injector,
    private router: Router
    ) {
      super(injector);
  }

  ngOnInit() {
    const  id = this.activatedRoute.snapshot.paramMap.get('id');
    if(id){
      this.danhBaThuHuongService.getDetail(id).subscribe((result) =>{
        if(result.data){
          this.item = result.data;
          console.log(this.item);
        }
      })
    }

    this.onInit();
    this.getBankReceiving();    
    this.selectedCar = 3 ;
  }

  getBankReceiving(){
    this.danhBaThuHuongService.getDsNH().subscribe((result) =>{
      if(result){
        this.bankReceivings = result;
      }
    })
  }

  addForm = this.formBuilder.group({
    product: new FormControl('', Validators.required),
    bankReceiving: new FormControl( Validators.required),
    receiveName: new FormControl('', Validators.required),
    receiveAccount: new FormControl('', Validators.required),
    sortname: new  FormControl(''),
  });


  back(){
    this.router.navigate(['/tien-ich/danh-ba-thu-huong']);
   }

   submit(){
    let item = Object.assign({}, this.item);
    console.log(item);
    this.isFormSubmit = true;
    if(this.addForm.valid){
      this.danhBaThuHuongService.update(item).subscribe((result) =>{
        if(result){
          if(FwError.THANHCONG === result.errorCode){
            this.toastrs.success("Thành công");
            this.back();
          }
          if(FwError.KHONGTHANHCONG === result.errorCode){
            this.toastrs.error("Không thành công");
          }
        }
      })
    }
   }

   gennerateProduct(product: any) {
    for (var i = 0; i < this.products.length; i++) {
      if (product == this.products[i].value)
        return this.products[i].name;
    }
    return "";
  }
}

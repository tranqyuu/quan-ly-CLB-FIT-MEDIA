import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CapNhatDanhBaThuHuongComponent } from './cap-nhat-danh-ba-thu-huong.component';

describe('CapNhatDanhBaThuHuongComponent', () => {
  let component: CapNhatDanhBaThuHuongComponent;
  let fixture: ComponentFixture<CapNhatDanhBaThuHuongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CapNhatDanhBaThuHuongComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CapNhatDanhBaThuHuongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

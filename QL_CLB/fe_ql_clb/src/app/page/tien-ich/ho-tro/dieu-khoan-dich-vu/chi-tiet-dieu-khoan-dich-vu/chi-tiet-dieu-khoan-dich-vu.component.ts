import { Component, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { TermsOfUse } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { DieuKhoanDichVuService } from 'src/app/services/tien-ich/ho-tro/dieu-khoan-dich-vu/dieu-khoan-dich-vu.service';

@Component({
  selector: 'app-chi-tiet-dieu-khoan-dich-vu',
  templateUrl: './chi-tiet-dieu-khoan-dich-vu.component.html',
  styleUrls: ['./chi-tiet-dieu-khoan-dich-vu.component.scss']
})
export class ChiTietDieuKhoanDichVuComponent extends BaseComponent<TermsOfUse>{
  item: TermsOfUse = new TermsOfUse();
  constructor(
    private dieuKhoanDichVuService: DieuKhoanDichVuService,
    private injector: Injector,
    private router: Router
    ) {
      super(injector);
  }

  ngOnInit() {
    const  id = this.activatedRoute.snapshot.paramMap.get('id');
    if(id){
      this.dieuKhoanDichVuService.getDetail(id).subscribe((result) =>{
        if(result.data){
          this.item = result.data;
          console.log(this.item);
        }
      })
    }
    this.onInit();
  }
}

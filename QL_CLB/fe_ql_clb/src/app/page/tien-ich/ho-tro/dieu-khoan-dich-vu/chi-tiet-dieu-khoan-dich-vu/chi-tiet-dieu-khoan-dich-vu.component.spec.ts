import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiTietDieuKhoanDichVuComponent } from './chi-tiet-dieu-khoan-dich-vu.component';

describe('ChiTietDieuKhoanDichVuComponent', () => {
  let component: ChiTietDieuKhoanDichVuComponent;
  let fixture: ComponentFixture<ChiTietDieuKhoanDichVuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChiTietDieuKhoanDichVuComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChiTietDieuKhoanDichVuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

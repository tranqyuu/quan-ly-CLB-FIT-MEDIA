import { Component, Injector } from '@angular/core';
import { TermsOfUse } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { DieuKhoanDichVuService } from 'src/app/services/tien-ich/ho-tro/dieu-khoan-dich-vu/dieu-khoan-dich-vu.service';

@Component({
  selector: 'app-dieu-khoan-dich-vu',
  templateUrl: './dieu-khoan-dich-vu.component.html',
  styleUrls: ['./dieu-khoan-dich-vu.component.scss']
})
export class DieuKhoanDichVuComponent extends BaseComponent<TermsOfUse>{
  item: TermsOfUse = new TermsOfUse();
  items: TermsOfUse[] = [];
  dropdown:boolean= false;
  

  constructor(
    private injector: Injector,
    private dieuKhoanDichVuService: DieuKhoanDichVuService
    ){
    super(injector);
  }

  ngOnInit() : void{
    this.onInit();
    this.getAllDieuKhoan();
  }

  getAllDieuKhoan(){
    this.dieuKhoanDichVuService.getAllDk().subscribe((res) =>{
      if(res.data){
        this.items = res.data;
      }
    })
  }
  changeDropD(i:any){
    if(i.isOpen != undefined){
      i.isOpen = !i.isOpen;
    }else{
      i.isOpen = false;
    }
    // this.dropdown = $event.target.value;
    // if(this.dropdown == false){
    //   this.dropdown = true;
    // }else{
    //   this.dropdown = false;
    // }
    
  }
  openDropDown(){
    return true;
  }
  closeDropDown(){
    return false;
  }
}

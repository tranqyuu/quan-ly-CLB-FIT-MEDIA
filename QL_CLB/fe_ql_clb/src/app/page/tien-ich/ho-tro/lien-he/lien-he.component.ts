import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ContactInfo } from 'src/app/model/model';
import { LienHeService } from 'src/app/services/tien-ich/ho-tro/lien-he/lien-he.service';

@Component({
  selector: 'app-lien-he',
  templateUrl: './lien-he.component.html',
  styleUrls: ['./lien-he.component.scss']
})
export class LienHeComponent implements OnInit{

  item: ContactInfo = new ContactInfo();
  items: ContactInfo[]= [];

  constructor( 
    private lienHeService: LienHeService,
    private toastrs: ToastrService){
    
  }

  ngOnInit(): void {
    this.getLienHe();
  }
  getLienHe(){
    this.lienHeService.getDetail().subscribe((result) => {
      if(result.data){
        this.item = result.data;

      }else{
        this.toastrs.error("Không thành công");
      }
      
    })
  }
}

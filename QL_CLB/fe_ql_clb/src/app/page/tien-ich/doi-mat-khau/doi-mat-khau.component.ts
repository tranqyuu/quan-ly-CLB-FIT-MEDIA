import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FwAction, FwError } from 'src/app/common/constants';
import { BaseResponse, Cust, UpdatePasswordRequest } from 'src/app/model/model';
import { DoiMatKhauService } from 'src/app/services/tien-ich/doi-mat-khau/doi-mat-khau.service';
import { BaseComponent } from '../../base/BaseComponent';

@Component({
  selector: 'app-doi-mat-khau',
  templateUrl: './doi-mat-khau.component.html',
  styleUrls: ['./doi-mat-khau.component.scss']
})
export class DoiMatKhauComponent implements OnInit {
  constructor( private formBuilder: FormBuilder,
    private doiMatKhauService: DoiMatKhauService,
    private toastrs: ToastrService,
    private router: Router) {
  
  }
  cust: Cust = new Cust();
  isSubmit = false;
  typeSubmit1 = false;
  typeSubmit2 = false;
  typeSubmit3 = false;
  checkConfirmPass:boolean = true;
  public showModalDangNhap: boolean = false;
  public nameModal: string = 'Thông báo';


  item: UpdatePasswordRequest = new UpdatePasswordRequest();
  fwAction = FwAction;
  //public toastrs: ToastrService;

    custForm = this.formBuilder.group({
    currentPass: new FormControl('', Validators.required),
    newPass: new FormControl('', [Validators.required,Validators.minLength(8),Validators.maxLength(20), Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,20}$/)]),
    confirmNewPass: new FormControl('', [Validators.required,Validators.minLength(8),Validators.maxLength(20), Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,20}$/)]),
  });

  ngOnInit(): void {
  }
  submit(){
    let item = Object.assign({}, this.item);
    console.log(item);
    if( item.newPass === item.confirmNewPass){
      this.checkConfirmPass = true;
    }else{
      this.checkConfirmPass = false;
    }
    this.isSubmit = true;
    if(this.custForm.valid && this.checkConfirmPass == true){
      this.doiMatKhauService.updatePassword(item).subscribe((res) => {
        switch(res.errorCode){
          case FwError.DUPLICATEPASSWORD:
            {
              this.toastrs.error(res.errorMessage);
              break;
            }
          case FwError.PASSWORDNOTFOUND:
            {
              this.toastrs.error(res.errorMessage);
              break;
            }  
          case FwError.PASSWORDFAILLIMIT:
            {
              this.toastrs.error(res.errorMessage);
              break;
            }  
          case FwError.THANHCONG:
            {
              if (res.data) {
                this.cust = res.data;
                this.openModalDangNhap();
                this.toastrs.success("Thành công");
              }
            }
             break;
          case FwError.KHONGTHANHCONG:
            this.toastrs.error(res.errorMessage);
            break;
        }
      });
    }
   
  }

  openModalDangNhap(){
    this.showModalDangNhap = true;
  }
  closeModalDangNhap(){
    this.showModalDangNhap = false;
  }

  ChangeTypePassword(cla: string){
    
    switch(cla){
      case '#currentPass':
        let a = document.querySelector('#currentPass');
        const typea = a?.getAttribute('type');
        if(typea === 'password'){
          this.typeSubmit1 = true;
        }else{
          this.typeSubmit1 = false;
        }
        a?.setAttribute('type',typea === 'password'?'text':'password');
        break;
      case '#newPass':
        let b = document.querySelector('#newPass');
        const typeb = b?.getAttribute('type');
        if(typeb === 'password'){
          this.typeSubmit2 = true;
        }else{
          this.typeSubmit2 = false;
        }
        b?.setAttribute('type',typeb === 'password'?'text':'password');
        break;
      case '#confirmNewPass':
        let c = document.querySelector('#confirmNewPass');
        const typec = c?.getAttribute('type');
        if(typec === 'password'){
          this.typeSubmit3 = true;
        }else{
          this.typeSubmit3 = false;
        }
        c?.setAttribute('type',typec === 'password'?'text':'password');
        break;
    }
    
    
  }
  redirectDangNhap(){
    this.router.navigate(['/login']);
    this.closeModalDangNhap();
  }

  redirectTrangChu(){
    this.router.navigate(['/trang-chu']);
  }
}

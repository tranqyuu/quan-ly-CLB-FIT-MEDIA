import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranSearch } from 'src/app/model/modelSearch';
import { BaseComponent } from '../base/BaseComponent';
import { Activities, ActivitiesRequest, BankReceiving, Cust, CustAcc, PagesRequest, Tran, TransSchedule } from 'src/app/model/model';
import { TranService } from 'src/app/services/chuyen-tien/tran.service';
import { TransScheduleService } from 'src/app/services/chuyen-tien/trans-schedule.service';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';
import { QlTaiKhoanService } from 'src/app/services/cai-dat/ql-tai-khoan.service';
import { ACTION, ACTIVITIESSTATUS, ACTIVITIESTYPE, FwError, LoaiGiaoDich, MEMBERSTATUS, ProductEnum, ProductType, STATUS, STATUSTRAN, StatusTran, TYPE, TYPETRAN } from 'src/app/common/constants';
import { TaiKhoanService } from 'src/app/services/tai-khoan/tai-khoan.service';
import { Observable, Subject, catchError, concat, distinctUntilChanged, of, switchMap, tap } from 'rxjs';
import { NganHangThuHuongService } from 'src/app/services/ngan-hang-thu-huong/ngan-hang-thu-huong.service';
import { MemberService } from 'src/app/services/member/member.service';


@Component({
  selector: 'app-quan-ly-hd',
  templateUrl: './quan-ly-hd.component.html',
  styleUrls: ['./quan-ly-hd.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class QuanLyHdComponent extends BaseComponent<Tran> {

  filterForm: FormGroup;

  STATUS = STATUS;
  productType = ProductType;
  MEMBERSTATUS = MEMBERSTATUS;
  ACTIVITIESTYPE = ACTIVITIESTYPE;

  activities: Activities[];

  loaiGDList: any[] = [
    { value: '0', label: 'Tất cả' },
    { value: '1', label: 'Loại GD 1' },
    { value: '2', label: 'Loại GD 1' },
  ];
  taiKhoanNguonList: any[] = [
    { value: '1', label: 'Tài khoản nguồn 1' },
    { value: '2', label: 'Tài khoản nguồn 2' },
    { value: '3', label: 'Tài khoản nguồn 3' },
  ];
  taiKhoanThuHuongList: any[] = [
    { value: '1', label: 'Tài khoản thụ hưởng 1' },
    { value: '2', label: 'Tài khoản thụ hưởng 2' },
  ];
  nguoiLapList: any[] = [
    { value: '1', label: 'Người lập 1' },
    { value: '2', label: 'Người lập 2' },
  ];
  loaiTienList: any[] = [
    { value: '1', label: 'Loại tiền 1' },
    { value: '2', label: 'Loại tiền 2' },
  ];
  displayMonths = 2;
  placement: any = ['bottom-start', 'bottom-end', 'top-start', 'top-end'];

  tranSearch: TranSearch = new TranSearch();

  ACTIVITIESSTATUS = ACTIVITIESSTATUS;
  LoaiGiaoDichs = LoaiGiaoDich;
  statusTrans = StatusTran;
  ACTION = ACTION;
  TYPE = TYPE;

  pageTrans: PagesRequest = new PagesRequest();
  pageTransSchedule: PagesRequest = new PagesRequest();

  labelsTran: any = {
    previousLabel: 'Trước',
    nextLabel: 'Sau',
  };

  labelsTranSchedule: any = {
    previousLabel: 'Trước',
    nextLabel: 'Sau',
  };


  constructor(
    private tranService: TranService,
    private memberServices: MemberService,
    private transScheduleService: TransScheduleService,
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private qlTaiKhoanService: QlTaiKhoanService,
    private injector: Injector,
    private modalService: NgbModal,
    private taiKhoanService: TaiKhoanService,
    private bankReceiveService: NganHangThuHuongService,

  ) {
    super(injector);
    this.pageTrans.curentPage = 1;
    this.pageTrans.size = 10;
    this.pageTransSchedule.curentPage = 1;
    this.pageTransSchedule.size = 10;
    this.tranSearch.status = STATUSTRAN.CHODUYET;

  }


  asyncDataTran: Activities[];
  totalTran: number = 0;
  maxSizeTran: number = 10;

  asyncDataTranSchedule?: TransSchedule[];
  totalTranSchedule: number = 0;
  maxSizeTranSchedule: number = 10;

  loadCustAccItem: CustAcc;
  loadCustAcc: Observable<CustAcc[]>;
  loadCustAccInput = new Subject<string>();
  loadCustAccLoading = false;

  bankReceive: BankReceiving[] = [];
  activitiesRequest: ActivitiesRequest = new ActivitiesRequest();


  ngOnInit(): void {
    this.onInit();
  }



  override search(page: any) {
    this.searchActivities(page);
  }


  searchActivities(page: any) {
    this.pageTrans.curentPage = page;
    this.memberServices
      .searchActivitiesPaging(this.pageTrans, this.activitiesRequest)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data?.totalElements) this.totalTran = res.data.totalElements;
          if (res.data?.content) this.asyncDataTran = res.data.content;
          this.closeFilterGDC();
        } else {
          this.toastrs.error(res.errorMessage);
        }

      });
  }


  public open(filterGDCDModal: any): void {
    this.modalService.open(filterGDCDModal, {
      scrollable: true,
      centered: true,
    });
  }

  getCurrentTranPageNumber(index: number) {
    if (this.pageTrans.size) {
      return (this.pageTrans.curentPage - 1) * this.pageTrans.size + index;
    }
    return 0;
  }

  getCurrentTranSchedulePageNumber(index: number) {
    if (this.pageTransSchedule.size) {
      return (this.pageTransSchedule.curentPage - 1) * this.pageTransSchedule.size + index;
    }
    return 0;
  }

  public closeFilterGDC(filterGDCDModal?: any): void {
    this.modalService.dismissAll(filterGDCDModal);
  }


  trackByFnCustAcc(item: CustAcc) {
    return item.acc;
  }

  CommaFormatted(event: { which: number; }, type: string) {
    // skip for arrow keys
    if (event.which >= 37 && event.which <= 40) return;
    // format number
    if (type == 'from') {
      if (this.tranSearch.khoangTienTu) {
        this.tranSearch.khoangTienTu = this.tranSearch.khoangTienTu.replace(/\D/g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
    } else {
      if (this.tranSearch.khoangTienDen) {
        this.tranSearch.khoangTienDen = this.tranSearch.khoangTienDen.replace(/\D/g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
    }

  }

  getStatus(status: any) {
    let se = this.ACTIVITIESSTATUS.find(element => element.value === status);
    return se?.name;
  }

  getActivitiesType(status: any) {
    let se = this.ACTIVITIESTYPE.find(element => element.value === status);
    return se?.name;
  }

  numberCheck(args: { key: string; }) {
    if (args.key === 'e' || args.key === '+' || args.key === '-') {
      return false;
    } else {
      return true;
    }
  }

  checkLoaiGiaoDich(type: any, schedule: any) {
    let se = this.LoaiGiaoDichs.find(element => element.value === type && element.schedule === schedule);
    return se?.name;
  }

  genStatus(status: any) {
    let se = this.statusTrans.find(element => element.value === status);
    return se?.name;
  }

  downloadExcel() {
      this.memberServices
        .exportActivities(this.activitiesRequest, this.pageTrans);
  }

}

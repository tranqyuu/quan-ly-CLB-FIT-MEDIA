import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { BaseComponent } from '../../base/BaseComponent';
import { Activities, ActivitiesMembers, ActivitiesRequest, Cust, Member, PagesRequest, Tran, Trans, TransLimitCust } from 'src/app/model/model';
import { ACTION, ACTIVITIESTYPE, AWARDSTATUS, AWARDTYPE, FwAction, FwError, ISSCHEDULE, LoaiGiaoDich, MISSIONSTATUS, MOHINH, POSITION, ProductType, STATUSTRAN, StatusTran, TYPETRAN } from 'src/app/common/constants';
import { TranService } from 'src/app/services/chuyen-tien/tran.service';
import { TransScheduleService } from 'src/app/services/chuyen-tien/trans-schedule.service';
import { Router } from '@angular/router';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';
import {
  InvalidFormatError,
  InvalidNumberError,
  UnitNotEnoughError,
  ReadingConfig,
  validateNumber,
  parseNumberData,
  readNumber,
} from 'read-vietnamese-number'
import { ChuyenKhoanService } from 'src/app/services/chuyen-tien/chuyen-khoan/chuyen-khoan.service';
import { MemberService } from 'src/app/services/member/member.service';
import { map } from 'rxjs';

@Component({
  selector: 'app-quan-ly-chi-tiet-hd-thanh-vien',
  templateUrl: './quan-ly-chi-tiet-hd-thanh-vien.component.html',
  styleUrls: ['./quan-ly-chi-tiet-hd-thanh-vien.component.scss']
})
export class QuanLyChiTietHdThanhVienComponent extends BaseComponent<ActivitiesMembers>{
  activities: Activities = new Activities();

  activitiesMembers: ActivitiesMembers = new ActivitiesMembers();

  type: any;
  moHinh: any;
  role: any;

  MISSIONSTATUS = MISSIONSTATUS;

  status = [{
    id: 1, name: 'Không hoạt động'
  }, {
    id: 0, name: 'Hoạt động'
  }]

  amountString: string;

  ACTIVITIESTYPE = AWARDTYPE;

  TYPETRAN = TYPETRAN;
  statusTrans = StatusTran;
  STATUSTRAN = STATUSTRAN;
  MOHINH = MOHINH;
  POSITION = POSITION;
  ACTION = ACTION;
  FwAction = FwAction;
  ISSCHEDULE = ISSCHEDULE;

  alertOpen = false;

  alertModal: string = 'Thông báo';

  today: Date = new Date();
  currentUser = localStorage.getItem('currentUser');
  cust: Cust = new Cust();

  custTrans: number;
  transLimitCust: TransLimitCust;
  user: any;

  checkSoDu: boolean;

  productType = ProductType;

  itemId: number;

  constructor(
    private chuyenKhoanNoiBoService: ChuyenKhoanService,
    private memberService: MemberService,
    private transScheduleService: TransScheduleService,
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private injector: Injector,
    private router: Router,
  ) {
    super(injector);
    this.type = this.activatedRoute.snapshot.paramMap.get('type');
    this.moHinh = MOHINH.CAP2;
  }

  LoaiGiaoDichs = LoaiGiaoDich;
  public id: number;
  ngOnInit(): void {
    this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.itemId = Number(this.activatedRoute.snapshot.paramMap.get('itemid'));
    this.activitiesMembers.missionStatus = '0';
    this.getMembers();
    this.getActivitiesDetail(this.id);
    this.getAllActivitesMem(this.id);

    this.onInit();
    //lay du lieu theo khach hang
    let currentUser = localStorage.getItem('currentUser');
    if (currentUser) {
      this.user = JSON.parse(currentUser);
    }

    //lay translimitcust(han muc toi da theo ngay va theo giao dich) tu khach hang
    // this.getTransLimit(this.user.id);
    // this.getCustTrans(this.user.id);
  }

  pageTrans: PagesRequest = new PagesRequest();
  asyncDataTran: ActivitiesMembers[];
  totalTran: number = 0;
  maxSizeTran: number = 10;

  pageTrans2: PagesRequest = new PagesRequest();
  asyncDataTran2: ActivitiesMembers[];
  totalTran2: number = 0;
  maxSizeTran2: number = 10;

  members: Member[];

  membersActivites: Member[];

  AWARDSTATUS = AWARDSTATUS;

  getMembers() {
    this.memberService
      .getAllMembers()
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.members = res.data;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  getAllActivitesMem(id: any) {
    this.memberService
      .getAllActivitiesMember(id)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.membersActivites = res.data;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }



  getActivitiesDetail(id: any) {
    this.memberService
      .getDetailActivities(id)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.activities = res.data;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  updateActivitiesMembers() {
    this.memberService
      .updateActivitiesMembers(this.activitiesMembers)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.return();
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }


  getCurrentTranPageNumber(index: number) {
    if (this.pageTrans.size) {
      return (this.pageTrans.curentPage - 1) * this.pageTrans.size + index;
    }
    return 0;
  }

  getCurrentTran2PageNumber(index: number) {
    if (this.pageTrans2.size) {
      return (this.pageTrans2.curentPage - 1) * this.pageTrans2.size + index;
    }
    return 0;
  }


  getActivitiesMembers(page: any) {
    this.pageTrans.curentPage = page;
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    let activitiesRequest = new ActivitiesRequest();
    activitiesRequest.id = Number(id);
    this.memberService
      .searchActivitiesMembersPaging(this.pageTrans, activitiesRequest)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            if (res.data?.totalElements) this.totalTran = res.data.totalElements;
            if (res.data?.content) this.asyncDataTran = res.data.content;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }


  getActivitiesMembersAward(page: any) {

    this.pageTrans.curentPage = page;
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    let activitiesRequest = new ActivitiesRequest();
    activitiesRequest.id = Number(id);
    this.memberService
      .searchActivitiesMembersAwardPaging(this.pageTrans2, activitiesRequest)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            if (res.data?.totalElements) this.totalTran2 = res.data.totalElements;
            if (res.data?.content) this.asyncDataTran2 = res.data.content;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  override getDetail(id: any) {
    let itemId = this.activatedRoute.snapshot.paramMap.get('itemid');
    console.log('here');
    console.log(itemId);
    this.memberService
      .getDetailActivitiesMembers(itemId)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.activitiesMembers = res.data;
            // console.log(this.member);
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  return() {
    // this.router.navigate(['', 'quan-ly-giao-dich', 'gd-cho-duyet']);
    this._location.back();
  }


  openAlert() {
    this.alertOpen = true;
  }

  closeAlert(val: any) {
    this.alertOpen = val;
  }

  transType = [{
    id: 1, name: ' Chuyển khoản nhanh Napas 247 qua số tài khoản'
  }]


  modal: boolean;
  modalTitle: string = 'Xác nhận';
  closeModalDenied(val: any) {
    this.modal = val;
  }

  openModalDenied(val: any) {
    this.modal = val;
  }


  createAwardMembers() {
    if (this.type != 'mission') {
      this.activitiesMembers.activities = this.activities;
      this.activitiesMembers.type = '2';
      this.activitiesMembers.target = '1';
      this.memberService
        .createActivitiesMembers(this.activitiesMembers)
        .subscribe((res) => {
          if (FwError.THANHCONG == res.errorCode) {
            if (res.data) {
              this.return();
            }
          } else {
            this.toastrs.error(res.errorMessage);
          }
        });
    } else {
      this.activitiesMembers.activities = this.activities;
      this.activitiesMembers.type = '1';
      this.memberService
        .createActivitiesMembers(this.activitiesMembers)
        .subscribe((res) => {
          if (FwError.THANHCONG == res.errorCode) {
            if (res.data) {
              this.return();
            }
          } else {
            this.toastrs.error(res.errorMessage);
          }
        });
    }

  }
}

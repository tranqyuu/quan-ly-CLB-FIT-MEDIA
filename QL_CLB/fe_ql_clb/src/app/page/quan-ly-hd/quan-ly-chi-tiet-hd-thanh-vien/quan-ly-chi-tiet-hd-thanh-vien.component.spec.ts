import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanLyChiTietHdThanhVienComponent } from './quan-ly-chi-tiet-hd-thanh-vien.component';

describe('QuanLyChiTietHdThanhVienComponent', () => {
  let component: QuanLyChiTietHdThanhVienComponent;
  let fixture: ComponentFixture<QuanLyChiTietHdThanhVienComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanLyChiTietHdThanhVienComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuanLyChiTietHdThanhVienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

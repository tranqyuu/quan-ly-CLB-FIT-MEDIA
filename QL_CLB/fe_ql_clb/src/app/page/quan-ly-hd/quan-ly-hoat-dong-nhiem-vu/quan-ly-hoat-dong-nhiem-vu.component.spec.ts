import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanLyHoatDongNhiemVuComponent } from './quan-ly-hoat-dong-nhiem-vu.component';

describe('QuanLyHoatDongNhiemVuComponent', () => {
  let component: QuanLyHoatDongNhiemVuComponent;
  let fixture: ComponentFixture<QuanLyHoatDongNhiemVuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanLyHoatDongNhiemVuComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuanLyHoatDongNhiemVuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

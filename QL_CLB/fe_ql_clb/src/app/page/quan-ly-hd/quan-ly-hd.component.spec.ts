import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanLyHdComponent } from './quan-ly-hd.component';

describe('QuanLyHdComponent', () => {
  let component: QuanLyHdComponent;
  let fixture: ComponentFixture<QuanLyHdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanLyHdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuanLyHdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

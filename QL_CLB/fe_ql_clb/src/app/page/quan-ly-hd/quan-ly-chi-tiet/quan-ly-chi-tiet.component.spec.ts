import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanLyChiTietComponent } from './quan-ly-chi-tiet.component';

describe('QuanLyChiTietComponent', () => {
  let component: QuanLyChiTietComponent;
  let fixture: ComponentFixture<QuanLyChiTietComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanLyChiTietComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuanLyChiTietComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

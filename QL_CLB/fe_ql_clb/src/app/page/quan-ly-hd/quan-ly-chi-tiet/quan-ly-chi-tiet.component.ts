import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { BaseComponent } from '../../base/BaseComponent';
import { Activities, ActivitiesMembers, ActivitiesRequest, Cust, Member, PagesRequest, Tran, Trans, TransLimitCust } from 'src/app/model/model';
import { ACTION, ACTIVITIESTYPE, AWARDSTATUS, FwAction, FwError, ISSCHEDULE, LoaiGiaoDich, MISSIONSTATUS, MOHINH, POSITION, ProductType, STATUSTRAN, StatusTran, TYPETRAN } from 'src/app/common/constants';
import { TranService } from 'src/app/services/chuyen-tien/tran.service';
import { TransScheduleService } from 'src/app/services/chuyen-tien/trans-schedule.service';
import { Router } from '@angular/router';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';
import {
  InvalidFormatError,
  InvalidNumberError,
  UnitNotEnoughError,
  ReadingConfig,
  validateNumber,
  parseNumberData,
  readNumber,
} from 'read-vietnamese-number'
import { ChuyenKhoanService } from 'src/app/services/chuyen-tien/chuyen-khoan/chuyen-khoan.service';
import { MemberService } from 'src/app/services/member/member.service';
import { map } from 'rxjs';

@Component({
  selector: 'app-quan-ly-chi-tiet',
  templateUrl: './quan-ly-chi-tiet.component.html',
  styleUrls: ['./quan-ly-chi-tiet.component.scss']
})
export class QuanLyChiTietComponent extends BaseComponent<Activities>{
  activities: Activities = new Activities();
  type: any;
  moHinh: any;
  role: any;

  status = [{
    id: 1, name: 'Không hoạt động'
  }, {
    id: 0, name: 'Hoạt động'
  }]

  amountString: string;

  ACTIVITIESTYPE = ACTIVITIESTYPE;

  TYPETRAN = TYPETRAN;
  statusTrans = StatusTran;
  STATUSTRAN = STATUSTRAN;
  MOHINH = MOHINH;
  POSITION = POSITION;
  ACTION = ACTION;
  FwAction = FwAction;
  ISSCHEDULE = ISSCHEDULE;

  alertOpen = false;

  alertModal: string = 'Thông báo';

  today: Date = new Date();
  currentUser = localStorage.getItem('currentUser');
  cust: Cust = new Cust();

  custTrans: number;
  transLimitCust: TransLimitCust;
  user: any;

  members: Member[];

  checkSoDu: boolean;

  productType = ProductType;

  constructor(
    private chuyenKhoanNoiBoService: ChuyenKhoanService,
    private memberService: MemberService,
    private transScheduleService: TransScheduleService,
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private injector: Injector,
    private router: Router,
  ) {
    super(injector);
    this.type = this.activatedRoute.snapshot.paramMap.get('type');
    this.moHinh = MOHINH.CAP2;
  }

  LoaiGiaoDichs = LoaiGiaoDich;
  public id: number = 0;
  ngOnInit(): void {
    this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.onInit();
    this.getActivitiesMembers(1);
    this.getActivitiesMembersAward(1);
    //lay du lieu theo khach hang
    let currentUser = localStorage.getItem('currentUser');
    if (currentUser) {
      this.user = JSON.parse(currentUser);
    }

    //lay translimitcust(han muc toi da theo ngay va theo giao dich) tu khach hang
    // this.getTransLimit(this.user.id);
    // this.getCustTrans(this.user.id);
  }

  pageTrans: PagesRequest = new PagesRequest();
  asyncDataTran: ActivitiesMembers[];
  totalTran: number = 0;
  maxSizeTran: number = 10;

  pageTrans2: PagesRequest = new PagesRequest();
  asyncDataTran2: ActivitiesMembers[];
  totalTran2: number = 0;
  maxSizeTran2: number = 10;

  MISSIONSTATUS = MISSIONSTATUS;

  AWARDSTATUS = AWARDSTATUS;

  getCurrentTranPageNumber(index: number) {
    if (this.pageTrans.size) {
      return (this.pageTrans.curentPage - 1) * this.pageTrans.size + index;
    }
    return 0;
  }

  getCurrentTran2PageNumber(index: number) {
    if (this.pageTrans2.size) {
      return (this.pageTrans2.curentPage - 1) * this.pageTrans2.size + index;
    }
    return 0;
  }

  getMembers() {
    this.memberService
      .getAllMembers()
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.members = res.data;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  getMissionStatus(status: any) {
    let se = this.MISSIONSTATUS.find(element => element.value === status);
    return se?.name;
  }

  getAward(status: any) {
    let se = this.AWARDSTATUS.find(element => element.value === status);
    return se?.name;
  }

  getActivitiesMembers(page: any) {
    console.log(this.id);

    this.pageTrans.curentPage = page;
    let activitiesRequest = new ActivitiesRequest();
    activitiesRequest.id = Number(this.id);
    this.memberService
      .searchActivitiesMembersPaging(this.pageTrans, activitiesRequest)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            if (res.data?.totalElements) this.totalTran = res.data.totalElements;
            if (res.data?.content) this.asyncDataTran = res.data.content;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }


  getActivitiesMembersAward(page: any) {

    this.pageTrans.curentPage = page;
    let activitiesRequest = new ActivitiesRequest();
    activitiesRequest.id = Number(this.id);
    this.memberService
      .searchActivitiesMembersAwardPaging(this.pageTrans2, activitiesRequest)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            if (res.data?.totalElements) this.totalTran2 = res.data.totalElements;
            if (res.data?.content) this.asyncDataTran2 = res.data.content;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  override getDetail(id: any) {
    this.memberService
      .getDetailActivities(id)
      .pipe(
        map((res) => {
          let items = res.data;
          if (items)
            if (items.startDate)
              items.startDate = new Date(items.startDate);
          return res;
        })
      )
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.activities = res.data;
            // console.log(this.member);
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  return() {
    // this.router.navigate(['', 'quan-ly-giao-dich', 'gd-cho-duyet']);
    this._location.back();
  }


  openAlert() {
    this.alertOpen = true;
  }

  closeAlert(val: any) {
    this.alertOpen = val;
  }

  transType = [{
    id: 1, name: ' Chuyển khoản nhanh Napas 247 qua số tài khoản'
  }]


  modal: boolean;
  modalTitle: string = 'Xác nhận';
  closeModalDenied(val: any) {
    this.modal = val;
  }

  openModalDenied(val: any) {
    this.modal = val;
  }


  createActivities() {
    this.memberService
      .createActivities(this.activities)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.router.navigate(['', 'quan-ly-hoat-dong', 'quan-ly-hoat-dong']);
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  updateActivities() {
    this.memberService
      .updateActivities(this.activities)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.router.navigate(['', 'quan-ly-hoat-dong', 'quan-ly-hoat-dong']);
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

}

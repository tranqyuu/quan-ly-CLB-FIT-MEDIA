import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanLyChucVuChiTietComponent } from './quan-ly-chuc-vu-chi-tiet.component';

describe('QuanLyChucVuChiTietComponent', () => {
  let component: QuanLyChucVuChiTietComponent;
  let fixture: ComponentFixture<QuanLyChucVuChiTietComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanLyChucVuChiTietComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuanLyChucVuChiTietComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

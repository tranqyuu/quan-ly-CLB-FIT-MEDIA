import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanLyChucVuComponent } from './quan-ly-chuc-vu.component';

describe('QuanLyChucVuComponent', () => {
  let component: QuanLyChucVuComponent;
  let fixture: ComponentFixture<QuanLyChucVuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanLyChucVuComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuanLyChucVuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

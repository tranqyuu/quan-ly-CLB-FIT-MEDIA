import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/BaseComponent';
import { Cust } from 'src/app/model/model';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';
import { FormControl } from '@angular/forms';
import { CustStatus, FwError, Position, YesNo } from 'src/app/common/constants';


@Component({
  selector: 'app-ql-ma-truy-cap',
  templateUrl: './ql-ma-truy-cap.component.html',
  styleUrls: ['./ql-ma-truy-cap.component.scss']
})

export class QlMaTruyCapComponent extends BaseComponent<Cust> {
  custSearch: Cust = new Cust();
  cust: Cust = new Cust();
  custStatus = CustStatus;
  public showModalCustKhoa: boolean = false;
  public showModalCustMoKhoa: boolean = false;
  public showModalCustHuy: boolean = false;
  public showModalCustSms: boolean = false;
  public showModalCustNoti: boolean = false;
  public nameModal: string = 'Xác nhận';

  constructor(
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private injector: Injector,
    ) {
      super(injector);
      this.page.size = 100;
  }
  searchForm = this.formBuilder.group({
    code: new FormControl(''),
    email: new FormControl(''),
    tel: new FormControl(''),
    status: new FormControl(''),
  });

  YesNo = YesNo;
  
  codes: any;

  tels: any;

  emails: any;

  status: any = [
    { name: 'Hoạt động', value: '1' },
    { name: 'Khóa', value: '2' },
    { name: 'Hủy', value: '3' },
    { name: 'Chờ kích hoạt', value: '0' },
  ];

  position = Position;
  ngOnInit() : void{
    this.onInit();
    this.getAllCode();
    this.getAllTel();
    this.getAllEmail();
  }

  override search(page: any) {
    this.page.curentPage = page;
    this.qlMaTruyCapServiceService
      .searchPaging(this.page, this.custSearch)
      .subscribe((res) => {
        if(FwError.THANHCONG== res.errorCode){
          if (res.data?.totalElements) this.total = res.data.totalElements;
          if (res.data?.content) this.asyncData = res.data.content;
        }else{
          this.toastrs.error(res.errorMessage);
        }
        
      });
  }

  getAllCode(){
      this.qlMaTruyCapServiceService.getAllCode().subscribe((res) =>{
        if(FwError.THANHCONG== res.errorCode){
          if (res.data) {
            this.codes = res.data;
          }
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  getAllTel(){
    this.qlMaTruyCapServiceService.getAllTel().subscribe((res) =>{
      if(FwError.THANHCONG== res.errorCode){
        if (res.data) {
          this.tels = res.data;
        }
      }else{
        this.toastrs.error(res.errorMessage);
      }
    });
}

getAllEmail(){
  this.qlMaTruyCapServiceService.getAllEmail().subscribe((res) =>{
    if(FwError.THANHCONG== res.errorCode){
      if (res.data) {
        this.emails = res.data;
      }
    }else{
      this.toastrs.error(res.errorMessage);
    }
  });
}

  genPositon(code: any){
    let po =  this.position.find((o: { value: any; }) => o.value == code);
    return po?.name;
  }

  override update(){
      this.qlMaTruyCapServiceService
      .update(this.cust)
      .subscribe((res) => {
        if(FwError.THANHCONG== res.errorCode){
          if (res.data) {
            this.cust = res.data;
            this.toastrs.success("Thành công");
          }
          this.closeAllModal();
          // this._location.back();
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  updateStatus(status : any){
    this.cust.status = status;
    this.update();
  }

  updateSms(sms : any){
    this.cust.sms = sms;
    this.update();
  }

  updateNotification(noti : any){
    this.cust.notification = noti;
    this.update();
  }

  public closeModal(val: any) {
    this.showModalCustKhoa = val;
    this.showModalCustMoKhoa = val;
    this.showModalCustHuy = val;
    this.showModalCustSms = val;
    this.showModalCustNoti = val;
  }

  public closeAllModal() {
    this.showModalCustKhoa = false;
    this.showModalCustMoKhoa = false;
    this.showModalCustHuy = false;
    this.showModalCustSms = false;
    this.showModalCustNoti = false;
  }

  openModalCustKhoa(cust : Cust){
    this.cust = cust;
    this.showModalCustKhoa = true;
  }

  openModalCustMoKhoa(cust : Cust){
    this.cust = cust;
    this.showModalCustMoKhoa = true;
  }

  openModalCustHuy(cust : Cust){
    this.cust = cust;
    this.showModalCustHuy = true;
  }

  openModalCustSms(cust : Cust){
    this.cust = cust;
    this.showModalCustSms = true;
  }

  openModalCustNoti(cust : Cust){
    this.cust = cust;
    this.showModalCustNoti = true;
  }
}

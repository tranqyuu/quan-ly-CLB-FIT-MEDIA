import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LsChinhSuaMaTruyCapComponent } from './ls-chinh-sua-ma-truy-cap.component';

describe('LsChinhSuaMaTruyCapComponent', () => {
  let component: LsChinhSuaMaTruyCapComponent;
  let fixture: ComponentFixture<LsChinhSuaMaTruyCapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LsChinhSuaMaTruyCapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LsChinhSuaMaTruyCapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

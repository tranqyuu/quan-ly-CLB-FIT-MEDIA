import { Component, Injector } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Cust } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { AuthService } from 'src/app/services/auth/auth.service';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';

@Component({
  selector: 'app-ls-chinh-sua-ma-truy-cap',
  templateUrl: './ls-chinh-sua-ma-truy-cap.component.html',
  styleUrls: ['./ls-chinh-sua-ma-truy-cap.component.scss']
})
export class LsChinhSuaMaTruyCapComponent extends BaseComponent<Cust>{
  cust=this.authService.getAuthorization();
  public showModalDetail: boolean = false;
  public nameModal: string = 'Lịch sử';

  constructor(
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private authService:AuthService,
    private injector: Injector
  ) {
    super(injector);
  }

  searchForm = this.formBuilder.group({
    tuNgay: new FormControl(''),
    denNgay: new FormControl(''),
  });

  ngOnInit(): void {
    this.onInit()
  }

  public closeModal(val: any) {
    this.showModalDetail= val;
  }

  openModalDetail(cust : Cust){
    this.cust = cust;
    this.showModalDetail = true;
  }
  
}

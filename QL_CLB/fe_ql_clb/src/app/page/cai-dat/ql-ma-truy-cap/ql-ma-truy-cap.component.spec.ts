/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { QlMaTruyCapComponent } from './ql-ma-truy-cap.component';

describe('QlMaTruyCapComponent', () => {
  let component: QlMaTruyCapComponent;
  let fixture: ComponentFixture<QlMaTruyCapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QlMaTruyCapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QlMaTruyCapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

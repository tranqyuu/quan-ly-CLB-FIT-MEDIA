import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChinhSuaMaTruyCapComponent } from './chinh-sua-ma-truy-cap.component';

describe('ChinhSuaMaTruyCapComponent', () => {
  let component: ChinhSuaMaTruyCapComponent;
  let fixture: ComponentFixture<ChinhSuaMaTruyCapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChinhSuaMaTruyCapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChinhSuaMaTruyCapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

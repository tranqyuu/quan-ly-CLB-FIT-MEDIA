import { Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { Position } from 'src/app/common/constants';
import { Cust } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';

@Component({
  selector: 'app-ket-qua-ma-truy-cap',
  templateUrl: './ket-qua-ma-truy-cap.component.html',
  styleUrls: ['./ket-qua-ma-truy-cap.component.scss']
})
export class KetQuaMaTruyCapComponent extends BaseComponent<Cust>{

  @Output() onClickBtn = new EventEmitter<string>();
  @Input() cust: Cust;
  @Input() maGiaoDich: string;

  today: Date;
  positions = Position;
  constructor(
    private injector: Injector
  ) {
    super(injector);
    this.today = new Date();
  }

  ngOnInit(): void {
    this.onInit();
  }

  handleClickBtn(value: string) {
    this.onClickBtn.emit(value);
  }

  back(){
    this._location.back();
  }

  genPositon(code: any){
    let po =  this.positions.find((o: { value: any; }) => o.value == code);
    return po?.name;
  }
}

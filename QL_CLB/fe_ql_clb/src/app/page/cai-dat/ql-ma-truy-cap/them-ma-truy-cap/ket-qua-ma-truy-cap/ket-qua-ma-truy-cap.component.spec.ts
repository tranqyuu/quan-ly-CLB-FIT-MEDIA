import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KetQuaMaTruyCapComponent } from './ket-qua-ma-truy-cap.component';

describe('KetQuaMaTruyCapComponent', () => {
  let component: KetQuaMaTruyCapComponent;
  let fixture: ComponentFixture<KetQuaMaTruyCapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KetQuaMaTruyCapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KetQuaMaTruyCapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

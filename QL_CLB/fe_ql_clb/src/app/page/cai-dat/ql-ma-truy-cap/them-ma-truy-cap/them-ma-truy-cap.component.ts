import { Component, Injector } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { FwAction, FwError, POSITION, Position } from 'src/app/common/constants';
import { BusinessForm, Company, Cust } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { AuthService } from 'src/app/services/auth/auth.service';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';

@Component({
  selector: 'app-them-ma-truy-cap',
  templateUrl: './them-ma-truy-cap.component.html',
  styleUrls: ['./them-ma-truy-cap.component.scss']
})
export class ThemMaTruyCapComponent extends BaseComponent<Cust> {
  cust: Cust = new Cust();
  id = this.activatedRoute.snapshot.paramMap.get('id');
  statusPage: string;
  code: any
  constructor(
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.onInit();
    this.checkAction();
  }

  checkAction(){
    if(this.id){
      this.statusPage = "chinh-sua"; 
    }else{
      this.statusPage = "khoi-tao"; 
    }
  }

  back(){
    this._location.back();
  }

  setStatusPage(newStatusPage: string) {
    this.statusPage = newStatusPage;
  }

  khoiTaoGiaoDich(cust: Cust) {
      this.cust = cust;
  }
 

  override getDetail(id: any) {
    this.qlMaTruyCapServiceService
      .getDetail(id)
      .subscribe((res) => {
        if(FwError.THANHCONG== res.errorCode){
          if (res.data) {
            this.cust = res.data;
          }
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  setCode(code: string) {
    this.code = code;
  }
  

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemMaTruyCapComponent } from './them-ma-truy-cap.component';

describe('ThemMaTruyCapComponent', () => {
  let component: ThemMaTruyCapComponent;
  let fixture: ComponentFixture<ThemMaTruyCapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThemMaTruyCapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ThemMaTruyCapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

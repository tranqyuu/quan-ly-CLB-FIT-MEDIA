import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XacNhanThemMaTruyCapComponent } from './xac-nhan-them-ma-truy-cap.component';

describe('XacNhanThemMaTruyCapComponent', () => {
  let component: XacNhanThemMaTruyCapComponent;
  let fixture: ComponentFixture<XacNhanThemMaTruyCapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XacNhanThemMaTruyCapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(XacNhanThemMaTruyCapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

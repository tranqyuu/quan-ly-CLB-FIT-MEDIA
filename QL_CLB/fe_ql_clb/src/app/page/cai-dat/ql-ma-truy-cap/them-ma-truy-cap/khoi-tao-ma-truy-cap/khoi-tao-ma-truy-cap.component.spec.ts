import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KhoiTaoMaTruyCapComponent } from './khoi-tao-ma-truy-cap.component';

describe('KhoiTaoMaTruyCapComponent', () => {
  let component: KhoiTaoMaTruyCapComponent;
  let fixture: ComponentFixture<KhoiTaoMaTruyCapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KhoiTaoMaTruyCapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KhoiTaoMaTruyCapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { FwError, Position } from 'src/app/common/constants';
import { Cust } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';

@Component({
  selector: 'app-xac-nhan-them-ma-truy-cap',
  templateUrl: './xac-nhan-them-ma-truy-cap.component.html',
  styleUrls: ['./xac-nhan-them-ma-truy-cap.component.scss']
})
export class XacNhanThemMaTruyCapComponent extends BaseComponent<Cust>{

  @Output() onClickBtn = new EventEmitter<string>();
  @Output() maGiaoDich = new EventEmitter<string>();

  @Input() cust: Cust;

  positions = Position;
  code : any;
  
  constructor(
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.onInit();
  }

  handleClickBtn(value: string) {
    this.onClickBtn.emit(value);
  }

  override update(){
    this.qlMaTruyCapServiceService
    .update(this.cust)
    .subscribe((res) => {
      if(FwError.THANHCONG== res.errorCode){
        if (res.data) {
          this.cust = res.data;
          this.toastrs.success("Thành công");
          this.maGiaoDich.emit(this.code);
          this.onClickBtn.emit('ket-qua');
        }
      }else{
        this.toastrs.error(res.errorMessage);
      }
    });
  }
  override create(){
    this.cust.verifyType = "2"; // hình thức xác nhận: 1 là sms , 2 là smart otp
    this.qlMaTruyCapServiceService
    .create(this.cust)
    .subscribe((res) => {
      if(FwError.THANHCONG== res.errorCode){
        if (res.data) {
          this.cust = res.data;
          this.toastrs.success("Thành công");
          this.maGiaoDich.emit(this.code);
          this.onClickBtn.emit('ket-qua');
        }
      }else{
        this.toastrs.error(res.errorMessage);
      }
    });
  }

  submit(){
    if(this.cust.id){
      this.update();
    }else{
      this.create();
    }
  }

  genPositon(code: any){
    let po =  this.positions.find((o: { value: any; }) => o.value == code);
    return po?.name;
  }

  setCode(code: string) {
    this.code = code;
  }

}

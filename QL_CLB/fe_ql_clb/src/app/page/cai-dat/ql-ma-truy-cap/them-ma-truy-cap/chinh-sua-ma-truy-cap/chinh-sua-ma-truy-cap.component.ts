import { Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { of, switchMap, timer } from 'rxjs';
import { FwError, Position } from 'src/app/common/constants';
import { Cust } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';

@Component({
  selector: 'app-chinh-sua-ma-truy-cap',
  templateUrl: './chinh-sua-ma-truy-cap.component.html',
  styleUrls: ['./chinh-sua-ma-truy-cap.component.scss']
})
export class ChinhSuaMaTruyCapComponent extends BaseComponent<Cust>  {
  @Output() onClickBtn = new EventEmitter<string>();
  @Output() khoiTaoGiaoDich = new EventEmitter<Cust>();
  @Input() cust : Cust;

  constructor(
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.onInit();
  }

  indentitypapers: any = [
    { name: 'CMND', value: 'CMND' },
    { name: 'CCCD', value: 'CCCD' },
    { name: 'HOCHIEU', value: 'HOCHIEU' },
  ];

  positions = Position;

  custEditForm = this.formBuilder.group({
    indentitypapers: new FormControl('',Validators.required),
    idno: new FormControl('', [Validators.required], this.checkIdnoE.bind(this)),
    email: new FormControl('', [Validators.required, Validators.maxLength(300), Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)], this.checkEmailE.bind(this)),
  });

  back(){
    this._location.back();
  }

  handleClickBtn(value: string) {
    this.onClickBtn.emit(value);
  }

  goToSave() {
    this.isSubmit=true
    if (this.custEditForm.status == 'INVALID') {
      this.focusInValidForm()
      return;
    } else if (this.custEditForm.status == 'PENDING') {
      this.toastrs.warning('Hệ thống đang xử lý! Vui lòng đợi');
      return;
    } else {
      this.handleClickBtn('xac-thuc');
      this.khoiTaoGiaoDich.emit(this.cust);
    }
  }

  genPositon(code: any){
    let po =  this.positions.find((o: { value: any; }) => o.value == code);
    return po?.name;
  }

  checkEmailE(control: AbstractControl){
    return timer(200).pipe(
      switchMap(() => {
        if (this.isReadOnly() || (!this.isCreate() && !this.isUpdate()) || !control.value) {
          return of(null);
        }
        return this.qlMaTruyCapServiceService.checkEmail(control.value, this.cust.id).then((res:any)=>{
          if(FwError.THANHCONG== res.errorCode){
            if (res.data) {
              return {
                isExist: true
              }
            }
          }else{
            this.toastrs.error(res.errorMessage);
          }
          return null
        })
      })
    )
  }

  checkIdnoE(control: AbstractControl){
    return timer(200).pipe(
      switchMap(() => {
        if (this.isReadOnly() || (!this.isCreate() && !this.isUpdate()) || !control.value) {
          return of(null);
        }
        return this.qlMaTruyCapServiceService.checkIdno(control.value, this.cust.id).then((res:any)=>{
          if(FwError.THANHCONG== res.errorCode){
            if (res.data) {
              return {
                isExist: true
              }
            }
          }else{
            this.toastrs.error(res.errorMessage);
          }
          return null
        })
      })
    )
  }
}

import { Component, EventEmitter, Injector, Output } from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { of, switchMap, timer } from 'rxjs';
import { FwError, MOHINH, POSITION, Position } from 'src/app/common/constants';
import { Cust } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';

@Component({
  selector: 'app-khoi-tao-ma-truy-cap',
  templateUrl: './khoi-tao-ma-truy-cap.component.html',
  styleUrls: ['./khoi-tao-ma-truy-cap.component.scss']
})
export class KhoiTaoMaTruyCapComponent extends BaseComponent<Cust> {
  @Output() onClickBtn = new EventEmitter<string>();
  @Output() khoiTaoGiaoDich = new EventEmitter<Cust>();
  cust: Cust = new Cust();

  mohinh: any;
  MOHINH: MOHINH;
  constructor(
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private injector: Injector
  ) {
    super(injector);
    this.mohinh = MOHINH.CAP2; // mặc định mô hình là 2
  }

  ngOnInit(): void {
    this.onInit();
    this.checkMoHinh();
    this.positions = this.positions.filter(item => { return item.value != POSITION.QUANTRI});
  }

  indentitypapers: any = [
    { name: 'CMND', value: 'CMND' },
    { name: 'CCCD', value: 'CCCD' },
    { name: 'HOCHIEU', value: 'HOCHIEU' },
  ];

  positions = Position;
  POSITION = POSITION;

  custForm = this.formBuilder.group({
    fullname: new FormControl('', [Validators.required, Validators.maxLength(300)]),
    indentitypapers: new FormControl('',Validators.required),
    idno: new FormControl('', [Validators.required], this.checkIdno.bind(this)),
    birthday: new FormControl('', [Validators.required]),
    tel: new FormControl('', [Validators.required, Validators.pattern(/(84|0[1-9])+([0-9]{8})\b/)], this.checkTel.bind(this)),
    email: new FormControl('', [Validators.required, Validators.maxLength(300), Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)], this.checkEmail.bind(this)),
    position: new FormControl('', [Validators.required]),
  });

  handleClickBtn(value: string) {
    this.onClickBtn.emit(value);
  }

  checkMoHinh(){
    this.positions.filter(item => {return item.value != POSITION.QUANTRI});
    if(this.mohinh == MOHINH.CAP1){
      this.positions.filter(item => {return item.value != POSITION.DUYETLENH});
    }
  }

  back(){
    this._location.back();
  }

  goToSave() {
    this.isSubmit=true
    if (this.custForm.status == 'INVALID') {
      this.focusInValidForm()
      return;
    } else if (this.custForm.status == 'PENDING') {
      this.toastrs.warning('Hệ thống đang xử lý! Vui lòng đợi');
      return;
    } else {
      this.handleClickBtn('xac-thuc');
      this.khoiTaoGiaoDich.emit(this.cust);
    }
  }

  checkTel(control: AbstractControl){
    return timer(200).pipe(
      switchMap(() => {
        if (this.isReadOnly() || !this.isCreate() || !control.value) {
          return of(null);
        }
        return this.qlMaTruyCapServiceService.checkTel(control.value, null).then((res:any)=>{
          if(FwError.THANHCONG== res.errorCode){
            if (res.data) {
              return {
                isExist: true
              }
            }
          }else{
            this.toastrs.error(res.errorMessage);
          }
          return null
        })
      })
    )
  }

  checkEmail(control: AbstractControl){
    return timer(200).pipe(
      switchMap(() => {
        if (this.isReadOnly() || !this.isCreate() || !control.value) {
          return of(null);
        }
        return this.qlMaTruyCapServiceService.checkEmail(control.value, null).then((res:any)=>{
          if(FwError.THANHCONG== res.errorCode){
            if (res.data) {
              return {
                isExist: true
              }
            }
          }else{
            this.toastrs.error(res.errorMessage);
          }
          return null
        })
      })
    )
  }

  checkIdno(control: AbstractControl){
    return timer(200).pipe(
      switchMap(() => {
        if (this.isReadOnly() || !this.isCreate() || !control.value) {
          return of(null);
        }
        return this.qlMaTruyCapServiceService.checkIdno(control.value, null).then((res:any)=>{
          if(FwError.THANHCONG== res.errorCode){
            if (res.data) {
              return {
                isExist: true
              }
            }
          }else{
            this.toastrs.error(res.errorMessage);
          }
          return null
        })
      })
    )
  }
}

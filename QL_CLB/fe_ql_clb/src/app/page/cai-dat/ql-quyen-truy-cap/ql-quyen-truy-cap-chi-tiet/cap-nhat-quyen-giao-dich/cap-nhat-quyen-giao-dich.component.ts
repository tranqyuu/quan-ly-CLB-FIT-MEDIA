import { Component, Injector } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { CHUYENTIEN, FwError, POSITION, Position, Service, THANHTOAN, TypeAcc, YesNo } from 'src/app/common/constants';
import { Account, ChangeCustProduct, Cust, CustAcc, CustProduct } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';
import { QlTaiKhoanService } from 'src/app/services/cai-dat/ql-tai-khoan.service';

@Component({
  selector: 'app-cap-nhat-quyen-giao-dich',
  templateUrl: './cap-nhat-quyen-giao-dich.component.html',
  styleUrls: ['./cap-nhat-quyen-giao-dich.component.scss']
})
export class CapNhatQuyenGiaoDichComponent extends BaseComponent<Cust> {
  custSearch: Cust = new Cust();
  cust: Cust = new Cust();
  lstCustDL: Cust[] = [];
  form: FormGroup;
  tAccDd: boolean = false;
  tienGui: boolean = false;
  custProduct : CustProduct[] = [];
  constructor(
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private qlTaiKhoanService: QlTaiKhoanService,
    private injector: Injector,
    ) {
      super(injector);
      this.page.size = 100;
      this.form = this.formBuilder.group({
        accDd: this.formBuilder.array([])
      });
  }
  position = Position;
  service = Service;
  chuyenTien = CHUYENTIEN;
  thanhToan = THANHTOAN;
  custAcc: any = []; // list acc save
  lstService : any = [];
  lstCustProduct: ChangeCustProduct[] = [];

  ngOnInit() : void{
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.onInit();
    this.search(1);
    this.getAccDd(id);
  }

  AccDd: CustAcc[] = [];
  Acc: any;

  name = 'ng-toggle-button';
  config = {
    value: false,
    name: '',
    disabled: false,
    height: 24,
    width: 44,
    margin: 2,
    fontSize: 10,
    speed: 300,
    color: {
      checked: '#015AAB',
      unchecked: '#C0C2C3',
    },
    switchColor: {
      checked: '#FAFAFA',
      unchecked: '#FAFAFA',
    },
    labels: {
      unchecked: '',
      checked: '',
    },
    checkedLabel: '',
    uncheckedLabel: '',
    fontColor: {
      checked: '#fafafa',
      unchecked: '#ffffff',
    },
    // change: 'changeEvent()',
  };

  override getDetail(id: any) {
    this.qlMaTruyCapServiceService
      .getDetail(id)
      .subscribe((res) => {
        if(FwError.THANHCONG== res.errorCode){
          if (res.data) {
            this.cust = res.data;
            this.check();
            this.checkTienGui();
            this.checkService();
            this.checkDLService();
          }
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
  }
  
  genPositon(code: any){
    let po =  this.position.find((o: { value: any; }) => o.value == code);
    return po?.name;
  }

  getAccDd(id: any){
    this.AccDd.length = 0;
    if(id){
      this.qlTaiKhoanService.searchCustTAccDd(id).subscribe((res) =>{
        if(FwError.THANHCONG== res.errorCode){
          if (res.data) {
            this.Acc = res.data;
            this.Acc.forEach((element: Account) => {
              let custAcc: CustAcc = new CustAcc();
              custAcc.id = Number(element.id);
              custAcc.acc = element.acc;
              custAcc.type = TypeAcc.GIAODICH;
              this.AccDd.push(custAcc);
            });
            this.checkAcc();
          }
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
    }
  }

  check(){
    if(this.cust.tallDd == YesNo.YES){
      this.tAccDd = true;
    } 
  }

  checkAcc(){
    this.AccDd.forEach(element => {
      if(element.id){
        this.custAcc.push(element);
      }
    });
  }

  changeDd(e : any) {
    if (e.target.checked) {
      this.custAcc.push(this.AccDd.find(item=> item.acc === e.target.value));
    } else {
       this.custAcc = this.custAcc.filter(function(el: { acc: any; }) { return el.acc != e.target.value; });
    }
  }
  back(){
    this._location.back();
  }

  override update(){
    this.qlMaTruyCapServiceService
    .updateGiaoDich(this.cust)
    .subscribe((res) => {
      if(FwError.THANHCONG== res.errorCode){
        if (res.data) {
          this.cust = res.data;
          this.toastrs.success("Thành công");
        }
        this._location.back();
      }else{
        this.toastrs.error(res.errorMessage);
      }
    });
  }

  override search(page: any) {
    this.page.curentPage = page;
    this.custSearch.position = POSITION.DUYETLENH;
    this.qlMaTruyCapServiceService
      .searchPaging(this.page, this.custSearch)
      .subscribe((res) => {
        if(FwError.THANHCONG== res.errorCode){
          if (res.data?.content) this.lstCustDL = res.data.content;
        }else{
          this.toastrs.error(res.errorMessage);
        }
        
      });
  }


  //thao tac tab dịch vụ
  checkService(){
    this.cust.custProduct?.forEach(element => {
      this.lstService.push(element.product);
    });
  }

  checkTichService(value: any){
    return this.cust.custProduct?.find(item => item.product == value)
  }

  checkTienGui(){
    if(this.cust.custProduct?.find(item => item.product == '10')){
      this.tienGui = true;
    }else this.tienGui = false;
  }

  changeService(e: any){
    if (e.target.checked) {
      this.lstService.push(e.target.value);
      const product =  this.cust.custProduct?.find( x => x.product === e.target.value);
      if(product){
        const custProduct =  this.lstCustProduct.find( x => x.product === e.target.value);
        if(custProduct){
          custProduct.code = product.custCode?.split(',');
        } 
      }
    } else {
      this.lstService = this.lstService.filter(function(el: any){ return el != e.target.value;});
      const product =  this.lstCustProduct.find( x => x.product === e.target.value);
      if(product){
        product.code = [];
      }
    }
  }

  changeTienGui(){
    if(this.tienGui){
      this.lstService.push('10');
    } else {
      this.lstService = this.lstService.filter(function(el: any){ return el != '10';});
    }
  }

  genService(code: any){
    let se =  this.service.find((o: { value: any; }) => o.value == code);
    return se?.name;
  }

  
  checkChuyenTien(product: any){
    if(Number(product) >=0 && Number(product) <=4) return true;
    else return false;
  }

  checkThanhToan(product: any){
    if(Number(product) >=5 && Number(product) <=9) return true;
    else return false;
  }


  checkTichTienGui(){
    if(this.lstService?.find((item: string) => item == '10')) return true;
    else return false;
  }

  checkTichChuyenTien(){
    if(this.lstService?.find((item: any) => Number(item) >= 0 && Number(item) <= 4)) return true;
    else return false;
  }

  checkTichThanhToan(){
    if(this.lstService?.find((item: any) => Number(item) >= 5 && Number(item) <= 9)) return true;
    else return false;
  }


  //thao tác tab duyệt lệnh
  changeDLService(e: any, item: any){
    if (e.target.checked){
      let changeCustProduct = this.lstCustProduct.find(x => x.product === item);
      if(changeCustProduct){
        changeCustProduct.code?.push(e.target.value);
      }else{
        let changeCustProduct = new ChangeCustProduct();
        changeCustProduct.product = item;
        let code = [];
        code.push(e.target.value);
        changeCustProduct.code = code;
        this.lstCustProduct.push(changeCustProduct);
      }
    }else{
      const product =  this.lstCustProduct.find( x => x.product === item);
      if(product){
        product.code = product.code?.filter(function(el: any) { return el != e.target.value; });
      }
    }
    console.log(this.lstCustProduct)
  }

  checkDLService(){
    this.cust.custProduct?.forEach(ele => {
          let changeCustProduct = new ChangeCustProduct();
          changeCustProduct.id = ele.id;
          changeCustProduct.product = ele.product;
          changeCustProduct.code = ele.custCode?.split(',') || [];
          this.lstCustProduct.push(changeCustProduct);
    });
  }

  checkTichDLService(itemDL: any, item: string){
    let chk = false;
    this.cust.custProduct?.forEach(ele => {
      if(ele.product == item){
        if(ele.custCode?.split(',').includes(itemDL.code)){
          chk = true;
        }
      }
    });
    return chk;
  }

  submit(){
    //lấy thông tin accc
    if(!this.tAccDd){
      this.cust.sallDd = YesNo.NO;
    }else {
      this.cust.sallDd = YesNo.YES;
      this.custAcc = []; 
    }

    //lấy thông tin dịch vụ
    let custProduct: CustProduct[] = [];
    this.lstCustProduct.forEach(item=> {
      if(item.code && item.code?.length > 0){
        let product: CustProduct = new CustProduct();
        product.id = item.id;
        product.product = item.product;
        product.custCode = item.code?.join();
        custProduct.push(product);
      }
    })

    this.cust.custProduct = custProduct;
    this.cust.custAcc = this.custAcc;
    this.update();
  }
}

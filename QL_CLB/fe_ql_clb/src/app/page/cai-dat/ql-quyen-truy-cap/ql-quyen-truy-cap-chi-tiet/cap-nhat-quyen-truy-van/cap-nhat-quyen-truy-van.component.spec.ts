import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CapNhatQuyenTruyVanComponent } from './cap-nhat-quyen-truy-van.component';

describe('CapNhatQuyenTruyVanComponent', () => {
  let component: CapNhatQuyenTruyVanComponent;
  let fixture: ComponentFixture<CapNhatQuyenTruyVanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CapNhatQuyenTruyVanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CapNhatQuyenTruyVanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

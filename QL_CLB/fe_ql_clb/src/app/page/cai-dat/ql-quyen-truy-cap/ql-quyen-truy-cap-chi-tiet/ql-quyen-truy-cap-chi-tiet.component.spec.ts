/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { QlQuyenTruyCapChiTietComponent } from './ql-quyen-truy-cap-chi-tiet.component';

describe('QlQuyenTruyCapChiTietComponent', () => {
  let component: QlQuyenTruyCapChiTietComponent;
  let fixture: ComponentFixture<QlQuyenTruyCapChiTietComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QlQuyenTruyCapChiTietComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QlQuyenTruyCapChiTietComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

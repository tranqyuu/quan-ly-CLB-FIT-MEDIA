import { Component, Injector, OnInit } from '@angular/core';
import { AccType, FwError, Position, Service, TypeAcc, YesNo, POSITION } from 'src/app/common/constants';
import { Cust } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';

@Component({
  selector: 'app-ql-quyen-truy-cap-chi-tiet',
  templateUrl: './ql-quyen-truy-cap-chi-tiet.component.html',
  styleUrls: ['./ql-quyen-truy-cap-chi-tiet.component.css'],
})
export class QlQuyenTruyCapChiTietComponent extends BaseComponent<Cust> {
  cust: Cust = new Cust();
  sAccDd: boolean = false;
  sAccFd: boolean = false;
  sAccLn: boolean = false;
  tAccDd: boolean = false;
  roleTrans: boolean = false;
  roleSearch: boolean = false;
  showModalTrans: boolean = false;
  showModalSearch: boolean = false;

  nameModal = "Thông báo";

  constructor(
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private injector: Injector,
    ) {
      super(injector);
  }

  position = Position;
  service = Service;

  ngOnInit() : void{
    this.onInit()
  }

  POSITION = POSITION;
  TypeAcc = TypeAcc;
  AccType = AccType;
  YesNo = YesNo;

  name = 'ng-toggle-button';
  config = {
    value: false,
    name: '',
    disabled: false,
    height: 24,
    width: 44,
    margin: 2,
    fontSize: 10,
    speed: 300,
    color: {
      checked: '#015AAB',
      unchecked: '#C0C2C3',
    },
    switchColor: {
      checked: '#FAFAFA',
      unchecked: '#FAFAFA',
    },
    labels: {
      unchecked: '',
      checked: '',
    },
    checkedLabel: '',
    uncheckedLabel: '',
    fontColor: {
      checked: '#fafafa',
      unchecked: '#ffffff',
    },
    // change: 'changeEvent()',
  };
  
  changeEventSearch() {
    this.showModalSearch = true;
  }

  changeEventTran() {
    this.showModalTrans = true;
  }



  override getDetail(id: any) {
    this.qlMaTruyCapServiceService
      .getDetail(id)
      .subscribe((res) => {
        if(FwError.THANHCONG== res.errorCode){
          if (res.data) {
            this.cust = res.data;
            this.check();
          }
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  override update(){
      this.qlMaTruyCapServiceService
      .update(this.cust)
      .subscribe((res) => {
        if(FwError.THANHCONG== res.errorCode){
          if (res.data) {
            this.cust = res.data;
            this.toastrs.success("Thành công");
          }
          this.showModalTrans = false;
          this.showModalSearch = false;
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  updateRoleSearch(){
    if(this.roleSearch) this.cust.rolesearch = YesNo.YES; 
    else this.cust.rolesearch = YesNo.NO;
    this.update();
  }

  updateRoleTran(){
    if(this.roleTrans) this.cust.roletrans = YesNo.YES; 
    else this.cust.roletrans = YesNo.NO;
    this.update();
  }

  genPositon(code: any){
    let po =  this.position.find((o: { value: any; }) => o.value == code);
    return po?.name;
  }

  genService(code: any){
    let se =  this.service.find((o: { value: any; }) => o.value == code);
    return se?.name;
  }

  getArrAcc(accs : any){
    return accs.split(',');
  }

  check(){
    if(this.cust.sallDd == YesNo.YES){
      this.sAccDd = true;
    }else{
      if(this.cust.custAcc){
        let count =  this.cust.custAcc?.filter(acc => acc.type == TypeAcc.TRUYVAN && acc.accType == AccType.TAIKHOAN).length;
        if(count && count > 0){
          this.sAccDd = true;
        }
      }
    } 

    if(this.cust.sallFd == YesNo.YES){
      this.sAccFd = true;
    }else{
      if(this.cust.custAcc){
        let count =  this.cust.custAcc?.filter(acc => acc.type == TypeAcc.TRUYVAN && acc.accType == AccType.TIENGUI).length;
        if(count && count > 0){
          this.sAccFd = true;
        }
      }
    }

    if(this.cust.sallLn == YesNo.YES){
      this.sAccLn = true;
    }else{
      if(this.cust.custAcc){
        let count =  this.cust.custAcc?.filter(acc => acc.type == TypeAcc.TRUYVAN && acc.accType == AccType.TIENVAY).length;
        if(count && count > 0){
          this.sAccLn = true;
        }
      }
    }

    if(this.cust.tallDd == YesNo.YES){
      this.tAccDd = true;
    }else{
      if(this.cust.custAcc){
        let count =  this.cust.custAcc?.filter(acc => acc.type == TypeAcc.GIAODICH).length;
        if(count && count > 0){
          this.tAccDd = true;
        }
      }
    }

    if(this.cust.roletrans == YesNo.YES){
      this.roleTrans = true;
    }else this.roleTrans = false;

    if(this.cust.rolesearch == YesNo.YES){
      this.roleSearch = true;
    }else this.roleSearch = false;
  }

  closeModalSearch(){
    this.roleSearch = !this.roleSearch;
    this.showModalSearch = false;
  }

  closeModalTran(){
    this.roleTrans = !this.roleTrans;
    this.showModalTrans = false;
  }

}

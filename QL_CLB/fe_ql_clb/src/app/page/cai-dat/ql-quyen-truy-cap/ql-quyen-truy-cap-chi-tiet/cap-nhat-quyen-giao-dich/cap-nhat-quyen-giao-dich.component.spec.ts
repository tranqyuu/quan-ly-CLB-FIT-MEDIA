import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CapNhatQuyenGiaoDichComponent } from './cap-nhat-quyen-giao-dich.component';

describe('CapNhatQuyenGiaoDichComponent', () => {
  let component: CapNhatQuyenGiaoDichComponent;
  let fixture: ComponentFixture<CapNhatQuyenGiaoDichComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CapNhatQuyenGiaoDichComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CapNhatQuyenGiaoDichComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

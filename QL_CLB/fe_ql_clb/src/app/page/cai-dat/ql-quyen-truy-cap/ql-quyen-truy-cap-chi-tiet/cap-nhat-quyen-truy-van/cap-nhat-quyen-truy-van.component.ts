import { Component, Injector } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { AccType, FwError, Position, TypeAcc, YesNo } from 'src/app/common/constants';
import { Account, Cust, CustAcc } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';
import { QlTaiKhoanService } from 'src/app/services/cai-dat/ql-tai-khoan.service';

@Component({
  selector: 'app-cap-nhat-quyen-truy-van',
  templateUrl: './cap-nhat-quyen-truy-van.component.html',
  styleUrls: ['./cap-nhat-quyen-truy-van.component.scss']
})
export class CapNhatQuyenTruyVanComponent extends BaseComponent<Cust>{
  cust: Cust = new Cust();
  sAccDd: boolean = false;
  sAccFd: boolean = false;
  sAccLn: boolean = false;
  form: FormGroup;
  constructor(
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private qlTaiKhoanService: QlTaiKhoanService,
    private injector: Injector,
    ) {
      super(injector);
      this.form = this.formBuilder.group({
      });
  }
  position = Position;
  ngOnInit() : void{
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.onInit();
    this.getAccDd(id);
    this.getAccFd(id);
    this.getAccLn(id);
    
  }

  name = 'ng-toggle-button';
  config = {
    value: false,
    name: '',
    disabled: false,
    height: 24,
    width: 44,
    margin: 2,
    fontSize: 10,
    speed: 300,
    color: {
      checked: '#015AAB',
      unchecked: '#C0C2C3',
    },
    switchColor: {
      checked: '#FAFAFA',
      unchecked: '#FAFAFA',
    },
    labels: {
      unchecked: '',
      checked: '',
    },
    checkedLabel: '',
    uncheckedLabel: '',
    fontColor: {
      checked: '#fafafa',
      unchecked: '#ffffff',
    },
    // change: 'changeEvent()',
  };



  AccDd: CustAcc[] = [];

  AccFd: CustAcc[] = [];

  AccLn: CustAcc[] = [];
  
  custAcc: any = []; // list acc save

  Acc: any;

  override getDetail(id: any) {
    this.qlMaTruyCapServiceService
      .getDetail(id)
      .subscribe((res) => {
        if(FwError.THANHCONG== res.errorCode){
          if (res.data) {
            this.cust = res.data;
            this.check();
          }
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  override update(){
      this.qlMaTruyCapServiceService
      .updateSAcc(this.cust)
      .subscribe((res) => {
        if(FwError.THANHCONG== res.errorCode){
          if (res.data) {
            this.cust = res.data;
            this.toastrs.success("Thành công");
          }
          this._location.back();
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  getAccDd(id: any){
    this.AccDd.length = 0;
    if(id){
      this.qlTaiKhoanService.searchCustSAccDd(id).subscribe((res) =>{
        if(FwError.THANHCONG== res.errorCode){
          if (res.data) {
            this.Acc = res.data;
            this.Acc.forEach((element: Account) => {
              let custAcc: CustAcc = new CustAcc();
              custAcc.id = Number(element.id);
              custAcc.acc = element.acc;
              custAcc.type = TypeAcc.TRUYVAN;
              custAcc.accType = AccType.TAIKHOAN;
              this.AccDd.push(custAcc);
            });
            this.checkAccDd();
          }
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
    }
  }

  getAccFd(id: any){
    this.AccFd.length = 0;
    if(id){
      this.qlTaiKhoanService.searchCustAccFd(id).subscribe((res) =>{
        if(FwError.THANHCONG== res.errorCode){
          if (res.data) {
            this.Acc = res.data;
            this.Acc.forEach((element: Account) => {
              let custAcc: CustAcc = new CustAcc();
              custAcc.id = Number(element.id);
              custAcc.acc = element.acc;
              custAcc.type = TypeAcc.TRUYVAN;
              custAcc.accType = AccType.TIENGUI;
              this.AccFd.push(custAcc);
            });
            this.checkAccFd();
          }
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
    }
  }

  getAccLn(id: any){
    this.AccLn.length = 0;
    if(id){
      this.qlTaiKhoanService.searchCustAccLn(id).subscribe((res) =>{
        if(FwError.THANHCONG== res.errorCode){
          if (res.data) {
            this.Acc = res.data;
            this.Acc.forEach((element: Account) => {
              let custAcc: CustAcc = new CustAcc();
              custAcc.id = Number(element.id);
              custAcc.acc = element.acc;
              custAcc.type = TypeAcc.TRUYVAN;
              custAcc.accType = AccType.TIENVAY;
              this.AccLn.push(custAcc);
            });
            this.checkAccLn();
          }
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
    }
  }

  checkAcc(){

  }

  genPositon(code: any){
    let po =  this.position.find((o: { value: any; }) => o.value == code);
    return po?.name;
  }

  changeDd(e : any) {
    if (e.target.checked) {
      this.custAcc.push(this.AccDd.find(item=> item.acc === e.target.value));
    } else {
       this.custAcc = this.custAcc.filter(function(el: { acc: any; }) { return el.acc != e.target.value; });
    }
  }

  changeFd(e : any) {
    if (e.target.checked) {
      this.custAcc.push(this.AccFd.find(obj =>obj.acc === e.target.value));
    } else {
      this.custAcc = this.custAcc.filter(function(el: { acc: any; }) { return el.acc != e.target.value; });
    }
  }

  changeLn(e : any) {
    if (e.target.checked) {
      this.custAcc.push(this.AccLn.find(obj =>obj.acc === e.target.value));
    } else {
      this.custAcc = this.custAcc.filter(function(el: { acc: any; }) { return el.acc != e.target.value; });
    }
  }
  check(){
    if(this.cust.sallDd == YesNo.YES){
      this.sAccDd = true;
    } 

    if(this.cust.sallFd == YesNo.YES){
      this.sAccFd = true;
    }

    if(this.cust.sallLn == YesNo.YES){
      this.sAccLn = true;
    }
  }

  checkAccDd(){
    this.AccDd.forEach((element) => {
      if(element.id){
        this.custAcc.push(element);
      }
    });
  }

  checkAccFd(){
    this.AccFd.forEach((element) => {
      if(element.id){
        this.custAcc.push(element);
      }
    });
  }

  checkAccLn(){
    this.AccLn.forEach((element) => {
      if(element.id){
        this.custAcc.push(element);
      }
    });
  }


  back(){
    this._location.back();
  }

  submit(){
    if(!this.sAccDd){
      this.cust.sallDd = YesNo.NO;
    }else {
      this.cust.sallDd = YesNo.YES;
      this.custAcc = this.custAcc.filter(function(el: { accType: AccType; }) { return el.accType != AccType.TAIKHOAN; });
    }

    if(!this.sAccFd){
      this.cust.sallFd = YesNo.NO;
    }else {
      this.cust.sallFd = YesNo.YES;
      this.custAcc = this.custAcc.filter(function(el: { accType: AccType; }) { return el.accType != AccType.TIENGUI; });
    }

    if(!this.sAccLn){
      this.cust.sallLn = YesNo.NO;
    }else {
      this.cust.sallLn = YesNo.YES;
      this.custAcc = this.custAcc.filter(function(el: { accType: AccType; }) { return el.accType != AccType.TIENVAY; });
    }

    this.cust.custAcc = this.custAcc;
    this.update();
  }

}

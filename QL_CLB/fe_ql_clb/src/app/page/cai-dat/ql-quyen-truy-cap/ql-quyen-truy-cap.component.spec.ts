/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { QlQuyenTruyCapComponent } from './ql-quyen-truy-cap.component';

describe('QlQuyenTruyCapComponent', () => {
  let component: QlQuyenTruyCapComponent;
  let fixture: ComponentFixture<QlQuyenTruyCapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QlQuyenTruyCapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QlQuyenTruyCapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

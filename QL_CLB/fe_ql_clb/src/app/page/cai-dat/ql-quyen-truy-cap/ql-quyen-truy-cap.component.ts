import { Component, Injector, OnInit } from '@angular/core';
import { AccType, CustStatus, FwError, Position, TypeAcc, YesNo } from 'src/app/common/constants';
import { Account, Cust, CustAcc } from 'src/app/model/model';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';
import { QlTaiKhoanService } from 'src/app/services/cai-dat/ql-tai-khoan.service';
import { BaseComponent } from '../../base/BaseComponent';

@Component({
  selector: 'app-ql-quyen-truy-cap',
  templateUrl: './ql-quyen-truy-cap.component.html',
  styleUrls: ['./ql-quyen-truy-cap.component.css'],
})
export class QlQuyenTruyCapComponent extends BaseComponent<Cust> {
  custSearch: Cust = new Cust();
  cust: Cust = new Cust();
  custAccS: any = [];
  custAccT: any = [];
  AccDd: any = [];
  AccFd: any = [];
  AccLn: any = [];
  custStatus = CustStatus;
  constructor(
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private qlTaiKhoanService: QlTaiKhoanService,
    private injector: Injector,
    ) {
      super(injector);
      this.page.size = 100;
  }
  TypeAcc = TypeAcc;
  position = Position;

  ngOnInit() : void{
    this.onInit();
    this.getAccDd();
    this.getAccFd();
    this.getAccLn();
  }

  override search(page: any) {
    this.page.curentPage = page;
    this.qlMaTruyCapServiceService
      .searchCustPaging(this.page, this.custSearch)
      .subscribe((res) => {
        if(FwError.THANHCONG== res.errorCode){
          if (res.data?.totalElements) this.total = res.data.totalElements;
          if (res.data?.content) this.asyncData = res.data.content;
        }else{
          this.toastrs.error(res.errorMessage);
        }
        
      });
  }

  genPositon(code: any){
    let po =  this.position.find((o: { value: any; }) => o.value == code);
    return po?.name;
  }

  checkSAcc(cust: Cust){
    this.custAccS.length = 0;
    if(cust.sallDd == YesNo.YES){
      this.AccDd.forEach((element: Account) => {
        this.custAccS.push(element.acc);
      });
    }else{
      if(cust.custAcc){
        cust.custAcc?.forEach(element => {
          if(element.type == TypeAcc.TRUYVAN && element.accType == AccType.TAIKHOAN){
            this.custAccS.push(element.acc);
          }
        });
      }
    }
    if(cust.sallFd == YesNo.YES){
      this.AccFd.forEach((element: Account) => {
        this.custAccS.push(element.acc);
      });
    }else{
      if(cust.custAcc){
        cust.custAcc?.forEach(element => {
          if(element.type == TypeAcc.TRUYVAN && element.accType == AccType.TIENGUI){
            this.custAccS.push(element.acc);
          }
        });
      }
    }
    if(cust.sallLn == YesNo.YES){
      this.AccLn.forEach((element: Account) => {
        this.custAccS.push(element.acc);
      });
    }else{
      if(cust.custAcc){
        cust.custAcc?.forEach(element => {
          if(element.type == TypeAcc.TRUYVAN && element.accType == AccType.TIENVAY){
            this.custAccS.push(element.acc);
          }
        });
      }
    }
    if(this.custAccS.length >0 ){
      return true;
    }else{
      return false;
    }
  }

  checkTAcc(cust: Cust){
    this.custAccT.length = 0;
    if(cust.tallDd == YesNo.YES){
      this.AccDd.forEach((element: Account) => {
        this.custAccT.push(element.acc);
      });
    }else{
      if(cust.custAcc){
        cust.custAcc?.forEach(element => {
          if(element.type == TypeAcc.GIAODICH){
            this.custAccT.push(element.acc);
          }
        });
      }
    }
    if(this.custAccT.length >0 ){
      return true;
    }else{
      return false;
    }
  }

  getAccDd(){
    this.qlTaiKhoanService.getAccDd().subscribe((res) =>{
      if(FwError.THANHCONG== res.errorCode){
        if (res.data) {
          this.AccDd = res.data;
        }
      }else{
        this.toastrs.error(res.errorMessage);
      }
    });
  }

  getAccFd(){
    this.qlTaiKhoanService.getAccFd().subscribe((res) =>{
      if(FwError.THANHCONG== res.errorCode){
        if (res.data) {
          this.AccFd = res.data;
        }
      }else{
        this.toastrs.error(res.errorMessage);
      }
    });
  }

  getAccLn(){
    this.qlTaiKhoanService.getAccLn().subscribe((res) =>{
      if(FwError.THANHCONG== res.errorCode){
        if (res.data) {
          this.AccLn = res.data;
        }
      }else{
        this.toastrs.error(res.errorMessage);
      }
    });
  }


}

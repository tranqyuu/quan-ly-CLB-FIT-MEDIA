import { Component, Injector } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Cust, TransLimitCust } from 'src/app/model/model';
import { CaiDatHanMucService } from 'src/app/services/cai-dat/cai-dat-han-muc.service';
import { BaseComponent } from '../../base/BaseComponent';

@Component({
  selector: 'app-cai-dat-han-muc',
  templateUrl: './cai-dat-han-muc.component.html',
  styleUrls: ['./cai-dat-han-muc.component.css']
})
export class CaiDatHanMucComponent extends BaseComponent<TransLimitCust> {

  itemLL: TransLimitCust = new TransLimitCust();
  itemDL: TransLimitCust = new TransLimitCust();
  itemLLs: TransLimitCust[]= [];
  itemDLs: TransLimitCust[]= [];
  cust: Cust = new Cust();
  newCust1: Cust = new Cust();
  newCust2: Cust = new Cust();
  lstCustLL: Cust[] = [];
  lstCustDL: Cust[] = [];
  MaLL: string;
  MaDL: string;
  action1: boolean = false;
  sumMaxTrainLL: number;
  sumMaxDailyLL: number;
  sumDL: number;

  detailForm = this.formBuilder.group({
    maxdaily: new FormControl( Validators.required),
    maxtrans: new FormControl( Validators.required),
    product: new FormControl( Validators.required),
    productName: new FormControl( Validators.required),
     
  });

  constructor(
    private caiDatHanMucService: CaiDatHanMucService,
    private injector: Injector,
    ) {
      super(injector);

  }

  getListCust(position : number){
    if(position === 2){
      this.caiDatHanMucService.getCust(position).subscribe((result) =>{
        if(result.data){
          this.lstCustLL = result.data;
          console.log("DS MaLL:" + this.lstCustLL);
        }
      })
    }else if(position === 3){
      this.caiDatHanMucService.getCust(position).subscribe((result) =>{
        if(result.data){
          this.lstCustDL = result.data;
          console.log("DS MaDL:" + this.lstCustDL.toString());
        }
      })
    }
  }

  getTransLimitByCust1(cust: Cust){
    this.itemLLs=[];
    this.sumMaxTrainLL = 0;
    this.sumMaxDailyLL = 0;
    this.caiDatHanMucService.getDetailTransLimitCust(cust).subscribe((result) => {
      if(result.data){
        this.itemLLs = result.data;
        console.log("TransLimit: "+this.itemLLs);
        this.itemLLs.forEach((element) =>{
          if(element.maxtrans){
            this.sumMaxTrainLL = this.sumMaxTrainLL + element.maxtrans;
          }
          if(element.maxdaily){
            this.sumMaxDailyLL = this.sumMaxDailyLL + element.maxdaily;
          }
        })
        
      }
    })
  }

  getTransLimitByCust2(cust: Cust){
    this.itemDLs=[];
    this.sumDL = 0;
    this.caiDatHanMucService.getDetailTransLimitCust(cust).subscribe((result) => {
      if(result.data){
        this.itemDLs = result.data;
        console.log("TransLimit: "+this.itemDLs);
        this.itemDLs.forEach((element) =>{
          if(element.maxtrans){
            this.sumDL =this.sumDL + element.maxtrans;
          }
        })
      }
    })
  }

  checkReadOnly(){
    this.action1 = true;
  }

  changeMaLL(){
    this.action1 = true;
    this.newCust1 = this.itemLL.cust;
    if(this.newCust1.code){
      this.MaLL = this.newCust1.code;
    }
    this.getTransLimitByCust1(this.newCust1);
  }

  changeMaDL(){
    this.action1 = true;
    this.newCust2 = this.itemDL.cust;
    if(this.newCust2.code){
      this.MaDL = this.newCust2.code;
    }
    this.getTransLimitByCust2(this.newCust2);
  }

  ngOnInit() {
    this.getListCust(2);
    this.getListCust(3);

  }

}

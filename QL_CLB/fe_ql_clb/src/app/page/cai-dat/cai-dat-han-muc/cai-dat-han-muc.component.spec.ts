/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CaiDatHanMucComponent } from './cai-dat-han-muc.component';

describe('CaiDatHanMucComponent', () => {
  let component: CaiDatHanMucComponent;
  let fixture: ComponentFixture<CaiDatHanMucComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaiDatHanMucComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaiDatHanMucComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CapNhatHanMucComponent } from './cap-nhat-han-muc.component';

describe('CapNhatHanMucComponent', () => {
  let component: CapNhatHanMucComponent;
  let fixture: ComponentFixture<CapNhatHanMucComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CapNhatHanMucComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CapNhatHanMucComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

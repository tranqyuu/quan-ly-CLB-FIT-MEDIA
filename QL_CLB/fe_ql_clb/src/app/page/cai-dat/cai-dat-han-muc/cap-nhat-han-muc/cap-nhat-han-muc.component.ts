import { Component, Injector } from '@angular/core';
import { FormArray, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FwAction, FwError } from 'src/app/common/constants';
import { Cust, TransLimitCust } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { CaiDatHanMucService } from 'src/app/services/cai-dat/cai-dat-han-muc.service';

@Component({
  selector: 'app-cap-nhat-han-muc',
  templateUrl: './cap-nhat-han-muc.component.html',
  styleUrls: ['./cap-nhat-han-muc.component.scss']
})
export class CapNhatHanMucComponent extends BaseComponent<TransLimitCust>{
  item: TransLimitCust = new TransLimitCust();
  items: TransLimitCust[]= [];
  itemLLs: TransLimitCust[]= [];
  itemDLs: TransLimitCust[]= [];
  maLL: string;
  maDL: string;
  cust: Cust;
  checkTabLL:boolean = false;
  checkTabDL:boolean = false;
  checkbox: boolean = false;
  // listTransLimitCust:TransLimitCust[]= [];
  transLimitCust: TransLimitCust = new TransLimitCust();
  constructor(
    private caiDatHanMucService: CaiDatHanMucService,
    private injector: Injector,
    private router: Router
    ) {
      super(injector);

  }
  detailForm = this.formBuilder.group({
    checkbox: new FormControl('', [Validators.required]),
    // cust: new FormControl('', [Validators.required]),
    // maxtrans:new FormControl('', [Validators.required]),
    // maxdaily:new FormControl('', [Validators.required]),
    transLimitCust: this.formBuilder.array([], Validators.required)
  });


  get lstTransLimitCust(): FormArray {
    return this.detailForm.get('transLimitCust') as FormArray;
  }

  getCustByCode(code:string){
    this.caiDatHanMucService.getCustByCode(code).subscribe((result) =>{
      if(result){
       this.cust = result;
        if(result.position && result.position == "2"){
          this.checkTabDL = false;
          this.checkTabLL = true;
          if(result.code){
            this.maLL = result.code;
            console.log("MaLL: "+this.maLL);
          }
          this.getTransLimitByCust1(result);
         
        }
        if(result.position && result.position == "3"){
          this.checkTabDL = true;
          this.checkTabLL = false;
          if(result.code){
            this.maDL = result.code;
            console.log("MaDL: "+this.maDL);
          }
          this.getTransLimitByCust2(result);
        }
      }
    })
  }
  getTransLimitByCust(cust: Cust){
    // this.items=[];
    this.caiDatHanMucService.getDetailTransLimitCust(cust).subscribe((result) => {
      if(result.data){
        this.items = result.data;
      }
      let add = this.detailForm.controls['transLimitCust'] as FormArray;
      this.items.forEach((element: any) => {
        add.push(this.formBuilder.group({
          productName:  new FormControl(),
          maxtrans:     new FormControl(),
          maxdaily:     new FormControl()
        }));
      })
    })
  }

  getTransLimitByCust1(cust: Cust){
    this.itemLLs=[];
    this.caiDatHanMucService.getDetailTransLimitCust(cust).subscribe((result) => {
      if(result.data){
        this.itemLLs = result.data;
        console.log("TransLimit: "+this.itemLLs);
      }
      let add = this.detailForm.controls['transLimitCust'] as FormArray;
      this.itemLLs.forEach((element: any) => {
        add.push(this.formBuilder.group({
          productName:  new FormControl(),
          maxtrans:     new FormControl(),
          maxdaily:     new FormControl()
        }));
      })
    })
  }

  getTransLimitByCust2(cust: Cust){
    this.itemDLs=[];
    this.caiDatHanMucService.getDetailTransLimitCust(cust).subscribe((result) => {
      if(result.data){
        this.itemDLs = result.data;
        console.log("TransLimit: "+this.itemDLs);
      }
      let add = this.detailForm.controls['transLimitCust'] as FormArray;
      this.itemDLs.forEach((element: any) => {
        add.push(this.formBuilder.group({
          productName:  new FormControl(),
          maxtrans:     new FormControl()
        }));
      })
    })
  }
 
  ngOnInit() {
    const  codes = this.activatedRoute.snapshot.paramMap.get('code');
    if(codes){
      this.getCustByCode(codes);
      console.log("CUst: "+this.cust);
    }
    
  }

  submit(){
    if(this.checkTabLL == true){
      let add = this.detailForm.controls['transLimitCust'] as FormArray;
      this.itemLLs.forEach((element: any) => {
        add.push(this.formBuilder.group({
          productName:  new FormControl(),
          maxtrans:     new FormControl(),
          maxdaily:     new FormControl()
        }));
      })
      this.itemLLs.forEach((element) =>{
        element.cust = this.cust;
      })
      if(this.detailForm.valid && this.checkbox == true){
        this.caiDatHanMucService.update(this.itemLLs).subscribe((result) =>{
          if(FwError.THANHCONG ==  result.errorCode){
            if(result.data){
              this.transLimitCust = result.data;
              this.toastrs.success("Thành công");
              // alert("Thành công");
            }
          } else {
            this.toastrs.error(result.errorMessage);
          }
        })
      }
    }
    if(this.checkTabDL == true){
      let add = this.detailForm.controls['transLimitCust'] as FormArray;
      this.itemDLs.forEach((element: any) => {
        add.push(this.formBuilder.group({
          productName:  new FormControl(),
          maxtrans:     new FormControl()
        }));
      })
      this.itemDLs.forEach((element) =>{
        element.cust = this.cust;
      })
      if(this.detailForm.valid && this.checkbox == true){
        this.caiDatHanMucService.update(this.itemDLs).subscribe((result) =>{
          if(FwError.THANHCONG ==  result.errorCode){
            if(result.data){
              this.transLimitCust = result.data;
              this.toastrs.success("Thành công");
            }
          } else {
            this.toastrs.error(result.errorMessage);
          }
        })
      }
    }
  }

  back(){
    this.router.navigate(['/cai-dat/cai-dat-han-muc']);
  }
}

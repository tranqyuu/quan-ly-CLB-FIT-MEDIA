import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { BaseComponent } from '../../base/BaseComponent';
import { ActivitiesMembers, ActivitiesRequest, Csvc, Cust, Member, PagesRequest, TaiChinh, Tran, Trans, TransLimitCust } from 'src/app/model/model';
import { ACTION, AWARDSTATUS, CSVCSTATUS, FwAction, FwError, ISSCHEDULE, LOCATION, LoaiGiaoDich, MISSIONSTATUS, MOHINH, POSITION, ProductType, STATUSTRAN, StatusTran, TAICHINH, TYPETRAN } from 'src/app/common/constants';
import { TranService } from 'src/app/services/chuyen-tien/tran.service';
import { TransScheduleService } from 'src/app/services/chuyen-tien/trans-schedule.service';
import { Router } from '@angular/router';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';
import {
  InvalidFormatError,
  InvalidNumberError,
  UnitNotEnoughError,
  ReadingConfig,
  validateNumber,
  parseNumberData,
  readNumber,
} from 'read-vietnamese-number'
import { ChuyenKhoanService } from 'src/app/services/chuyen-tien/chuyen-khoan/chuyen-khoan.service';
import { MemberService } from 'src/app/services/member/member.service';
import { map } from 'rxjs';
@Component({
  selector: 'app-quan-ly-tai-chinh-chi-tiet',
  templateUrl: './quan-ly-tai-chinh-chi-tiet.component.html',
  styleUrls: ['./quan-ly-tai-chinh-chi-tiet.component.scss']
})
export class QuanLyTaiChinhChiTietComponent extends BaseComponent<Csvc>{
  member: Member = new Member();
  type: any;
  moHinh: any;
  role: any;

  taiChinh: TaiChinh = new TaiChinh();

  csvc: Csvc = new Csvc();

  CSVCSTATUS = CSVCSTATUS;

  TAICHINH = TAICHINH;

  status = [{
    id: 0, name: 'Không hoạt động'
  }, {
    id: 1, name: 'Hoạt động'
  }]

  LOCATION = LOCATION;
  amountString: string;

  TYPETRAN = TYPETRAN;
  statusTrans = StatusTran;
  STATUSTRAN = STATUSTRAN;
  MOHINH = MOHINH;
  POSITION = POSITION;
  ACTION = ACTION;
  FwAction = FwAction;
  ISSCHEDULE = ISSCHEDULE;

  alertOpen = false;

  alertModal: string = 'Thông báo';

  today: Date = new Date();
  currentUser = localStorage.getItem('currentUser');
  cust: Cust = new Cust();

  custTrans: number;
  transLimitCust: TransLimitCust;
  user: any;

  checkSoDu: boolean;

  productType = ProductType;

  pageTrans: PagesRequest = new PagesRequest();
  asyncDataTran: ActivitiesMembers[];
  totalTran: number = 0;
  maxSizeTran: number = 10;

  pageTrans2: PagesRequest = new PagesRequest();
  asyncDataTran2: ActivitiesMembers[];
  totalTran2: number = 0;
  maxSizeTran2: number = 10;

  constructor(
    private chuyenKhoanNoiBoService: ChuyenKhoanService,
    private memberService: MemberService,
    private transScheduleService: TransScheduleService,
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private injector: Injector,
    private router: Router,
  ) {
    super(injector);
    this.type = this.activatedRoute.snapshot.paramMap.get('type');
    this.moHinh = MOHINH.CAP2;
  }

  AWARDSTATUS = AWARDSTATUS;
  LoaiGiaoDichs = LoaiGiaoDich;
  id: number;
  MISSIONSTATUS = MISSIONSTATUS;
  ngOnInit(): void {

    this.onInit();
    this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    //lay du lieu theo khach hang
    let currentUser = localStorage.getItem('currentUser');
    if (currentUser) {
      this.user = JSON.parse(currentUser);
    }

    //lay translimitcust(han muc toi da theo ngay va theo giao dich) tu khach hang
    // this.getTransLimit(this.user.id);
    // this.getCustTrans(this.user.id);
  }

  override getDetail(id: any) {
    this.memberService
      .getDetailTaiChinh(id)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.taiChinh = res.data;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  return() {
    // this.router.navigate(['', 'quan-ly-giao-dich', 'gd-cho-duyet']);
    this._location.back();
  }

  getActivitiesMembersAward(page: any) {
    this.pageTrans.curentPage = page;
    let activitiesRequest = new ActivitiesRequest();
    activitiesRequest.membersId = Number(this.id);
    this.memberService
      .searchActivitiesMembersAwardPaging(this.pageTrans2, activitiesRequest)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            if (res.data?.totalElements) this.totalTran2 = res.data.totalElements;
            if (res.data?.content) this.asyncDataTran2 = res.data.content;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  getActivitiesMembers(page: any) {
    this.pageTrans.curentPage = page;
    let activitiesRequest = new ActivitiesRequest();
    activitiesRequest.membersId = Number(this.id);
    this.memberService
      .searchActivitiesMembersPaging(this.pageTrans, activitiesRequest)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            if (res.data?.totalElements) this.totalTran = res.data.totalElements;
            if (res.data?.content) this.asyncDataTran = res.data.content;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  getMissionStatus(status: any) {
    let se = this.MISSIONSTATUS.find(element => element.value === status);
    return se?.name;
  }

  CommaFormatted(event: { which: number; }) {
    this.checkSoDu = true;
    // skip for arrow keys
    if (event.which >= 37 && event.which <= 40) return;
    // format number
    if (this.taiChinh.quantity) {
      this.taiChinh.quantity = this.taiChinh.quantity.replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  }

  numberCheck(args: { key: string; }) {
    this.checkSoDu = true;

    if (args.key === 'e' || args.key === '+' || args.key === '-') {
      return false;
    } else {
      return true;
    }
  }

  getAward(status: any) {
    let se = this.AWARDSTATUS.find(element => element.value === status);
    return se?.name;
  }

  openAlert() {
    this.alertOpen = true;
  }

  closeAlert(val: any) {
    this.alertOpen = val;
  }

  transType = [{
    id: 1, name: ' Chuyển khoản nhanh Napas 247 qua số tài khoản'
  }]


  modal: boolean;
  modalTitle: string = 'Xác nhận';
  closeModalDenied(val: any) {
    this.modal = val;
  }

  openModalDenied(val: any) {
    this.modal = val;
  }


  createTaiChinh() {
    this.memberService
      .createTaiChinh(this.taiChinh)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.return();
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  updateTaiChinh() {
    this.memberService
      .updateTaiChinh(this.taiChinh)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.return();
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanLyTaiChinhChiTietComponent } from './quan-ly-tai-chinh-chi-tiet.component';

describe('QuanLyTaiChinhChiTietComponent', () => {
  let component: QuanLyTaiChinhChiTietComponent;
  let fixture: ComponentFixture<QuanLyTaiChinhChiTietComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanLyTaiChinhChiTietComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuanLyTaiChinhChiTietComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

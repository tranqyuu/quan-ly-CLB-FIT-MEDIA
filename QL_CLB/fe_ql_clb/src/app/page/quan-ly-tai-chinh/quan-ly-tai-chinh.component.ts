import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranSearch } from 'src/app/model/modelSearch';
import { BaseComponent } from '../base/BaseComponent';
import { BankReceiving, Cust, CustAcc, Member, MemberRequest, PagesRequest, TaiChinh, TaiChinhRequest, Tran, TransSchedule } from 'src/app/model/model';
import { TranService } from 'src/app/services/chuyen-tien/tran.service';
import { TransScheduleService } from 'src/app/services/chuyen-tien/trans-schedule.service';
import { QlMaTruyCapServiceService } from 'src/app/services/cai-dat/ql-ma-truy-cap-service.service';
import { QlTaiKhoanService } from 'src/app/services/cai-dat/ql-tai-khoan.service';
import { ACTION, FwError, LoaiGiaoDich, MEMBERSTATUS, ProductEnum, ProductType, SEX, STATUS, STATUSTRAN, StatusTran, TYPE, TYPETRAN, TaiChinhType } from 'src/app/common/constants';
import { TaiKhoanService } from 'src/app/services/tai-khoan/tai-khoan.service';
import { Observable, Subject, catchError, concat, distinctUntilChanged, of, switchMap, tap } from 'rxjs';
import { NganHangThuHuongService } from 'src/app/services/ngan-hang-thu-huong/ngan-hang-thu-huong.service';
import { MemberService } from 'src/app/services/member/member.service';

@Component({
  selector: 'app-quan-ly-tai-chinh',
  templateUrl: './quan-ly-tai-chinh.component.html',
  styleUrls: ['./quan-ly-tai-chinh.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class QuanLyTaiChinhComponent extends BaseComponent<Tran>{
  filterForm: FormGroup;

  productType = ProductType;

  loaiGDList: any[] = [
    { value: '0', label: 'Tất cả' },
    { value: '1', label: 'Loại GD 1' },
    { value: '2', label: 'Loại GD 1' },
  ];
  taiKhoanNguonList: any[] = [
    { value: '1', label: 'Tài khoản nguồn 1' },
    { value: '2', label: 'Tài khoản nguồn 2' },
    { value: '3', label: 'Tài khoản nguồn 3' },
  ];
  taiKhoanThuHuongList: any[] = [
    { value: '1', label: 'Tài khoản thụ hưởng 1' },
    { value: '2', label: 'Tài khoản thụ hưởng 2' },
  ];
  nguoiLapList: any[] = [
    { value: '1', label: 'Người lập 1' },
    { value: '2', label: 'Người lập 2' },
  ];
  loaiTienList: any[] = [
    { value: '1', label: 'Loại tiền 1' },
    { value: '2', label: 'Loại tiền 2' },
  ];
  displayMonths = 2;
  placement: any = ['bottom-start', 'bottom-end', 'top-start', 'top-end'];

  tranSearch: TranSearch = new TranSearch();

  taiChinhSearch: TaiChinhRequest = new TaiChinhRequest();

  membeRequest: MemberRequest = new MemberRequest();

  TaiChinhType = TaiChinhType;
  SEX = SEX;
  LoaiGiaoDichs = LoaiGiaoDich;
  statusTrans = StatusTran;
  MEMBERSTATUS = MEMBERSTATUS;
  ACTION = ACTION;
  TYPE = TYPE;

  STATUS = STATUS;
  pageTrans: PagesRequest = new PagesRequest();
  pageTransSchedule: PagesRequest = new PagesRequest();

  labelsTran: any = {
    previousLabel: 'Trước',
    nextLabel: 'Sau',
  };

  labelsTranSchedule: any = {
    previousLabel: 'Trước',
    nextLabel: 'Sau',
  };

  status = [{
    id: 1, name: 'Không hoạt động'
  }, {
    id: 0, name: 'Hoạt động'
  }]


  constructor(
    private memberServices: MemberService,
    private transScheduleService: TransScheduleService,
    private qlMaTruyCapServiceService: QlMaTruyCapServiceService,
    private qlTaiKhoanService: QlTaiKhoanService,
    private injector: Injector,
    private modalService: NgbModal,
    private taiKhoanService: TaiKhoanService,
    private bankReceiveService: NganHangThuHuongService,

  ) {
    super(injector);
    this.pageTrans.curentPage = 1;
    this.pageTrans.size = 10;
    this.pageTransSchedule.curentPage = 1;
    this.pageTransSchedule.size = 10;
    this.tranSearch.status = STATUSTRAN.CHODUYET;

  }


  asyncDataTran?: TaiChinh[];
  asyncDataTranThu: TaiChinh[] = [];
  asyncDataTranChi: TaiChinh[] = [];
  totalTran: number = 0;
  maxSizeTran: number = 10;

  asyncDataTranSchedule?: TransSchedule[];
  totalTranSchedule: number = 0;
  maxSizeTranSchedule: number = 10;

  loadCustAccItem: CustAcc;
  loadCustAcc: Observable<CustAcc[]>;
  loadCustAccInput = new Subject<string>();
  loadCustAccLoading = false;

  bankReceive: BankReceiving[] = [];


  ngOnInit(): void {
    this.onInit();

  }

  getBankReceive() {
    this.bankReceiveService
      .getBankReceive()
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data) {
            this.bankReceive = res.data;
          }
        } else {
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  override search(page: any) {
    this.searchTaiChinh(page);
  }

  private getCustAcc() {
    this.loadCustAcc = concat(
      this.taiKhoanService.getCusAccByName(), // default items
      this.loadCustAccInput.pipe(
        distinctUntilChanged(), tap(),
        switchMap(accInput => this.taiKhoanService.getCusAccByName(accInput).pipe(
          catchError(() => of([])), tap()
        ))
      )
    );
  }


  searchTaiChinh(page: any) {
    this.asyncDataTranThu = [];
    this.asyncDataTranChi = [];
    this.pageTrans.curentPage = page;
    this.memberServices
      .searchTaiChinhPaging(this.pageTrans, this.taiChinhSearch)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data?.totalElements) this.totalTran = res.data.totalElements;
          if (res.data?.content) {
            this.asyncDataTran = res.data.content;
            res.data.content.forEach(element => {
              console.log(element);

              if (element.type == '0') {
                this.asyncDataTranThu?.push(element);
              } else {
                this.asyncDataTranChi?.push(element);
              }
            });

          }
          this.closeFilterGDC();
        } else {
          this.toastrs.error(res.errorMessage);
        }

      });
  }

  searchTranSchedule(page: any) {
    this.pageTransSchedule.curentPage = page;
    console.log(page);
    console.log(this.pageTrans.curentPage);
    this.transScheduleService
      .searchPagings(this.pageTransSchedule, this.tranSearch)
      .subscribe((res) => {
        if (FwError.THANHCONG == res.errorCode) {
          if (res.data?.totalElements) this.totalTranSchedule = res.data.totalElements;
          if (res.data?.content) this.asyncDataTranSchedule = res.data.content;
          this.closeFilterGDC();
        } else {
          this.toastrs.error(res.errorMessage);
        }

      });
  }

  public open(filterGDCDModal: any): void {
    this.modalService.open(filterGDCDModal, {
      scrollable: true,
      centered: true,
    });
  }

  getCurrentTranPageNumber(index: number) {
    if (this.pageTrans.size) {
      return (this.pageTrans.curentPage - 1) * this.pageTrans.size + index;
    }
    return 0;
  }

  getCurrentTranSchedulePageNumber(index: number) {
    if (this.pageTransSchedule.size) {
      return (this.pageTransSchedule.curentPage - 1) * this.pageTransSchedule.size + index;
    }
    return 0;
  }

  public closeFilterGDC(filterGDCDModal?: any): void {
    this.modalService.dismissAll(filterGDCDModal);
  }


  trackByFnCustAcc(item: CustAcc) {
    return item.acc;
  }

  CommaFormatted(event: { which: number; }, type: string) {
    // skip for arrow keys
    if (event.which >= 37 && event.which <= 40) return;
    // format number
    if (type == 'from') {
      if (this.tranSearch.khoangTienTu) {
        this.tranSearch.khoangTienTu = this.tranSearch.khoangTienTu.replace(/\D/g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
    } else {
      if (this.tranSearch.khoangTienDen) {
        this.tranSearch.khoangTienDen = this.tranSearch.khoangTienDen.replace(/\D/g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
    }

  }

  numberCheck(args: { key: string; }) {
    if (args.key === 'e' || args.key === '+' || args.key === '-') {
      return false;
    } else {
      return true;
    }
  }

  checkLoaiGiaoDich(type: any, schedule: any) {
    let se = this.LoaiGiaoDichs.find(element => element.value === type && element.schedule === schedule);
    return se?.name;
  }

  getTaiChinh(type: any) {
    let se = this.TaiChinhType.find(element => element.value === type);
    return se?.name;
  }


  getSex(sex: any) {
    let se = this.SEX.find(element => element.value === sex);
    return se?.name;
  }

  genStatus(status: any) {
    let se = this.statusTrans.find(element => element.value === status);
    return se?.name;
  }

  getStatus(status: any) {
    let se = this.MEMBERSTATUS.find(element => element.value === status);
    return se?.name;
  }

  downloadExcel() {
      //0 la giao dich thuong
      this.memberServices
        .exportTaiChinh(this.taiChinhSearch, this.pageTrans);
  }

}

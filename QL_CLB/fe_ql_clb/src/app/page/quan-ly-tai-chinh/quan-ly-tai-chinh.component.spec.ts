import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanLyTaiChinhComponent } from './quan-ly-tai-chinh.component';

describe('QuanLyTaiChinhComponent', () => {
  let component: QuanLyTaiChinhComponent;
  let fixture: ComponentFixture<QuanLyTaiChinhComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuanLyTaiChinhComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuanLyTaiChinhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

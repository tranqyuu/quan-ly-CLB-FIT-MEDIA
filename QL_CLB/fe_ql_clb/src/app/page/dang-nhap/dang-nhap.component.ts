import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Constants } from 'src/app/common/constants';
import { LoginRequest } from 'src/app/model/model';
import { AuthService } from 'src/app/services/auth/auth.service';
import { I18nService } from 'src/app/shared/i18n.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dang-nhap',
  templateUrl: './dang-nhap.component.html',
  styleUrls: ['./dang-nhap.component.css'],
})
export class DangNhapComponent implements OnInit {
  isSubmit = false
  loginRequest: LoginRequest = new LoginRequest()
  homeUrl = 'trang-chu'
  loginForm = this.formBuilder.group({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });
  constructor(private formBuilder: FormBuilder, private authService: AuthService,
    private router: Router, private el: ElementRef, public i18nService: I18nService) {

  }
  ngOnInit() {
  }
  focusInValidForm() {
    let invalidElements = this.el.nativeElement.querySelectorAll('textarea.form-control.ng-invalid, input.form-control.ng-invalid, datepicker.form-control.ng-invalid input');
    if (invalidElements.length > 0) {
      invalidElements[0].focus();
    }
  }
  onSubmit(): void {
    // Process checkout data here
    this.isSubmit = true
    if (this.loginForm.status == 'INVALID') {
      this.focusInValidForm()
      return;
    } else if (this.loginForm.status == 'PENDING') {
      Swal.fire({
        position: 'top-end',
        icon: 'info',
        title: this.i18nService.getString('hethongdangxuly'),
        showConfirmButton: false,
        timer: 1500
      })
      return;
    } else {
      this.authService.login(this.loginRequest).subscribe({
        next: loginResponse => {
          if (Constants.OK == loginResponse.errorCode) {
            this.router.navigate([this.homeUrl]);
          } else {
            Swal.fire({
              icon: 'error',
              title: loginResponse.errorMessage,
              showConfirmButton: false,
              timer: 1500
            })
          }
        },
        error: error => {
          if (error.error instanceof ProgressEvent) {
            Swal.fire(this.i18nService.getString('loiketnoi'), '', 'error');
          } else {
            if (404 == error.error.status) {
              Swal.fire(this.i18nService.getString('loiketnoi'), '', 'error');
            } else if (401 == error.error.status) {
              Swal.fire({
                icon: 'error',
                title: this.i18nService.getString('saithongtindangnhap'),
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal.fire({
                icon: 'error',
                title: this.i18nService.getString('loikhongxacdinh'),
                showConfirmButton: false,
                timer: 1500
              })
            }
          }
        }
      });
    }
  }
}


export class Constants {
    public static OK = "OK"
    public static ERROR = "ERROR"
}

export enum FwError {
    THANHCONG = "THANHCONG",
    KHONGTHANHCONG = "KHONGTHANHCONG",
    DLKHONGTONTAI = "DLKHONGTONTAI",
    DLDATONTAI = "DLDATONTAI",
    DUPLICATEPASSWORD = "DUPLICATEPASSWORD",
    PASSWORDNOTFOUND = "PASSWORDNOTFOUND",
    PASSWORDFAILLIMIT = "PASSWORDFAILLIMIT"
}
export enum FwAction {
    CREATE = "CREATE",
    UPDATE = "UPDATE",
    DETAIL = "DETAIL",
    SEARCH = "SEARCH"
}

export enum CustStatus {
    CHOKICHHOAT = "0",
    HOATDONG = "1",
    KHOA = "2",
    HUY = "3"
}

export const Service = [
    {
        value: '1',
        name: 'Chuyển khoản 24/7 đến số tài khoản',
    },
    {
        value: '2',
        name: 'Chuyển khoản liên ngân hàng',
    },
    {
        value: '3',
        name: 'Chuyển khoản nội bộ CB',
    },
    {
        value: '4',
        name: 'Chuyển khoản theo danh sách',
    },
    {
        value: '5',
        name: 'Thanh toán hóa đơn điện',
    },
    {
        value: '6',
        name: 'Thanh toán hóa đơn nước',
    },
    {
        value: '7',
        name: 'Thanh toán cước điện thoại cố định',
    },
    {
        value: '8',
        name: 'Thanh toán cước di động trả sau',
    },
    {
        value: '9',
        name: 'Thanh toán cước Internet ADSL',
    },
    {
        value: '10',
        name: 'Gửi tiết kiệm online',
    },
]

export const CHUYENTIEN = [
    {
        value: '1',
        name: 'Chuyển khoản 24/7 đến số tài khoản',
    },
    {
        value: '2',
        name: 'Chuyển khoản liên ngân hàng',
    },
    {
        value: '3',
        name: 'Chuyển khoản nội bộ CB',
    },
    {
        value: '4',
        name: 'Chuyển khoản theo danh sách',
    }
]
export const THANHTOAN = [
    {
        value: '5',
        name: 'Thanh toán hóa đơn điện',
    },
    {
        value: '6',
        name: 'Thanh toán hóa đơn nước',
    },
    {
        value: '7',
        name: 'Thanh toán cước điện thoại cố định',
    },
    {
        value: '8',
        name: 'Thanh toán cước di động trả sau',
    },
    {
        value: '9',
        name: 'Thanh toán cước Internet ADSL',
    }
]


export const Position = [
    {
        value: '1',
        name: 'Quản lý',
    },
    {
        value: '2',
        name: 'Lập lệnh',
    },
    {
        value: '3',
        name: 'Duyệt lệnh',
    }
]

export enum YesNo {
    YES = "1",
    NO = "0"
}

export enum TypeAcc {
    TRUYVAN = "1",
    GIAODICH = "2"
}

export enum AccType {
    TAIKHOAN = "1",
    TIENGUI = "2",
    TIENVAY = "3"
}

export enum POSITION {
    QUANTRI = "1",
    LAPLENH = "2",
    DUYETLENH = "3"
}

export const SEX = [
    {
        value: 0,
        name: 'Nam',
    },
    {
        value: 1,
        name: 'Nữ',
    }
]

export const TaiChinhType = [
    {
        value: "0",
        name: 'Khoản thu',
    },
    {
        value: "1",
        name: 'Khoản chi',
    }
]


export const LoaiGiaoDich = [
    {
        value: null,
        name: 'Tất cả',
        schedule: null,
    },
    {
        value: 'CORE',
        name: 'Chuyển tiền nội bộ',
        schedule: null,
    },
    {
        value: 'CORE',
        name: 'Chuyển tiền định kỳ nội bộ',
        schedule: '1',
    },
    {
        value: 'CORE',
        name: 'Chuyển tiền tương lai nội bộ',
        schedule: '0',
    },
    {
        value: 'CITAD',
        name: 'Chuyển tiền liên ngân hàng',
        schedule: null,
    },
    {
        value: 'CITAD',
        name: 'Chuyển tiền định kỳ liên ngân hàng',
        schedule: '1',
    },
    {
        value: 'CITAD',
        name: 'Chuyển tiền tương lai liên ngân hàng',
        schedule: '0',
    },
    {
        value: 'NAPAS',
        name: 'Chuyển tiền nhanh Napas 247',
        schedule: null,
    },
    // {
    //     value: '1',
    //     name: 'Thanh toán',
    //     schedule: null,
    // }
]

export const LoaiGiaoDichTuongLai = [
    {
        value: 'CORE',
        name: 'Chuyển tiền định kỳ nội bộ',
        schedule: '1',
    },
    {
        value: 'CORE',
        name: 'Chuyển tiền tương lai nội bộ',
        schedule: '0',
    },
    {
        value: 'CITAD',
        name: 'Chuyển tiền định kỳ liên ngân hàng',
        schedule: '1',
    },
    {
        value: 'CITAD',
        name: 'Chuyển tiền tương lai liên ngân hàng',
        schedule: '0',
    }
]

export const StatusTran = [
    {
        value: null,
        name: 'Tất cả',
    },
    {
        value: '0',
        name: 'Chờ duyệt',
    },
    {
        value: '1',
        name: 'Từ chối',
    },
    {
        value: '2',
        name: 'Đã duyệt',
    },
    {
        value: '3',
        name: 'Hủy',
    },
    {
        value: '4',
        name: 'Hoàn thành',
    },
    {
        value: '5',
        name: 'Không thực hiện được',
    }
]

export const MEMBERSTATUS = [
    {
        value: 1,
        name: 'Đang hoạt động',
    },
    {
        value: 0,
        name: 'Không còn hoạt động',
    }
]

export const CHUCVUSTATUS = [
    {
        value: "1",
        name: 'Còn hiệu lực',
    },
    {
        value: "0",
        name: 'Không hiệu lực',
    }
]

export const LOCATION = [
    {
        value: "0",
        name: 'Hà Nội',
    },
    {
        value: "1",
        name: 'Hà Nam',
    }
]

export const ACTIVITIESSTATUS = [
    {
        value: "0",
        name: 'Đang hoạt động',
    },
    {
        value: "2",
        name: 'Đã hoàn thành',
    }
]

export const CSVCSTATUS = [
    {
        value: "0",
        name: 'Còn trong kho',
    },
    {
        value: "1",
        name: 'Không còn trong kho',
    }
]

export const TAICHINH = [
    {
        value: "0",
        name: 'Khoản thu',
    },
    {
        value: "1",
        name: 'Khoản chi',
    }
]

export const MISSIONSTATUS = [
    {
        value: "0",
        name: 'Đang thực hiện',
    },
    {
        value: "1",
        name: 'Đã hoàn thành',
    }
]

export const AWARDSTATUS = [
    {
        value: "0",
        name: 'Cộng điểm do có thành tích tốt',
    },
    {
        value: "1",
        name: 'Trừ điểm vì không tích cực tham gia',
    }
]

export const ACTIVITIESTYPE = [
    {
        value: "0",
        name: 'Hoạt động của Khoa',
    },
    {
        value: "1",
        name: 'Hoạt động của trường',
    },
    {
        value: "2",
        name: 'Hoạt động của CLB',
    }
]

export const AWARDTYPE = [
    {
        value: "0",
        name: 'Cộng điểm do có thành tích tốt',
    },
    {
        value: "1",
        name: 'Trừ điểm vì không tích cực tham gia',
    },

]



export const StatusTranCap1 = [
    {
        value: null,
        name: 'Tất cả',
    },
    {
        value: '4',
        name: 'Hoàn thành',
    },
    {
        value: '5',
        name: 'Không thực hiện được',
    }
]

export enum STATUSTRAN {
    CHODUYET = "0",
    TUCHOI = "1",
    DADUYET = "2",
    HUY = "3",
    HOANTHANH = "4",
    LOI = "5"
}
export enum STATUS {
    HOATDONG = "0",
    HETHOATDONG = "1",
    HOANTHANH = "2"
}

export const PaymentStatus = [
    {
        value: null,
        name: 'Tất cả',
    },
    {
        value: '0',
        name: 'Chờ xử lý',
    },
    {
        value: '1',
        name: 'Thành công',
    },
    {
        value: '2',
        name: 'Lỗi',
    },
    {
        value: '3',
        name: 'Hủy',
    }
]

export enum PAYMENTSTATUS {
    CHUATHUCHIEN = "0",
    THANHCONG = "1",
    LOI = "2",
    HUY = "3",
}

export enum TYPETRAN {
    CORE = "CORE",
    NAPAS = "NAPAS",
    CITAD = "CITAD"
}

export const ProductEnum = [
    {
        value: 1,
        name: 'Chuyển khoản 24/7 đến số tài khoản',
    },
    {
        value: 2,
        name: 'Chuyển khoản liên ngân hàng',
    },
    {
        value: 3,
        name: 'Chuyển khoản nội bộ CB',
    }
]

export const ProductType = [
    {
        value: 'CORE',
        name: 'Chuyển khoản nội bộ CB',
        product: '3'
    },
    {
        value: 'CITAD',
        name: 'Chuyển khoản liên ngân hàng',
        product: '2'
    },
    {
        value: 'NAPAS',
        name: 'Chuyển khoản 24/7 đến số tài khoản',
        product: '1'
    }
]

export enum MOHINH {
    CAP1 = "1",
    CAP2 = "2"
}

export enum TYPESCHEDULE {
    TUONGLAI = "0",
    DINHKY = "1"
}


export enum FREQUENCY {
    NGAY = "1",
    TUAN = "2",
    THANG = "3",
    QUY = "4",
    NAM = "5"
}

export const Frequency = [
    {
        value: 1,
        name: 'Ngày',
    },
    {
        value: 2,
        name: 'Tuần',
    },
    {
        value: 3,
        name: 'Tháng',
    },
    {
        value: 4,
        name: 'Quý',
    },
    {
        value: 5,
        name: 'Năm',
    }
]

export enum TRANSLOTSTATUS {
    DANGXULY = "0",
    BANGKELOI = "1",
    BANGKEHOPLE = "2"
}

export const TransLotStatus = [
    {
        value: 0,
        name: 'Đang xử lý',
    },
    {
        value: 1,
        name: 'Bảng kê lỗi',
    },
    {
        value: 2,
        name: 'Bảng kê hợp lệ',
    }
]
export enum ACTION {
    DUYET = "duyet",
    QUANLY = "quanly",
    XEM = "xem"
}

export enum TYPE {
    THUONG = 'THUONG',
    TUONGLAIDINHKY = 'TUONGLAIDINHKY'
}
export enum ISSCHEDULE {
    YES = "1",
    NO = "0"
}

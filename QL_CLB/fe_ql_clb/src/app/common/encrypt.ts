import * as CryptoJS from 'crypto-js';
import { Payload } from '../model/model';
export class encrypt{
    static password = 'Thisisatestkeyfortesting';
public static encryptAES = (message:any) => {
    console.log('message :' + message);
   

    var hash = CryptoJS.SHA1(this.password);
    var key = CryptoJS.lib.WordArray.create(hash.words.slice(0, 16 / 4));
    console.log(key.toString(CryptoJS.enc.Hex));

    const encrypted = CryptoJS.AES.encrypt(JSON.stringify(message), key, {
      //keySize: 16,									// not necessary
      //iv: iv,												// not necessary for ECB
      mode: CryptoJS.mode.ECB,
      //padding: CryptoJS.pad.Pkcs7		// default
    });
    console.log('Encrypted :' + encrypted.toString());
    let payload=new Payload(); 
    payload.d=encrypted.toString()
    return payload;
  };

  /**
   * Decrypt an encrypted message
   * @param encryptedBase64 encrypted data in base64 format
   * @param key The secret key
   * @return The decrypted content
   */
public static decryptAES = (encryptedBase64:Payload) => {
    var hash = CryptoJS.SHA1(this.password);
    var key = CryptoJS.lib.WordArray.create(hash.words.slice(0, 16 / 4));
    console.log(key.toString(CryptoJS.enc.Hex));

    const decrypted = CryptoJS.AES.decrypt(encryptedBase64.d, key, {
      //keySize: 16,									// not necessary
      //iv: iv,												// not necessary for ECB
      mode: CryptoJS.mode.ECB,
      //padding: CryptoJS.pad.Pkcs7		// default
    });
    console.log('decrypted :' + decrypted);
    const str = decrypted.toString(CryptoJS.enc.Utf8);
    console.log('decrypted :' + str);
    return JSON.parse(str);;
  };
}
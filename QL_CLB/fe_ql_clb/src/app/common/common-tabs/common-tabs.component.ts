import {
  Component,
  AfterContentInit,
  ContentChildren,
  QueryList,
  Input,
} from '@angular/core';
import { CommonTabComponent } from './common-tab/common-tab.component';

@Component({
  selector: 'app-common-tabs',
  templateUrl: './common-tabs.component.html',
  styleUrls: ['./common-tabs.component.css'],
})
export class CommonTabsComponent implements AfterContentInit {
  @ContentChildren(CommonTabComponent) tabs?: QueryList<CommonTabComponent>;
  @Input('bodyClass') bodyClass?: string;

  // contentChildren are set
  ngAfterContentInit() {
    // get all active tabs
    let activeTabs = this.tabs!.filter((tab) => tab.active);

    // if there is no active tab set, activate the first
    if (activeTabs.length === 0) {
      this.selectTab(this.tabs!.first);
    }
  }

  selectTab(tab: any) {
    // deactivate all tabs
    this.tabs!.toArray().forEach((tab) => (tab.active = false));

    // activate the tab the user has clicked on.
    tab.active = true;
  }
  getClass(bodyClass: string) {
    let value = bodyClass;
    return {
      value: bodyClass,
    };
  }
}

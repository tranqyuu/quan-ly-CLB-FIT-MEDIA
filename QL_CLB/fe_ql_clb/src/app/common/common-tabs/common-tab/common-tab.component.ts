import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-common-tab',
  templateUrl: './common-tab.component.html',
  styleUrls: ['./common-tab.component.css'],
})
export class CommonTabComponent implements OnInit {
  constructor() {}
  @Input('title') title: string = 'Title';
  @Input('badge') badge: number = 0;
  @Input('bodyClass') bodyClass?: string;
  @Input() active = false;
  ngOnInit() {}
}

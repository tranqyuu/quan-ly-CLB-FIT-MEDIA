import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';

import { map } from 'rxjs/operators';
import { encrypt } from '../common/encrypt';
import { environment } from 'src/environments/environment';

@Injectable()
export class EncryptInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    // debugger
    if (environment.encrypt && request.method == "POST" && request.body) {
        const cloneReq = request.clone({
            body: encrypt.encryptAES(request.body)
        });
        return next.handle(cloneReq).pipe(
          map(event => this.decryptResponse(event))
        );
    }
    return next.handle(request);
  }
  private decryptResponse(event: HttpEvent<any>) {
    if (event instanceof HttpResponse) {
      return event.clone({body: encrypt.decryptAES(event.body)});
    } else {
      return event;
    }
  }
}

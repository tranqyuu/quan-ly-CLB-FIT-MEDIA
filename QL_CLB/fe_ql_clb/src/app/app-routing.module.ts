import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from 'src/app/page/page-not-found/page-not-found.component';
import { TrangChuComponent } from 'src/app/page/trang-chu/trang-chu.component';
import { DangNhapComponent } from './page/dang-nhap/dang-nhap.component';
import { GdChoDuyetComponent } from './page/gd-cho-duyet/gd-cho-duyet.component';
import { QlMaTruyCapComponent } from './page/cai-dat/ql-ma-truy-cap/ql-ma-truy-cap.component';
import { QlQuyenTruyCapComponent } from './page/cai-dat/ql-quyen-truy-cap/ql-quyen-truy-cap.component';
import { ThemMaTruyCapComponent } from './page/cai-dat/ql-ma-truy-cap/them-ma-truy-cap/them-ma-truy-cap.component';
import { LsChinhSuaMaTruyCapComponent } from './page/cai-dat/ql-ma-truy-cap/ls-chinh-sua-ma-truy-cap/ls-chinh-sua-ma-truy-cap.component';
import { CaiDatHanMucComponent } from './page/cai-dat/cai-dat-han-muc/cai-dat-han-muc.component';
import { QlQuyenTruyCapChiTietComponent } from './page/cai-dat/ql-quyen-truy-cap/ql-quyen-truy-cap-chi-tiet/ql-quyen-truy-cap-chi-tiet.component';
import { AuthGuard } from './auth/auth.guard';
import { Layout1Component } from './layout/layout1/layout1.component';
import { FwAction } from './common/constants';
import { DoiMatKhauComponent } from './page/tien-ich/doi-mat-khau/doi-mat-khau.component';
import { LienHeComponent } from './page/tien-ich/ho-tro/lien-he/lien-he.component';
import { CapNhatQuyenTruyVanComponent } from './page/cai-dat/ql-quyen-truy-cap/ql-quyen-truy-cap-chi-tiet/cap-nhat-quyen-truy-van/cap-nhat-quyen-truy-van.component';
import { CapNhatQuyenGiaoDichComponent } from './page/cai-dat/ql-quyen-truy-cap/ql-quyen-truy-cap-chi-tiet/cap-nhat-quyen-giao-dich/cap-nhat-quyen-giao-dich.component';
import { CapNhatHanMucComponent } from './page/cai-dat/cai-dat-han-muc/cap-nhat-han-muc/cap-nhat-han-muc.component';
import { GdChoDuyetChiTietComponent } from './page/gd-cho-duyet/gd-cho-duyet-chi-tiet/gd-cho-duyet-chi-tiet.component';
import { DanhBaThuHuongComponent } from './page/tien-ich/quan-ly-danh-ba/danh-ba-thu-huong/danh-ba-thu-huong.component';
import { ThemMoiDanhBaThuHuongComponent } from './page/tien-ich/quan-ly-danh-ba/them-moi-danh-ba-thu-huong/them-moi-danh-ba-thu-huong.component';
import { CapNhatDanhBaThuHuongComponent } from './page/tien-ich/quan-ly-danh-ba/cap-nhat-danh-ba-thu-huong/cap-nhat-danh-ba-thu-huong.component';
import { LichSuChinhSuaDanhBaThuHuongComponent } from './page/tien-ich/quan-ly-danh-ba/lich-su-chinh-sua-danh-ba-thu-huong/lich-su-chinh-sua-danh-ba-thu-huong.component';
import { DieuKhoanDichVuComponent } from './page/tien-ich/ho-tro/dieu-khoan-dich-vu/dieu-khoan-dich-vu.component';
import { ChiTietDieuKhoanDichVuComponent } from './page/tien-ich/ho-tro/dieu-khoan-dich-vu/chi-tiet-dieu-khoan-dich-vu/chi-tiet-dieu-khoan-dich-vu.component';
import { QuanLyHdComponent } from './page/quan-ly-hd/quan-ly-hd.component';
import { QuanLyChiTietComponent } from './page/quan-ly-hd/quan-ly-chi-tiet/quan-ly-chi-tiet.component';
import { QuanLyCsvcComponent } from './page/quan-ly-csvc/quan-ly-csvc.component';
import { QuanLyCsvcChiTietComponent } from './page/quan-ly-csvc/quan-ly-csvc-chi-tiet/quan-ly-csvc-chi-tiet.component';
import { QuanLyTaiChinhComponent } from './page/quan-ly-tai-chinh/quan-ly-tai-chinh.component';
import { QuanLyTaiChinhChiTietComponent } from './page/quan-ly-tai-chinh/quan-ly-tai-chinh-chi-tiet/quan-ly-tai-chinh-chi-tiet.component';
import { QuanLyChiTietHdThanhVienComponent } from './page/quan-ly-hd/quan-ly-chi-tiet-hd-thanh-vien/quan-ly-chi-tiet-hd-thanh-vien.component';
import { QuanLyChucVuComponent } from './page/quan-ly-chuc-vu/quan-ly-chuc-vu.component';
import { QuanLyChucVuChiTietComponent } from './page/quan-ly-chuc-vu/quan-ly-chuc-vu-chi-tiet/quan-ly-chuc-vu-chi-tiet.component';

const routes: Routes = [

  { path: 'login', component: DangNhapComponent },
  {
    path: '', component: Layout1Component, canActivate: [AuthGuard],
    children:
      [
        { path: '', pathMatch: 'full', redirectTo: 'trang-chu' },
        {
          path: 'trang-chu',
          component: TrangChuComponent,
          title: 'Trang chủ',
        },
        {
          path: 'quan-ly-hoat-dong',
          title: 'Quản lý hoạt động',
          children: [
            {
              path: 'quan-ly-hoat-dong',
              title: 'Quản lý hoạt động',
              children: [
                {
                  path: '',
                  data: { action: FwAction.SEARCH },
                  component: QuanLyHdComponent,
                  title: 'Quản lý hoạt động',
                },
                {
                  path: ':type/:id',
                  data: { action: FwAction.DETAIL },
                  component: QuanLyChiTietComponent,
                  title: 'Chi tiết quản lý hoạt động',
                },
                {
                  path: ':id/thanhvien/:type/create',
                  data: { action: FwAction.CREATE },
                  component: QuanLyChiTietHdThanhVienComponent,
                  title: 'Chi tiết thành viên hoạt động',
                },
                {
                  path: ':id/thanhvien/:type/detail',
                  data: { action: FwAction.CREATE },
                  component: QuanLyChiTietHdThanhVienComponent,
                  title: 'Chi tiết thành viên hoạt động',
                },
                {
                  path: ':id/thanhvien/:type/detail/:itemid',
                  data: { action: FwAction.DETAIL },
                  component: QuanLyChiTietHdThanhVienComponent,
                  title: 'Chi tiết thành viên hoạt động',
                },
                {
                  path: ':type',
                  data: { action: FwAction.CREATE },
                  component: QuanLyChiTietComponent,
                  title: 'Chi tiết quản lý hoạt động',
                }
              ]
            },
          ]
        },
        {
          path: 'quan-ly-csvc',
          title: 'Quản lý cơ sở vật chất',
          children: [
            {
              path: 'quan-ly-csvc',
              title: 'Quản lý cơ sở vật chất',
              children: [
                {
                  path: '',
                  data: { action: FwAction.SEARCH },
                  component: QuanLyCsvcComponent,
                  title: 'Quản lý cơ sở vật chất',
                },
                {
                  path: ':type/:id',
                  data: { action: FwAction.DETAIL },
                  component: QuanLyCsvcChiTietComponent,
                  title: 'Chi tiết quản lý cơ sở vật chất',
                },
                {
                  path: ':type',
                  data: { action: FwAction.CREATE },
                  component: QuanLyCsvcChiTietComponent,
                  title: 'Chi tiết quản lý cơ sở vật chất',
                }
              ]
            },
          ]
        }, {
          path: 'quan-ly-chuc-vu',
          title: 'Quản lý chức vụ',
          children: [
            {
              path: 'quan-ly-chuc-vu',
              title: 'Quản lý chức vụ',
              children: [
                {
                  path: '',
                  data: { action: FwAction.SEARCH },
                  component: QuanLyChucVuComponent,
                  title: 'Quản lý chức vụ',
                },
                {
                  path: ':type/:id',
                  data: { action: FwAction.DETAIL },
                  component: QuanLyChucVuChiTietComponent,
                  title: 'Chi tiết quản lý chức vụ',
                },
                {
                  path: ':type',
                  data: { action: FwAction.CREATE },
                  component: QuanLyChucVuChiTietComponent,
                  title: 'Chi tiết quản lý chức vụ',
                }
              ]
            },
          ]
        },
        {
          path: 'quan-ly-thanh-vien',
          title: 'Quản lý thành viên',
          children: [
            {
              path: 'quan-ly-thanh-vien',
              title: 'Quản lý hoạt động',
              children: [
                {
                  path: '',
                  data: { action: FwAction.SEARCH },
                  component: GdChoDuyetComponent,
                  title: 'Quản lý thành viên',
                },
                {
                  path: ':type/:id',
                  data: { action: FwAction.DETAIL },
                  component: GdChoDuyetChiTietComponent,
                  title: 'Chi tiết quản lý thành viên',
                },
                {
                  path: ':type',
                  data: { action: FwAction.CREATE },
                  component: GdChoDuyetChiTietComponent,
                  title: 'Chi tiết quản lý thành viên',
                }
              ]
            },
          ]
        },
        {
          path: 'quan-ly-tai-chinh',
          title: 'Quản lý tài chính',
          children: [
            {
              path: 'quan-ly-tai-chinh',
              title: 'Quản lý tài chính',
              children: [
                {
                  path: '',
                  data: { action: FwAction.SEARCH },
                  component: QuanLyTaiChinhComponent,
                  title: 'Quản lý tài chính',
                },
                {
                  path: ':type/:id',
                  data: { action: FwAction.DETAIL },
                  component: QuanLyTaiChinhChiTietComponent,
                  title: 'Chi tiết quản lý tài chính',
                },
                {
                  path: ':type',
                  data: { action: FwAction.CREATE },
                  component: QuanLyTaiChinhChiTietComponent,
                  title: 'Chi tiết quản lý tài chính',
                }
              ]
            },
          ]
        },

        {
          path: 'quan-ly-giao-dich',
          title: 'Quản lý giao dịch',
          children: [
            {
              path: 'gd-cho-duyet',
              title: 'Giao dịch chờ duyệt',
              children: [
                {
                  path: '',
                  data: { action: FwAction.SEARCH },
                  component: GdChoDuyetComponent,
                  title: 'Giao dịch chờ duyệt',
                },
                {
                  path: ':type/:id',
                  data: { action: FwAction.DETAIL },
                  component: GdChoDuyetChiTietComponent,
                  title: 'Giao dịch chờ duyệt chi tiết',
                }
              ]
            },
          ],
        },
        {
          path: 'cai-dat',
          title: 'Cài đặt',
          children: [
            {
              path: 'ql-ma-truy-cap',
              // component: QlMaTruyCapComponent,
              title: 'Quản lý mã truy cập',
              children: [
                {
                  path: '',
                  data: { action: FwAction.SEARCH },
                  component: QlMaTruyCapComponent,
                  title: 'Quản lý mã truy cập',
                },
                {
                  path: 'them-ma-truy-cap',
                  data: { action: FwAction.CREATE },
                  component: ThemMaTruyCapComponent,
                  title: 'Thêm mã truy cập',
                },
                {
                  path: 'sua-ma-truy-cap/:id',
                  data: { action: FwAction.UPDATE },
                  component: ThemMaTruyCapComponent,
                  title: 'Chỉnh sửa mã truy cập',
                },
                {
                  path: 'ls-chinh-sua-ma-truy-cap/:id',
                  component: LsChinhSuaMaTruyCapComponent,
                  title: 'Lịch sử chỉnh sửa mã truy cập',
                }
              ]
            },
            {
              path: 'ql-quyen-truy-cap',
              data: { action: FwAction.SEARCH },
              component: QlQuyenTruyCapComponent,
              title: 'Quản lý quyền truy cập',
            },
            {
              path: 'ql-quyen-truy-cap/:id',
              title: 'Quản lý quyền truy cập',
              // component: QlQuyenTruyCapChiTietComponent,
              children: [
                {
                  path: '',
                  data: { action: FwAction.DETAIL },
                  component: QlQuyenTruyCapChiTietComponent,
                  title: 'Quản lý chi tiết quyền truy cập',
                },
                {
                  path: 'cap-nhat-quyen-truy-van',
                  data: { action: FwAction.UPDATE },
                  component: CapNhatQuyenTruyVanComponent,
                  title: 'Cập nhật quyền truy vấn',
                },
                {
                  path: 'cap-nhat-quyen-giao-dich',
                  data: { action: FwAction.UPDATE },
                  component: CapNhatQuyenGiaoDichComponent,
                  title: 'Cập nhật quyền giao dịch',
                }
              ]
            },
            {
              path: 'cai-dat-han-muc',
              component: CaiDatHanMucComponent,
              title: 'Cài đặt hạn mức',
            },
            {
              path: 'cap-nhat-han-muc/:code',
              component: CapNhatHanMucComponent,
              title: 'Cập nhật hạn mức',
            },

          ],
        },
        {
          path: 'tien-ich',
          title: 'Tiện ích',
          children: [
            {
              path: 'doi-mat-khau',
              component: DoiMatKhauComponent,
              title: 'Đổi mật khẩu',

            },
            {
              path: 'lien-he',
              component: LienHeComponent,
              title: 'Liên hệ',

            },
            {
              path: 'dieu-khoan-dich-vu',
              // component: DieuKhoanDichVuComponent,
              title: 'Điều khoản dịch vụ',
              children: [
                {
                  path: '',
                  component: DieuKhoanDichVuComponent,
                  title: 'Điều khoản dịch vụ',
                },
                {
                  path: 'chi-tiet-dieu-khoan-dich-vu/:id',
                  component: ChiTietDieuKhoanDichVuComponent,
                  title: 'Chi tiết điều khoản dịch vụ',
                },
              ]
            },
            {
              path: 'danh-ba-thu-huong',
              component: DanhBaThuHuongComponent,
              title: 'Danh bạ thụ hưởng',
            },
            {
              path: 'them-moi-danh-ba-thu-huong',
              component: ThemMoiDanhBaThuHuongComponent,
              title: 'Thêm mới danh bạ thụ hưởng',
            },
            {
              path: 'cap-nhat-danh-ba-thu-huong/:id',
              component: CapNhatDanhBaThuHuongComponent,
              title: 'Cập nhật danh bạ thụ hưởng',
            },
            {
              path: 'lich-su-chinh-sua-danh-ba-thu-huong/:id',
              component: LichSuChinhSuaDanhBaThuHuongComponent,
              title: 'Lịch sử chỉnh sửa danh bạ thụ hưởng',
            }
          ],
        }
      ],
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }

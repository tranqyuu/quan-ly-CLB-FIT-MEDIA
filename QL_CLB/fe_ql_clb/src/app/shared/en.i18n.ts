const enI18n = {
  loiketnoi: 'Không thể kết nối đến hệ thống!',
  saithongtindangnhap: 'Thông tin đăng nhập không chính xác!',
  hethongdangxuly: 'Hệ thống đang xử lý dữ liệu!',
  thongtinbatbuoc: 'Trường thông tin bắt buộc!',
  loikhongxacdinh: 'Lỗi không xác định! Liên hệ quản trị viên',
  sodutaikhoankhongkhadung: 'Số dư tài khoản không khả dụng!',
  khongthaynguoithuhuong: 'Không tìm thấy người thụ hưởng!',
};


export default enI18n;

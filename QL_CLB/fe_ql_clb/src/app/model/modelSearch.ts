export class TranSearch {
  type: string | undefined;
  schedule: string | undefined;
  taiKhoanNguon: string | undefined;
  taiKhoanThuHuong: string | undefined;
  tenNguoiThuHuong: string | undefined;
  thoiGianLapLenhTu: Date | undefined;
  thoiGianLapLenhDen: Date | undefined;
  thoiGianDuyetLenhTu: Date | undefined;
  thoiGianDuyetLenhDen: Date | undefined;
  userLapLenh: string | undefined;
  userDuyetLenh: string | undefined;
  status: string | undefined;
  paymentStatus: string | undefined;
  maGiaoDich: string | undefined;
  khoangTienTu: string | undefined;
  khoangTienDen: string | undefined;
  nganHangThuHuong: string | undefined;
}


export class CsvcSearch {
  ma?: string;
  status?: string;
}
export interface PhotosModel {
  id: string
  url: string
}

export class LoginRequest {
  username: string
  password: string
  captcha: string
}

export class LoginResponse {
  errorCode: string
  errorMessage: string
  token: string
  type: string
  refreshToken: string
  id: Number
  username: string
  email: string
  roles: string[]
}

export class Payload {
  d: string
}

export class BaseResponse<T> {
  [x: string]: any;
  errorCode?: string;
  errorMessage?: string;
  data?: T;
}

export class BasePagingResponse<T> {
  errorCode?: string;
  errorMessage?: string;
  data?: PageResponse<T>;
}
export class PageResponse<T> {
  first: boolean = false;
  last: boolean = false;
  totalPages?: number;
  totalElements?: number;
  size?: number;
  number?: number;
  numberOfElements?: number;
  content?: T[];
}

export class Auditable {
  createdBy: User | undefined;
  createdAt: Date | undefined;
  updatedBy: User | undefined;
  updatedAt: Date | undefined;
  createdByCust: Cust | undefined;
  updatedByCust: Cust | undefined;
}
export class BankCitad extends Auditable {
  code: string | undefined;
  isttt: string | undefined;
  name: string | undefined;
  nameEn: string | undefined;
  sortname: string | undefined;
  status: string | undefined;
  swiftCode: string | undefined;
}
export class BankNapa extends Auditable {
  code: string | undefined;
  address: string | undefined;
  name: string | undefined;
  nameEn: string | undefined;
  sortname: string | undefined;
  status: string | undefined;
}
export class Branch extends Auditable {
  code: string | undefined;
  address: string | undefined;
  fax: string | undefined;
  location: string | undefined;
  name: string | undefined;
  status: string | undefined;
  tel: string | undefined;
  type: string | undefined;
  province: Province | undefined;
}
export class BusinessForm extends Auditable {
  code: string | undefined;
  name: string | undefined;
  companies: Company[] | undefined;
}
export class Company extends Auditable {
  id?: number;
  address: string | undefined;
  approveType: string | undefined;
  branchCode: string | undefined;
  businessCode: string | undefined;
  businessIssuedon: Date | undefined;
  businessPlaceofissue: string | undefined;
  businesslineCode: string | undefined;
  businesslineName: string | undefined;
  cif: string | undefined;
  cifCreatedDate: Date | undefined;
  cifStatus: string | undefined;
  country: string | undefined;
  custBirthday: Date | undefined;
  custCode: string | undefined;
  custEmail: string | undefined;
  custFullname: string | undefined;
  custIdno: string | undefined;
  custIndentitypapers: string | undefined;
  custNmemonicName: string | undefined;
  custPosition: string | undefined;
  custTel: string | undefined;
  email: string | undefined;
  fax: string | undefined;
  feeAccountNo: string | undefined;
  feeBranchCode: string | undefined;
  fullname: string | undefined;
  fullnameEn: string | undefined;
  lrAuthorized: string | undefined;
  lrAuthorizedEnddate: Date | undefined;
  lrAuthorizedStartdate: Date | undefined;
  lrDate: Date | undefined;
  lrDefault: string | undefined;
  lrFullname: string | undefined;
  lrIdno: string | undefined;
  lrIndentitypapers: string | undefined;
  lrOccupations: string | undefined;
  lrPlaceofissue: string | undefined;
  lrPosition: string | undefined;
  payAccountNo: string | undefined;
  payBranchCode: string | undefined;
  servicesPackage: string | undefined;
  sortname: string | undefined;
  sortnameEn: string | undefined;
  status: string | undefined;
  taxcode: string | undefined;
  tel: string | undefined;
  businessForm: BusinessForm | undefined;
  custType: CustType | undefined;
  companyTmps: CompanyTmp[] | undefined;
  custs: Cust[] | undefined;
}
export class CompanyHi extends Auditable {
  id?: number;
  address: string | undefined;
  approveType: string | undefined;
  branchCode: string | undefined;
  businessCode: string | undefined;
  businessFormCode: string | undefined;
  businessIssuedon: Date | undefined;
  businessPlaceofissue: string | undefined;
  businesslineCode: string | undefined;
  businesslineName: string | undefined;
  cif: string | undefined;
  cifCreatedDate: Date | undefined;
  cifStatus: string | undefined;
  country: string | undefined;
  creatatedAt: string | undefined;
  creatatedBy: string | undefined;
  custBirthday: Date | undefined;
  custCode: string | undefined;
  custEmail: string | undefined;
  custFullname: string | undefined;
  custIdno: string | undefined;
  custIndentitypapers: string | undefined;
  custNmemonicName: string | undefined;
  custPosition: string | undefined;
  custTel: string | undefined;
  custTypeId?: number;
  email: string | undefined;
  fax: string | undefined;
  feeAccountNo: string | undefined;
  feeBranchCode: string | undefined;
  fullname: string | undefined;
  fullnameEn: string | undefined;
  lrAuthorized: string | undefined;
  lrAuthorizedEnddate: Date | undefined;
  lrAuthorizedStartdate: Date | undefined;
  lrDate: Date | undefined;
  lrDefault: string | undefined;
  lrFullname: string | undefined;
  lrIdno: string | undefined;
  lrIndentitypapers: string | undefined;
  lrOccupations: string | undefined;
  lrPlaceofissue: string | undefined;
  lrPosition: string | undefined;
  payAccountNo: string | undefined;
  payBranchCode: string | undefined;
  servicesPackage: string | undefined;
  sortname: string | undefined;
  sortnameEn: string | undefined;
  status: string | undefined;
  taxcode: string | undefined;
  tel: string | undefined;
}
export class CompanyTmp extends Auditable {
  id?: number;
  address: string | undefined;
  approveType: string | undefined;
  branchCode: string | undefined;
  businessCode: string | undefined;
  businessFormCode: string | undefined;
  businessIssuedon: Date | undefined;
  businessPlaceofissue: string | undefined;
  businesslineCode: string | undefined;
  businesslineName: string | undefined;
  cif: string | undefined;
  cifCreatedDate: Date | undefined;
  cifStatus: string | undefined;
  country: string | undefined;
  custBirthday: Date | undefined;
  custCode: string | undefined;
  custEmail: string | undefined;
  custFullname: string | undefined;
  custIdno: string | undefined;
  custIndentitypapers: string | undefined;
  custNmemonicName: string | undefined;
  custPosition: string | undefined;
  custTel: string | undefined;
  email: string | undefined;
  fax: string | undefined;
  feeAccountNo: string | undefined;
  feeBranchCode: string | undefined;
  fullname: string | undefined;
  fullnameEn: string | undefined;
  lrAuthorized: string | undefined;
  lrAuthorizedEnddate: Date | undefined;
  lrAuthorizedStartdate: Date | undefined;
  lrDate: Date | undefined;
  lrDefault: string | undefined;
  lrFullname: string | undefined;
  lrIdno: string | undefined;
  lrIndentitypapers: string | undefined;
  lrOccupations: string | undefined;
  lrPlaceofissue: string | undefined;
  lrPosition: string | undefined;
  payAccountNo: string | undefined;
  payBranchCode: string | undefined;
  servicesPackage: string | undefined;
  sortname: string | undefined;
  sortnameEn: string | undefined;
  status: string | undefined;
  taxcode: string | undefined;
  tel: string | undefined;
  company: Company | undefined;
  custType: CustType | undefined;
}
export class ContactInfo extends Auditable {
  code: string | undefined;
  address: string | undefined;
  email: string | undefined;
  fax: string | undefined;
  hotline: string | undefined;
  tel: string | undefined;
}
export class Cust extends Auditable {
  id?: number;
  birthday: Date | undefined;
  cif: string | undefined;
  code: string | undefined;
  email: string | undefined;
  fullname: string;
  idno: string | undefined;
  indentitypapers: string | undefined;
  nmemonicName: string | undefined;
  notification: string | undefined;
  position: string | undefined;
  sms: string | undefined;
  status: string | undefined;
  tel: string | undefined;
  password: string | undefined;
  rolesearch: string | undefined;
  roletrans: string | undefined;
  lastLogin: Date | undefined;
  loginFalseNumber: string | undefined;
  sallDd: string | undefined;
  sallFd: string | undefined;
  sallLn: string | undefined;
  tallDd: string | undefined;
  verifyType: string | undefined;
  company: Company | undefined;
  custAcc: CustAcc[] | undefined;
  passwordHi: PasswordHi[] | undefined;
  custProduct: CustProduct[] | undefined;
  refreshtokenIbs: RefreshtokenIbs[] | undefined;
  transLimitCust: TransLimitCust | undefined;

}

export class Member extends Auditable {
  id?: number;
  birthday: Date | undefined;
  cif: string | undefined;
  code: string | undefined;
  email: string | undefined;
  fullname: string;
  idno: string | undefined;
  indentitypapers: string | undefined;
  nmemonicName: string | undefined;
  notification: string | undefined;
  position: string | undefined;
  sms: string | undefined;
  status: string | undefined;
  tel: string | undefined;
  password: string | undefined;
  rolesearch: string | undefined;
  roletrans: string | undefined;
  lastLogin: Date | undefined;
  loginFalseNumber: string | undefined;
  sallDd: string | undefined;
  sallFd: string | undefined;
  sallLn: string | undefined;
  tallDd: string | undefined;
  verifyType: string | undefined;
  company: Company | undefined;
  custAcc: CustAcc[] | undefined;
  passwordHi: PasswordHi[] | undefined;
  custProduct: CustProduct[] | undefined;
  refreshtokenIbs: RefreshtokenIbs[] | undefined;
  transLimitCust: TransLimitCust | undefined;
  sex?: number;
  classes?: string;
  schoolYear?: string;
  location?: string;
  point?: string;
  chucVu?: ChucVu;
  ban?: string;
}


export class Activities {
  id?: number;
  ma: string | undefined;
  mota?: string;
  startDate?: Date;
  memberRequire?: number;
  memberCurrent?: number
  status: string | undefined;
  type?: string;
  fileName?: string;
  missionCurrent: number;
  progress: number;
}


export class ActivitiesMembers {
  id?: number;
  mission?: string;
  members?: Member;
  activities?: Activities;
  award?: string;
  type?: string;
  target?: string;
  mota?: string;
  missionStatus?: string;
}

export class Mission {
  id?: number;
  ten: string | undefined;
  status?: string;
}


export class Csvc {
  id?: number;
  ma: string | undefined;
  mota?: string;
  quantity?: number;
  status: string | undefined;
}

export class ChucVu extends Auditable {
  id?: number;
  ma: string | undefined;
  mota?: string;
  status: string | undefined;
}

export class TaiChinh extends Auditable {
  id?: number;
  ma: string | undefined;
  mota?: string;
  quantity?: string;
  type: string | undefined;
}

export class CustAcc extends Auditable {
  id?: number;
  acc?: string;
  type?: string | undefined;
  accType?: string | undefined;
  cust?: Cust | undefined;
}

export class PasswordHi extends Auditable {
  id?: number;
  product: string | undefined;
  cust: Cust | undefined;
}

export class CustProduct extends Auditable {
  id?: number;
  product: string | undefined;
  custCode: string | undefined;
  cust: Cust | undefined;
}


export class RefreshtokenIbs extends Auditable {
  id?: number;
  token: string | undefined;
  expiryDate: Date | undefined;
  cust: Cust | undefined;
}

export class CustType extends Auditable {
  id?: number;
  code: string | undefined;
  description: string | undefined;
  name: string | undefined;
  status: string | undefined;
  companies: Company[] | undefined;
  companyTmps: CompanyTmp[] | undefined;
  packageFees: PackageFee[] | undefined;
}
export class CustomerGuide extends Auditable {
  id: string | undefined;
  data: Blob | undefined;
  filename: string | undefined;
  findex?: number;
  name: string | undefined;
  status: string | undefined;
}
export class Fdtype extends Auditable {
  code: string | undefined;
  ib: string | undefined;
  name: string | undefined;
  status: string | undefined;
}
export class Function extends Auditable {
  code: string | undefined;
  name: string | undefined;
  parentCode: string | undefined;
  status: string | undefined;
  rolesGroupFuns: RolesGroupFun[] | undefined;
  subfunctions: Subfunction[] | undefined;
}
export class Issue extends Auditable {
  id?: number;
  answer: string | undefined;
  iindex: string | undefined;
  question: string | undefined;
  status: string | undefined;
  issueCategory: IssueCategory | undefined;
}
export class IssueCategory extends Auditable {
  id?: number;
  description: string | undefined;
  name: string | undefined;
  status: string | undefined;
  issues: Issue[] | undefined;
}
export class Package extends Auditable {
  code: string | undefined;
  defaultPackageCode: string | undefined;
  description: string | undefined;
  endDate: Date | undefined;
  ispublic: string | undefined;
  name: string | undefined;
  startDate: Date | undefined;
  packageFees: PackageFee[] | undefined;
}
export class PackageFee extends Auditable {
  id?: number;
  day: string | undefined;
  fee?: number;
  feeReg?: number;
  free: string | undefined;
  intfeemethod: string | undefined;
  custType: CustType | undefined;
  packageCode: Package | undefined;
}
export class PaymentDesDefine extends Auditable {
  id?: number;
  code: string | undefined;
  content: string | undefined;
  status: string | undefined;
}
export class Product extends Auditable {
  id?: number;
  code: string | undefined;
  custDescription: string | undefined;
  depositsTurm: string | undefined;
  depositsType: string | undefined;
  description: string | undefined;
  document: string | undefined;
  image: Blob | undefined;
  name: string | undefined;
  promotions: string | undefined;
  rate: string | undefined;
  status: string | undefined;
  utilities: string | undefined;
  productGroup: ProductGroup | undefined;
  transFees: TransFee[] | undefined;
}
export class ProductGroup extends Auditable {
  id?: number;
  code: string | undefined;
  description: string | undefined;
  image: Blob | undefined;
  name: string | undefined;
  nameEn: string | undefined;
  status: string | undefined;
  products: Product[] | undefined;
  productType: ProductType | undefined;
}
export class ProductType extends Auditable {
  id?: number;
  code: string | undefined;
  description: string | undefined;
  name: string | undefined;
  nameEn: string | undefined;
  status: string | undefined;
  productGroups: ProductGroup[] | undefined;
}
export class Province extends Auditable {
  code: string | undefined;
  name: string | undefined;
  status: string | undefined;
  branches: Branch[] | undefined;
}
export class Refreshtoken extends Auditable {
  id?: number;
  expiryDate: Date | undefined;
  token: string | undefined;
  user: User | undefined;
}
export class RolesGroup extends Auditable {
  id?: number;
  departmentCode: string | undefined;
  departmentId: string | undefined;
  departmentName: string | undefined;
  description: string | undefined;
  name: string | undefined;
  positionCode: string | undefined;
  positionId: string | undefined;
  positionName: string | undefined;
  status: string | undefined;
  type: string | undefined;
  rolesGroupFuns: RolesGroupFun[] | undefined;
  users: User[] | undefined;
}
export class RolesGroupFun {
  id?: number;
  function: Function | undefined;
  rolesGroup: RolesGroup | undefined;
  rolesGroupSubs: RolesGroupSub[] | undefined;
}
export class RolesGroupSub {
  id?: number;
  rolesGroupFun: RolesGroupFun | undefined;
  subfunction: Subfunction | undefined;
}
export class Subfunction extends Auditable {
  code: string | undefined;
  name: string | undefined;
  status: string | undefined;
  rolesGroupSubs: RolesGroupSub[] | undefined;
  function: Function | undefined;
}
export class SysErrorDefine extends Auditable {
  code: string | undefined;
  content: string | undefined;
  contentEn: string | undefined;
  description: string | undefined;
  function: string | undefined;
  name: string | undefined;
}
export class SysParameter extends Auditable {
  code: string | undefined;
  name: string | undefined;
  type: string | undefined;
  value: string | undefined;
}
export class TermsOfUse extends Auditable {
  id?: number;
  code: string | undefined;
  content: string | undefined;
  name: string | undefined;
  status: string | undefined;
  isOpen: boolean;
}
export class TransFee extends Auditable {
  id?: number;
  beginAmount?: number;
  beginNumber?: number;
  ccy: string | undefined;
  endAmount?: number;
  endNumber?: number;
  fee?: number;
  feeMaxbal?: number;
  feeMinbal?: number;
  feePresent?: number;
  feetype: string | undefined;
  status: string | undefined;
  vat?: number;
  product: Product | undefined;
}
export class User extends Auditable {
  id?: number;
  branchCode: string | undefined;
  departmentCode: string | undefined;
  departmentId: string | undefined;
  departmentName: string | undefined;
  email: string | undefined;
  fullname: string | undefined;
  password: string | undefined;
  positionCode: string | undefined;
  positionId: string | undefined;
  positionName: string | undefined;
  status: string | undefined;
  username: string | undefined;
  refreshtokens: Refreshtoken[] | undefined;
  rolesGroup: RolesGroup | undefined;
}

export class PagesRequest {
  curentPage = 0;
  size?: number;
  sort: string | undefined;
}

export class ParentFunction {
  item: Function | undefined;
  children: Function[] | undefined;
}

export class UpdatePasswordRequest {
  currentPass: string;
  newPass: string;
  confirmNewPass: string;
}

export class ForgotPasswordRequest {
  username: string;
  email: string;
  tel: string;
  cif: string;
  capcha: string;
  checkbox: boolean;
}

export class MemberRequest {
  fullname?: string;
  schoolYear?: string;
  location?: string;
  status?: string;

}

export class ActivitiesRequest {
  ma?: string;
  type?: string;
  status?: string;
  id?: number | null;
  membersId?: number;
  tuNgay ?: Date;
  denNgay ?: Date;
}

export class TaiChinhRequest {
  tuNgay?: Date;
  denNgay?: Date;
}

export class Account {
  acc: string | undefined;
  id: string | undefined;
}

export class Trans {
  code?: string;
  amount?: string;
  approvedBy?: string;
  approvedDate?: Date;
  branch?: string;
  ccy?: string;
  content?: string;
  custId?: number;
  fee?: string;
  feeType?: string;
  paymentStatus?: string;
  receiveAccount?: string;
  receiveBank?: string;
  receiveName?: string;
  reqId?: string;
  resCode?: string;
  resDes?: string;
  resId?: string;
  revertCode?: string;
  revertDes?: string;
  revertFeeCode?: string;
  revertFeeDes?: string;
  sendAccount?: string;
  sendName?: string;
  status?: string;
  transDate?: string;
  transDatetime?: Date;
  transTime?: string;
  txnum?: string;
  type?: string;
  username?: string;
  vat?: string;
}

export class TransRequest {
  code?: string;
  amount?: string;
  amountString?: string;
  approvedBy?: string;
  approvedDate?: Date;
  branch?: string;
  ccy?: string;
  content?: string;
  custId?: number;
  fee?: string;
  feeType?: string;
  paymentStatus?: string;
  receiveAccount?: string;
  receiveBank?: string;
  receiveName?: string;
  reqId?: string;
  resCode?: string;
  resDes?: string;
  resId?: string;
  revertCode?: string;
  revertDes?: string;
  revertFeeCode?: string;
  revertFeeDes?: string;
  sendAccount?: string;
  sendName?: string;
  status?: string;
  transDate?: string;
  transDatetime?: Date;
  transTime?: string;
  txnum?: string;
  type?: string;
  username?: string;
  vat?: string;
  schedules?: string;
  scheduleFuture?: Date;
  schedulesFromDate?: Date;
  schedulesToDate?: Date;
  schedulesFrequency?: string;
  schedulesTimes?: number;
  schedulesTimesFrequency?: string;
  haveContact?: boolean;
  bankReceiveObject?: BankReceiving | undefined;
}


// export class TransSchedule {
//   code?: string;
//   amount?: string;
//   approvedBy?: string;
//   approvedDate?: Date;
//   branch?: string;
//   ccy?: string;
//   content?: string;
//   custId?: number;
//   fee?: string;
//   feePayer?: string;
//   paymentStatus?: string;
//   receiveAccount?: string;
//   receiveBank?: string;
//   receiveName?: string;
//   reqId?: string;
//   resCode?: string;
//   resDes?: string;
//   resId?: string;
//   revertCode?: string;
//   revertDes?: string;
//   revertFeeCode?: string;
//   revertFeeDes?: string;
//   sendAccount?: string;
//   sendName?: string;
//   status?: string;
//   transDate?: string;
//   transDatetime?: Date;
//   transTime?: string;
//   txnum?: string;
//   type?: string;
//   username?: string;
//   vat?: string;
// }
export class ChangeCustProduct {
  id: number | undefined;
  product: string | undefined;
  code: string[] | undefined;
}

export class TransLimitCust extends Auditable {
  id?: number;
  maxdaily: number | undefined;
  maxtrans: number | undefined;
  product: string | undefined;
  productName: boolean | undefined;
  cust: Cust;
}

export class Tran extends Auditable {
  code: string | undefined;
  amount: string | undefined;
  approvedBy: Cust | undefined;
  approvedDate: Date | undefined;
  branch: string | undefined;
  ccy: string | undefined;
  content: string | undefined;
  custId: number | undefined;
  fee: string | undefined;
  paymentStatus: string | undefined;
  receiveAccount: string | undefined;
  receiveBank: string | undefined;
  receiveName: string | undefined;
  reqId: string | undefined;
  resCode: string | undefined;
  resDes: string | undefined;
  resId: string | undefined;
  revertCode: string | undefined;
  revertDes: string | undefined;
  revertFeeCode: string | undefined;
  revertFeeDes: string | undefined;
  sendAccount: string | undefined;
  sendName: string | undefined;
  status: string | undefined;
  transDate: string | undefined;
  transDatetime: Date | undefined;
  transTime: string | undefined;
  txnum: string | undefined;
  type: string | undefined;
  username: string | undefined;
  vat: string | undefined;
  feeType: string | undefined;
  reason: string | undefined;
}

export class TransSchedule extends Auditable {
  code: string | undefined;
  amount: string | undefined;
  approvedBy: Cust | undefined;
  approvedDate: Date | undefined;
  branch: string | undefined;
  ccy: string | undefined;
  content: string | undefined;
  custId: number | undefined;
  fee: string | undefined;
  paymentStatus: string | undefined;
  receiveAccount: string | undefined;
  receiveBank: string | undefined;
  receiveName: string | undefined;
  sendAccount: string | undefined;
  sendName: string | undefined;
  status: string | undefined;
  transDate: string | undefined;
  transDatetime: Date | undefined;
  transTime: string | undefined;
  txnum: string | undefined;
  type: string | undefined;
  username: string | undefined;
  vat: string | undefined;
  schedules: string | undefined;
  schedulesFromDate: Date | undefined;
  schedulesToDate: Date | undefined;
  schedulesFrequency: string | undefined;
  schedulesTimes: number | undefined;
  feeType: string | undefined;
  reason: string | undefined;
  transSchedulesDetails: TransSchedulesDetails[] | undefined;
}

export class TransSchedulesDetails extends Auditable {
  id: number | undefined;
  code: string | undefined;
  paymentStatus: string | undefined;
  reqId: string | undefined;
  resCode: string | undefined;
  resDes: string | undefined;
  resId: string | undefined;
  revertCode: string | undefined;
  revertDes: number | undefined;
  revertFeeCode: string | undefined;
  revertFeeDes: string | undefined;
  scheduledDate: Date | undefined;
  transDate: Date | undefined;
  reason: string | undefined;
  transSchedule: TransSchedule | undefined;
}

export class CustContact extends Auditable {
  id?: number;
  custId: Cust;
  product: number | undefined;
  receiveAccount: string | undefined;
  bankReceiving: BankReceiving | undefined;
  sortname: string | undefined;
  receiveName: string | undefined;
  keyWord: string | undefined;
  checked: boolean;
}

export class BankReceiving extends Auditable {
  code?: string;
  citad: string | undefined;
  isttt: string | undefined;
  name: string | undefined;
  nameEn: string | undefined;
  napas: string | undefined;
  sortname: string | undefined;
  status: string | undefined;
  swiftCode: string | undefined;
}

export class TransLot extends Auditable {
  code?: string;
  amount: string | undefined;
  approvedBy: string | undefined;
  approvedDate: Date | undefined;
  branch: string | undefined;
  content: string | undefined;
  custId: string | undefined;
  fee: string | undefined;
  fileData: any | undefined;
  sendAccount: string | undefined;
  status: string | undefined;
  total: string | undefined;
  transDate: string | undefined;
  transDatetime: Date | undefined;
  transTime: string | undefined;
  username: string | undefined;
  vat: string | undefined;
  reason: string | undefined;
  transLotDetails: TransLotDetail | undefined;
  sumTrans: number | undefined;
  startAmount: string | undefined;
  endAmount: string | undefined;
  startDate: string | undefined;
  endDate: string | undefined;
  transLotStatus: string | undefined;
}

export class TransLotDetail extends Auditable {
  id?: string;
  code: string | undefined;
  amount: string | undefined;
  approvedBy: string | undefined;
  approvedDat: Date | undefined;
  ccy: string | undefined;
  fee: string | undefined;
  paymentStatus: string | undefined;
  receiveAccount: string | undefined;
  receiveBank: any | undefined;
  receiveName: string | undefined;
  reqId: string | undefined;
  resCode: string | undefined;
  resDes: string | undefined;
  resId: string | undefined;
  revertCode: string | undefined;
  revertDes: string | undefined;
  revertFeeCode: string | undefined;
  revertFeeDes: string | undefined;
  transDate: string | undefined;
  transDatetime: Date | undefined;
  transTime: string | undefined;
  txnum: string | undefined;
  type: string | undefined;
  vat: string | undefined;
  transLot: TransLot | undefined;
  content: string | undefined;
}
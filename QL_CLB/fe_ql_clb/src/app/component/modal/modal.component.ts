import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @ViewChild('content')
  private content: TemplateRef<any>;

  @Input() isOpen: boolean = false;
  @Input() sizeModal: string = 'lg';

  @Input() type: string = 'modal';
  @Input() typeNotification: string = 'notification'; //notification or confirm
  @Input() titleModal: string = '';

  @Output() closeEvent = new EventEmitter();

  public titleNotification : string = '';

  constructor(
    config: NgbModalConfig,
    private modalService: NgbModal
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit(): void {
    if (this.typeNotification === 'notification') {
      this.titleNotification = 'Thông báo'
    }
    else if(this.typeNotification === 'lyDoTuChoi') {
      this.titleNotification = 'Lý do hủy duyệt'
    }
    else {
      this.titleNotification = 'Xác nhận'
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['isOpen'].currentValue) {
      this.modalService.open(this.content, {
        size: this.sizeModal,
        centered: true,
        windowClass: this.type === 'notification' ? 'custom-notification' : ''
      });
    } else {
      this.modalService.dismissAll()
    }
  }

  closeModal() {
    this.isOpen = false
    this.closeEvent.emit(false);
    this.modalService.dismissAll()
  }

}

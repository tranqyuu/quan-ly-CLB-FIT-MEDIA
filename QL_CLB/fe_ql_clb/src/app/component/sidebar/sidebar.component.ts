import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: [
    '../../../css/vendors/bootstrap/bootstrap.bundles.e93aa61a.css',
    '../../../css/vendors/swiper/swiper.bundles.fdb47a37.css',
    '../../../css/vendors/select2/select2.bundles.275e9abe.css',
    '../../../css/vendors/croppie/croppie.bundles.35bb28ca.css',
    '../../../css/style.07f22eba.css',
    '../../../css/custom.bundles.22fe35d6.css',
    './sidebar.component.css',
  ],
})
export class SidebarComponent implements OnInit {
  constructor(
    private router: Router,
  ) { }

  ngOnInit() { }
  isSidebarLeftActive: boolean = false;
  sidebarMenu: String = '';

  sidebarItemClick(type: String) {
    if (this.sidebarMenu === type) {
      this.isSidebarLeftActive = false;
      this.sidebarMenu = '';
    } else {
      this.sidebarMenu = type;
      if (this.sidebarMenu !== '') {
        this.isSidebarLeftActive = true;
      } else {
        this.isSidebarLeftActive = false;
      }
    }
  }

  navigateHref(href: string) {
    this.router.navigate(['', href, href]);
  }
}

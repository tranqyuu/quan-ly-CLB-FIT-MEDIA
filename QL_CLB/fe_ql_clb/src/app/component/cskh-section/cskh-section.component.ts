import { Component } from '@angular/core';

@Component({
  selector: 'app-cskh-section',
  templateUrl: './cskh-section.component.html',
  styleUrls: [
    './cskh-section.component.scss',
    '../../../css/style.07f22eba.css',
    '../../../css/custom.bundles.22fe35d6.css',
  ],
})
export class CskhSectionComponent {}

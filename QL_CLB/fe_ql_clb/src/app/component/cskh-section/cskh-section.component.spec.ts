import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CskhSectionComponent } from './cskh-section.component';

describe('CskhSectionComponent', () => {
  let component: CskhSectionComponent;
  let fixture: ComponentFixture<CskhSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CskhSectionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CskhSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-right-bar',
  templateUrl: './right-bar.component.html',
  styleUrls: [
    '../../../css/vendors/bootstrap/bootstrap.bundles.e93aa61a.css',
    '../../../css/vendors/swiper/swiper.bundles.fdb47a37.css',
    '../../../css/vendors/select2/select2.bundles.275e9abe.css',
    '../../../css/vendors/croppie/croppie.bundles.35bb28ca.css',
    '../../../css/style.07f22eba.css',
    '../../../css/custom.bundles.22fe35d6.css',
    './right-bar.component.css',
  ],
})
export class RightBarComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}

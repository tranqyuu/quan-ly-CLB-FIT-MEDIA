import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.component.html',
  styleUrls: ['./qr-code.component.scss']
})
export class QrCodeComponent {
  @Output() maGiaoDich = new EventEmitter<string>();
  code: any;

  constructor() {
    this.code = "12345678";
  }

  ngOnInit(): void {
    this.maGiaoDich.emit(this.code);
  }

}

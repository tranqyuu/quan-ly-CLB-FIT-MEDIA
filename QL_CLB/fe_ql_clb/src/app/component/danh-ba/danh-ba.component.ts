import { Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { FwError } from 'src/app/common/constants';
import { CustContact } from 'src/app/model/model';
import { BaseComponent } from 'src/app/page/base/BaseComponent';
import { DanhBaThuHuongService } from 'src/app/services/tien-ich/quan-ly-danh-ba/danh-ba-thu-huong.service';

@Component({
  selector: 'app-danh-ba',
  templateUrl: './danh-ba.component.html',
  styleUrls: ['./danh-ba.component.scss']
})
export class DanhBaComponent extends BaseComponent<CustContact>{
  @Output() contact = new EventEmitter<CustContact>();
  // @Input() isOpen: boolean;
  custContactSearch: CustContact = new CustContact();
  constructor(
    private danhBaThuHuongService: DanhBaThuHuongService,
    private injector: Injector,
    ) {
      super(injector);
  }

  ngOnInit() : void{
    this.onInit();
    this.search(1);
  }


  override search(page: any) {
    this.page.curentPage = page;
    this.danhBaThuHuongService
      .searchPaging(this.page, this.custContactSearch)
      .subscribe((res) => {
        if(FwError.THANHCONG== res.errorCode){
          if (res.data?.totalElements) this.total = res.data.totalElements;
          if (res.data?.content) this.asyncData = res.data.content;
        }else{
          this.toastrs.error(res.errorMessage);
        }
      });
  }

  selected(item: CustContact){
    this.contact.emit(item);
  }
}

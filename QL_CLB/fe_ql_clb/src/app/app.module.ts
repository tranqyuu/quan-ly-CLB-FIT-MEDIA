
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from 'src/app/component/sidebar/sidebar.component';
import { UserInformationComponent } from 'src/app/component/user-information/user-information.component';
import { RightBarComponent } from 'src/app/component/right-bar/right-bar.component';
import { HeaderComponent } from 'src/app/component/header/header.component';
import { FooterComponent } from 'src/app/component/footer/footer.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { TrangChuComponent } from 'src/app/page/trang-chu/trang-chu.component';

import { NgSelectModule } from '@ng-select/ng-select';
import { NgToggleModule } from 'ng-toggle-button';
import { CskhSectionComponent } from './component/cskh-section/cskh-section.component';
import { SwitchComponent } from './component/switch/switch.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonTabsComponent } from './common/common-tabs/common-tabs.component';
import { CommonTabComponent } from './common/common-tabs/common-tab/common-tab.component';
import { GdChoDuyetComponent } from './page/gd-cho-duyet/gd-cho-duyet.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { DonutChartComponent } from './component/donut-chart/donut-chart.component';
import { QlQuyenTruyCapComponent } from './page/cai-dat/ql-quyen-truy-cap/ql-quyen-truy-cap.component';
import { QlQuyenTruyCapChiTietComponent } from './page/cai-dat/ql-quyen-truy-cap/ql-quyen-truy-cap-chi-tiet/ql-quyen-truy-cap-chi-tiet.component';
import { QlMaTruyCapComponent } from './page/cai-dat/ql-ma-truy-cap/ql-ma-truy-cap.component';
import { NgbDateParserFormatter, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DangNhapComponent } from './page/dang-nhap/dang-nhap.component';
import { I18nModule } from './shared/i18n.module';
import { I18nComponent } from './shared/i18n.component';
import { I18nPipe } from './shared/i18n.pipe';
import * as enI18n from './shared/en.i18n';
import * as viI18n from './shared/vi.i18n';
import { LoaderComponent } from './common/loader/loader/loader.component';
import { EncryptInterceptor } from './interceptor/encrypt.interceptor';
import { LoaderService } from './services/loader/loader.service';
import { LoaderInterceptor } from './interceptor/loader.interceptor';
const languages = [
  { lang: 'VI', flag: 'assets/img/fr.i18n.png', file: viI18n },
  { lang: 'EN', flag: 'assets/img/en.i18n.png', file: enI18n }
]
import { ToastrModule } from 'ngx-toastr';

import { Layout1Component } from './layout/layout1/layout1.component';
import { ModalComponent } from './component/modal/modal.component';
import { CustomDateParserFormatter } from './services/common/CustomDateParserFormatter ';
import { ThemMaTruyCapComponent } from './page/cai-dat/ql-ma-truy-cap/them-ma-truy-cap/them-ma-truy-cap.component';
import { LsChinhSuaMaTruyCapComponent } from './page/cai-dat/ql-ma-truy-cap/ls-chinh-sua-ma-truy-cap/ls-chinh-sua-ma-truy-cap.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { HandleErrorsInterceptor } from './interceptor/handle-errors.interceptor';
import { DoiMatKhauComponent } from './page/tien-ich/doi-mat-khau/doi-mat-khau.component';
import { LienHeComponent } from './page/tien-ich/ho-tro/lien-he/lien-he.component';

import { CapNhatQuyenGiaoDichComponent } from './page/cai-dat/ql-quyen-truy-cap/ql-quyen-truy-cap-chi-tiet/cap-nhat-quyen-giao-dich/cap-nhat-quyen-giao-dich.component';
import { CapNhatQuyenTruyVanComponent } from './page/cai-dat/ql-quyen-truy-cap/ql-quyen-truy-cap-chi-tiet/cap-nhat-quyen-truy-van/cap-nhat-quyen-truy-van.component';
import { NgbdDatepickerPopupModule } from './common/date-picker/datepicker-popup.module';


import { CaiDatHanMucComponent } from './page/cai-dat/cai-dat-han-muc/cai-dat-han-muc.component';
import { CapNhatHanMucComponent } from './page/cai-dat/cai-dat-han-muc/cap-nhat-han-muc/cap-nhat-han-muc.component';
import { commons } from './common/commons';
import { CommonModule, DatePipe } from '@angular/common';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { CustomAdapter } from './component/date-picker/datepicker';

import { GdChoDuyetChiTietComponent } from './page/gd-cho-duyet/gd-cho-duyet-chi-tiet/gd-cho-duyet-chi-tiet.component';

import { DanhBaThuHuongComponent } from './page/tien-ich/quan-ly-danh-ba/danh-ba-thu-huong/danh-ba-thu-huong.component';

import { ThemMoiDanhBaThuHuongComponent } from './page/tien-ich/quan-ly-danh-ba/them-moi-danh-ba-thu-huong/them-moi-danh-ba-thu-huong.component';
import { XacNhanThemMaTruyCapComponent } from './page/cai-dat/ql-ma-truy-cap/them-ma-truy-cap/xac-nhan-them-ma-truy-cap/xac-nhan-them-ma-truy-cap.component';
import { KetQuaMaTruyCapComponent } from './page/cai-dat/ql-ma-truy-cap/them-ma-truy-cap/ket-qua-ma-truy-cap/ket-qua-ma-truy-cap.component';
import { KhoiTaoMaTruyCapComponent } from './page/cai-dat/ql-ma-truy-cap/them-ma-truy-cap/khoi-tao-ma-truy-cap/khoi-tao-ma-truy-cap.component';
import { ChinhSuaMaTruyCapComponent } from './page/cai-dat/ql-ma-truy-cap/them-ma-truy-cap/chinh-sua-ma-truy-cap/chinh-sua-ma-truy-cap.component';
import { QrCodeComponent } from './component/qr-code/qr-code.component';

import { CapNhatDanhBaThuHuongComponent } from './page/tien-ich/quan-ly-danh-ba/cap-nhat-danh-ba-thu-huong/cap-nhat-danh-ba-thu-huong.component';
import { LichSuChinhSuaDanhBaThuHuongComponent } from './page/tien-ich/quan-ly-danh-ba/lich-su-chinh-sua-danh-ba-thu-huong/lich-su-chinh-sua-danh-ba-thu-huong.component';
import { DieuKhoanDichVuComponent } from './page/tien-ich/ho-tro/dieu-khoan-dich-vu/dieu-khoan-dich-vu.component';

import { DanhBaComponent } from './component/danh-ba/danh-ba.component';
import { ChiTietDieuKhoanDichVuComponent } from './page/tien-ich/ho-tro/dieu-khoan-dich-vu/chi-tiet-dieu-khoan-dich-vu/chi-tiet-dieu-khoan-dich-vu.component';

import { QuanLyHdComponent } from './page/quan-ly-hd/quan-ly-hd.component';
import { QuanLyChiTietComponent } from './page/quan-ly-hd/quan-ly-chi-tiet/quan-ly-chi-tiet.component';
import { QuanLyCsvcComponent } from './page/quan-ly-csvc/quan-ly-csvc.component';
import { QuanLyCsvcChiTietComponent } from './page/quan-ly-csvc/quan-ly-csvc-chi-tiet/quan-ly-csvc-chi-tiet.component';
import { QuanLyTaiChinhComponent } from './page/quan-ly-tai-chinh/quan-ly-tai-chinh.component';
import { QuanLyTaiChinhChiTietComponent } from './page/quan-ly-tai-chinh/quan-ly-tai-chinh-chi-tiet/quan-ly-tai-chinh-chi-tiet.component';
import { QuanLyChiTietHdThanhVienComponent } from './page/quan-ly-hd/quan-ly-chi-tiet-hd-thanh-vien/quan-ly-chi-tiet-hd-thanh-vien.component';
import { QuanLyHoatDongNhiemVuComponent } from './page/quan-ly-hd/quan-ly-hoat-dong-nhiem-vu/quan-ly-hoat-dong-nhiem-vu.component';
import { QuanLyChucVuComponent } from './page/quan-ly-chuc-vu/quan-ly-chuc-vu.component';
import { QuanLyChucVuChiTietComponent } from './page/quan-ly-chuc-vu/quan-ly-chuc-vu-chi-tiet/quan-ly-chuc-vu-chi-tiet.component';


@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    UserInformationComponent,
    RightBarComponent,
    HeaderComponent,
    FooterComponent,
    TrangChuComponent,

    CskhSectionComponent,

    SwitchComponent,
    CommonTabsComponent,
    CommonTabComponent,
    GdChoDuyetComponent,
    DonutChartComponent,
    QlQuyenTruyCapComponent,
    QlQuyenTruyCapChiTietComponent,
    QlQuyenTruyCapComponent,
    QlMaTruyCapComponent,
    DangNhapComponent,
    I18nComponent,
    I18nPipe,
    LoaderComponent,

    Layout1Component,
    ModalComponent,
    ThemMaTruyCapComponent,
    LsChinhSuaMaTruyCapComponent,
    DoiMatKhauComponent,
    LienHeComponent,

    CapNhatQuyenGiaoDichComponent,
    CapNhatQuyenTruyVanComponent,
    CaiDatHanMucComponent,
    CapNhatHanMucComponent,

    CaiDatHanMucComponent,

    GdChoDuyetChiTietComponent,
    DanhBaThuHuongComponent,

    ThemMoiDanhBaThuHuongComponent,
    XacNhanThemMaTruyCapComponent,
    KetQuaMaTruyCapComponent,
    KhoiTaoMaTruyCapComponent,
    ChinhSuaMaTruyCapComponent,
    QrCodeComponent,

    CapNhatDanhBaThuHuongComponent,
    LichSuChinhSuaDanhBaThuHuongComponent,
    DieuKhoanDichVuComponent,

    DanhBaComponent,
    ChiTietDieuKhoanDichVuComponent,

    QuanLyHdComponent,
    QuanLyChiTietComponent,
    QuanLyCsvcComponent,
    QuanLyCsvcChiTietComponent,
    QuanLyTaiChinhComponent,
    QuanLyTaiChinhChiTietComponent,
    QuanLyChiTietHdThanhVienComponent,
    QuanLyHoatDongNhiemVuComponent,
    QuanLyChucVuComponent,
    QuanLyChucVuChiTietComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CarouselModule,
    NgSelectModule,
    NgToggleModule,
    FormsModule,
    ReactiveFormsModule,
    NgApexchartsModule,
    NgbModule,
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    I18nModule.forRoot(languages),
    NgxPaginationModule,
    NgbdDatepickerPopupModule
  ],
  providers: [
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: EncryptInterceptor, multi: true },
    { provide: NgbDateAdapter, useClass: CustomAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HandleErrorsInterceptor,
      multi: true,
    },
    commons,
    DatePipe
    // { provide: HTTP_INTERCEPTORS, useClass: DecryptInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
